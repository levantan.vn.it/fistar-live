import axios from "axios";
import Cookies from "js-cookie";

const config = {
  // timeout: 1500,
  baseURL: process.env.REACT_APP_BASE_API,
  headers: {
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "*",
    "Access-Control-Allow-Headers": "*",
  }
};

// if (Cookies.get('token')) {
//   config.headers.common = {
//     'Authorization': `Bearer ${Cookies.get('token')}`
//   }
// }

const instance = axios.create(config);

export default instance;
