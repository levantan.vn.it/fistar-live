import "babel-polyfill";
import "core-js/fn/object/assign";
import "core-js/fn/promise";
import "core-js/fn/string/starts-with";
import "core-js/fn/string/includes";
import "core-js/fn/string/repeat";
import "core-js/fn/array/includes";

import React, { Suspense } from "react";
import { render, hydrate } from "react-dom";
import { Provider } from "react-redux";
import Loadable from "react-loadable";
import { Frontload } from "react-frontload";
import { ConnectedRouter } from "connected-react-router";
import createStore from "./store";
import { BrowserRouter } from 'react-router-dom';
import HttpsRedirect from 'react-https-redirect';

import { I18nextProvider } from "react-i18next";
import i18n from "./i18n";

import App from "./App";
import "./index.css";

const Loader = () => <div>loading...</div>;

// Create a store and get back itself and its history object
const { store, history } = createStore();

// Running locally, we should run on a <ConnectedRouter /> rather than on a <StaticRouter /> like on the server
// Let's also let React Frontload explicitly know we're not rendering on the server here
const Application = (
  <Provider store={store}>
    <Suspense fallback={<Loader />}>
      <I18nextProvider
        i18n={i18n}
        initialI18nStore={window.initialI18nStore}
        initialLanguage={window.initialLanguage}
      >
        <ConnectedRouter history={history}>
          <Frontload noServerRender={true}>
            <BrowserRouter>
                <App />
            </BrowserRouter>
          </Frontload>
        </ConnectedRouter>
      </I18nextProvider>
    </Suspense>
  </Provider>
);

const root = document.querySelector("#root");

if (root.hasChildNodes() === true) {
  // If it's an SSR, we use hydrate to get fast page loads by just
  // attaching event listeners after the initial render
  Loadable.preloadReady().then(() => {
    hydrate(Application, root);
  });
} else {
  // If we're not running on the server, just render like normal
  render(Application, root);
}
