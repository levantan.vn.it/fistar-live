import Loadable from "react-loadable";

import * as routeName from "./../routes/routeName";

import BaseMaster from "./../components/partial/Master";
import Home from "./../pages/Home";
import FaqWrapper from "../components/partial/front/faq/faqWrapper";

// // fistar
import FiStarMaster from "./../components/fiStar/Master";
// // auth
import FistarAuthWrapper from "./../components/fiStar/Auth";
import FistarAuthRegisterWrapper from "./../components/fiStar/Auth/Register/Wrapper";
import FiStarLogin from "./../pages/fiStar/Auth/Login";
import FistarFindEmail from "./../pages/fiStar/Auth/FindEmail";
import FistarForgotPassword from "./../pages/fiStar/Auth/ForgotPassword";
import FistarJoin from "./../pages/fiStar/Auth/Join";
import FistarJoinComplete from "./../pages/fiStar/Auth/JoinComplete";
import FistarRegister from "./../pages/fiStar/Auth/Register";
import Private from "./../pages/fiStar/Private";
//profile
import FistarProfileWrapper from "./../components/fiStar/Auth/Profile/Wrapper";
import FistarProfileGaneral from "./../pages/fiStar/Auth/Profile/Ganeral";
import FistarProfileInformartion from "./../pages/fiStar/Auth/Profile/Information";
import FistarProfileResetPassword from "./../pages/fiStar/Auth/Profile/ResetPassword";
import FistarProfileSetting from "./../pages/fiStar/Auth/Profile/Setting";

//qa
import FistarQA from "./../pages/fiStar/Dashboard/Q&A";
import FistarQADetail from "./../pages/fiStar/Dashboard/Q&A/Q&ADetail";
import FistarAnswerQuestion from "./../pages/fiStar/Dashboard/Q&A/AnswerQuestions";

//campaign front
import CampaignWrapper from "./../components/partial/front/campaign/campaignWrapper";
//fistar front
import FistarWrapper from "./../components/partial/front/fistar/fistarWrapper";
//service front
import ServiceWrapper from "./../components/partial/front/service/serviceWrapper";
//dashboard
import FistarDashboardWrapper from "./../components/fiStar/Dashboard/Wrapper";
import FistarDashboard from "./../pages/fiStar/Dashboard";

import FistarSearchCampaign from "./../pages/fiStar/Dashboard/SearchCampaign";
import FistarMyCampaign from "./../pages/fiStar/Dashboard/MyCampaign";

import FistarNewCampaign from "./../pages/fiStar/Dashboard/NewCampaign";

import FistarMyCampaignDetail from "./../pages/fiStar/Dashboard/MyCampaign/campaignDetail";

//partner
import PartnerMaster from "./../components/partner/Master";
import PartnerMasterDashboard from "./../components/partner/Dashboard/Master";
//auth
import PartnerLogin from "./../pages/Partner/Auth/Login";
import PartnerRegister from "./../pages/Partner/Auth/Register";
import PartnerAuthWrapper from "./../components/partner/Auth";
import PartnerAuthWrapperRegister from "./../components/partner/Auth/Register";
import PartnerFindEmail from "./../pages/Partner/Auth/FindEmail";
import PartnerForgotPassword from "./../pages/Partner/Auth/ForgotPassword";

import PartnerApproveRegister from "./../pages/Partner/Auth/Register/ApproveRegister";
import FistarApproveRegister from "./../pages/fiStar/Auth/Register/ApproveRegister";

import PartnerCreateCampainWrapper from "./../components/partner/Dashboard/CreateCampain";
import PartnerCreateCampain from "./../pages/Partner/DashBoard/CreateCampain";

import PartnerProfile from "./../pages/Partner/DashBoard/Profile";
import PartnerManagerInformation from "./../pages/Partner/DashBoard/Profile/ManagerInformation";
import PartnerCompanyInformation from "./../pages/Partner/DashBoard/Profile/CompanyInformation";
import PartnerChangePassword from "./../pages/Partner/DashBoard/Profile/ChangePassword";
import PartnerAuthWrapperProfile from "./../components/partner/Dashboard/Profile";

import PartnerAuthWrapperDashboard from "./../components/partner/Dashboard/Layout/Wrapper";

import PartnerDashboard from "./../pages/Partner/DashBoard";
import CampaignTracking from "./../pages/Partner/DashBoard/CampaignTracking";
import CampaignTrackingDetail from "./../pages/Partner/DashBoard/CampaignTrackingDetail";
//qa
import PartnerQA from "./../pages/Partner/DashBoard/QA";
import PartnerQADetail from "./../pages/Partner/DashBoard/QA/Q&ADetail";
import PartnerAnswerQuestion from "./../pages/Partner/DashBoard/QA/AnswerQuestions";
//review sns
import FistarWriteReview from "./../pages/fiStar/Dashboard/ReviewSns/WriteReview";
import FistarReadReview from "./../pages/fiStar/Dashboard/ReviewSns/ReadReview";
import FistarEditReview from "./../pages/fiStar/Dashboard/ReviewSns/EditReview";

import PartnerMyFistar from "./../pages/Partner/DashBoard/MyFistar";
import PartnerSearchFistar from "./../pages/Partner/DashBoard/CreateCampain/search";
import contactWrapper from "../components/partial/front/contact/contactWrapper";

export default [
  // home page
  {
    path: routeName.HOME,
    exact: true,
    component: Loadable({
      loader: () =>
        import(/* webpackChunkName: "homepage" */ "./../pages/NewHome"),
      loading: () => null,
      modules: ["./../pages/NewHome"],
      webpack: () => [require.resolveWeak("./../pages/NewHome")]
    }),
    layout: BaseMaster
    // type: 'GuestRoute'
  },
  {
    path: routeName.CAMPAIGN,
    component: Loadable({
      loader: () =>
        import(/* webpackChunkName: "homepage" */ "./../pages/front/Campaign"),
      loading: () => null,
      modules: ["./../pages/front/Campaign"],
      webpack: () => [require.resolveWeak("./../pages/front/Campaign")]
    }),
    layout: BaseMaster,
    wrapper: CampaignWrapper,
    type: "Route"
  },
  {
    path: routeName.FISTAR_FRONT,
    component: Loadable({
      loader: () =>
        import(/* webpackChunkName: "homepage" */ "./../pages/front/Fistar/index"),
      loading: () => null,
      modules: ["./../pages/front/Fistar/index"],
      webpack: () => [require.resolveWeak("./../pages/front/Fistar/index")]
    }),
    layout: BaseMaster,
    wrapper: FistarWrapper,
    type: "Route"
  },
  {
    path: routeName.PRIVACY_TERMS,
    component: Loadable({
      loader: () =>
        import(/* webpackChunkName: "homepage" */ "./../pages/front/PrivacyTerms/index"),
      loading: () => null,
      modules: ["./../pages/front/PrivacyTerms/index"],
      webpack: () => [require.resolveWeak("./../pages/front/PrivacyTerms/index")]
    }),
    layout: BaseMaster,
    type: "Route"
  },

  {
    path: routeName.TERMS,
    component: Loadable({
      loader: () =>
        import(/* webpackChunkName: "homepage" */ "./../pages/front/PrivacyTerms/Terms"),
      loading: () => null,
      modules: ["./../pages/front/PrivacyTerms/Terms"],
      webpack: () => [require.resolveWeak("./../pages/front/PrivacyTerms/Terms")]
    }),
    layout: BaseMaster,
    type: "Route"
  },
  {
    path: routeName.FAQ_FISTAR,
    component: Loadable({
      loader: () =>
        import(/* webpackChunkName: "homepage" */ "./../pages/front/faq/faqFistar"),
      loading: () => null,
      modules: ["./../pages/front/faq/faqFistar"],
      webpack: () => [require.resolveWeak("./../pages/front/faq/faqFistar")]
    }),
    layout: BaseMaster,
    wrapper: FaqWrapper,
    type: "Route"
  },
  {
    path: routeName.FAQ_PARTNER,
    component: Loadable({
      loader: () =>
        import(/* webpackChunkName: "homepage" */ "./../pages/front/faq/faqPartner"),
      loading: () => null,
      modules: ["./../pages/front/faq/faqPartner"],
      webpack: () => [require.resolveWeak("./../pages/front/faq/faqPartner")]
    }),
    layout: BaseMaster,
    wrapper: FaqWrapper,
    type: "Route"
  },
  {
    path: routeName.FAQ,
    component: Loadable({
      loader: () =>
        import(/* webpackChunkName: "homepage" */ "./../pages/front/faq/faq"),
      loading: () => null,
      modules: ["./../pages/front/faq/faq"],
      webpack: () => [require.resolveWeak("./../pages/front/faq/faq")]
    }),
    layout: BaseMaster,
    wrapper: FaqWrapper,
    type: "Route"
  },
  {
    path: routeName.SERVICE_PARTNER,
    component: Loadable({
      loader: () =>
        import(/* webpackChunkName: "homepage" */ "./../pages/front/Service/partner"),
      loading: () => null,
      modules: ["./../pages/front/Service/partner"],
      webpack: () => [require.resolveWeak("./../pages/front/Service/partner")]
    }),
    layout: BaseMaster,
    wrapper: ServiceWrapper,
    type: "Route"
  },
  {
    path: routeName.SERVICE_FISTAR,
    component: Loadable({
      loader: () =>
        import(/* webpackChunkName: "homepage" */ "./../pages/front/Service/fistar"),
      loading: () => null,
      modules: ["./../pages/front/Service/fistar"],
      webpack: () => [require.resolveWeak("./../pages/front/Service/fistar")]
    }),
    layout: BaseMaster,
    wrapper: ServiceWrapper,
    type: "Route"
  },
  {
    path: routeName.SERVICE,
    component: Loadable({
      loader: () =>
        import(/* webpackChunkName: "homepage" */ "./../pages/front/Service/index"),
      loading: () => null,
      modules: ["./../pages/front/Service/index"],
      webpack: () => [require.resolveWeak("./../pages/front/Service/index")]
    }),
    layout: BaseMaster,
    wrapper: ServiceWrapper,
    type: "Route"
  },
  {
    path: routeName.CONTACTUS,
    component: Loadable({
      loader: () =>
        import(/* webpackChunkName: "homepage" */ "./../pages/front/ContactUs"),
      loading: () => null,
      modules: ["./../pages/front/ContactUs"],
      webpack: () => [require.resolveWeak("./../pages/front/ContactUs")]
    }),
    layout: BaseMaster,
    wrapper: contactWrapper,
    type: "Route"
  },

  // fistar
  // auth
  {
    path: routeName.FISTAR_LOGIN,
    component: Loadable({
      loader: () =>
        import(/* webpackChunkName: "login" */ "./../pages/fiStar/Auth/Login"),
      loading: () => null,
      modules: ["login"],
      webpack: () => [require.resolveWeak("./../pages/fiStar/Auth/Login")]
    }),
    layout: BaseMaster,
    wrapper: FistarAuthWrapper,
    type: "GuestRoute"
  },
  {
    path: routeName.FISTAR_FIND_EMAIL,
    component: FistarFindEmail,
    layout: BaseMaster,
    wrapper: FistarAuthWrapper,
    type: "GuestRoute"
  },
  {
    path: routeName.FISTAR_FORGOT_PASSWORD,
    component: FistarForgotPassword,
    layout: BaseMaster,
    wrapper: FistarAuthWrapper,
    type: "GuestRoute"
  },

  {
    path: routeName.FISTAR_APPROVE_REGISTER,
    component: FistarApproveRegister,
    layout: BaseMaster,
    wrapper: FistarAuthWrapper,
    type: "Route"
  },

  {
    path: routeName.FISTAR_JOIN,
    component: FistarJoin,
    layout: BaseMaster,
    wrapper: FistarAuthWrapper,
    type: "GuestRoute"
  },
  {
    path: routeName.FISTAR_JOIN_FB,
    component: FistarJoinComplete,
    layout: BaseMaster,
    wrapper: FistarAuthWrapper,
    type: "GuestRoute"
  },
  {
    path: routeName.FISTAR_REGISTER,
    component: FistarRegister,
    layout: BaseMaster,
    wrapper: FistarAuthRegisterWrapper,
    type: "GuestRoute"
  },
  {
    path: routeName.FISTAR_PRIVATE,
    component: Private,
    layout: BaseMaster,
    type: "UserRoute"
  },

  //profile
  // {
  //     from: routeName.FISTAR_PROFILE,
  //     to: routeName.FISTAR_PROFILE_GANERAL,
  //     exact: true,
  // },
  {
    path: routeName.FISTAR_PROFILE_GANERAL,
    component: FistarProfileGaneral,
    layout: FiStarMaster,
    wrapper: FistarProfileWrapper,
    usertype: "fistar",
    type: "UserRoute"
  },
  {
    path: routeName.FISTAR_PROFILE_INFORMARTION,
    component: FistarProfileInformartion,
    layout: FiStarMaster,
    wrapper: FistarProfileWrapper,
    usertype: "fistar",
    type: "UserRoute"
  },
  {
    path: routeName.FISTAR_PROFILE_RESET_PASSWORD,
    component: FistarProfileResetPassword,
    layout: FiStarMaster,
    wrapper: FistarProfileWrapper,
    usertype: "fistar",
    type: "UserRoute"
  },
  {
    path: routeName.FISTAR_PROFILE_SETTING,
    component: FistarProfileSetting,
    layout: FiStarMaster,
    wrapper: FistarProfileWrapper,
    usertype: "fistar",
    type: "UserRoute"
  },
  {
    path: routeName.FISTAR_DASHBOARD,
    component: FistarDashboard,
    layout: FiStarMaster,
    wrapper: FistarDashboardWrapper,
    usertype: "fistar",
    type: "UserRoute"
  },
  {
    path: routeName.FISTAR_SEARCH_CAMPAIGN,
    component: FistarSearchCampaign,
    layout: FiStarMaster,
    wrapper: FistarDashboardWrapper,
    usertype: "fistar",
    type: "UserRoute"
  },
  {
    path: routeName.FISTAR_MY_CAMPAIGN,
    component: FistarMyCampaign,
    layout: FiStarMaster,
    wrapper: FistarDashboardWrapper,
    usertype: "fistar",
    type: "UserRoute"
  },
  {
    path: routeName.FISTAR_NEW_CAMPAIGN,
    component: FistarNewCampaign,
    layout: FiStarMaster,
    wrapper: FistarDashboardWrapper,
    usertype: "fistar",
    type: "UserRoute"
  },
  {
    path: routeName.FISTAR_WRITE_REVIEW,
    component: FistarWriteReview,
    layout: FiStarMaster,
    wrapper: FistarDashboardWrapper,
    usertype: "fistar",
    type: "UserRoute"
  },
  {
    path: routeName.FISTAR_READ_REVIEW,
    component: FistarReadReview,
    layout: FiStarMaster,
    wrapper: FistarDashboardWrapper,
    usertype: "fistar",
    type: "UserRoute"
  },
  {
    path: routeName.FISTAR_EDIT_REVIEW,
    component: FistarEditReview,
    layout: FiStarMaster,
    wrapper: FistarDashboardWrapper,
    usertype: "fistar",
    type: "UserRoute"
  },

  {
    path: routeName.FISTAR_CAMPAIGN_DETAIL,
    component: FistarMyCampaignDetail,
    layout: FiStarMaster,
    wrapper: FistarDashboardWrapper,
    usertype: "fistar",
    type: "UserRoute"
  },

  {
    path: routeName.FISTAR_QA_DETAIL,
    component: FistarQADetail,
    layout: FiStarMaster,
    wrapper: FistarDashboardWrapper,
    usertype: "fistar",
    type: "UserRoute"
  },
  {
    path: routeName.FISTAR_QA_ANSWER_QUESTION,
    component: FistarAnswerQuestion,
    layout: FiStarMaster,
    wrapper: FistarDashboardWrapper,
    usertype: "fistar",
    type: "UserRoute"
  },
  {
    path: routeName.FISTAR_QA,
    component: FistarQA,
    layout: FiStarMaster,
    wrapper: FistarDashboardWrapper,
    usertype: "fistar",
    type: "UserRoute"
  },
  //partner
  {
    path: routeName.PARTNER_LOGIN,
    component: Loadable({
      loader: () =>
        import(/* webpackChunkName: "login" */ "./../pages/Partner/Auth/Login"),
      loading: () => null,
      modules: ["login"],
      webpack: () => [require.resolveWeak("./../pages/Partner/Auth/Login")]
    }),
    layout: PartnerMaster,
    wrapper: PartnerAuthWrapper,
    type: "GuestRoute"
  },
  // {
  //     path: routeName.PARTNER_LOGIN,
  //     component: PartnerLogin,
  //     layout: PartnerMaster,
  //     wrapper: PartnerAuthWrapper,
  //     type: 'GuestRoute'
  // },
  {
    path: routeName.PARTNER_FIND_EMAIL,
    component: PartnerFindEmail,
    layout: PartnerMaster,
    wrapper: PartnerAuthWrapper,
    type: "GuestRoute"
  },
  {
    path: routeName.PARTNER_FORGOT_PASSWORD,
    component: PartnerForgotPassword,
    layout: PartnerMaster,
    wrapper: PartnerAuthWrapper,
    type: "GuestRoute"
  },
  {
    path: routeName.PARTNER_REGISTER,
    component: PartnerRegister,
    layout: PartnerMaster,
    wrapper: PartnerAuthWrapperRegister,
    type: "GuestRoute"
  },

  {
    path: routeName.PARTNER_APPROVE_REGISTER,
    component: PartnerApproveRegister,
    layout: BaseMaster,
    type: "Route"
  },

  {
    path: routeName.PARTNER_CREATE_CAMPAIN,
    component: PartnerCreateCampain,
    layout: PartnerMasterDashboard,
    wrapper: PartnerAuthWrapperDashboard,
    usertype: "partner",
    type: "UserRoute"
  },

  {
    path: routeName.PARTNER_COMPANY_INFORMATION,
    component: PartnerCompanyInformation,
    layout: PartnerMasterDashboard,
    wrapper: PartnerAuthWrapperProfile,
    usertype: "partner",
    type: "UserRoute"
  },
  {
    path: routeName.PARTNER_CHANGE_PASSWORD,
    component: PartnerChangePassword,
    layout: PartnerMasterDashboard,
    wrapper: PartnerAuthWrapperProfile,
    usertype: "partner",
    type: "UserRoute"
  },
  {
    path: routeName.PARTNER_MANAGER_INFORMATION,
    component: PartnerManagerInformation,
    layout: PartnerMasterDashboard,
    wrapper: PartnerAuthWrapperProfile,
    usertype: "partner",
    type: "UserRoute"
  },
  {
    path: routeName.PARTNER_DASHBOARD,
    component: PartnerDashboard,
    layout: PartnerMasterDashboard,
    wrapper: PartnerAuthWrapperDashboard,
    usertype: "partner",
    type: "UserRoute"
  },
  {
    path: routeName.PARTNER_CAMPAIGN_TRACKING,
    component: CampaignTracking,
    layout: PartnerMasterDashboard,
    wrapper: PartnerAuthWrapperDashboard,
    usertype: "partner",
    type: "UserRoute"
  },
  {
    path: routeName.PARTNER_CAMPAIGN_TRACKING_DETAIL,
    component: CampaignTrackingDetail,
    layout: PartnerMasterDashboard,
    wrapper: PartnerAuthWrapperDashboard,
    usertype: "partner",
    type: "UserRoute"
  },
  {
    path: routeName.PARTNER_QA_DETAIL,
    component: PartnerQADetail,
    layout: PartnerMasterDashboard,
    wrapper: PartnerAuthWrapperDashboard,
    usertype: "partner",
    type: "UserRoute"
  },
  {
    path: routeName.PARTNER_QA_ANSWER_QUESTION,
    component: PartnerAnswerQuestion,
    layout: PartnerMasterDashboard,
    wrapper: PartnerAuthWrapperDashboard,
    usertype: "partner",
    type: "UserRoute"
  },
  {
    path: routeName.PARTNER_QA,
    component: PartnerQA,
    layout: PartnerMasterDashboard,
    wrapper: PartnerAuthWrapperDashboard,
    usertype: "partner",
    type: "UserRoute"
  },
  {
    path: routeName.PARTNER_MY_FISTAR,
    component: PartnerMyFistar,
    layout: PartnerMasterDashboard,
    wrapper: PartnerAuthWrapperDashboard,
    usertype: "partner",
    type: "UserRoute"
  },
  {
    path: routeName.PARTNER_SEARCH_FISTAR,
    component: PartnerSearchFistar,
    layout: PartnerMasterDashboard,
    wrapper: PartnerAuthWrapperDashboard,
    usertype: "partner",
    type: "UserRoute"
  }
];
