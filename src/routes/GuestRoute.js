import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Route, Redirect } from "react-router-dom";

const GuestRoute = ({
  isAuthenticated,
  auth,
  user,
  component: Component,
  layout: Layout,
  wrapper: Wrapper,
  ...rest
}) => {
  // If have layout
  console.log(auth);
  if (Layout) {
    if (Wrapper) {
      return (
        <Route
          {...rest}
          render={props =>
            !isAuthenticated ? (
              <Layout>
                <Wrapper>
                  <Component {...props} />
                </Wrapper>
              </Layout>
            ) : user && user.pid ? (
              auth.first_login == 1 ? (
                <Redirect to="/partner/approval-register" />
              ) : (
                <Redirect to="/partner/dashboard" />
              )
            ) : auth.first_login == 1 ? (
              <Redirect to="/fi-star/join/approval-register" />
            ) : (
              <Redirect to="/fi-star/dashboard" />
            )
          }
        />
      );
    }
    return (
      <Route
        {...rest}
        render={props =>
          !isAuthenticated ? (
            <Layout>
              <Component {...props} />
            </Layout>
          ) : user && user.pid ? (
            auth.first_login == 1 ? (
              <Redirect to="/partner/approval-register" />
            ) : (
              <Redirect to="/partner/dashboard" />
            )
          ) : auth.first_login == 1 ? (
            <Redirect to="/fi-star/join/approval-register" />
          ) : (
            <Redirect to="/fi-star/dashboard" />
          )
        }
      />
    );
  }
  if (Wrapper) {
    return (
      <Route
        {...rest}
        render={props =>
          !isAuthenticated ? (
            <Wrapper>
              <Component {...props} />
            </Wrapper>
          ) : user && user.pid ? (
            auth.first_login == 1 ? (
              <Redirect to="/partner/approval-register" />
            ) : (
              <Redirect to="/partner/dashboard" />
            )
          ) : auth.first_login == 1 ? (
            <Redirect to="/fi-star/join/approval-register" />
          ) : (
            <Redirect to="/fi-star/dashboard" />
          )
        }
      />
    );
  }

  return (
    <Route
      {...rest}
      render={props =>
        !isAuthenticated ? (
          <Component {...props} />
        ) : user && user.pid ? (
          auth.first_login == 1 ? (
            <Redirect to="/partner/approval-register" />
          ) : (
            <Redirect to="/partner/dashboard" />
          )
        ) : auth.first_login == 1 ? (
          <Redirect to="/fi-star/join/approval-register" />
        ) : (
          <Redirect to="/fi-star/dashboard" />
        )
      }
    />
  );
};

GuestRoute.propTypes = {
  // component: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool.isRequired
};

function mapStateToProps(state) {
  return {
    isAuthenticated: !!state.auth.token,
    user: state.auth.user,
    auth: state.auth
    // isAuthenticated: !!state.auth
  };
}

export default connect(mapStateToProps)(GuestRoute);
