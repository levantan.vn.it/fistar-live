import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import {
  Panel,
  PanelGroup,
  Accordion,
  Card,
  CustomToggle,
  Button
} from "react-bootstrap";
// import Accordion from 'react-bootstrap/Accordion'

class faq extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      activeKey: 0,
      accordion: false
    };
  }
  handleSelect = activeKey => {
    this.setState({ activeKey });
  };

  toggleShow = () => {
    console.log(this.state.show);
    this.setState({ show: !this.state.show });
  };
  clickToggle = (key, e) => {
    console.log(e);
    console.log(key);

    if (key === this.state.accordion) {
      this.setState({
        accordion: ""
      });
    } else {
      this.setState({
        accordion: key
      });
    }
  };

  render() {
    const { t, faq, keyTo } = this.props;
    console.log(faq);
    console.log(keyTo);
    let status = "";

    return (
      <Fragment key={keyTo}>
        <Card>
          <Card.Header
            className={this.state.accordion === keyTo ? "selected" : ""}
          >
            <Accordion.Toggle
              as={Button}
              variant="link"
              onClick={e => this.clickToggle(keyTo, e)}
              eventKey={keyTo}
            >
              <p className="collapsed">{faq.faq_title}</p>
              <i
                className={
                  this.state.accordion === keyTo
                    ? "fas fa-chevron-up"
                    : "fas fa-chevron-down"
                }
              />
            </Accordion.Toggle>
          </Card.Header>
          <Accordion.Collapse eventKey={keyTo} className="collapse-faq">
            <Card.Body>
              <span>A</span>
              <p>{faq.faq_content}</p>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
      </Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps)(withTranslation("translations")(faq));
