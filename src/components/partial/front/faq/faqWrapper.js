import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import {BackToTop} from "./../../../partial/BackToTop"

let lastScrollY = 0;
let ticking = false;

class faqWrapper extends Component {
  constructor(props) {
    super(props);
    this.state = {
      transform: false
    };
  }

  scrollStep = () => {
    if (typeof window !== 'undefined' && window.document && window.document.createElement) {
      if (window.pageYOffset === 0) {
        clearInterval(this.state.intervalId);
      }
      window.scroll(0, window.pageYOffset - this.props.scrollStepInPx);
    }
  };

  scrollToTop = () => {
    let intervalId = setInterval(this.scrollStep, 16.66);
    this.setState({ intervalId: intervalId });
  };

  componentDidMount() {
    if (typeof window !== 'undefined' && window.document && window.document.createElement) {
      window.addEventListener("scroll", this.handleScroll);
    }
  }

  componentWillUnmount() {
    if (typeof window !== 'undefined' && window.document && window.document.createElement) {
      window.removeEventListener("scroll", this.handleScroll);
    }
  }

  nav = React.createRef();

  handleScroll = () => {
    lastScrollY = (typeof window !== 'undefined' && window.document && window.document.createElement) ? window.pageYOffset : 0;
    console.log(lastScrollY);

    if (lastScrollY > 150) {
      console.log("zozozozo");
      this.setState({
        transform: true
      });
    } else {
      console.log("zozozozo else");
      this.setState({
        transform: false
      });
    }
  };

  render() {
    const { t } = this.props;
    const { transform } = this.state;
    return (
      <Fragment>
        <main className="main-container">
          <section
            className={
              transform
                ? "form-login mm-faq  mm-campaign-scroll"
                : "form-login mm-faq"
            }
            id="on-scroll"
            ref={this.nav}
          >
            <div className="banner-container">
              <div className="row">
                <div className="col-md-12" />
              </div>
            </div>

            {this.props.children}
          </section>
        </main>

      </Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps)(
  withTranslation("translations")(faqWrapper)
);
