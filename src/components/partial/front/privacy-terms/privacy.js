import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import imageIdol from "./../../../../images/dash-02.png";
import imageClose from "./../../../../images/close-popup.svg";
import imageFb from "./../../../../images/face.png";
import imageYt from "./../../../../images/toutube.png";
import imageIstar from "./../../../../images/intar.png";
import imageRead from "./../../../../images/mk.png";
import { nFormatter } from "./../../../../common/helper";
import ModalStatusCampaign from "./../../../fiStar/Dashboard/MyCampaign/ModalStatusCampaign";

import { nextStepAction } from "./../../../../store/actions/campaign";

import { Modal, Tabs, Tab, Nav } from "react-bootstrap";

class Privacy extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <React.Fragment>
        <div className="container">
          <h1 className="main-title mb-3">Chính sách bảo mật</h1>
          <h2 className="section-title mb-3">Quy tắc chung</h2>
          <p className="section-content">
            DM &amp; C cam kết bảo vệ sự riêng tư của các thành viên, phù hợp
            với các quy định của pháp luật và các quy định liên quan đến sự
            riêng tư và cam kết Chính sách bảo vệ sự riêng tư của người dùng bổ
            nhiệm quyền phù hợp với các luật và quy định có liên quan. Công ty
            sẽ thông báo cho bạn về mục đích và cách thức thông tin cá nhân được
            cung cấp bởi thành viên được sử dụng thông qua chính sách xử lý
            thông tin cá nhân và các biện pháp được thực hiện để bảo vệ thông
            tin cá nhân. Chính sách xử lý thông tin cá nhân thiết lập các thủ
            tục cần thiết để cải thiện liên tục chính sách xử lý thông tin cá
            nhân nhằm thay đổi luật pháp và quy định của chính phủ và cung cấp
            các dịch vụ tốt hơn. Trong trường hợp có bất kỳ thay đổi hoặc sửa
            đổi nào, công ty sẽ thông báo cho bạn thông qua chính sách xử lý
            thông tin cá nhân của trang web và cũng sẽ thông báo cho bạn qua
            e-mail. Người dùng được yêu cầu kiểm tra thường xuyên khi truy cập
            trang web. Bạn có thể từ chối đồng ý với bất kỳ vấn đề nào sau đây
            liên quan đến việc thu thập, sử dụng, cung cấp hoặc ký gửi thông tin
            cá nhân. Tuy nhiên, nếu người dùng từ chối chấp thuận, dịch vụ sẽ
            không được sử dụng toàn bộ hoặc một phần.
          </p>
          <ul>
            <li className="section-item pl-3">
              1. Mục đích thu thập và sử dụng thông tin cá nhân
              <ul>
                <li className="sub-item pl-4">
                  {" "}
                  A. Công ty thu thập thông tin tối thiểu cần thiết cho việc đặt
                  hàng trơn tru hàng hóa và dịch vụ, thanh toán hàng hóa và
                  thông tin tùy chỉnh thuận tiện và hữu ích cho các thành viên
                  khi đăng ký.
                </li>
                <li className="sub-item pl-4">
                  {" "}
                  B. Công ty không thu thập thông tin cá nhân có thể vi phạm
                  đáng kể quyền con người cơ bản của các thành viên, bao gồm
                  chủng tộc và dân tộc, tư tưởng và tín ngưỡng, quê hương và địa
                  phương, định hướng chính trị và hồ sơ tội phạm và tình trạng
                  sức khỏe. Và Thông tin cá nhân mà chúng tôi thu thập sẽ được
                  sử dụng cho các mục đích sau: Liên quan đến thành viên: Nhận
                  dạng, thủ tục nhận dạng cá nhân, yêu cầu của khách hàng và xử
                  lý khiếu nại, các thông báo khác nhau, thông báo liên quan đến
                  việc sử dụng dịch vụ thành viên. Bảo quản hồ sơ giải quyết
                  tranh chấp. Liên quan đến phân phối sản phẩm hoặc dịch vụ:
                  Cung cấp thông tin về thông tin dịch vụ / sự kiện mới liên
                  quan đến tiếp thị và cung cấp thông tin sản phẩm như cung cấp
                  hàng hóa sản phẩm / sự kiện hoặc gửi hóa đơn và sử dụng nó làm
                  dữ liệu thống kê cho khảo sát sự hài lòng của khách hàng
                </li>
              </ul>
            </li>
            <li className="section-item pl-3">
              {" "}
              2. Các mục thu thập thông tin cá nhân{" "}
              <ul>
                <li className="sub-item pl-4">
                  {" "}
                  A. Công ty thu thập thông tin cá nhân như sau cho mục đích
                  đăng ký thành viên, tư vấn thông qua trung tâm khách hàng và
                  các dịch vụ khác nhau. Tư cách thành viên: ID, tên, số liên
                  lạc, số điện thoại di động, địa chỉ (thông tin địa chỉ giao
                  hàng)
                </li>
              </ul>
            </li>
            <li className="section-item pl-3">
              {" "}
              3. Thời gian lưu giữ và sử dụng thông tin cá nhân Theo nguyên tắc
              chung, sau khi thu thập và sử dụng thông tin cá nhân được thực
              hiện, công ty sẽ ngay lập tức phá hủy thông tin cá nhân. Tuy
              nhiên, nếu cần bảo quản theo chính sách nội bộ và pháp luật có
              liên quan, Công ty sẽ lưu giữ thông tin thành viên trong một
              khoảng thời gian xác định bởi các luật và quy định liên quan như
              sau.
              <ul>
                <li className="sub-item pl-4">
                  {" "}
                  A. Biên bản thỏa thuận hoặc rút tiền Nền tảng bảo tồn: Đạo
                  luật bảo vệ người tiêu dùng trong thương mại điện tử Thời gian
                  lưu giữ: 5 năm
                </li>
                <li className="sub-item pl-4">
                  {" "}
                  B. Hồ sơ thanh toán và cung cấp hàng hóa Nền tảng bảo tồn: Đạo
                  luật bảo vệ người tiêu dùng trong thương mại điện tử Thời gian
                  lưu giữ: 5 năm Và Hồ sơ khiếu nại hoặc tranh chấp của người
                  tiêu dùng Nền tảng bảo tồn: Đạo luật bảo vệ người tiêu dùng
                  trong thương mại điện tử Thời hạn sử dụng: 3 năm
                </li>
              </ul>
            </li>
            <li className="section-item pl-3">
              {" "}
              4. Thủ tục và phương thức hủy thông tin cá nhân Về nguyên tắc,
              thông tin cá nhân của thành viên sẽ bị hủy bỏ không chậm trễ khi
              mục đích thu thập và sử dụng thông tin cá nhân đạt được.
              <ul>
                <li className="sub-item pl-4">
                  {" "}
                  A. Quy trình hủy Thông tin cá nhân được cung cấp bởi thành
                  viên để sử dụng thành viên và dịch vụ sẽ bị xóa và hủy trong
                  một khoảng thời gian nhất định theo chính sách nội bộ và các
                  luật và quy định khác có liên quan sau khi mục đích sử dụng
                  được thực hiện.
                </li>
                <li className="sub-item pl-4">
                  {" "}
                  B. Cách tiêu diệt Thông tin cá nhân được in trên giấy bị
                  nghiền nát bởi máy nghiền hoặc bị phá hủy do đốt. Thông tin cá
                  nhân được lưu trữ dưới dạng tệp tin điện tử sẽ bị xóa bằng
                  phương pháp kỹ thuật không thể tái tạo bản ghi.
                </li>
              </ul>
            </li>
            <li className="section-item pl-3">
              {" "}
              5. Cung cấp cho bên thứ ba thông tin cá nhân
              <ul>
                <li className="sub-item pl-4">
                  {" "}
                  A. Công ty sẽ chỉ sử dụng thông tin cá nhân của thành viên
                  trong phạm vi thông báo được nêu trong "Mục đích và mục đích
                  thu thập thông tin cá nhân" của chính sách xử lý thông tin cá
                  nhân hoặc trong điều kiện sử dụng dịch vụ, Tôi thì không.
                  Nhưng trong trường hợp của một số sản phẩm, có thể được chuyển
                  giao cho người bán thông tin cá nhân bên ngoài phạm vi này,
                  thì chúng ta sẽ đặt ra mục đích của trường hợp này, cung cấp,
                  thông tin, khung thời gian, phân phôi thu được bên thứ ba cung
                  cấp đồng ý.
                </li>
              </ul>
            </li>
            <li className="section-item pl-3">
              {" "}
              6. Quyền của thành viên và người đại diện hợp pháp của họ và cách
              họ được thực hiện{" "}
              <ul>
                <li className="sub-item pl-4">
                  {" "}
                  A. Thành viên và người đại diện hợp pháp của họ có thể xem
                  hoặc sửa đổi thông tin cá nhân của họ được đăng ký bất kỳ lúc
                  nào và rút lại sự chấp thuận của họ để sử dụng thông tin cá
                  nhân thông qua quy trình rút tên thành viên.
                </li>
                <li className="sub-item pl-4">
                  {" "}
                  B. Để xem / chỉnh sửa thông tin cá nhân, và có thể được kiểm
                  tra trong [Bảo mật] mục trong [My Page] nhà, huỷ đăng ký (rút
                  lui) được đọc / chỉnh sửa số liên lạc trong văn bản, bằng điện
                  thoại hoặc email để Cán bộ bảo mật của Công ty / Bạn có thể
                  yêu cầu rút tiền. Và Nếu một thành viên và đại diện pháp lý
                  của mình yêu cầu sửa lỗi trong thông tin cá nhân, chúng tôi sẽ
                  không sử dụng hoặc cung cấp thông tin cá nhân cho đến khi
                  chúng tôi hoàn tất việc sửa đổi. Công ty xử lý thông tin cá
                  nhân đã bị chấm dứt hoặc xóa theo yêu cầu của các thành viên
                  và người đại diện hợp pháp của họ theo quy định trong thời
                  gian nắm giữ và sử dụng thông tin cá nhân và không được xem
                  hoặc truy cập vào bất kỳ mục đích nào khác.
                </li>
              </ul>
            </li>
            <li className="section-item pl-3">
              {" "}
              7. Nghĩa vụ thành viên{" "}
              <ul>
                <li className="sub-item pl-4">
                  {" "}
                  A. Các thành viên có nghĩa vụ bảo vệ thông tin cá nhân của họ
                  và không chịu trách nhiệm cho bất kỳ vấn đề nào do rò rỉ thông
                  tin cá nhân do sự sơ suất của thành viên hoặc các vấn đề trên
                  internet.
                </li>
                <li className="sub-item pl-4">
                  {" "}
                  B. Vui lòng đảm bảo nhập thông tin cá nhân của thành viên của
                  bạn ở trạng thái cập nhật nhất. Chịu trách nhiệm về các vấn đề
                  gây ra bởi đầu vào thông tin không chính xác của các thành
                  viên của các thành viên và cho chính mình, theo như để ăn cắp
                  thông tin cá nhân, thành viên hoặc thành viên dịch vụ "Luật
                  Khuyến khích Thông tin và Truyền thông Mạng Sử dụng và bảo vệ
                  thông tin" của người dân bị mất có trình độ Bạn có thể bị
                  trừng phạt.
                </li>
                <li className="sub-item pl-4">
                  {" "}
                  C. Và Các thành viên có quyền bảo vệ thông tin cá nhân của họ
                  và tự bảo vệ mình cũng như không vi phạm thông tin của người
                  khác. Hãy cẩn thận không tiết lộ thông tin cá nhân của các
                  thành viên bao gồm cả ID và mật khẩu của bạn, và không làm
                  hỏng thông tin cá nhân của những người khác bao gồm cả thông
                  tin đăng.
                </li>
                <li className="sub-item pl-4">
                  {" "}
                  D. Các thành viên bắt buộc phải tuân thủ Đạo luật về Khuyến
                  khích Thông tin và Sử dụng Mạng Thông tin và Đạo luật Bảo vệ
                  Thông tin, Đạo luật Bảo vệ Thông tin Cá nhân, Đạo luật Đăng ký
                  Cư trú và các luật thông tin cá nhân khác.
                </li>
              </ul>
            </li>
            <li className="section-item pl-3">
              {" "}
              9. Liên kết trang web{" "}
              <ul>
                <li className="sub-item pl-4">
                  {" "}
                  Công ty có thể cung cấp cho các thành viên liên kết đến các
                  trang web hoặc tài liệu của các công ty khác. Trong trường hợp
                  này, Công ty không có quyền kiểm soát các trang web và tài
                  liệu bên ngoài, và do đó không thể và không thể đảm bảo tính
                  hữu ích của các dịch vụ hoặc tài liệu được cung cấp. Nếu bạn
                  chuyển đến một trang trên trang web bên ngoài thông qua liên
                  kết mà công ty đã đưa vào, vui lòng xem lại chính sách của
                  trang web mới vì chính sách bảo mật của trang web đó độc lập
                  với công ty.
                </li>
              </ul>
            </li>
            <li className="section-item pl-3">
              {" "}
              10. Các biện pháp bảo vệ công nghệ / hành chính về thông tin cá
              nhân Để bảo vệ thông tin cá nhân của các thành viên, Công ty đã
              thực hiện các biện pháp bảo vệ sau.{" "}
              <ul>
                <li className="sub-item pl-4">
                  {" "}
                  A. Chuẩn bị cho hack và virus máy tính Công ty sử dụng các
                  biện pháp an ninh cần thiết để ngăn chặn rò rỉ hoặc tham nhũng
                  thông tin cá nhân của thành viên bằng cách hack hoặc virus máy
                  tính và cố gắng có tất cả các biện pháp kỹ thuật có thể để đảm
                  bảo các biện pháp an ninh tốt hơn.
                </li>
                <li className="sub-item pl-4">
                  {" "}
                  B. Hạn chế và đào tạo bộ xử lý thông tin cá nhân Công ty giới
                  hạn số lượng nhân viên xử lý thông tin cá nhân ở mức tối thiểu
                  và tiến hành đào tạo nhân viên có liên quan theo thời gian để
                  xác nhận việc tuân thủ chính sách này và tuân thủ chính sách
                  này.
                </li>
                <li className="sub-item pl-4">
                  {" "}
                  C. Thiết lập và thực thi kế hoạch quản lý nội bộ Công ty thiết
                  lập và thực hiện các kế hoạch quản lý nội bộ.
                </li>
                <li className="sub-item pl-4">
                  {" "}
                  D. Giữ lịch sử kết nối của bạn và ngăn chặn giả mạo Công ty
                  lưu giữ và quản lý hồ sơ (nhật ký web, thông tin tóm tắt,
                  v.v.) được truy cập vào hệ thống xử lý thông tin cá nhân trong
                  ít nhất sáu tháng và quản lý để ngăn chặn sự giả mạo, trộm cắp
                  và mất hồ sơ truy cập.
                </li>
                <li className="sub-item pl-4">
                  {" "}
                  E. Kiểm soát truy cập cho những người không được phép Công ty
                  thiết lập và kiểm soát quy trình kiểm soát truy cập để lưu trữ
                  vật lý của hệ thống thông tin cá nhân lưu trữ thông tin cá
                  nhân.
                </li>
              </ul>
            </li>
            <li className="section-item pl-3">
              {" "}
              11. Quản lý thông tin cá nhân / quản lý thông tin cá nhân Để bảo
              vệ thông tin cá nhân của thành viên và xử lý các khiếu nại liên
              quan đến thông tin cá nhân, công ty có một bộ phận dịch vụ khách
              hàng. Nếu bạn có bất kỳ câu hỏi nào về thông tin cá nhân của thành
              viên, vui lòng liên hệ với bộ phận dịch vụ khách hàng bên dưới.{" "}
              <ul>
                <li className="sub-item pl-4">
                  {" "}
                  Bộ phận dịch vụ khách hàng - Điện thoại: 0903029423 - Email:
                  fimevn@gmail.com
                </li>
              </ul>
            </li>
            <li className="section-item pl-3">
              {" "}
              12. Bảo vệ quyền riêng tư của trẻ em{" "}
              <ul>
                <li className="sub-item pl-4">
                  {" "}
                  Một số trẻ em dưới 14 tuổi có thể bị hạn chế đối với một số
                  dịch vụ yêu cầu sự đồng ý của người đại diện hợp pháp.
                </li>
              </ul>
            </li>
            <li className="section-item pl-3">
              {" "}
              13. Trách nhiệm thông báo{" "}
              <ul>
                <li className="sub-item pl-4">
                  {" "}
                  Chính sách bảo mật sẽ được áp dụng từ ngày có hiệu lực và nếu
                  các thay đổi hoặc sửa đổi được thực hiện theo luật và chính
                  sách, chúng sẽ được công bố trên trang web 7 ngày trước khi
                  thực thi.
                </li>
              </ul>
            </li>
            <li className="section-item pl-3">
              {" "}
              Ngày có hiệu lực: tháng 7 năm 2018
            </li>
          </ul>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(Privacy));
