import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import imageIdol from "./../../../../images/dash-02.png";
import imageClose from "./../../../../images/close-popup.svg";
import imageFb from "./../../../../images/face.png";
import imageYt from "./../../../../images/toutube.png";
import imageIstar from "./../../../../images/intar.png";
import imageRead from "./../../../../images/mk.png";
import { nFormatter, calcAge } from "./../../../../common/helper";
import ModalStatusCampaign from "./../../../fiStar/Dashboard/MyCampaign/ModalStatusCampaign";

import { nextStepAction } from "./../../../../store/actions/campaign";

import { Modal, Tabs, Tab, Nav } from "react-bootstrap";
import { getImageLink } from "./../../../../common/helper";
import { IMAGE_SIZE, IMAGE_TYPE } from "./../../../../constants/index";

class Fistar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      step: null,
      loading: false,
      fime: 0,
      facebook: 0,
      youtube: 0,
      instagram: 0,

      dataFistar: {}
    };
  }

  componentDidMount() {
    const { fistar } = this.props;
    var fime = "";
    var facebook = "";
    var youtube = "";
    var instagram = "";

    fistar &&
      fistar.channels &&
      fistar.channels.map(channel => {
        if (channel.sns_id == 1) {
          fime = channel.usn_follower;
        }
        if (channel.sns_id == 2) {
          facebook = channel.usn_follower;
        }
        if (channel.sns_id == 3) {
          youtube = channel.usn_follower;
        }
        if (channel.sns_id == 4) {
          instagram = channel.usn_follower;
        }
      });
    this.setState({
      fime: fime,
      facebook: facebook,
      youtube: youtube,
      instagram: instagram
    });
  }

  render() {
    const { t, fistar } = this.props;
    const { dataFistar } = this.state;
    let oldString=fistar.dob;
    let year = new Date(fistar.dob);
    let yearConver = year.toDateString();
    let yearFistar = yearConver.split(" ").pop();
    fistar.dob = yearFistar;
    let yearCurrent = new Date().getFullYear();
    let oldCurrent = yearCurrent - fistar.dob;
    return (
      <div className="card align-items-center">
        <div className="left">
          <a href="javascript:void(0)" className="disabled-hover">
            <img
              src={getImageLink(
                fistar.picture,
                IMAGE_TYPE.FISTARS,
                IMAGE_SIZE.ORIGINAL
              )}
              alt=""
            />
          </a>
          <div className="btn over-flow">
            <i className="fas fa-star" />
            <span className="pl-1">
              {fistar.rate_scrap ? fistar.rate_scrap : 0}
            </span>
          </div>
        </div>
        <div className="right">
          <div className="top-name">
            <div className="name">
              <i
                className={`fas ${
                  fistar.gender &&
                  fistar.gender.cd_id &&
                  fistar.gender.cd_id == 10
                    ? " fa-venus"
                    : "  fa-mars"
                }`}
              />
              <h2>
                <a href="javascript:void(0)" className="disabled-hover">
                  {fistar && fistar.fullname ? fistar.fullname : ""}
                </a>
              </h2>
            </div>
            <div className="address-old">
              <span>
                {fistar.dob}({calcAge(oldString)})
              </span>
              <span className="address">{fistar.location.cd_label}</span>
            </div>
          </div>
          <div className="content-card">
            <h3>{fistar.status_message}</h3>
            <p>“{fistar.self_intro}”</p>
          </div>
          <div className="tab-mmfistar">
            {fistar.keywords &&
              fistar.keywords.map((keyword, key) => (
                <a href="javascript:void(0)" key={key}>
                  {keyword.code.cd_label}
                </a>
              ))}
          </div>
          <div className="card-footer">
            <ul className="list-item">
              <li className="item">
                <a href="javascript:void(0)" className="disabled-hover">
                  <img src={imageRead} alt="" />
                </a>
                <span>{nFormatter(this.state.fime, 0)}</span>
              </li>
              <li className="item">
                <a href="javascript:void(0)" className="disabled-hover">
                  <img src={imageFb} alt="" />
                </a>
                <span>{nFormatter(this.state.facebook, 0)}</span>
              </li>
              <li className="item">
                <a href="javascript:void(0)" className="disabled-hover">
                  <img src={imageIstar} alt="" />
                </a>
                <span>{nFormatter(this.state.youtube, 0)}</span>
              </li>
              <li className="item">
                <a href="javascript:void(0)" className="disabled-hover">
                  <img src={imageYt} alt="" />
                </a>
                <span>{nFormatter(this.state.instagram, 0)}</span>
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(Fistar));
