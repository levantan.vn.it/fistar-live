import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import Image08 from "./../../../../images/service/08.png";
import Image01 from "./../../../../images/service/01.png";
import ImageNext from "./../../../../images/service/next-fistare.svg";
import * as routeName from "./../../../../routes/routeName"

import ModalStatusCampaign from "./../../../fiStar/Dashboard/MyCampaign/ModalStatusCampaign";

import { nextStepAction } from "./../../../../store/actions/campaign";

import { Modal, Tabs, Tab, Nav } from "react-bootstrap";
import example from "./../../../../images/thumnaildetail.png";

class Fistar extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { t } = this.props;

    return (
      <div className="content">
        <div className="alway-stay">
          <h2>{t("SERVICE.SER_FISTAR_title")}</h2>
          <p>
              {t("SERVICE.SER_FISTAR_des")}
          </p>
        </div>
        <div className="banner-platform-fistar">
          <img src={Image08} alt="" />
        </div>
        <div className="sytar-of">
          <h2>{t("SERVICE.SER_FISTAR_fistar_title")}</h2>
          <p>
              {t("SERVICE.SER_FISTAR_fistar_des")}
          </p>
        </div>
        <div className="banner-worlk">
          <img src={Image01} alt="" />
        </div>
        <div className="btn-fistars">
          <Link to={routeName.FISTAR_LOGIN} className="btn btn-flatform-fistar">
            <span>
                {t("SERVICE.SER_FISTAR_btn_title")}
              <span className="text">{t("SERVICE.SER_FISTAR_btn_des")}</span>
            </span>
            <img src={ImageNext} alt="" />
          </Link>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(Fistar));
