import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import Image06 from "./../../../../images/service/06.png";
import Image07 from "./../../../../images/service/07.png";
import Image05 from "./../../../../images/service/05.png";
import * as routeName from "./../../../../routes/routeName"

import ModalStatusCampaign from "./../../../fiStar/Dashboard/MyCampaign/ModalStatusCampaign";

import { nextStepAction } from "./../../../../store/actions/campaign";

import { Modal, Tabs, Tab, Nav } from "react-bootstrap";
import example from "./../../../../images/thumnaildetail.png";

class Service extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { t } = this.props;

    return (
      <div className="content">
        <div className="top-title">
          <h2>
              {t("SERVICE.SER_title_label")}
          </h2>
          <p>
              {t("SERVICE.SER_description")}

          </p>
        </div>
        <div className="banner-services">
          <img src={Image06} alt="" />
        </div>
        <div className="platform-box">
          <div className="item-platform">
            <h2 className="title">{t("SERVICE.SER_platform")}</h2>
            <p>
                {t("SERVICE.SER_platform_description")}

            </p>
            <div className="img-plat-form">
              <img src={Image07} alt="" />
            </div>
          </div>
          <div className="item-platform">
            <h2 className="title">{t("SERVICE.SER_image_title")}</h2>
            <p>
                {t("SERVICE.SER_image_description")}

            </p>
            <div className="img-plat-form">
              <img src={Image05} alt="" />
            </div>
          </div>
        </div>
        <div className="gr-btn-platform">
          <Link to={routeName.SERVICE_FISTAR} className="btn btn-platform">
              {t("SERVICE.SER_btn_title_fistar")}
            <span>{t("SERVICE.SER_btn_fistar_des")}</span>
          </Link>
          <Link to={routeName.SERVICE_PARTNER} className="btn btn-platform">
              {t("SERVICE.SER_btn_title_partner")}

            <span>{t("SERVICE.SER_btn_partner_des")}</span>
          </Link>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(Service));
