import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

class contactWrapper extends Component {
  render() {
    const { t } = this.props;
    return (
      <Fragment>
        <section className="banner-container">
          <div className="row">
            <div className="col-md-12" />
          </div>
        </section>
        <main className="main-container">
          <section className="form-login mm-faq mm-contact">
            <div className="container-mmfaq">{this.props.children}</div>
          </section>
          <section>
            <div className="drop-up btn">
              <i className="fas fa-long-arrow-alt-up" />
            </div>
          </section>
        </main>
      </Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps)(
  withTranslation("translations")(contactWrapper)
);
