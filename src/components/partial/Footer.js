import React, { Component } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import logoFooter from "../../images/logo-footer.png";
import * as routeName from "../../routes/routeName";
import "./Footer.scss";

class MasterFooter extends Component {
  render() {
    const { t } = this.props;
    return (
      <footer className="footer-container">
        <div className="container">
          <div className="content-footer">
            <div className="row footer-site">
              <div className="col-md-2 ">
                <div className="logo">
                  <h1>
                    <a href="">
                      <img src={logoFooter} alt="fistar" />
                    </a>
                  </h1>
                </div>
              </div>
              <div className="col-md-7">
                <div className="address">
                  <p>
                    473 Dien Bien Phu, Ward 25, Binh Thanh District, Ho Chi Minh
                    City l clubpiaf@gmail.com l Tel 0903029423
                  </p>
                </div>
              </div>
              <div className="col-md-3">
                <div className="copyright">
                  <p className="footer-privacy">
                    <Link
                      className="nav-item nav-link privacy-terms"
                      to={routeName.PRIVACY_TERMS}
                    >
                      {t("FOOTER.FTR_privacy_policy")}
                    </Link>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps)(
  withTranslation("translations")(MasterFooter)
);
