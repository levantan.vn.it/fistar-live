import React, { Component } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import Header from "./Header";
import Footer from "./Footer";

import BackToTop from "./BackToTop";




class MasterFront extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showBacktoTop:false,

        };
        if (typeof window !== 'undefined' && window.document && window.document.createElement) {
          window.scrollTo(0, 0);
        }
    }


  render() {
    const { t } = this.props;

    return (
      <div className="wrapper front-page">
        <Header />

        {this.props.children}

        <BackToTop />

        <Footer />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps)(
  withTranslation("translations")(MasterFront)
);
