import React, { Component } from 'react';
import { withTranslation, Trans } from 'react-i18next';
import { Link } from 'react-router-dom'

import { connect } from 'react-redux'

class PartnerAuthFooter extends Component {
  render() {
    const { t } = this.props;
    return (
        <div className="card-content card-partner">
            <div className="row">

                <div className="col-md-12 text-card">
                    <div className="text-note">
                        <div className="text">
                            <p className="quoite text-center">❝</p>
                            <h3 className="text-center mb-2">새로운 마케팅 플랫폼</h3>
                            <h3 className="text-center mb-2">fiStar와 함께해요!</h3>
                            <p className="text-center">당신과 함께할 특별한 인플로언서를 만나보세요.</p>
                            <p className="quoite text-center">❠</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
  }
}

function mapStateToProps (state) {
  return {}
}

export default connect(mapStateToProps)(withTranslation('translations')(PartnerAuthFooter))
