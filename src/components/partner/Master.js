import React, { Component } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, NavLink } from "react-router-dom";

import Header from "./Layout/Head";
import Footer from "./Layout/Footer";

import { connect } from "react-redux";
import * as routeName from "../../routes/routeName";

import { Navbar, Nav, NavDropdown } from "react-bootstrap";
import "./master.scss";

import BackToTop from "./../partial/BackToTop";

class PartnerMaster extends Component {

    constructor(props) {
        super(props);
        this.state = {

        };
        if (typeof window !== 'undefined' && window.document && window.document.createElement) {
          window.scrollTo(0, 0);
        }
    }

  render() {
    const { t } = this.props;
    console.log(this.props);
    return (
      <div className="wrapper front-page">
        <Header />

        {this.props.children.props &&
        this.props.children.props.children.props.location.pathname ==
          routeName.PARTNER_REGISTER ? (
          <section className="register-banner-partner">
            <div className="header-join">
              <div className="bg-color">
                <div className="row mt-0 mr-0">
                  <div className="col-md-12">
                    <div className="title-join-account">
                      <h2 className="text-center">
                        {t("PARTNER_LOGIN.FLC_account")}
                      </h2>
                    </div>
                  </div>
                  <div className="col-md-12  col-join">
                    <div className="menu-join">
                      <Navbar
                        bg="light"
                        expand="lg"
                        className=" navbar-light bg-light"
                      >
                        <Navbar.Toggle aria-controls="basic-navbar-nav-register">
                          <span className="toggler-icon">
                            <i className="fas fa-bars" />
                          </span>
                        </Navbar.Toggle>
                        <Navbar.Collapse id="basic-navbar-nav-register">
                          <Nav>
                            <NavLink
                              className="nav-item nav-link"
                              to={routeName.PARTNER_LOGIN}
                              activeClassName="active"
                            >
                              {t("PARTNER_LOGIN.PLG_login")}
                            </NavLink>
                            <NavLink
                              className="nav-item nav-link"
                              to={routeName.PARTNER_FIND_EMAIL}
                              activeClassName="active"
                            >
                              {t("PARTNER_LOGIN.PLG_find_id")}
                            </NavLink>
                            <NavLink
                              className="nav-item nav-link"
                              to={routeName.PARTNER_FORGOT_PASSWORD}
                              activeClassName="active"
                            >
                              {t("PARTNER_LOGIN.PLG_ResetPW")}
                            </NavLink>
                            <NavLink
                              className="nav-item nav-link"
                              to={routeName.PARTNER_REGISTER}
                              activeClassName="active"
                            >
                              {t("PARTNER_LOGIN.PLG_Join")}
                            </NavLink>
                          </Nav>
                        </Navbar.Collapse>
                      </Navbar>

                      {/*<nav className="navbar navbar-expand-lg navbar-light bg-light">*/}
                      {/*<button*/}
                      {/*className="navbar-toggler collapsed"*/}
                      {/*type="button"*/}
                      {/*data-toggle="collapse"*/}
                      {/*data-target="#navbarNavAltMarkup"*/}
                      {/*aria-controls="navbarNavAltMarkup"*/}
                      {/*aria-expanded="false"*/}
                      {/*aria-label="Toggle navigation"*/}
                      {/*>*/}
                      {/*<span className="toggler-icon">*/}
                      {/*<i className="fas fa-bars" />*/}
                      {/*</span>*/}
                      {/*</button>*/}
                      {/*<div*/}
                      {/*className="navbar-collapse collapse"*/}
                      {/*id="navbarNavAltMarkup"*/}
                      {/*>*/}
                      {/*<div className="navbar-nav">*/}
                      {/*<NavLink*/}
                      {/*className="nav-item nav-link"*/}
                      {/*to={routeName.PARTNER_LOGIN}*/}
                      {/*activeClassName="active"*/}
                      {/*>*/}
                      {/*Login*/}
                      {/*</NavLink>*/}
                      {/*<NavLink*/}
                      {/*className="nav-item nav-link"*/}
                      {/*to={routeName.PARTNER_FIND_EMAIL}*/}
                      {/*activeClassName="active"*/}
                      {/*>*/}
                      {/*Find ID*/}
                      {/*</NavLink>*/}
                      {/*<NavLink*/}
                      {/*className="nav-item nav-link"*/}
                      {/*to={routeName.PARTNER_FORGOT_PASSWORD}*/}
                      {/*activeClassName="active"*/}
                      {/*>*/}
                      {/*Reset PW*/}
                      {/*</NavLink>*/}
                      {/*<NavLink*/}
                      {/*className="nav-item nav-link"*/}
                      {/*to={routeName.PARTNER_REGISTER}*/}
                      {/*activeClassName="active"*/}
                      {/*>*/}
                      {/*Join*/}
                      {/*</NavLink>*/}
                      {/*</div>*/}
                      {/*</div>*/}
                      {/*</nav>*/}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        ) : (
          ""
        )}
        {this.props.children.props &&
        this.props.children.props.children.props.location.pathname !==
          routeName.PARTNER_REGISTER ? (
          <section className="banner-container">
            <div className="row">
              <div className="col-md-12" />
            </div>
          </section>
        ) : (
          ""
        )}

        {this.props.children}
        <BackToTop />
        <Footer />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps)(
  withTranslation("translations")(PartnerMaster)
);
