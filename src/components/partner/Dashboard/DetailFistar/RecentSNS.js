import React, { Component, Fragment } from "react";
import ReactDOM from "react-dom";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect, Route } from "react-router-dom";
import { connect } from "react-redux";
import { getRecentSNSAction } from "./../../../../store/actions/channel";
import { Tab, Nav } from "react-bootstrap";
import { nFormatter } from "./../../../../common/helper";
import imageFime from "../../../../images/mk.png";
import imageFb from "../../../../images/face.png";
import imageYt from "../../../../images/toutube.png";
import imageInstar from "../../../../images/intar.png";

// import {AmCharts} from "@amcharts/amcharts3-react";

class RecenSNS extends Component {
  constructor(props) {
    super(props);
    this.state = {
      step: null,
      loading: false,
      recent: [],
      info: {},
      success: false,
      imageSNS: ""
    };
  }

  componentDidMount() {
    const { uid, channel } = this.props;
    if (!uid) {
      return;
    }
    this.setState(
      {
        loading: true
      },
      () => {
        this.props
          .getRecentSNS(uid, channel)
          .then(response => {
            if (!!response.data.recent) {
              response.data.recent.length = 4;
            }
            let imageSNS = "";
            switch (channel) {
              case "facebook": {
                imageSNS = imageFb;
                break;
              }
              case "youtube": {
                imageSNS = imageYt;
                break;
              }
              case "instagram": {
                imageSNS = imageInstar;
                break;
              }
              default:
                break;
            }
            this.setState({
              recent: response.data.recent,
              info: response.data.info,
              loading: false,
              success: true,
              imageSNS
            });
          })
          .catch(() => {
            this.setState({
              loading: false,
              success: false
            });
          });
      }
    );
  }

  render() {
    const { loading, success, info, recent, imageSNS } = this.state;

    if (!success || !recent || !Array.isArray(recent)) {
      return null;
    }

    if (loading) {
      return "loading";
    }
    return (
      <Fragment>
        <div className="solical-channel">
          <h4 className="title text-dark">{this.props.title}</h4>
          <div className="information-item">
            <div className="left">
              <div className="logo-solical">
                <a href="javascript:void(0)">
                  <img src={imageSNS} alt="" />
                </a>
              </div>
              <div className="text-solical">
                <p>{this.props.t("SEARCH_DETAIL.SDL_followers")}</p>
                <h4>{nFormatter(info.usn_follower)}</h4>
              </div>
            </div>
            <div className="right">
              <h4 className="title-right">Recent post's</h4>
              <div className="item-amount">
                <div className="item">
                  <p>{this.props.t("PARTNER_INFORMATION.PNI_like")}</p>
                  <h4>{nFormatter(info.recent_like)}</h4>
                </div>
                <div className="item">
                  <p>{this.props.t("PARTNER_INFORMATION.PNI_share")}</p>
                  <h4>{nFormatter(info.recent_share)}</h4>
                </div>
              </div>
            </div>
          </div>
          <div className="box-channel">
            <h4>{this.props.t("SEARCH_DETAIL.SDL_recent_post")}</h4>
            <div className="gr-card">
              <div className="row equal">
                {(recent || []).map((post, key) => {
                  if (post.post_thumb && post.post_description)
                    return (
                      <div className="col-md-3 d-flex" key={key}>
                        <a href={post.post_url} target="_blank">
                          <div className="card card-campaign">
                            <p className="text">{post.post_description}</p>
                            <div className="img-card">
                              <img
                                src={post.post_thumb}
                                className="card-img-top"
                                alt="..."
                              />
                            </div>
                            <div className="card-body body-campaign">
                              <div className="solical-item">
                                <div className="item">
                                  <i className="far fa-heart" />{" "}
                                  <span>
                                    {(
                                      post.post_statitics.like || 0
                                    ).toLocaleString()}
                                  </span>
                                </div>
                                <div className="item">
                                  <i className="far fa-comment-dots" />{" "}
                                  <span>
                                    {(
                                      post.post_statitics.comment || 0
                                    ).toLocaleString()}
                                  </span>
                                </div>
                                <div className="item">
                                  <i className="fas fa-share-alt" />{" "}
                                  <span>
                                    {(
                                      post.post_statitics.share || 0
                                    ).toLocaleString()}
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                      </div>
                    );
                })}
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};
const mapDispatchToProps = dispatch => {
  return {
    getRecentSNS: (uid, channel) => dispatch(getRecentSNSAction(uid, channel))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(RecenSNS));
