import React, {Component, Fragment} from "react";
import ReactDOM from "react-dom";
import {withTranslation, Trans} from "react-i18next";
import {Link, Redirect, Route} from "react-router-dom";
import {connect} from "react-redux";

// import calendar style
// You can customize style by copying asset folder.
import "@y0c/react-datepicker/assets/styles/calendar.scss";
import {format} from "date-fns";
// import PushMessageComponent from './../../../components/PushMessage'

import {getFistarAction} from "./../../../../store/actions/fistar";
import {getCodeAction} from "./../../../../store/actions/code";
import {nextStepAction} from "./../../../../store/actions/campaign";

import {Dropdown} from "react-bootstrap";
import "./detailFistar.css";
import {STATUS_STEP_PARTNER} from "./../../../../constants";
import PieChart from "./../../../charts/PieChart";
import {SNS_CHANNEL} from "./../../../../constants";
import {nFormatter, calcAge} from "./../../../../common/helper";
import RecentSNS from "./RecentSNS";
import CampaignHistory from "./CampaignHistory";

import imgaeIdol from "../../../../images/dash-02.png";
import imageClose from "../../../../images/close-popup.svg";
import imageFime from "../../../../images/mk.png";
import imageFb from "../../../../images/face.png";
import imageYt from "../../../../images/toutube.png";
import imageInstar from "../../../../images/intar.png";
import imageMk from "../../../../images/mk.png";
import imageThum from "../../../../images/thumnaildetail.png";

import {Modal, Tabs, Tab, Nav} from "react-bootstrap";
import {getImageLink} from "./../../../../common/helper";
import {
    IMAGE_SIZE,
    IMAGE_TYPE,
    PARTNER_FINISH_STEP,
    CAMPAIGN_STATUS
} from "./../../../../constants/index";
import Scrap from "./../../../../components/Scrap";
import * as routeName from "../../../../routes/routeName";


class detailFistar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fistar: "",
            campaign: null,
            status: null,
            url_channels: {},
            show: false,
            newFistarDashboard: false,
            step: null
        };
    }

    componentDidMount() {
        this.props
            .getFistar(this.props.uid)
            .then(response => {
                console.log(response)
                console.log(this.props)
                let url_fb = null
                let url_youtube = null
                let url_instagram = null
                let url_fime = null
                response.channels.length > 0 && response.channels.map((item) => {
                    switch (item.sns_id) {
                        case 2:
                            url_fb = item.sns_url
                            break;
                        case 3:
                            url_youtube = item.sns_url
                            break;
                        case 4:
                            url_instagram = item.sns_url
                            break;
                        default:
                            url_fime = item.sns_url
                    }
                })

                if (this.props.newFistarDashboard) {

                }

                this.setState({
                    fistar: response,
                    url_channels: {
                        url_fb: url_fb,
                        url_youtube: url_youtube,
                        url_instagram: url_instagram,
                        url_fime: url_fime
                    },
                    campaign: response.campaigns[0] || null,
                    status: this.getStatus(response.campaigns[0] || null)
                });
            })
            .catch(() => {
                this.props.onCloseModal();
            });
        if (!this.props.code.data.keyword) {
            this.props.getCode("keyword");
        }
    }

    handleClose = () => {
        this.props.onCloseModal();
        // this.setState({ show: !this.props.onClose() });
    };
    handleCloseSearchFistar = () => {
        if (this.props.newFistarDashboard) {
            console.log('chuyen link')
            this.setState({
                newFistarDashboard: true,
            });

        } else {
            console.log('else chuyen link')
            this.props.onCloseModal();
        }
    }

    getStatus = (campaign = null) => {
        if (!campaign) {
            return null;
        }
        let status = null;
        campaign.matchings.map(matching => {
            if (matching.uid === this.props.uid) {
                status = matching;
            }
        });
        return status;
    };

    handleChangeCampaign = e => {
        let {campaign} = this.state;
        this.state.fistar.campaigns.map(cam => {
            if (cam.cp_id == e.target.value) {
                campaign = cam;
            }
        });
        this.setState({
            campaign,
            status: this.getStatus(campaign)
        });
    };

    onClickNextStep = (m_id, stt_id) => {
        if (!this.state.loading) {
            this.setState(
                {
                    loading: true
                },
                () => {
                    this.props
                        .nextStep(m_id, stt_id)
                        .then(response => {
                            this.setState({
                                step: stt_id,
                                loading: false
                            });
                            this.props.reloadData();
                        })
                        .catch(er => {
                            this.setState({
                                loading: false
                            });
                        });
                }
            );
        }
    };

    renderButtonNextStep = (m_id, step) => {
        let nextSteps = STATUS_STEP_PARTNER[step];
        return (
            <Fragment>
                {nextSteps.button.map((nextStep, key) => (
                    <button
                        key={key}
                        type={"button"}
                        onClick={() => this.onClickNextStep(m_id, nextStep.id)}
                    >
                        {this.state.loading ? (
                            <div className="spinner-border" role="status">
                                {/*<span className="sr-only">Loading...</span>*/}
                            </div>
                        ) : (
                            <Fragment>{nextStep.text}</Fragment>
                        )}
                    </button>
                ))}
            </Fragment>
        );
    };

    renderTabSNSInfos = () => {
        const {fistar} = this.state;
        console.log(this.state)
        const {t} = this.props;
        if (!fistar) {
            return null;
        }
        let campaigns = (fistar.matchings || []).map(matching => ({
            ...matching.campaign,
            matching_channel: matching.matching_channel
        }));
        return (
            <Fragment>
                <div className="tab-sns w-100">
                    <div className="matching-fistar-track">
                        <Tab.Container id="tabs-fistar" defaultActiveKey={"snsReport"}>
                            <div className="tab">
                                <Nav variant="pills" className="tab-list">
                                    <Nav.Item className="tab-item">
                                        <Nav.Link className="tab-link" eventKey="snsReport">
                                            {t("SEARCH_DETAIL.SDL_sns_report")}
                                        </Nav.Link>
                                    </Nav.Item>
                                    {/*<Nav.Item className="tab-item">
                    <Nav.Link className="tab-link" eventKey="snsNewsFeed">
                      SNS News feed
                    </Nav.Link>
                  </Nav.Item>*/}
                                    <Nav.Item className="tab-item">
                                        <Nav.Link className="tab-link" eventKey="CampaignHistory">
                                            {t("SEARCH_DETAIL.SDL_campaign_history")}
                                        </Nav.Link>
                                    </Nav.Item>
                                </Nav>
                            </div>
                            <Tab.Content>
                                <Tab.Pane eventKey="snsReport">
                                    {this.renderSNSInfo()}
                                    <RecentSNS
                                        title={"Facebook Channel"}
                                        channel={"facebook"}
                                        uid={this.state.fistar ? this.state.fistar.uid : null}
                                    />
                                    <RecentSNS
                                        title={"Youtube Channel"}
                                        channel={"youtube"}
                                        uid={this.state.fistar ? this.state.fistar.uid : null}
                                    />
                                    <RecentSNS
                                        title={"Instagram Channel"}
                                        channel={"instagram"}
                                        uid={this.state.fistar ? this.state.fistar.uid : null}
                                    />
                                </Tab.Pane>
                                {/*<Tab.Pane eventKey="snsNewsFeed">
                  
                </Tab.Pane>*/}
                                <Tab.Pane eventKey="CampaignHistory">
                                    <CampaignHistory campaigns={campaigns} fistar={fistar}/>
                                </Tab.Pane>
                            </Tab.Content>
                        </Tab.Container>
                    </div>
                </div>
            </Fragment>
        );
    };

    renderCampaigns = () => {
        if (!this.state.campaign) {
            return null;
        }
        let {
            m_status: stt_id,
            label: {partner_status}
        } = this.state.status.matching_status;
        if (this.state.step) {
            partner_status = STATUS_STEP_PARTNER[this.state.step].display;
            stt_id = this.state.step;
        }
        console.log(this.state, this.props);
        return (
            <div className="dropdown-request w-100">
                <div className="left">
                    <select
                        className="form-control w-80 m-auto"
                        value={this.state.campaign ? this.state.campaign.cp_id : ""}
                        onChange={this.handleChangeCampaign}
                    >
                        {this.state.fistar.campaigns.map((campaign, key) => (
                            <option value={campaign.cp_id} key={key}>
                                {campaign.cp_name}
                            </option>
                        ))}
                    </select>
                </div>
                <div className="right">
                    <span className="icon-start"/>
                    {this.state.status ? (
                        <Fragment>
                            {partner_status.split("→").map((step, key) => {
                                if (partner_status.split("→").length === key + 1) {
                                    return (
                                        <Fragment key={key}>
                                            <p className="text-white font-weight-bold">{step}</p>
                                        </Fragment>
                                    );
                                }
                                return (
                                    <Fragment key={key}>
                                        <p>{step}</p>
                                        <span/>
                                    </Fragment>
                                );
                            })}
                        </Fragment>
                    ) : (
                        ""
                    )}
                    {!PARTNER_FINISH_STEP.includes(stt_id) && this.state.campaign.cp_status !== CAMPAIGN_STATUS.CLOSED && (
                        <i className="fas fa-long-arrow-alt-right"/>
                    )}
                    {this.state.campaign.cp_status !== CAMPAIGN_STATUS.CLOSED && this.renderButtonNextStep(this.state.status.m_id, stt_id)}
                </div>
            </div>
        );
    };

    renderSNSInfo = () => {
        const {fistar, url_channels} = this.state;
        if (!fistar) {
            return null;
        }

        let fime = {};
        let facebook = {};
        let youtube = {};
        let instagram = {};
        let pieData = [];
        let total = 0;
        fistar.channels.map(channel => {
            total += channel.usn_follower;
        });
        fistar.channels.map(channel => {
            switch (channel.sns_id) {
                case SNS_CHANNEL.FIME: {
                    pieData.push({
                        name: "Fime",
                        value: +(((channel.usn_follower || 0) * 100) / total).toFixed(1),
                        color: "#48BAFD"
                    });
                    fime = channel;
                    break;
                }
                case SNS_CHANNEL.FACEBOOK: {
                    pieData.push({
                        name: "Facebook",
                        value: +(((channel.usn_follower || 0) * 100) / total).toFixed(1),
                        color: "#7FC4FD"
                    });
                    facebook = channel;
                    break;
                }
                case SNS_CHANNEL.YOUTUBE: {
                    pieData.push({
                        name: "Youtube",
                        value: +(((channel.usn_follower || 0) * 100) / total).toFixed(1),
                        color: "#BCE0FD"
                    });
                    youtube = channel;
                    break;
                }
                case SNS_CHANNEL.INSTAGRAM: {
                    pieData.push({
                        name: "Instargram",
                        value: +(((channel.usn_follower || 0) * 100) / total).toFixed(1),
                        color: "#00ffff"
                    });
                    instagram = channel;
                    break;
                }
                default:
                    break;
            }
        });
        let flag = false;
        console.log(pieData);
        pieData.map(p => {
            if (+p.value > 0) {
                flag = true;
            }
        });
        console.log(flag);
        if (!flag) {
            pieData = [];
        }
        console.log(pieData);

        return (
            <Fragment>
                <div className="price-solical">
                    <h5 className="title">Campaign posting price</h5>
                    <div className="price">
                        <ul className="list-price">
                            <li>
                                <a href="javascript:void(0)">
                                    <img src={imageFime} alt=""/>
                                </a>
                                <p>
                                    {(+fime.cost || 0).toLocaleString()} <span>VND</span>
                                </p>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <img src={imageFb} alt=""/>
                                </a>
                                <p>
                                    {(+facebook.cost || 0).toLocaleString()} <span>VND</span>
                                </p>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <img src={imageYt} alt=""/>
                                </a>
                                <p>
                                    {(+youtube.cost || 0).toLocaleString()} <span>VND</span>
                                </p>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <img src={imageInstar} alt=""/>
                                </a>
                                <p>
                                    {(+instagram.cost || 0).toLocaleString()} <span>VND</span>
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="channel-engagement">
                    <div className="left w-40">
                        <div className="title">
                            <h4>{this.props.t("SEARCH_DETAIL.SDL_sns_channel")}</h4>
                        </div>
                        <div className="chart-channel">
                            <div className="list-chart">
                                {!!fime && (
                                    <div className="item">
                                        <div className="img-icon w-50">
                                            <a href={url_channels.url_fime}
                                               target={url_channels.url_fime ? "_blank" : "_self"}>
                                                <img src={imageFime} alt="fime"/>
                                            </a>
                                        </div>
                                        <div className="flow-sub w-100">
                                            <p>{this.props.t("SEARCH_DETAIL.SDL_followers")}</p>
                                            <h5>{nFormatter(+fime.usn_follower || 0, 0)}</h5>
                                        </div>
                                    </div>
                                )}
                                {!!facebook && (
                                    <div className="item">
                                        <div className="img-icon w-50">
                                            <a href={url_channels.url_fb}
                                               target={url_channels.url_fb ? "_blank" : "_self"}>
                                                <img src={imageFb} alt="facebook"/>
                                            </a>
                                        </div>
                                        <div className="flow-sub w-100">
                                            <p>{this.props.t("SEARCH_DETAIL.SDL_followers")}</p>
                                            <h5>{nFormatter(+facebook.usn_follower || 0, 0)}</h5>
                                        </div>
                                    </div>
                                )}
                                {!!youtube && (
                                    <div className="item">
                                        <div className="img-icon w-50">
                                            <a href={url_channels.url_youtube}
                                               target={url_channels.url_youtube ? "_blank" : "_self"}>
                                                <img src={imageYt} alt="youtube"/>
                                            </a>
                                        </div>
                                        <div className="flow-sub w-100">
                                            <p>{this.props.t("SEARCH_DETAIL.SDL_followers")}</p>
                                            <h5>{nFormatter(+youtube.usn_follower || 0, 0)}</h5>
                                        </div>
                                    </div>
                                )}
                                {!!instagram && (
                                    <div className="item">
                                        <div className="img-icon w-50">
                                            <a href={url_channels.url_instagram}
                                               target={url_channels.url_instagram ? "_blank" : "_self"}>
                                                <img src={imageInstar} alt="instagram"/>
                                            </a>
                                        </div>
                                        <div className="flow-sub w-100">
                                            <p>{this.props.t("SEARCH_DETAIL.SDL_followers")}</p>
                                            <h5>{nFormatter(+instagram.usn_follower || 0, 0)}</h5>
                                        </div>
                                    </div>
                                )}
                            </div>
                        </div>
                    </div>
                    <div className="right w-60 text-center">
                        {pieData.length > 0 ? (
                            <PieChart data={pieData}/>
                        ) : (
                            <div className="chart-empty">
                                {this.props.t("SEARCH_DETAIL.SDL_data_chart")}
                            </div>
                        )}
                    </div>
                </div>
            </Fragment>
        );
    };

    render() {
        const {t, code} = this.props;
        const {fistar} = this.state;
        const {url_channels} = this.state;
        if (!fistar) {
            return null;
        }

        let fistarKeywords = fistar.keywords.map(e => e.code.cd_id);
        if(this.state.newFistarDashboard) {
            return <Redirect to={routeName.PARTNER_SEARCH_FISTAR}/>;
        }

        return (
            <Fragment>
                <Modal
                    size="lg"
                    show={this.props.show}
                    onHide={this.handleClose}
                    aria-labelledby="example-modal-sizes-title-lg"
                    dialogClassName="fistar-search-template"
                >
                    <div className="content-fistarsearch">
                        <div className="container-detail">
                            <button
                                type="button"
                                className="close"
                                data-dismiss="modal"
                                aria-label="Close"
                                onClick={this.handleClose}
                            >
                                <img src={imageClose} alt=""/>
                            </button>
                            <div className="top-fistar-detail">
                                <div className="container">
                                    <h4>{this.props.t("SEARCH_DETAIL.SDL_fistar_detail")}</h4>
                                    <button onClick={this.handleCloseSearchFistar}>
                                        <i className="fas fa-long-arrow-alt-left"/>
                                        {this.props.t("SEARCH_DETAIL.SDL_search")}
                                    </button>
                                </div>
                                <div className="poup-scroll">
                                    <div className="container">
                                        <div className="content-detail w-100">
                                            <div className="thumnail-detial left">
                                                <div className="img-thumnail h-100">
                                                    <img
                                                        src={getImageLink(
                                                            fistar.picture,
                                                            IMAGE_TYPE.FISTARS,
                                                            IMAGE_SIZE.ORIGINAL
                                                        )}
                                                        alt=""
                                                    />
                                                    <Scrap
                                                        scraps_count={fistar.scraps_count}
                                                        is_scrap={fistar.is_scrap}
                                                        id={fistar.uid}
                                                        className="btn over-flow"
                                                    />
                                                </div>
                                            </div>
                                            <div className="right">
                                                <div className="top">
                                                    <div className="name-detail">
                                                        <h4>
                                                            <i className="fas fa-mars"/>
                                                            {fistar.fullname}
                                                        </h4>
                                                        <p>
                                                            {new Date(fistar.dob).getFullYear()}(
                                                            {calcAge(fistar.dob)}) {fistar.location.cd_label}
                                                        </p>
                                                    </div>
                                                    <div className="icon-solical">
                                                        <a href={url_channels.url_fb}
                                                           target={url_channels.url_fb ? "_blank" : "_self"}>
                                                            <img src={imageFb} alt=""/>
                                                        </a>
                                                        <a href={url_channels.url_youtube}
                                                           target={url_channels.url_youtube ? "_blank" : "_self"}>
                                                            <img src={imageYt} alt=""/>
                                                        </a>
                                                        <a href={url_channels.url_instagram}
                                                           target={url_channels.url_instagram ? "_blank" : "_self"}>
                                                            <img src={imageInstar} alt=""/>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div className="text">
                                                    <p>{fistar.self_intro}</p>
                                                </div>
                                                <div className="tag">
                                                    {fistar.keywords.map((keyword, key) => (
                                                        <Fragment key={key}>
                                                            <a
                                                                href="javascript:void(0)"
                                                                className={"selected"}
                                                            >
                                                                {keyword.code.cd_label}
                                                            </a>
                                                        </Fragment>
                                                    ))}
                                                </div>
                                            </div>
                                        </div>
                                        {this.renderCampaigns()}
                                        {this.renderTabSNSInfos()}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Modal>
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        code: state.code
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getFistar: (uid, search = 1) => dispatch(getFistarAction(uid, search)),
        getCode: type => dispatch(getCodeAction(type)),
        nextStep: (m_id, stt_id) => dispatch(nextStepAction(m_id, stt_id))
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withTranslation("translations")(detailFistar));
