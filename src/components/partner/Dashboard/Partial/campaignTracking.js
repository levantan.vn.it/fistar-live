import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, NavLink, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import * as routeName from "./../../../../routes/routeName";
import count from "../../../../store/reducer/count";
import { getCampaignAction } from "./../../../../store/actions/campaign";
import InfiniteScroll from "react-infinite-scroll-component";
import { getImageLink, getId } from './../../../../common/helper'
import { IMAGE_SIZE, IMAGE_TYPE } from './../../../../constants/index'
import CampaignMainImage from './../../../../components/CampaignMainImage'
import { CAMPAIGN_STATUS, TYPE, SNS_CHANNEL } from "./../../../../constants";
import imageFB from "./../../../../images/face.png";
import imageIG from "./../../../../images/intar.png";
import imageFime from "./../../../../images/mk.png";
import imageYT from "./../../../../images/toutube.png";
const VIDEO = {
  WIDTH: 450,
  HEIGHT: 200,
}

const MATCHING = {
  REQUEST: [1, 2, 3, 4, 5, 6, 7, 8],
  APPLY: [9, 10, 11, 12, 13, 14, 15, 16]
};

const ListFimes = ({ fimes }) => (
  <Fragment>
    <div className="user-fime">
      {fimes[0] && (
        <div className="thumnail-user">
          <a href="javascript:viod(0)">
            <img
              src={fimes[0].profile ? process.env.REACT_APP_FIME_URL + fimes[0].profile.PIC : ''}
              alt={fimes[0].profile ? fimes[0].profile.REG_NAME : ''}
            />
          </a>
        </div>
      )}
      {fimes[1] && (
        <div className="thumnail-user">
          <a href="javascript:viod(0)">
            <img
              src={fimes[1].profile ? process.env.REACT_APP_FIME_URL + fimes[1].profile.PIC : ''}
              alt={fimes[1].profile ? fimes[1].profile.REG_NAME : ''}
            />
          </a>
        </div>
      )}
      {fimes.length > 2 && (
        <div className="thumnail-user">
          <a href="javascript:viod(0)">
            <div className="all-user">
              <p>+{fimes.length - 2}</p>
            </div>
          </a>
        </div>
      )}
    </div>
  </Fragment>
);
const ListFistars = ({ fistars }) => (
  <Fragment>
    <div className="user-fime">
      {fistars[0] && (
        <div className="thumnail-user">
          <a href="javascript:viod(0)">
            <img
              src={getImageLink(
                fistars[0].picture,
                IMAGE_TYPE.FISTARS,
                IMAGE_SIZE.THUMBNAIL
              )}
              alt={fistars[0].fullname}
            />
          </a>
        </div>
      )}
      {fistars[1] && (
        <div className="thumnail-user">
          <a href="javascript:viod(0)">
            <img
              src={getImageLink(
                fistars[1].picture,
                IMAGE_TYPE.FISTARS,
                IMAGE_SIZE.THUMBNAIL
              )}
              alt={fistars[1].fullname}
            />
          </a>
        </div>
      )}
      {fistars.length > 2 && (
        <div className="thumnail-user">
          <a href="javascript:viod(0)">
            <div className="all-user">
              <p>+{fistars.length - 2}</p>
            </div>
          </a>
        </div>
      )}
    </div>
  </Fragment>
);

class PartnerCampaignTracking extends Component {
  constructor(props) {
    super(props);
    this.state = {
      campaigns: [],
      matching: 0,
      ready: 0,
      ongoing: 0,
      closed: 0,
      total: 0,
      isLoadingCampaign: false,
      last_page: 1,
      page: 1,
      hasMore: true,
      isRedirect: false,
    };
  }

  componentDidMount() {
    this.setState(
      {
        isLoadingCampaign: true
      },
      () => {
        this.props.getCampaign(this.state.page).then(response => {
          this.setState({
            campaigns: response.data,
            matching: response.matching,
            ready: response.ready,
            ongoing: response.ongoing,
            closed: response.closed,
            total: response.total,
            isLoadingCampaign: false,
            last_page: response.last_page
          });
        });
      }
    );
  }

  fetchMoreData = () => {
    if (this.state.page >= this.state.last_page) {
      this.setState({ hasMore: false });
      return;
    }
    this.props.getCampaign(this.state.page + 1).then(response => {
      this.setState({
        campaigns: [...this.state.campaigns, ...response.data],
        page: response.current_page
      });
    });
  };

  redirectToCampaignTracking = () => {
    this.setState({
      isRedirect: true
    });
  };

  renderCountMatching = (recruit, count_status, campaign, total_cost_selected) => {
  const { t } = this.props;
  let status = "";
  let request = [];
  let apply = [];
  let fimes = [];
  let moneyFime = 0;
  let moneyFacebook = 0;
  let moneyInstagram = 0;
  let moneyYoutube = 0;
  let moneyTotal = 0;

  let fimeSelected = {};
  let facebookSelected = {};
  let youtubeSelected = {};
  let instagramSelected = {};
  switch (campaign.cp_status) {
    case CAMPAIGN_STATUS.MATCHING: {
      status = "Matching";
      campaign.matchings.map(status => {
        // fistar
        fimes.push(status.influencer);
        if (MATCHING.REQUEST.includes(status.matching_status.label.stt_id)) {
          request.push(status.influencer);
        }
        if (MATCHING.APPLY.includes(status.matching_status.label.stt_id)) {
          apply.push(status.influencer);
        }
        status.matching_channel.map(channelMatching => {
          switch (channelMatching.sns_id) {
            case SNS_CHANNEL.FIME: {
              fimeSelected.m_ch_selected = channelMatching.m_ch_selected;
              fimeSelected.sns_id = channelMatching.sns_id;
              break;
            }
            case SNS_CHANNEL.FACEBOOK: {
              facebookSelected.m_ch_selected = channelMatching.m_ch_selected;
              facebookSelected.sns_id = channelMatching.sns_id;
              break;
            }
            case SNS_CHANNEL.YOUTUBE: {
              youtubeSelected.m_ch_selected = channelMatching.m_ch_selected;
              youtubeSelected.sns_id = channelMatching.sns_id;
              break;
            }
            case SNS_CHANNEL.INSTAGRAM: {
              instagramSelected.m_ch_selected = channelMatching.m_ch_selected;
              instagramSelected.sns_id = channelMatching.sns_id;
              break;
            }
          }
        });

        // money sns channel
        status.influencer.channels.map(channel => {
          switch (channel.sns_id) {
            case SNS_CHANNEL.FIME: {
              if (
                fimeSelected.sns_id == SNS_CHANNEL.FIME &&
                fimeSelected.m_ch_selected == 1
              ) {
                moneyFime += +channel.cost;
              } else {
                moneyFime += 0;
              }

              break;
            }
            case SNS_CHANNEL.FACEBOOK: {
              if (
                facebookSelected.sns_id == SNS_CHANNEL.FACEBOOK &&
                facebookSelected.m_ch_selected == 1
              ) {
                moneyFacebook += +channel.cost;
              } else {
                moneyFacebook += 0;
              }
              break;
            }
            case SNS_CHANNEL.YOUTUBE: {
              if (
                youtubeSelected.sns_id == SNS_CHANNEL.YOUTUBE &&
                youtubeSelected.m_ch_selected == 1
              ) {
                moneyYoutube += +channel.cost;
              } else {
                moneyYoutube += 0;
              }
              break;
            }
            case SNS_CHANNEL.INSTAGRAM: {
              if (
                instagramSelected.sns_id == SNS_CHANNEL.INSTAGRAM &&
                instagramSelected.m_ch_selected == 1
              ) {
                moneyInstagram += +channel.cost;
              } else {
                moneyInstagram += 0;
              }
              break;
            }
          }
        });
      });
      break;
    }
    case CAMPAIGN_STATUS.READY: {
      status = `${t("DASHBOARD_FISTAR.DFR_status_ready")}`;

      campaign.matchings.map(status => {
        // fistar
        fimes.push(status.influencer);
        if (MATCHING.REQUEST.includes(status.matching_status.label.stt_id)) {
          request.push(status.influencer);
        }
        if (MATCHING.APPLY.includes(status.matching_status.label.stt_id)) {
          apply.push(status.influencer);
        }

        status.matching_channel.map(channelMatching => {
          switch (channelMatching.sns_id) {
            case SNS_CHANNEL.FIME: {
              fimeSelected.m_ch_selected = channelMatching.m_ch_selected;
              fimeSelected.sns_id = channelMatching.sns_id;
              break;
            }
            case SNS_CHANNEL.FACEBOOK: {
              facebookSelected.m_ch_selected = channelMatching.m_ch_selected;
              facebookSelected.sns_id = channelMatching.sns_id;
              break;
            }
            case SNS_CHANNEL.YOUTUBE: {
              youtubeSelected.m_ch_selected = channelMatching.m_ch_selected;
              youtubeSelected.sns_id = channelMatching.sns_id;
              break;
            }
            case SNS_CHANNEL.INSTAGRAM: {
              instagramSelected.m_ch_selected = channelMatching.m_ch_selected;
              instagramSelected.sns_id = channelMatching.sns_id;
              break;
            }
          }
        });

        // money sns channel
        status.influencer.channels.map(channel => {
          switch (channel.sns_id) {
            case SNS_CHANNEL.FIME: {
              if (
                fimeSelected.sns_id == SNS_CHANNEL.FIME &&
                fimeSelected.m_ch_selected == 1
              ) {
                moneyFime += +channel.cost;
              } else {
                moneyFime += 0;
              }

              break;
            }
            case SNS_CHANNEL.FACEBOOK: {
              if (
                facebookSelected.sns_id == SNS_CHANNEL.FACEBOOK &&
                facebookSelected.m_ch_selected == 1
              ) {
                moneyFacebook += +channel.cost;
              } else {
                moneyFacebook += 0;
              }
              break;
            }
            case SNS_CHANNEL.YOUTUBE: {
              if (
                youtubeSelected.sns_id == SNS_CHANNEL.YOUTUBE &&
                youtubeSelected.m_ch_selected == 1
              ) {
                moneyYoutube += +channel.cost;
              } else {
                moneyYoutube += 0;
              }
              break;
            }
            case SNS_CHANNEL.INSTAGRAM: {
              if (
                instagramSelected.sns_id == SNS_CHANNEL.INSTAGRAM &&
                instagramSelected.m_ch_selected == 1
              ) {
                moneyInstagram += +channel.cost;
              } else {
                moneyInstagram += 0;
              }
              break;
            }
          }
        });
      });
      // campaign.v_review_status.map((status) => {
      //   fimes.push(status.influencer)
      //   if (MATCHING.REQUEST.includes(status.label_status.stt_id)) {
      //     request.push(status.influencer)
      //   }
      //   if (MATCHING.APPLY.includes(status.label_status.stt_id)) {
      //     apply.push(status.influencer)
      //   }
      // })
      break;
    }
    case CAMPAIGN_STATUS.ONGOING: {
      status = `${t("DASHBOARD_FISTAR.DFR_status_ongoing")}`;

      campaign.matchings.map(status => {
        // fistar
        fimes.push(status.influencer);
        if (MATCHING.REQUEST.includes(status.matching_status.label.stt_id)) {
          request.push(status.influencer);
        }
        if (MATCHING.APPLY.includes(status.matching_status.label.stt_id)) {
          apply.push(status.influencer);
        }
        status.matching_channel.map(channelMatching => {
          switch (channelMatching.sns_id) {
            case SNS_CHANNEL.FIME: {
              fimeSelected.m_ch_selected = channelMatching.m_ch_selected;
              fimeSelected.sns_id = channelMatching.sns_id;
              break;
            }
            case SNS_CHANNEL.FACEBOOK: {
              facebookSelected.m_ch_selected = channelMatching.m_ch_selected;
              facebookSelected.sns_id = channelMatching.sns_id;
              break;
            }
            case SNS_CHANNEL.YOUTUBE: {
              youtubeSelected.m_ch_selected = channelMatching.m_ch_selected;
              youtubeSelected.sns_id = channelMatching.sns_id;
              break;
            }
            case SNS_CHANNEL.INSTAGRAM: {
              instagramSelected.m_ch_selected = channelMatching.m_ch_selected;
              instagramSelected.sns_id = channelMatching.sns_id;
              break;
            }
          }
        });

        // money sns channel
        status.influencer.channels.map(channel => {
          switch (channel.sns_id) {
            case SNS_CHANNEL.FIME: {
              if (
                fimeSelected.sns_id == SNS_CHANNEL.FIME &&
                fimeSelected.m_ch_selected == 1
              ) {
                moneyFime += +channel.cost;
              } else {
                moneyFime += 0;
              }

              break;
            }
            case SNS_CHANNEL.FACEBOOK: {
              if (
                facebookSelected.sns_id == SNS_CHANNEL.FACEBOOK &&
                facebookSelected.m_ch_selected == 1
              ) {
                moneyFacebook += +channel.cost;
              } else {
                moneyFacebook += 0;
              }
              break;
            }
            case SNS_CHANNEL.YOUTUBE: {
              if (
                youtubeSelected.sns_id == SNS_CHANNEL.YOUTUBE &&
                youtubeSelected.m_ch_selected == 1
              ) {
                moneyYoutube += +channel.cost;
              } else {
                moneyYoutube += 0;
              }
              break;
            }
            case SNS_CHANNEL.INSTAGRAM: {
              if (
                instagramSelected.sns_id == SNS_CHANNEL.INSTAGRAM &&
                instagramSelected.m_ch_selected == 1
              ) {
                moneyInstagram += +channel.cost;
              } else {
                moneyInstagram += 0;
              }
              break;
            }
          }
        });
      });

      break;
    }
    case CAMPAIGN_STATUS.CLOSED: {
      status = `${t("DASHBOARD_FISTAR.DFR_status_close")}`;

      campaign.matchings.map(status => {
        // fistar
        fimes.push(status.influencer);
        if (MATCHING.REQUEST.includes(status.matching_status.label.stt_id)) {
          request.push(status.influencer);
        }
        if (MATCHING.APPLY.includes(status.matching_status.label.stt_id)) {
          apply.push(status.influencer);
        }
        status.matching_channel.map(channelMatching => {
          switch (channelMatching.sns_id) {
            case SNS_CHANNEL.FIME: {
              fimeSelected.m_ch_selected = channelMatching.m_ch_selected;
              fimeSelected.sns_id = channelMatching.sns_id;
              break;
            }
            case SNS_CHANNEL.FACEBOOK: {
              facebookSelected.m_ch_selected = channelMatching.m_ch_selected;
              facebookSelected.sns_id = channelMatching.sns_id;
              break;
            }
            case SNS_CHANNEL.YOUTUBE: {
              youtubeSelected.m_ch_selected = channelMatching.m_ch_selected;
              youtubeSelected.sns_id = channelMatching.sns_id;
              break;
            }
            case SNS_CHANNEL.INSTAGRAM: {
              instagramSelected.m_ch_selected = channelMatching.m_ch_selected;
              instagramSelected.sns_id = channelMatching.sns_id;
              break;
            }
          }
        });

        // money sns channel
        status.influencer.channels.map(channel => {
          switch (channel.sns_id) {
            case SNS_CHANNEL.FIME: {
              if (
                fimeSelected.sns_id == SNS_CHANNEL.FIME &&
                fimeSelected.m_ch_selected == 1
              ) {
                moneyFime += +channel.cost;
              } else {
                moneyFime += 0;
              }

              break;
            }
            case SNS_CHANNEL.FACEBOOK: {
              if (
                facebookSelected.sns_id == SNS_CHANNEL.FACEBOOK &&
                facebookSelected.m_ch_selected == 1
              ) {
                moneyFacebook += +channel.cost;
              } else {
                moneyFacebook += 0;
              }
              break;
            }
            case SNS_CHANNEL.YOUTUBE: {
              if (
                youtubeSelected.sns_id == SNS_CHANNEL.YOUTUBE &&
                youtubeSelected.m_ch_selected == 1
              ) {
                moneyYoutube += +channel.cost;
              } else {
                moneyYoutube += 0;
              }
              break;
            }
            case SNS_CHANNEL.INSTAGRAM: {
              if (
                instagramSelected.sns_id == SNS_CHANNEL.INSTAGRAM &&
                instagramSelected.m_ch_selected == 1
              ) {
                moneyInstagram += +channel.cost;
              } else {
                moneyInstagram += 0;
              }
              break;
            }
          }
        });
      });
      break;
    }
    default: {
      break;
    }
  }
  moneyTotal = moneyFime + moneyFacebook + moneyInstagram + moneyYoutube;
  // console.log(fimes.fime);
    return(
    <div>
      <div className="situation-matching">
        <div className="matching fime">
          <div className="title-user">
            <h4>
              Club Piaf <span>{campaign.cp_total_free}</span>
            </h4>
          </div>
          {/* <ListFimes fimes={campaign.fimelike} /> */}
        </div>
        <div className="matching fistar">
          <div className="title-user">
            <h4>
              fistar{" "}
              <span>
                {recruit}
              </span>
            </h4>
          </div>
          <div className="group-wf-item">
            <div
              className={
                [CAMPAIGN_STATUS.MATCHING].includes(campaign.cp_status)
                  ? "wf-item"
                  : "wf-item w-50"
              }
            >
              <div className="status">
                <h4>{count_status && count_status.requets_without_matched ? count_status.requets_without_matched : 0} Request</h4>
              </div>
              <ListFistars fistars={request} />
            </div>
            <div
              className={
                [CAMPAIGN_STATUS.MATCHING].includes(campaign.cp_status)
                  ? "wf-item-second"
                  : "wf-item-second w-50"
              }
            >
              <div className="status">
                <h4>
                  {count_status && count_status.apply_without_matched ? count_status.apply_without_matched : 0} Apply{" "}
                </h4>
              </div>
              <ListFistars fistars={apply} />
            </div>
            {[CAMPAIGN_STATUS.MATCHING].includes(campaign.cp_status) && (
                <div className="wf-item-last">
                  <div className="status">
                    <h4>0 {t("CAMPAI_TRACKING.CTG_recommended")}</h4>
                  </div>
                  <ListFistars fistars={[]} />
                </div>
              )}
          </div>
        </div>

        {/* <div className="footer-card">
        <div className="list-item">
          <div className="item">
            <p>
              <span>{recruit}</span>
            </p>
            <span>
              <a href="#">Recruiting fiStar</a>
            </span>
          </div>
          <div className="item">
            <p>
              <span>{count_status && count_status.matched ? count_status.matched : 0}</span>
            </p>
            <span>
              <a href="#">Matched</a>
            </span>
          </div>
          <div className="item">
            <p>
              <span>{count_status && count_status.requets_without_matched ? count_status.requets_without_matched : 0}</span>
            </p>
            <span>
              <a href="#">Request</a>
            </span>
          </div>
          <div className="item">
            <p>
              <span>{count_status && count_status.apply_without_matched ? count_status.apply_without_matched : 0}</span>
            </p>
            <span>
              <a href="#">Apply</a>
            </span>
          </div>
        </div>
      </div> */}
      </div>
      <div className="price-matching">
        <div className="item-price">
          <div className="icon-solical">
            <a href="javascript:viod(0)">
              <img src={imageFime} alt="Club Piaf" />
            </a>
          </div>
          <p>
          {moneyFime.toLocaleString()} <span>VND</span>
          </p>
        </div>
        <div className="item-price">
          <div className="icon-solical">
            <a href="javascript:viod(0)">
              <img src={imageFB} alt="Facebook" />
            </a>
          </div>
          <p>
          {moneyFacebook.toLocaleString()} <span>VND</span>
          </p>
        </div>
        <div className="item-price">
          <div className="icon-solical">
            <a href="javascript:viod(0)">
              <img src={imageIG} alt="Instagram" />
            </a>
          </div>
          <p>
          {moneyInstagram.toLocaleString()} <span>VND</span>
          </p>
        </div>
        <div className="item-price">
          <div className="icon-solical">
            <a href="javascript:viod(0)">
              <img src={imageYT} alt="Youtube" />
            </a>
          </div>
          <p>
          {moneyYoutube.toLocaleString()} <span>VND</span>
          </p>
        </div>
      </div>
      <div className="price-total">
          <p>
            Expected Cost{" "}
            <span>
              {" "}
              {moneyTotal.toLocaleString()} <b>VND</b>
            </span>
          </p>
        </div>
    </div>
  )};

  renderCampaign = () => {
    const { campaigns } = this.state;
    return campaigns.map((campaign, key) => {
      let status = "";
      switch (campaign.cp_status) {
        case 59: {
          status = `${this.props.t("DASHBOARD_FISTAR.DFR_status_matching")}`;
          break;
        }
        case 60: {
          status = `${this.props.t("DASHBOARD_FISTAR.DFR_status_ready")}`;
          break;
        }
        case 61: {
          status = `${this.props.t("DASHBOARD_FISTAR.DFR_status_ongoing")}`;
          break;
        }
        case 62: {
          status = `${this.props.t("DASHBOARD_FISTAR.DFR_status_close")}`;
          break;
        }
        default: {
          break;
        }
      }
      const { t } = this.props;
      return (
        <Fragment key={key}>
          <div className="item wf-item-matching">
            <div className="img-matching">
              <Link to={"/partner/campaign/" + campaign.cp_slug}>
                <CampaignMainImage campaign={campaign} className="w-100" />
              </Link>
              <button className="overlay-box disnabled-scrap">
              <i className="fas fa-heart m-0" />{" "}
              <span>{campaign.scraps_count}</span>
        </button>
            </div>
            <div className="status-campaign">
              <div className="title">
                <div className="left">
                  <span className="overlay-matching">{status}</span>
                  <Link to={"/partner/campaign/" + campaign.cp_slug}>
                    <h4>{campaign.cp_name}</h4>
                  </Link>
                </div>
                <p>
                  <Link to={"/partner/campaign/" + campaign.cp_slug}>
                    {t("DASHBOARD_FISTAR.DFR_open")}
                    <span>{campaign.cp_period_start.split(" ")[0]}{" "}</span>
                  </Link>
                </p>
              </div>
            </div>

            {/*{campaign.cp_status === 59 && (*/}
            {/*<Fragment>*/}
            {/*{this.renderCountMatching(*/}
            {/*campaign.cp_total_influencer,*/}
            {/*campaign.count_status*/}
            {/*)}*/}
            {/*</Fragment>*/}
            {/*)}*/}

            <Fragment>
              {this.renderCountMatching(
                campaign.cp_total_influencer,
                campaign.statitics,
                campaign,
              )}
            </Fragment>
          </div>
        </Fragment>
      );
    });
  };

  render() {
    const { t, height, count, auth } = this.props;
    const {
      campaigns,
      isLoadingCampaign,
      matching,
      ready,
      ongoing,
      closed,
      total,
      isRedirect
    } = this.state;
    if (isRedirect) {
      return <Redirect to={routeName.PARTNER_CAMPAIGN_TRACKING} />;
    }

    const loader = (
      <div className="campaign-track">
        <h1>{t("LOADING.LOADING")}</h1>
      </div>
    );

    // if (isLoadingCampaign) {
    //   return <div className="campaign-track">
    //     <h1>loading...</h1>
    //   </div>
    // }

    return (
      <Fragment>
        <div className="header-track">
          <div className="top">
            <h4>{t("DASHBOARD_FISTAR.DBF_partner")} {auth.name}'s  {t("DASHBOARD_FISTAR.DFR_campaign")}</h4>
            <button type="button" onClick={this.redirectToCampaignTracking}>
              {t("DASHBOARD_FISTAR.DBF_button_more")}
            </button>
          </div>
          <div className="amount-notification">
            <h2>{total}</h2>
          </div>
          <div className="list-amount">
            <div className="item">
              <p>
                <span>{count ? count.matching : ""}</span>
              </p>
              <span>{t("DASHBOARD_FISTAR.DBF_status_matching")}</span>
            </div>
            <div className="item">
              <p>
                <span>{count ? count.ready : ""}</span>
              </p>
              <span>{t("DASHBOARD_FISTAR.DBF_status_ready")}</span>
            </div>
            <div className="item">
              <p>
                <span>{count ? count.ongoing : ""}</span>
              </p>
              <span>{t("DASHBOARD_FISTAR.DBF_status_on_going")}</span>
            </div>
            <div className="item closed">
              <p>
                <span>{count ? count.closed : ""}</span>
              </p>
              <span>{t("DASHBOARD_FISTAR.DBF_close")}</span>
            </div>
          </div>
        </div>
        <div className="create-campai">
          <i className="fas fa-pen" />
          <span>
            <NavLink to={routeName.PARTNER_CREATE_CAMPAIN}>
              {t("DASHBOARD_FISTAR.DBF_button_create")}
            </NavLink>
          </span>
        </div>
        <div className="">
          {!isLoadingCampaign && this.state.campaigns.length === 0 ? (
            <Fragment>
              <div className={"text-center mt-5"}>
                {t("CAMPAIGN.CAMPAIGN_no_campaign")}
              </div>
            </Fragment>
          ) : (
              <InfiniteScroll
                dataLength={this.state.campaigns.length}
                next={this.fetchMoreData}
                loader={<h4>{t("LOADING.LOADING")}</h4>}
                hasMore={this.state.hasMore}
                height={height}
                className="card-matching"
              >
                {this.renderCampaign()}
              </InfiniteScroll>
            )}
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};
const mapDispatchToProps = dispatch => {
  return {
    getCampaign: (page = 1) => dispatch(getCampaignAction(page, "", 1))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withTranslation("translations")(PartnerCampaignTracking));
