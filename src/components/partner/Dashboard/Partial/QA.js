import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect, NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { getQAAction } from "./../../../../store/actions/campaign";
import * as routeName from "./../../../../routes/routeName";

class PartnerDashboardQA extends Component {
  constructor(props) {
    super(props);
    this.state = {
      type: 0,
      qa_state: 1,
      page: 1,
      isRedirect: false,
      qa: null,
      matching: 0,
      scrap: 0,
      request: 0,
      apply: 0,
      recommended: 0,
      total: 0,
      isLoadingFistar: false
    };
  }

  componentDidMount() {
    this.setState(
      {
        isLoadingFistar: true
      },
      () => {
        this.props.getQA(this.state.page, this.state.type).then(response => {
          console.log(response);
          this.setState({
            qa: response.data[0],
            matching: response.matching,
            scrap: response.scrap,
            request: response.request,
            apply: response.apply,
            recommended: response.recommended,
            total: response.total,
            isLoadingFistar: false
          });
        });
      }
    );
  }
  redirectDashboard = () => {
    this.setState({
      isRedirect: true
    });
  };

  render() {
    const { t } = this.props;
    const { qa, isRedirect, isLoadingFistar } = this.state;
    if (isRedirect) {
      return <Redirect to={routeName.PARTNER_QA} />;
    }
    if (isLoadingFistar) {
      return (
        <div className="campaign-track">
          <h1>{t("LOADING.LOADING")}</h1>
        </div>
      );
    }

    return (
      <Fragment>
        <div className="q-a-header">
          <div className="top">
            <h4>{t("QA_CONTACT_US.QCU_qa")}</h4>
            <button onClick={this.redirectDashboard}>
              {t("QA_CONTACT_US.QCU_button_contact")}
            </button>
          </div>
          {qa && (
            <div className="message-q-a">
              <div className="item item-q">
                <div className="thumnail-user">
                  <span className="user-name">Q</span>
                </div>
                <div className="message">
                  <span>{qa.qa_title}</span>
                </div>
              </div>
              <div className="item item-a">
                <div className="message-a">
                  <Link to={`/partner/qa/detail/${qa.qa_id}`}>
                    {qa.qa_state && qa.qa_state == 1
                      ? `${t("QA_DETAIL.QDL_button_completed")}`
                      : `${t("QA_DETAIL.QDL_button_preparing")}`}
                  </Link>
                </div>
                <div className="thumnail-user">
                  <span className="user-name">A</span>
                </div>
              </div>
            </div>
          )}
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};
const mapDispatchToProps = dispatch => {
  return {
    getQA: (page = "", type = "", state = "") =>
      dispatch(getQAAction(page, type, state))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(PartnerDashboardQA));
