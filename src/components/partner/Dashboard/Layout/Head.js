import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import {
  listProfileActionPartner,
  logoutAction
} from "../../../../store/actions/auth";
import * as routeName from "../../../../routes/routeName";
import "./head.scss";
import logoFistar from "./../../../../images/logo-03.png";

import { Dropdown, Navbar, Nav, NavDropdown } from "react-bootstrap";
import { getImageLink } from "./../../../../common/helper";
import { IMAGE_SIZE, IMAGE_TYPE } from "./../../../../constants/index";
import imageExample from "./../../../../images/example.jpg";

class PartnerDashboardHead extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      isToggleOn: true
    };
  }

  componentDidMount() {
    this.props.inforProfile().then(response => {
      console.log(response, "data profile");
      this.setState({
        data: response
      });
    });
  }

  changeLanguage = lng => {
    this.props.i18n.changeLanguage(lng);
  };

  onToggle = e => {
    e.preventDefault();
    this.setState(state => ({
      isToggleOn: !state.isToggleOn
    }));
  };

  logout = e => {
    // e.preventDefault()
    const { auth } = this.props;
    console.log(auth);
    this.props.logout();
    this.props.history.push(`${routeName.PARTNER_LOGIN}`);
  };

  render() {
    const { t, auth, user, i18n } = this.props;
    const { data } = this.state;

    return (
      <header className="header-container">
        <div className="row container-fuid">
          <div className="col-md-12 ">
            <div className="header-top">
            <div className="breadcrumd-main">
              <nav aria-label="breadcrumb">
                <ol className="breadcrumb">
                  <li className="breadcrumb-item breadcrumb-home"><Link to={routeName.HOME}>Home</Link></li>
                  <li className="breadcrumb-item active" aria-current="page"><Link to={routeName.PARTNER_DASHBOARD}>Mypage</Link></li>
                </ol>
              </nav>
            </div>
              <ul className="list-language">
                <li className={`language-item${i18n.language === 'ko' ? ' active' : ''}`}>
                  <a
                    href="javascript:void(0)"
                    className="language-link"
                    onClick={() => this.changeLanguage("ko")}
                  >
                    KO
                  </a>
                </li>
                <li className={`language-item${i18n.language === 'en' ? ' active' : ''}`}>
                  <a
                    href="javascript:void(0)"
                    className="language-link"
                    onClick={() => this.changeLanguage("en")}
                  >
                    EN
                  </a>
                </li>
                <li className={`language-item${i18n.language === 'vi' ? ' active' : ''}`}>
                  <a
                    href="javascript:void(0)"
                    className="language-link"
                    onClick={() => this.changeLanguage("vi")}
                  >
                    VN
                  </a>
                </li>
              </ul>
            </div>

            <div className="menu-top">
              <div className="logo">
                <NavLink to={routeName.PARTNER_DASHBOARD}>
                  <img src={logoFistar} alt="" />
                </NavLink>
              </div>

              <Navbar bg="light" expand="lg">
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                  <Nav className="mr-auto">
                    <NavLink
                      className="nav-item nav-link"
                      to={routeName.SERVICE}
                    >
                      {t("HEADER.HED_service")}
                    </NavLink>
                    <NavLink
                      className="nav-item nav-link"
                      to={routeName.CAMPAIGN}
                    >
                      {t("HEADER.HED_campaign")}
                    </NavLink>
                    <NavLink
                      className="nav-item nav-link"
                      to={routeName.FISTAR_FRONT}
                    >
                      {t("HEADER.HED_fistar")}
                    </NavLink>
                    <NavLink className="nav-item nav-link" to={routeName.FAQ}>
                      {t("HEADER.HED_faq")}
                    </NavLink>
                    <NavLink
                      className="nav-item nav-link"
                      to={routeName.CONTACTUS}
                    >
                      {t("HEADER.HED_contact_us")}
                    </NavLink>
                  </Nav>
                </Navbar.Collapse>
              </Navbar>

              {/*<nav className="navbar navbar-expand-lg navbar-light bg-light">*/}
              {/*<button*/}
              {/*className="navbar-toggler"*/}
              {/*type="button"*/}
              {/*data-toggle="collapse"*/}
              {/*data-target="#navbarNavAltMarkups"*/}
              {/*aria-controls="navbarNavAltMarkup"*/}
              {/*aria-expanded="false"*/}
              {/*aria-label="Toggle navigation"*/}
              {/*>*/}
              {/*<span className="navbar-toggler-icon" />*/}
              {/*</button>*/}
              {/*<div*/}
              {/*className="collapse navbar-collapse"*/}
              {/*id="navbarNavAltMarkups"*/}
              {/*>*/}
              {/*<div className="navbar-nav">*/}
              {/*<Link className="nav-item nav-link" to={routeName.SERVICE}>*/}
              {/*Service*/}
              {/*</Link>*/}
              {/*<Link className="nav-item nav-link" to={routeName.CAMPAIGN}>*/}
              {/*Campaign*/}
              {/*</Link>*/}
              {/*<Link*/}
              {/*className="nav-item nav-link"*/}
              {/*to={routeName.FISTAR_FRONT}*/}
              {/*>*/}
              {/*fistar*/}
              {/*</Link>*/}
              {/*<Link className="nav-item nav-link" to={routeName.FAQ}>*/}
              {/*FAQ*/}
              {/*</Link>*/}
              {/*<Link*/}
              {/*className="nav-item nav-link"*/}
              {/*to={routeName.CONTACTUS}*/}
              {/*>*/}
              {/*Contact us*/}
              {/*</Link>*/}
              {/*</div>*/}
              {/*</div>*/}
              {/*</nav>*/}

              <div className="login-info">
                {auth && auth.isAuthenticated ? (
                  <Fragment>
                    <div className="fistar-user partner-user">
                      <div className="thumnail-users">
                        <img
                          src={
                            auth.user
                              ? auth.user.p_image
                                ? getImageLink(
                                    auth.user.p_image,
                                    IMAGE_TYPE.PARTNERS,
                                    IMAGE_SIZE.LARGE
                                  )
                                : imageExample
                              : auth.avatar
                                ? getImageLink(
                                    auth.avatar,
                                    IMAGE_TYPE.PARTNERS,
                                    IMAGE_SIZE.LARGE
                                  )
                                : imageExample
                          }
                          alt=""
                        />
                      </div>

                      <Dropdown className="droppdow-name name">
                        <Dropdown.Toggle
                          // variant="success"
                          id="dropdown-basic"
                          className="btn dropdown-toggle"
                        >
                          {auth.user ? auth.user.pm_name : auth.name}
                        </Dropdown.Toggle>

                        <Dropdown.Menu className="dropdowns dropdown-profile">
                          <span>
                            <NavLink
                              to={routeName.PARTNER_DASHBOARD}
                              activeClassName="active"
                            >
                              {t("MENU_LEFT_PARTNER.MLP_dashboard")}
                            </NavLink>
                          </span>
                          <span>
                            <NavLink
                              to={routeName.PARTNER_CREATE_CAMPAIN}
                              activeClassName="active"
                            >
                              {t("MENU_LEFT_PARTNER.MLP_create_campaign")}
                            </NavLink>
                          </span>
                          <span>
                            <NavLink
                              to={routeName.PARTNER_SEARCH_FISTAR}
                              activeClassName="active"
                            >
                              {t("MENU_LEFT_PARTNER.MLP_search_fistar")}
                            </NavLink>
                          </span>
                          <span>
                            <NavLink
                              to={routeName.PARTNER_MY_FISTAR}
                              activeClassName="active"
                            >
                              {t("MENU_LEFT_PARTNER.MLP_myfistar")}
                            </NavLink>
                          </span>
                          <span>
                            <NavLink
                              to={routeName.PARTNER_CAMPAIGN_TRACKING}
                              activeClassName="active"
                            >
                              {t("MENU_LEFT_PARTNER.MLP_cp_tracking")}
                            </NavLink>
                          </span>
                          <span>
                            <NavLink
                              to={routeName.PARTNER_MANAGER_INFORMATION}
                              activeClassName="active"
                            >
                              {t("MENU_LEFT_PARTNER.MLP_setting")}
                            </NavLink>
                          </span>
                          <span>
                            <NavLink
                              to={routeName.PARTNER_QA}
                              activeClassName="active"
                            >
                              {t("MENU_LEFT_PARTNER.MLP_qa")}
                            </NavLink>
                          </span>

                          <button onClick={this.logout}>
                            {t("HEADER.HEA_button_logout")}
                          </button>
                        </Dropdown.Menu>
                      </Dropdown>

                      {/*<Dropdown className="droppdow-name name">*/}
                      {/*<Dropdown.Toggle*/}
                      {/*// variant="success"*/}
                      {/*id="dropdown-basic"*/}
                      {/*className="btn dropdown-toggle"*/}
                      {/*>*/}
                      {/*{auth.user ? auth.user.pm_name : auth.name}*/}
                      {/*</Dropdown.Toggle>*/}

                      {/*<Dropdown.Menu className="dropdowns dropdown-profile">*/}
                      {/*<h4>{auth.user ? auth.user.pm_name : auth.name}</h4>*/}
                      {/*<p>{auth.user ? auth.user.pc_name : auth.pc_name}</p>*/}

                      {/*<p>*/}
                      {/*<NavLink*/}
                      {/*to={routeName.PARTNER_MANAGER_INFORMATION}*/}
                      {/*activeClassName="active"*/}
                      {/*>*/}
                      {/*{auth.email ? auth.email : this.state.data.email}*/}
                      {/*</NavLink>*/}
                      {/*</p>*/}
                      {/*<button onClick={this.logout}>LOGOUT</button>*/}
                      {/*</Dropdown.Menu>*/}
                      {/*</Dropdown>*/}
                    </div>
                  </Fragment>
                ) : (
                  <Fragment>
                    <ul className="list-item" style={{ display: "flex" }}>
                      <li className="item">
                        <a className="link-item">{t("HEADER.HED_login")}</a>
                        <div className="droppdown-login">
                          <Link
                            to={routeName.FISTAR_LOGIN}
                            className="droppdown-item"
                          >
                            fi:star
                          </Link>
                          <Link
                            to={routeName.PARTNER_LOGIN}
                            className="droppdown-item"
                          >
                            Partner
                          </Link>
                        </div>
                      </li>
                      <li className="item">
                        <a href="" className="link-item">
                          {t("HEADER.HEA_button_logout")}
                        </a>
                      </li>
                    </ul>
                  </Fragment>
                )}
              </div>
            </div>
          </div>
        </div>
      </header>
    );
  }

  activeClassName = "active";
}

function mapStateToProps(state) {
  return {
    auth: state.auth,
    user: state
  };
}

const mapDispatchToProps = dispatch => {
  return {
    logout: data => dispatch(logoutAction()),
    inforProfile: data => dispatch(listProfileActionPartner(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(withRouter(PartnerDashboardHead)));
