import React, { Component } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import imageFooter from "./../../../../images/logo-footer.png";
import { connect } from "react-redux";
import * as routeName from "../../../../routes/routeName";

class PartnerDashboardFooter extends Component {
  render() {
    const { t } = this.props;
    return (
      <footer className="footer-container">
        <div className="container">
          <div className="content-footer">
            <div className="row footer-site">
              <div className="col-md-2 ">
                <div className="logo">
                  <a href="">
                    <img src={imageFooter} alt="" />
                  </a>
                </div>
              </div>
              <div className="col-md-7">
                <div className="address">
                  <p>
                    473 Dien Bien Phu, Ward 25, Binh Thanh District, Ho Chi Minh
                    City l fimevn@gmail.com l Tel 0903029423
                  </p>
                </div>
              </div>
              <div className="col-md-3">
                <div className="copyright">
                  {/*<p>Privacy Policy & Terms of Service</p>*/}
                  <Link
                    className="nav-item nav-link privacy-terms"
                    to={routeName.PRIVACY_TERMS}
                  >
                    {t("FOOTER.FTR_privacy_policy")}
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps)(
  withTranslation("translations")(PartnerDashboardFooter)
);
