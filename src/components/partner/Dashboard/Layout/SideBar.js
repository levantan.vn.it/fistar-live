import React, { Component,PureComponent } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, NavLink, Redirect } from "react-router-dom";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import * as routeName from "./../../../../routes/routeName";
import PushMessageComponent from "./../../../PushMessage";
import {
  checkExistActionPartner,
  listProfileActionPartner,
  updateManagerInformationActionPartner
} from "../../../../store/actions/auth";
import { getImageLink } from "./../../../../common/helper";
import { IMAGE_SIZE, IMAGE_TYPE } from "./../../../../constants/index";
import imageExample from "./../../../../images/example.jpg";
import {logoutAction} from "./../../../../store/actions/auth"

class PartnerSideBar extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      isToggleOnSibar: true,
      redirectProfile: false
    };
  }

  clickToggleOnSibar = e => {
    e.preventDefault();

    this.setState(state => ({
      isToggleOnSibar: !this.state.isToggleOnSibar
    }));
    this.props.toggleOnSibar(this.state.isToggleOnSibar);
  };

  clickToggleOnSibarNoti = e => {
    e.preventDefault();

    if (typeof window !== 'undefined' && window.document && window.document.createElement) {
      if (window.innerWidth <= 768 && !this.state.isToggleOnSibar) {
        this.setState(state => ({
          isToggleOnSibar: !this.state.isToggleOnSibar
        }));
        this.props.toggleOnSibar(this.state.isToggleOnSibar);
      }
    }
  };

  componentDidMount() {
    this.props.inforProfile().then(response => {
      this.setState({
        data: response
      });
    });
  }

  redirectProfile = () => {
    this.setState({
      redirectProfile: true
    });
  };

  logout = e => {
    // e.preventDefault()
    const { auth } = this.props;
    this.props.logout();
    this.props.history.push(`${routeName.PARTNER_LOGIN}`);
  };

  render() {
    const { t, auth } = this.props;
    const { redirectProfile } = this.state;
    if (
      this.state.redirectProfile &&
      !this.props.history.location.pathname.includes(
        routeName.PARTNER_MANAGER_INFORMATION
      )
    ) {
      return <Redirect to={routeName.PARTNER_MANAGER_INFORMATION} />;
    }

    return (
      <div className="sidebar">
        <div className="wf-menu">
          <div className="menu-left">
            <div className="top">
              <span
                className="fas fa-arrow-left"
                onClick={this.clickToggleOnSibar}
              />
              <div onClick={this.clickToggleOnSibarNoti}>
              <PushMessageComponent />
              </div>
            </div>
            <div className="body-menu">
              <h4 className="title text-center">
                {auth.name
                  ? auth.name
                  : this.state.data && this.state.data.pm_name
                    ? this.state.data.pm_name
                    : ""}
              </h4>
              <div className="img-dashbord">
                <span href="javascript:void(0)">
                  <img
                    src={
                      auth.avatar
                        ? getImageLink(
                            auth.avatar,
                            IMAGE_TYPE.PARTNERS,
                            IMAGE_SIZE.LARGE
                          )
                        : this.state.data && this.state.data.p_image
                          ? getImageLink(
                              this.state.data.p_image,
                              IMAGE_TYPE.PARTNERS,
                              IMAGE_SIZE.LARGE
                            )
                          : imageExample
                    }
                    alt=""
                  />
                </span>
              </div>
              <div className="information">
                <h6 className="text-center">
                  {this.state.data && this.state.data.pc_name
                    ? this.state.data.pc_name
                    : ""}
                </h6>
                <p className="text-center">
                  {this.state.data && this.state.data.email
                    ? this.state.data.email
                    : ""}{" "}
                </p>
                <p className="text-center">
                  {this.state.data && this.state.data.pm_phone
                    ? this.state.data.pm_phone
                    : ""}
                </p>
                <button onClick={this.redirectProfile}>
                  {t("MENU_LEFT_PARTNER.MLP_button_modify")}
                </button>
              </div>
            </div>
            <div className="menu">
              <ul className="list-menu-left mb-4">
                <li className="list-item">
                  <NavLink
                    activeClassName="active"
                    to={routeName.PARTNER_DASHBOARD}
                  >
                    {t("MENU_LEFT_PARTNER.MLP_dashboard")}
                  </NavLink>
                </li>
                <li className="list-item">
                  <NavLink
                    activeClassName="active"
                    to={routeName.PARTNER_CREATE_CAMPAIN}
                  >
                    {t("MENU_LEFT_PARTNER.MLP_create_campaign")}
                  </NavLink>
                </li>
                <li className="list-item">
                  <NavLink to={routeName.PARTNER_SEARCH_FISTAR}>
                    {t("MENU_LEFT_PARTNER.MLP_search_fistar")}
                  </NavLink>
                </li>
                <li className="list-item">
                  <NavLink to={routeName.PARTNER_MY_FISTAR}>
                    {t("MENU_LEFT_PARTNER.MLP_myfistar")}
                  </NavLink>
                </li>
                <li className="list-item">
                  <NavLink to={routeName.PARTNER_CAMPAIGN_TRACKING}>
                    {t("MENU_LEFT_PARTNER.MLP_cp_tracking")}
                  </NavLink>
                </li>
                <li className="list-item">
                  <NavLink
                    activeClassName="active"
                    to={routeName.PARTNER_MANAGER_INFORMATION}
                  >
                    {t("MENU_LEFT_PARTNER.MLP_setting")}
                  </NavLink>
                </li>
                <li className="list-item">
                  <NavLink to={routeName.PARTNER_QA}>
                    {t("MENU_LEFT_PARTNER.MLP_qa")}
                  </NavLink>
                </li>
              </ul>
              <button className="logout-btn" onClick={this.logout}>
                            {t("HEADER.HEA_button_logout")}
                          </button>
            </div>
          </div>
        </div>
        <div className="icom-sidebar" />
        <ul className="list-icon-sidebar">
          <li className="item" onClick={this.clickToggleOnSibar}>
            {/*<li className="item">*/}
            <span className="fas fa-arrow-right icon-sidebar" />
          </li>
          <PushMessageComponent isLi={true} />
          <li className="item">
            <span className="fas fa-bars icon-sidebar" />
            <ul className="list-menu-left">
              <li className="list-item">
                <NavLink
                  activeClassName="active"
                  to={routeName.PARTNER_DASHBOARD}
                >
                  {t("MENU_LEFT_PARTNER.MLP_dashboard")}
                </NavLink>
              </li>
              <li className="list-item">
                <NavLink
                  activeClassName="active"
                  to={routeName.PARTNER_CREATE_CAMPAIN}
                >
                  {t("MENU_LEFT_PARTNER.MLP_create_campaign")}
                </NavLink>
              </li>
              <li className="list-item">
                <NavLink to={routeName.PARTNER_SEARCH_FISTAR}>
                  {t("MENU_LEFT_PARTNER.MLP_search_fistar")}
                </NavLink>
              </li>
              <li className="list-item">
                <NavLink to={routeName.PARTNER_MY_FISTAR}>
                  {" "}
                  {t("MENU_LEFT_PARTNER.MLP_myfistar")}
                </NavLink>
              </li>
              <li className="list-item">
                <NavLink to={routeName.PARTNER_CAMPAIGN_TRACKING}>
                  {t("MENU_LEFT_PARTNER.MLP_cp_tracking")}
                </NavLink>
              </li>
              <li className="list-item">
                <NavLink
                  activeClassName="active"
                  to={routeName.PARTNER_MANAGER_INFORMATION}
                >
                  {t("MENU_LEFT_PARTNER.MLP_setting")}
                </NavLink>
              </li>
              <li className="list-item">
                <NavLink to={routeName.PARTNER_QA}>
                  {t("MENU_LEFT_PARTNER.MLP_qa")}
                </NavLink>
              </li>
            </ul>
          </li>
        </ul>
        
      </div>
    );
  }

  activeClassName = "active";
}

function mapStateToProps(state) {
  return {
    auth: state.auth
  };
}

const mapDispatchToProps = dispatch => {
  return {
    inforProfile: data => dispatch(listProfileActionPartner(data)),
    logout: data => dispatch(logoutAction()),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(withRouter(PartnerSideBar)));
