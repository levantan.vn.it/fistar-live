import React, { Component } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, NavLink } from "react-router-dom";
import { connect } from "react-redux";
import * as routeName from "../../../../routes/routeName";
import SideBar from "./../Layout/SideBar";

class PartnerProfileWrapper extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      isToggleOnSibar: true
    };
  }
  onToggleOnSibars = data => {
    // e.preventDefault()
    console.log(data);
    this.setState(state => ({
      isToggleOnSibar: !data
    }));
  };

  render() {
    const { t } = this.props;
    const { dataTogle } = this.state.isToggleOnSibar;
    console.log(this.state.isToggleOnSibar);
    return (
      <main className="main-container">
        <div
          className={`fistar-content ${
            this.state.isToggleOnSibar == false ? "dashboard-open" : " "
          }`}
        >
          <SideBar toggleOnSibar={this.onToggleOnSibars} />
          <section className="content mypage-partner">
            <div className="title">
              <div className="container">
                <h4>{t("PARTNER_SETTING.PSG_partner_information")}</h4>
              </div>
            </div>
            <div className="partner-infomaion">
              <div className="tab">
                <div className="container">
                  <ul className="list-item list-item-menu">
                    <li className="item">
                      <NavLink
                        to={routeName.PARTNER_MANAGER_INFORMATION}
                        className="link-item"
                        activeClassName="active"
                      >
                        {t("PARTNER_SETTING.PSG_tab_manager")}
                      </NavLink>
                    </li>
                    <li className="item">
                      <NavLink
                        to={routeName.PARTNER_COMPANY_INFORMATION}
                        className="link-item"
                        activeClassName="active"
                      >
                        {t("PARTNER_SETTING.PSG_tab_company")}
                      </NavLink>
                    </li>
                    <li className="item">
                      <NavLink
                        to={routeName.PARTNER_CHANGE_PASSWORD}
                        className="link-item"
                        activeClassName="active"
                      >
                        {t("PARTNER_SETTING.PSG_tab_change_password")}
                      </NavLink>
                    </li>
                  </ul>
                </div>
              </div>
              {this.props.children}
            </div>
          </section>
        </div>
      </main>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps)(
  withTranslation("translations")(PartnerProfileWrapper)
);
