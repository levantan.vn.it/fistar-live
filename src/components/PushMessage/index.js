import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import { withTranslation, Trans } from "react-i18next";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import {
  getPushMessageCampaignAction,
  getPushMessageFistarAction,
  getPushMessageNoticeAction,
  getPushMessageCountAction
} from "./../../store/actions/message";
import { Modal, Tabs, Tab, Nav } from "react-bootstrap";
import "./PushMessage.scss";
import closePopup from "./../../images/close-popup.svg";
import InfiniteScroll from "react-infinite-scroll-component";
import * as routeName from "./../../routes/routeName";
import { getImageLink } from "./../../common/helper";
import {
  IMAGE_SIZE,
  IMAGE_TYPE,
  CAMPAIGN_STATUS,
  FISTER_FINISH_STEP,
  PARTNER_FINISH_STEP
} from "./../../constants/index";

const INTERVEL_COUNT = 10000;

function cutString(text) {
  var wordsToCut = 27;
  var wordsArray = text.split(" ");
  if (wordsArray.length > wordsToCut) {
    var strShort = "";
    for (var i = 0; i < wordsToCut; i++) {
      strShort += wordsArray[i] + " ";
    }
    return strShort + "...";
  } else {
    return text;
  }
}
function cutStringNotice(text) {
  if (!text) {
    return ''
  }
  var wordsToCut = 21;
  var wordsArray = text.split(" ");
  if (wordsArray.length > wordsToCut) {
    var strShort = "";
    for (var i = 0; i < wordsToCut; i++) {
      strShort += wordsArray[i] + " ";
    }
    return strShort + "...";
  } else {
    return text;
  }
}

class PushMessageComponent extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      show: false,
      loading: false,
      last_page: 1,
      page: 1,
      hasMore: true,
      noticeDetail: null,
      count: 0,
      countCampaign: 0,
      countFistar: 0,
      countNotice: 0,
    };
    this.intervalCount = null;
  }

  componentDidMount() {
    if (!this.intervalCount) {
      this.getCount();
      this.intervalCount = setInterval(() => {
        this.getCount();
      }, INTERVEL_COUNT);
    }
  }

  componentWillUnmount() {
    clearInterval(this.intervalCount);
  }

  getCount = () => {
    this.props.getPushMessageCount().then(response => {
      this.setState({
        count: response
      });
    });
  };

  getData = () => {
    const { auth } = this.props;
    let by_relate = "";
    if (auth.type == "partner") {
      by_relate = 1;
    }
    this.setState(
      {
        countCampaign: this.state.count.campaign,
        countFistar: this.state.count.fistar,
        countNotice: this.state.count.notice,
        // loading: true
      },
      () => {
        Promise.all([
          this.props.getPushMessageCampaign(1, by_relate),
          this.props.getPushMessageFistar(),
          this.props.getPushMessageNotice()
        ])
          .then(res => {
            this.setState({
              loading: false
            });
          })
          .catch(() => {
            this.setState({
              loading: false
            });
          });
      }
    );
  };

  fetchMoreDataCampaign = () => {
    const { auth } = this.props;
    let by_relate = "";
    if (auth.type == "partner") {
      by_relate = 1;
    }
    if (
      this.props.message.data.campaigns.current_page >=
      this.props.message.data.campaigns.last_page
    ) {
      this.setState({ hasMore: false });
      return;
    }
    this.props
      .getPushMessageCampaign(
        this.props.message.data.campaigns.current_page + 1,
        by_relate
      )
      .catch(() => {
        this.setState({ hasMore: false });
      });
  };

  fetchMoreDataFistar = () => {
    if (
      this.props.message.data.fistars.current_page >=
      this.props.message.data.fistars.last_page
    ) {
      this.setState({ hasMore: false });
      return;
    }
    this.props
      .getPushMessageFistar(this.props.message.data.fistars.current_page + 1)
      .catch(() => {
        this.setState({ hasMore: false });
      });
  };

  fetchMoreDataNotice = () => {
    if (
      this.props.message.data.notices.current_page >=
      this.props.message.data.notices.last_page
    ) {
      this.setState({ hasMore: false });
      return;
    }
    this.props
      .getPushMessageNotice(this.props.message.data.notices.current_page + 1)
      .catch(() => {
        this.setState({ hasMore: false });
      });
  };

  handleClose = () => {
    this.setState({ show: false });
  };

  handleShow = () => {
    this.setState({ show: true });
  };

  onClickDetailNotice = (notice = null) => {
    this.setState({
      noticeDetail: notice
    });
  };

  renderMatchingStatus = fistar => {
    const { auth } = this.props;
    if (!fistar || !fistar.campaign) {
      return null;
    }
    if (
      [
        CAMPAIGN_STATUS.MATCHING,
        CAMPAIGN_STATUS.ONGOING,
        CAMPAIGN_STATUS.CLOSED
      ].includes(fistar.campaign.cp_status)
    ) {
      if (!fistar.matching_status) {
        return null;
      }
      let status = "";
      let finishStep = [];
      if (auth.type === "fistar") {
        status = fistar.matching_status.label.fistar_status;
        finishStep = FISTER_FINISH_STEP;
      }
      if (auth.type === "partner") {
        status = fistar.matching_status.label.partner_status;
        finishStep = PARTNER_FINISH_STEP;
      }
      const { label, m_id } = fistar.matching_status;
      let { stt_id, fistar_status } = label;
      return (
        <div className="process-apply">
          <span className="icon-start" />
          {status.split("→").map((s, key) => {
            if (status.split("→").length === key + 1) {
              return (
                <Fragment key={key}>
                  <p className="font-weight-bold">{s}</p>
                </Fragment>
              );
            }
            return (
              <Fragment key={`${key}`}>
                <p className="active">{s}</p>
                {status.split("→").length - 1 !== key && <span />}
              </Fragment>
            );
          })}
          {!finishStep.includes(stt_id) && (
            <i className="fas fa-long-arrow-alt-right" />
          )}
        </div>
      );
    }
    if (fistar.campaign.cp_status === 60) {
      if (!fistar.review_status) {
        return null;
      }
      return (
        <div className="process-apply">
          <span className="icon-start" />
          <p className="active">
            {fistar.review_status.label_status.fistar_status}
          </p>
          <i className="fas fa-long-arrow-alt-right" />
        </div>
      );
    }
  };

  renderReviewStatus = (label_status, name) => {
    if (!label_status) {
      return null;
    }
    return (
      <div className="process-apply">
        <span className="icon-start" />
        <i className="fas fa-long-arrow-alt-right" />
      </div>
    );
  };

  renderTabFiStar = () => {
    const {
      t,
      message: {
        data: { fistars }
      },
      auth
    } = this.props;

    if (this.state.loading) {
      return (
        <h1 className="text-center pt-3 pb-3">
          <div className="spinner-border" role="status">
            <span className="sr-only">{t("LOADING.LOADING")}</span>
          </div>
        </h1>
      );
    }

    if (!fistars || Object.keys(fistars).length === 0) {
      return null;
    }

    let { data } = fistars;

    if (data.length === 0) {
      return <div className="text-center p-5">{t("FISTAR.no_fistar")}</div>;
    }

    console.log(data, '----------');

    return (
      <InfiniteScroll
        dataLength={data.length}
        next={this.fetchMoreDataFistar}
        loader={<h4>{t("LOADING.LOADING")}</h4>}
        hasMore={this.state.hasMore}
        height={"50vh"}
        className="message-content-fistar"
      >
        {data.map((fistar, key) => {
          let image = fistar.avatar
          let title = '';
          let name = fistar.name;
          let date = fistar.created_at;
          let status = ''
          let finishStep = [];
          if (auth.type === "partner") {
            status = fistar.status.partner_status
            finishStep = PARTNER_FINISH_STEP;
          } else {
            status = fistar.status.fistar_status
            finishStep = FISTER_FINISH_STEP;
          }
          let linkTo =
            this.props.auth && this.props.auth.type === "fistar"
              ? "/fi-star/my-campaign-detail/"
              : "/partner/campaign/";
          if (fistar.campaign) {
            linkTo = linkTo + fistar.campaign.cp_slug;
          }
          if (
            this.props.auth &&
            this.props.auth.type === "partner" &&
            (!fistar.campaign || fistar.campaign && fistar.campaign.p_id != auth.id)
          ) {
            linkTo = '#'
          }
          return (
            <div className="item" key={`${key}`}>
              <Link
                to={linkTo}
                className="d-flex w-100"
                onClick={this.handleClose}
              >
                <div className="left">
                  <div className="thumnail-fistar">
                    <img
                      className="rounded-circle"
                      src={image}
                      alt={title}
                      title={title}
                    />
                  </div>
                  <div className="user-name">
                    <h4>{name}</h4>
                    <div className="process-apply">
                      <span className="icon-start" />
                      {status.split("→").map((s, key) => {
                        if (status.split("→").length === key + 1) {
                          return (
                            <Fragment key={key}>
                              <p className="font-weight-bold">{s}</p>
                            </Fragment>
                          );
                        }
                        return (
                          <Fragment key={`${key}`}>
                            <p className="active">{s}</p>
                            {status.split("→").length - 1 !== key && <span />}
                          </Fragment>
                        );
                      })}
                      {!finishStep.includes(fistar.stt_id) && (
                        <i className="fas fa-long-arrow-alt-right" />
                      )}
                    </div>
                  </div>
                </div>
                <div className="right">
                  <p className="date">{date}</p>
                </div>
              </Link>
            </div>
          );
          return null;
        })}
      </InfiniteScroll>
    );
  };

  renderTabCampaign = () => {
    const {
      t,
      auth,
      location,
      message: {
        data: { campaigns }
      }
    } = this.props;
    if (this.state.loading) {
      return (
        <h1 className="text-center pt-3 pb-3">
          <div className="spinner-border" role="status">
            <span className="sr-only">{t("LOADING.LOADING")}</span>
          </div>
        </h1>
      );
    }

    if (!campaigns || Object.keys(campaigns).length === 0) {
      return null;
    }

    const { data } = campaigns;

    if (data.length === 0) {
      return (
        <div className="text-center p-5">
          {t("CAMPAIGN.CAMPAIGN_no_campaign")}
        </div>
      );
    }


    return (
      <InfiniteScroll
        dataLength={data.length}
        next={this.fetchMoreDataCampaign}
        loader={<h4>{t("LOADING.LOADING")}</h4>}
        hasMore={this.state.hasMore}
        height={"50vh"}
        className="message-content"
      >
        {data.map(({campaign}, key) => {
          let linkTo =
            this.props.auth && this.props.auth.type === "fistar"
              ? "/fi-star/my-campaign-detail/"
              : "/partner/campaign/";
          let name = campaign.cp_name;
          linkTo = linkTo + campaign.cp_slug;
          if (
            this.props.auth &&
            this.props.auth.type === "partner" &&
            campaign.p_id != auth.id
          ) {
            linkTo = routeName.PARTNER_DASHBOARD;
          }
          return (
            <div className="item" key={key}>
              <div className="left">
                <div className="icon-campaign">
                  <i className="fas fa-bell" />
                </div>
                <div className="message">
                  <h4 className="title-campaign">{name}</h4>
                  {/*<p className="text-ellipsis">{campaign.cp_description}</p>*/}
                  <p
                    className="text"
                    dangerouslySetInnerHTML={{
                      __html: cutString(campaign.cp_description)
                    }}
                  />
                </div>
              </div>
              <div className="right">
                <p className="date">{campaign.created_at}</p>
                <Link
                  to={linkTo}
                  className="btn btn-more"
                  onClick={this.handleClose}
                >
                  {" "}
                  {t("DASHBOARD_FISTAR.DBF_button_more")}{" "}
                  <i className="fas fa-arrow-right" />
                </Link>
              </div>
            </div>
          );
        })}
      </InfiniteScroll>
    );
  };

  renderTabSystem = () => {
    const {
      t,
      message: {
        data: { notices }
      },
      auth
    } = this.props;
    const { noticeDetail } = this.state;

    if (this.state.loading) {
      return (
        <h1 className="text-center pt-3 pb-3">
          <div className="spinner-border" role="status">
            <span className="sr-only">{t("LOADING.LOADING")}</span>
          </div>
        </h1>
      );
    }

    if (!notices || Object.keys(notices).length === 0) {
      return null;
    }

    const { data } = notices;

    if (noticeDetail) {
      return (
        <article className="message-content message-content-notice message-back-list">
          <div className="row">
            <div className="col-md-12">
              <div className="back-list">
                <div
                  className="btn btn-back"
                  onClick={() => this.onClickDetailNotice(null)}
                >
                  <i className="fas fa-arrow-left" />
                  {t("PARTNER_INFORMATION.PNI_button_list")}
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12">
              <div className="item-back">
                <div className="left">
                  <h4>{noticeDetail.notice_title}</h4>
                </div>
                <div className="right">
                  <p>{(noticeDetail.created_at || "").split(" ")[0]}</p>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12">
              <article className="text">
                <p>{noticeDetail.notice_message}</p>
              </article>
            </div>
          </div>
        </article>
      );
    }

    if (data.length === 0) {
      return <div className="text-center p-5">{t("PUSH_MESS.notice_no")}</div>;
    }

    return (
      <InfiniteScroll
        dataLength={data.length}
        next={this.fetchMoreDataNotice}
        loader={<h4>{t("LOADING.LOADING")}</h4>}
        hasMore={this.state.hasMore}
        height={"50vh"}
        className="message-content message-content-notice"
      >
        {data.map((notice, key) => {
          return (
            <Fragment key={key}>
              <div className="item item-notice">
                <div className="left">
                  <div className="icon-notice">
                    <i className="fas fa-cog" />
                  </div>
                  <div className="message">
                    <h4 className="title-notice">{notice.notice_title}</h4>

                    <p
                      dangerouslySetInnerHTML={{
                        __html: cutStringNotice(notice.notice_message)
                      }}
                    />
                  </div>
                </div>
                <div className="right">
                  <p className="date">
                    {notice.created_at}
                  </p>
                  <button
                    className="btn btn-more-notice"
                    onClick={() => this.onClickDetailNotice(notice)}
                  >
                    {t("DASHBOARD_FISTAR.DBF_button_more")}{" "}
                    <i className="fas fa-arrow-right" />
                  </button>
                </div>
              </div>
            </Fragment>
          );
          return null;
        })}
      </InfiniteScroll>
    );
  };

  renderTabLoading = (loading, text) => {
    return (
      <Fragment>
        {loading ? (
          <div className="text-center pt-3 pb-3">
            <div className="spinner-border" role="status">
              <span className="sr-only">{this.props.t("LOADING.LOADING")}</span>
            </div>
          </div>
        ) : (
          <p>{text}</p>
        )}
      </Fragment>
    );
  };

  renderTabs = () => {
    const {
      t,
      message: {
        data: { campaigns, fistars, notices }
      }
    } = this.props;
    let campaign = 0;
    let fistar = 0;
    let notice = 0;
    if (
      campaigns &&
      Object.keys(campaigns).length > 0 &&
      campaigns.data &&
      Array.isArray(campaigns.data)
    ) {
      campaign = campaigns.total;
    }
    if (
      fistars &&
      Object.keys(fistars).length > 0 &&
      fistars.data &&
      Array.isArray(fistars.data)
    ) {
      fistar = fistars.total;
    }
    if (
      notices &&
      Object.keys(notices).length > 0 &&
      notices.data &&
      Array.isArray(notices.data)
    ) {
      notice = notices.total;
    }

    return (
      <Tab.Container id="left-tabs-example" defaultActiveKey={"first"}>
        <Nav variant="pills">
          <Nav.Item>
            <Nav.Link eventKey="first">
              {t("POPUP_MESSEAGE.PME_campaign")}
            </Nav.Link>
            {this.renderTabLoading(this.state.loading, this.state.countCampaign)}
          </Nav.Item>
          <Nav.Item className="tab-fistar">
            <Nav.Link eventKey="second">
              {t("POPUP_MESSEAGE.PME_fistar")}
            </Nav.Link>
            {this.renderTabLoading(this.state.loading, this.state.countFistar)}
          </Nav.Item>
          <Nav.Item className="tab-notice">
            <Nav.Link eventKey="third">
              {t("POPUP_MESSEAGE.PME_notice")}
            </Nav.Link>
            {this.renderTabLoading(this.state.loading, this.state.countNotice)}
          </Nav.Item>
        </Nav>
        <Tab.Content>
          <Tab.Pane eventKey="first">{this.renderTabCampaign()}</Tab.Pane>
          <Tab.Pane eventKey="second">{this.renderTabFiStar()}</Tab.Pane>
          <Tab.Pane eventKey="third">{this.renderTabSystem()}</Tab.Pane>
        </Tab.Content>
      </Tab.Container>
    );
  };

  render() {
    let count = (this.state.count.campaign + this.state.count.fistar + this.state.count.notice) || 0
    return (
      <Fragment>
        {/*<button variant="primary" className="puss-mesage" onClick={this.handleShow}>*/}
        {/*Message*/}
        {/*</button>*/}
        {this.props.isLi ? (
          <li className="item wf-fistar-item" onClick={this.handleShow}>
            <div className="notification">
              <span className="fas fa-bell icon-sidebar" />
              <span className="number-notification">{count}</span>
            </div>
          </li>
        ) : (
          <div className="notification" onClick={this.handleShow}>
            <span className="fas fa-bell icon-sidebar" />
            <span className="number-notification">{count}</span>
          </div>
        )}
        <Modal
          size="lg"
          show={this.state.show}
          onHide={this.handleClose}
          aria-labelledby="example-modal-sizes-title-lg"
          dialogClassName="popup-modal-message"
          onShow={this.getData}
        >
          <section className="popup-message-campaign">
            <div className="popup-container">
              <div className="top">
                <div className="left">
                  <h4>{this.props.t("POPUP_MESSEAGE.PME_message")}</h4>
                </div>
                <div className="right">
                  <button
                    className="btn btn-close-popup close"
                    type="button"
                    data-dismiss="modal"
                    aria-label="Close"
                    onClick={this.handleClose}
                  >
                    <img src={closePopup} alt="" />
                  </button>
                </div>
              </div>
            </div>
            <div className="tab-message">{this.renderTabs()}</div>
          </section>
        </Modal>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    message: state.message,
    auth: state.auth
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getPushMessageCampaign: (page = 1, by_relate = "") =>
      dispatch(getPushMessageCampaignAction(page, by_relate)),
    getPushMessageFistar: (page = 1) =>
      dispatch(getPushMessageFistarAction(page)),
    getPushMessageNotice: (page = 1) =>
      dispatch(getPushMessageNoticeAction(page)),
    getPushMessageCount: () => dispatch(getPushMessageCountAction())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(withRouter(PushMessageComponent)));
