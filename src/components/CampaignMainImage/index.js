import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import { withTranslation, Trans } from "react-i18next";
import { connect } from "react-redux";
import { scrapAction } from "./../../store/actions/scrap";
import { Modal, Tabs, Tab, Nav } from "react-bootstrap";
import closePopup from "./../../images/close-popup.svg";
import InfiniteScroll from "react-infinite-scroll-component";
import * as routeName from "./../../routes/routeName";
import {
  CAMPAIGN_STATUS,
  IMAGE_SIZE,
  IMAGE_TYPE
} from "./../../constants/index";
import "./CampaignMainImage.css";
import { getImageLink, getId } from "./../../common/helper";
import imageExample from "./../../images/example.jpg";

class CampaignMainImage extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { campaign, className } = this.props;
    switch (+campaign.cp_main_image_type) {
      case 1: {
        return (
          <img
            src={getImageLink(
              campaign.cp_main_image,
              IMAGE_TYPE.CAMPAIGNS,
              IMAGE_SIZE.ORIGINAL
            )}
            alt={campaign.cp_name}
            title={campaign.cp_image_title}
            className={className}
          />
        );
        break;
      }
      case 2: {
        return (
          <img
            src={
              "//img.youtube.com/vi/" + getId(campaign.cp_main_image) + "/0.jpg"
            }
            alt={campaign.cp_name}
            title={campaign.cp_image_title}
            className={className}
          />
        );
        break;
      }
      case 3: {
        // video upload thumbnail
        // let src = getImageLink(
        //     campaign.cp_main_image,
        //     IMAGE_TYPE.CAMPAIGNS,
        //     IMAGE_TYPE.ORIGINAL
        //   )
        // let video = document.createElement('video');
        // video.src = src

        // video.width = 360;
        // video.height = 240;

        // let canvas = document.createElement('canvas');
        // canvas.width = 360;
        // canvas.height = 240;
        // let context = canvas.getContext('2d');
        // video.addEventListener('loadedmetadata', () => {
        //   context.drawImage(video, 0, 0, canvas.width, canvas.height);
        //   let image = document.getElementById('campaign-' + campaign.cp_id)
        //   image.src = canvas.toDataURL('image/jpeg')
        // });
        return (
          <img
            id={"campaign-" + campaign.cp_id}
            src={getImageLink(campaign.cp_video_thumb, IMAGE_TYPE.CAMPAIGNS)}
            alt={campaign.cp_name}
            title={campaign.cp_image_title}
            className={className}
          />
        );
        break;
      }
      default:
        return <img src={imageExample} alt="" title="" />;
        break;
    }
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    scrap: id => dispatch(scrapAction(id))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(CampaignMainImage));
