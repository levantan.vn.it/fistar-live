import React, { Component } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, NavLink } from "react-router-dom";
import { connect } from "react-redux";
import * as routeName from "./../../../../routes/routeName";
import SideBar from "./../../Sidebar/index";

class FistarProfileWrapper extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      isToggleOnSibar: true
    };
  }

  onToggleOnSibars = data => {
    // e.preventDefault()
    console.log(data);
    this.setState(state => ({
      isToggleOnSibar: !data
    }));
  };

  getNavLinkClass = path => {
    return this.props.children.props.location.pathname === path
      ? "item tab-item active"
      : "item tab-item";
  };

  render() {
    const { t } = this.props;
    const { dataTogle } = this.state.isToggleOnSibar;

    return (
      <main className="main-container">
        <div
          className={`fistar-content ${
            this.state.isToggleOnSibar == false ? "dashboard-open" : " "
          }`}
        >
          <SideBar toggleOnSibar={this.onToggleOnSibars} />
          <section className="content mypage-partner mypage-fistar">
            <div className="title">
              <div className="container">
                <h4>{t("PROFILE_FISTAR.PFR_fistar_information_title")}</h4>
              </div>
            </div>
            <div className="partner-infomaion fistar-infomaion">
              <div className="tab">
                <div className="container">
                  <ul className="list-item">
                    <li
                      className={this.getNavLinkClass(
                        routeName.FISTAR_PROFILE_GANERAL
                      )}
                    >
                      <NavLink
                        className="link-item"
                        to={routeName.FISTAR_PROFILE_GANERAL}
                        activeClassName="active"
                      >
                        {t("PROFILE_FISTAR.PFR_tab_basic")}
                      </NavLink>
                    </li>
                    <li
                      className={this.getNavLinkClass(
                        routeName.FISTAR_PROFILE_INFORMARTION
                      )}
                    >
                      <NavLink
                        className="link-item"
                        to={routeName.FISTAR_PROFILE_INFORMARTION}
                        activeClassName="active"
                      >
                        {t("PROFILE_FISTAR.PFR_fistar_information")}
                      </NavLink>
                    </li>
                    <li
                      className={this.getNavLinkClass(
                        routeName.FISTAR_PROFILE_RESET_PASSWORD
                      )}
                    >
                      <NavLink
                        className="link-item"
                        to={routeName.FISTAR_PROFILE_RESET_PASSWORD}
                        activeClassName="active"
                      >
                        {t("PROFILE_FISTAR.PFR_tab_change_password")}
                      </NavLink>
                    </li>
                    <li
                      className={this.getNavLinkClass(
                        routeName.FISTAR_PROFILE_SETTING
                      )}
                    >
                      <NavLink
                        className="link-item"
                        to={routeName.FISTAR_PROFILE_SETTING}
                        activeClassName="active"
                      >
                        {t("PROFILE_FISTAR.PFR_tab_setting")}
                      </NavLink>
                    </li>
                  </ul>
                </div>
              </div>
              {this.props.children}
            </div>
          </section>
        </div>
      </main>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps)(
  withTranslation("translations")(FistarProfileWrapper)
);
