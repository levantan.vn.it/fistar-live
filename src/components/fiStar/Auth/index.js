import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { withRouter } from "react-router";

import FiStarAuthHead from "./Head";
import FiStarAuthBanner from "./Banner";
import "./wrapper.scss";
import * as routeName from "./../../../routes/routeName";
import {
    getBannerAction, getCampaignAction, getCountMainMenuAction,
    getDataFistarCampaign, getFistarAction
} from "../../../store/actions/auth";
import CampaignMainImage from "./../../../components/CampaignMainImage/index"
import iconHeat from "./../../../../src/images/home/icon-heat.svg";
import iconComment from "./../../../../src/images/home/icon-comment.svg";
import iconShare from "./../../../../src/images/home/icon-share.svg";

class FistarAuthWrapper extends Component {


    constructor(props) {
        super(props);
        this.state = {
            status: [59, 60, 61, 62],
            isLoadingCampaign: false,
            type: "",
            campaigns: "",
        };

    }


  scrollStep = () => {
    if (typeof window !== 'undefined' && window.document && window.document.createElement) {
      if (window.pageYOffset === 0) {
        clearInterval(this.state.intervalId);
      }
      window.scroll(0, window.pageYOffset - this.props.scrollStepInPx);
    }
  };

    componentDidMount() {

        this.setState(
            {
                isLoadingCampaign: true
            },
            () => {
                this.initData();
            }
        );

    }

    initData = () => {
        this.props
            .getCampaign(this.state.page, this.state.type, this.state.status)
            .then(response => {
                this.setState({
                    campaigns: response.data,
                    isLoadingCampaign: false
                });
            })
            .catch(() => {
                this.setState({});
            });
    };

  scrollToTop = () => {
    let intervalId = setInterval(this.scrollStep, 16.66);
    this.setState({ intervalId: intervalId });
  };

  render() {
    const { t } = this.props;
    console.log(this.props.history.location.pathname);
      const {
          form_data, loading, form_validate,
          banners,
          countMainMenu,
          fistars,
          campaigns,
          redirectCampaign,
          isLoadingCampaign,
          redirectService,
          redirectSignUp,
          redirectSignUpPartner,
          count_campaign,
          fistarTop,
          campaignsTop,
          count_partner,
          count_fistar
      } = this.state;

    return (
      <Fragment>
        <FiStarAuthBanner />
        <main className="main-container">
          <section className="form-login">
            <div className="container">
              <div className="form-site">
                <FiStarAuthHead />
                <div className="body-form">
                  <div className="container-form">{this.props.children}</div>
                </div>
                {this.props.history.location.pathname ==
                routeName.FISTAR_JOIN ? (
                  <div className="row">
                    <div className="col-md-12">
                      <div className="footer-register  banner-join">
                        <span>
                          <img src="https://www.clubpiaf.vn/assets/images/logo.png" />
                        </span>
                        <div className="text-banner">
                          <h3>{t("FISTAR_JOIN.FJN_advertisement_slogan")}</h3>
                          <a href="http:www.clubpiaf.vn">
                            {t("FISTAR_JOIN.FJN_advertisement")}
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                ) : (
                  ""
                )}
              </div>

              {this.props.history.location.pathname ==
              routeName.FISTAR_APPROVE_REGISTER ? (
                <div className="title-approve-register">
                  {/*<p>{t("FISTAR_JOIN_CONFIRM.FJC_text_banner_1")}</p>*/}
                  <h1 className="title-campaign-head">{t("FISTAR_JOIN_CONFIRM.FJC_text_banner_2")}</h1>

                    <div className="main-fistar-campaign">
                        <div className="container-custom">
                            <div className="row">
                                {campaigns &&
                                campaigns.length > 0 &&
                                campaigns.slice(0, 6).map((campaign, key) => {

                                    return <div className="col-lg-4" key={key}>
                                        <div className="card card-campaign">
                                            <div className="img-card">
                                                <Link to={"/fi-star/my-campaign-detail/" + campaign.cp_slug}>

                                                    <CampaignMainImage campaign={campaign} />
                                                </Link>
                                            </div>
                                            <Link to={"/fi-star/my-campaign-detail/" + campaign.cp_slug} className="card-body body-campaign">
                                                <h5 className="card-title">
                                                    <Link to={routeName.CAMPAIGN}>{campaign.cp_name}</Link>
                                                </h5>
                                                <p className="text-content" dangerouslySetInnerHTML={{
                                                    __html: campaign ? campaign.cp_description : ""
                                                }}>

                                                </p>
                                            </Link>
                                            <Link to={"/fi-star/my-campaign-detail/" + campaign.cp_slug}>
                                                <div className="card-footer">
                                                    <div className="solical-item">
                                                        <div className="item">
                                                            <img src={iconShare} alt=""/>
                                                            <span>{campaign.review_statitics &&
                                                            campaign.review_statitics.sum_like
                                                                ? campaign.review_statitics.sum_like
                                                                : 0}</span>
                                                        </div>

                                                        <div className="item">
                                                            <img src={iconComment} alt=""/>
                                                            <span>{campaign.review_statitics &&
                                                            campaign.review_statitics.sum_comment
                                                                ? campaign.review_statitics.sum_comment
                                                                : 0}</span>
                                                        </div>
                                                        <div className="item">
                                                            <img src={iconHeat} alt="H"/>
                                                            <span>{campaign.review_statitics &&
                                                            campaign.review_statitics.sum_share
                                                                ? campaign.review_statitics.sum_share
                                                                : 0}</span>
                                                        </div>

                                                    </div>
                                                </div>
                                            </Link>
                                        </div>
                                    </div>;

                                })}

                            </div>

                        </div>
                    </div>

                </div>
              ) : (
                ""
              )}
            </div>
          </section>

        </main>
      </Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

const mapDispatchToProps = dispatch => {
    return {
        getCampaign: (page = 1, type = "", status = []) =>
            dispatch(getCampaignAction(page, type, status)),

    };
};

export default connect(mapStateToProps,mapDispatchToProps)(
  withTranslation("translations")(withRouter(FistarAuthWrapper))
);
