import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { createAccessTokenFBAction } from "./../../../store/actions/auth";
import { Modal, Button } from "react-bootstrap";
import cancelImage from "./../../../images/cancel.svg";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";
import "./ModalGetAccessToken.css";

class ModalGetAccessToken extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowModal: this.props.isShowModal,
      isShowModalFalse: false
    };
    if (typeof window !== 'undefined' && window.document && window.document.createElement) {
      if (window.FB) {
        window.FB.init({
          appId: process.env.REACT_APP_FACEBOOK_FISTAR_ID,
          status: true,
          xfbml: true,
          version: "v3.2" // or v2.6, v2.5, v2.4, v2.3
        });
      }
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.isShowModalFalse !== prevProps.isShowModalFalse) {
      this.setState({
        isShowModal: !this.props.isShowModalFalse,
        isShowModalFalse: this.props.isShowModalFalse
      })
    }
  }

  responseFacebook = e => {
    console.log(e);
    // let accessToken = e.accessToken;
    // let { uid, isRegister = '', isNew = '' } = this.props
    this.props.callback(e).catch(e => {
      console.log(e, "eeeeeeeeeeeeeeeeeeee");
      this.setState({
        isShowModalFalse: true,
        isShowModal: false
      });
    });
    // this.props
    //   .createAccessTokenFB(uid, accessToken, isRegister, isNew)
    //   .then(() => {
    //     this.props.callback();
    //   })
    //   .catch(() => {
    //     this.setState({
    //       isShowModalFalse: true,
    //       isShowModal: false
    //     });
    //   });
  };

  handleClose = () => {
    this.setState({ isShowModal: false });
    this.props.handleClose();
  };

  onClickCloseModal = () => {
    this.setState({ isShowModalFalse: false });
  };

  handleShow = () => {
    this.setState({ isShowModal: true });
  };

  onExited = () => {
    this.props.handleClose();
  };

  renderLoginFB = () => (
    <Fragment>
    {this.props.isCallback ? (
      <a
        className="fb"
        onClick={this.props.callback}
      >
        <i className="fab fa-facebook-f fa-2x" /> I agree
      </a>
    ) : (
      <a
        className="fb"
        href={this.props.url}
      >
        <i className="fab fa-facebook-f fa-2x" /> I agree
      </a>
    )}
    </Fragment>
  )

  renderLoginFBLoading = () => (
    <a
      className="fb"
    >
      <i className="fab fa-facebook-f fa-2x" />
      {' '}
      <div className="spinner-border" role="status">
        <span className="sr-only">Loading...</span>
      </div>
    </a>
  )

  render() {
    const { t } = this.props;
    const { data, errors, genders, isLoading } = this.state;

    return (
      <Fragment>
        <Modal
          show={this.state.isShowModal}
          onHide={this.handleClose}
          dialogClassName="howto-register modal-dialog-centered"
          backdrop={"static"}
        >
          <div className="modal-content">
            <div className="register">
              <div className="top modal-header">
                <h4>fiStar</h4>
                {!this.props.isLoadingModal && <button
                  type="button"
                  className="btn btn-close close"
                  onClick={this.handleClose}
                >
                  <img src={cancelImage} alt="close" />
                </button>}
              </div>
              <div className="body-popup">
                <p className="mb-3">
                  User_friends permission allows the system to see how many
                  friends that user has on Facebook.
                </p>
                <p className="mb-3">
                  User_posts, user_photos permission will allow partners on the
                  system to see the pictures or post that Fistar has posted on
                  facebook recently.
                </p>
                <p className="mb-3">
                  User_posts permission allows the statistics system to have the
                  number of reaction, comment, share the review that Fistar
                  posted on facebook.
                </p>
                {this.props.isLoadingModal && this.renderLoginFBLoading()}
                {!this.props.isLoadingModal && this.renderLoginFB()}
                {/*<FacebookLogin
                  appId={process.env.REACT_APP_FACEBOOK_FISTAR_ID}
                  autoLoad={false}
                  fields="name,email,picture,id"
                  scope="public_profile,user_photos,user_friends,user_posts"
                  callback={this.responseFacebook}
                  render={renderProps => (
                    <a
                      className="fb"
                      href={`https://www.facebook.com/v4.0/dialog/oauth?&client_id=197684317620676&redirect_uri=https://10141f14.ngrok.io/fi-star/join`}
                    >
                      <i className="fab fa-facebook-f fa-2x" /> I agree
                    </a>
                  )}
                />*/}
                {/*<button type="button" className="fb" onClick={renderProps.onClick}>*/}
                {/*<i className="fab fa-facebook-f fa-2x"></i> I agree*/}
                {/*</button>*/}
              </div>
            </div>
          </div>
        </Modal>
        <Modal
          show={this.state.isShowModalFalse}
          onHide={this.handleClose}
          onExited={this.onExited}
          dialogClassName="howto-register modal-dialog-centered"
          backdrop={"static"}
        >
          <div className="modal-content">
            <div className="register">
              <div className="top modal-header">
                <h4>fiStar</h4>
                <button
                  type="button"
                  className="btn btn-close close"
                  onClick={this.onClickCloseModal}
                >
                  <img src={cancelImage} alt="close" />
                </button>
              </div>
              <div className="body-popup">
                <p className="mb-3">
                  Your account is not correct with the information you have
                  registered.
                </p>
                <button type="button" onClick={this.onClickCloseModal}>
                  Close
                </button>
              </div>
            </div>
          </div>
        </Modal>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    createAccessTokenFB: (data, accessToken, isRegister, isNew = "") =>
      dispatch(createAccessTokenFBAction(data, accessToken, isRegister, isNew))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ModalGetAccessToken);
