import React, { Component, Fragment } from 'react';
import { withTranslation, Trans } from 'react-i18next';
import { connect } from 'react-redux'

class FistarLocation extends React.PureComponent {

  handleChangeInput = (e) => {
    this.props.handleChange(e)
  }

  renderLocation = () => {
    const { code: { data: { location } }, value, error } = this.props

    if (!location) return <select
          className={`form-control form-control-sm${error ? ' is-invalid' : ''}`}
          value={value}
          name="location"
          id="location"
        >
          <option>---</option>
        </select>

    return (
      <Fragment>
        <select
          className={`form-control form-control-sm${error ? ' is-invalid' : ''}`}
          value={value}
          onChange={this.handleChangeInput}
          name="location"
          id="location"
        >
          <option>---</option>
          {(location.code || []).map((item, key) => (
            <option value={item.cd_id} key={key}>{item.cd_label}</option>
          ))}
        </select>
      </Fragment>
    )
  }

  render() {
    return (
      <Fragment>
        {this.renderLocation()}
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    code: state.code
  }
}

const mapDispatchToProps = dispatch => {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(withTranslation('translations')(FistarLocation))
