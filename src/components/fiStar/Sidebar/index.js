import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, NavLink, Redirect } from "react-router-dom";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import * as routeName from "./../../../routes/routeName";
import { logoutAction } from "./../../../store/actions/auth";
import PushMessageComponent from "./../../PushMessage";
import { getImageLink } from "./../../../common/helper";
import { IMAGE_SIZE, IMAGE_TYPE } from "./../../../constants/index";

class SideBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      isToggleOnSibar: true,
      isModify: false
    };
    this.child = React.createRef();
  }

  clickToggleOnSibar = e => {
    e.preventDefault();

    this.setState(state => ({
      isToggleOnSibar: !this.state.isToggleOnSibar
    }));
    this.props.toggleOnSibar(this.state.isToggleOnSibar);
  };

  clickToggleOnSibarNoti = e => {
    e.preventDefault();

    if (typeof window !== 'undefined' && window.document && window.document.createElement) {
      if (window.innerWidth <= 768 && !this.state.isToggleOnSibar) {
        this.setState(state => ({
          isToggleOnSibar: !this.state.isToggleOnSibar
        }));
        this.props.toggleOnSibar(this.state.isToggleOnSibar);
      }
    }
  };

  onClickModify = () => {
    this.setState({
      isModify: true
    });
  };

  getNavLinkClass = path => {
    return this.props.location.pathname.includes(path)
      ? "list-item active"
      : "list-item";
  };

  onClickShowPushMessage = () => {
    // this.child.handleShow();
  };
  logout = e => {
    // e.preventDefault()
    const { auth } = this.props;
    this.props.logout();
    this.props.history.push(`${routeName.FISTAR_LOGIN}`);
  };

  render() {
    const { t, auth } = this.props;
    if (
      this.state.isModify &&
      !this.props.history.location.pathname.includes(routeName.FISTAR_PROFILE)
    ) {
      return <Redirect to={routeName.FISTAR_PROFILE_GANERAL} />;
    }

    return (
      <div className="sidebar sidebar-fistar">
        <ul className="list-icon-sidebar">
          <li className="item wf-fistar-item" onClick={this.clickToggleOnSibar}>
            <span className="fas fa-arrow-right icon-sidebar" />
          </li>
          <PushMessageComponent isLi={true} />
          <li className="item wf-fistar-item">
            <span className="fas fa-bars icon-sidebar" />
            <ul className="list-menu-left">
              <li className="list-item">
                <NavLink to={routeName.FISTAR_DASHBOARD}>
                  {t("MENU_LEFT_PARTNER.MLP_dashboard")}
                </NavLink>
              </li>
              <li className="list-item">
                <NavLink to={routeName.FISTAR_PROFILE_GANERAL}>
                  {t("HEADER.HEA_profile")}
                </NavLink>
              </li>
              {auth.type === "partner" && (
                <li className="list-item">
                  <Link to="/partner/create-campain">
                    {t("MENU_LEFT_PARTNER.MLP_create_campaign")}
                  </Link>
                </li>
              )}
              <li className="list-item">
                <NavLink to={routeName.FISTAR_MY_CAMPAIGN}>
                  {t("HEADER.HEA_my_campaign")}
                </NavLink>
              </li>
              <li className="list-item">
                <NavLink to={routeName.FISTAR_SEARCH_CAMPAIGN}>
                  {t("HEADER.HEA_campaign_search")}
                </NavLink>
              </li>
              <li className="list-item">
                <NavLink to={routeName.FISTAR_QA}>{t("HEADER.HEA_qa")}</NavLink>
              </li>
              {/*<li className="list-item">*/}
              {/*<NavLink to={routeName.FISTAR_CAMPAIGN_DETAIL} >My fiStar</NavLink>*/}
              {/*</li>*/}
              {/*<li className="list-item">
                                <a href="javascript:void(0)">Campaign Tracking</a>
                            </li>
                            <li className="list-item">
                                <NavLink to={routeName.FISTAR_QA}>Q&amp;A</NavLink>
                            </li>*/}
            </ul>
          </li>
        </ul>
        <div className="wf-menu">
          <div className="menu-left">
            <div className="top">
              <span
                className="fas fa-arrow-left"
                onClick={this.clickToggleOnSibar}
              />
              <div className="notification" onClick={this.clickToggleOnSibarNoti}>
                <PushMessageComponent />
              </div>
            </div>
            <div className="body-menu">
              <h4 className="title text-center">{auth.name}</h4>
              <div className="img-dashbord">
                <a href="javascript:void(0)">
                  <img
                    src={getImageLink(
                      auth.avatar,
                      IMAGE_TYPE.FISTARS,
                      IMAGE_SIZE.THUMBNAIL
                    )}
                    alt={auth.name}
                  />
                </a>
              </div>
              <div className="information">
                {/*<h6 className="text-center">{auth.pc_name}</h6>*/}
                <p className="text-center">{auth.email} </p>
                <p className="text-center">{auth.phone} </p>
                <button onClick={this.onClickModify}>
                  {t("MENU_LEFT_PARTNER.MLP_button_modify")}
                </button>
              </div>
            </div>
            <div className="menu">
              <ul className="list-menu-left mb-4">
                <li
                  className={this.getNavLinkClass(routeName.FISTAR_DASHBOARD)}
                >
                  <Link to={routeName.FISTAR_DASHBOARD}>
                    {t("MENU_LEFT_PARTNER.MLP_dashboard")}
                  </Link>
                </li>
                <li className={this.getNavLinkClass(routeName.FISTAR_PROFILE)}>
                  <Link to={routeName.FISTAR_PROFILE_GANERAL}>
                    {t("HEADER.HEA_profile")}
                  </Link>
                </li>
                {auth.type === "partner" && (
                  <li className="list-item">
                    <Link to="/partner/create-campain">
                      {t("MENU_LEFT_PARTNER.MLP_create_campaign")}
                    </Link>
                  </li>
                )}
                <li
                  className={this.getNavLinkClass(routeName.FISTAR_MY_CAMPAIGN)}
                >
                  <NavLink to={routeName.FISTAR_MY_CAMPAIGN}>
                    {t("HEADER.HEA_my_campaign")}
                  </NavLink>
                </li>
                <li
                  className={this.getNavLinkClass(
                    routeName.FISTAR_SEARCH_CAMPAIGN
                  )}
                >
                  <NavLink to={routeName.FISTAR_SEARCH_CAMPAIGN}>
                    {t("HEADER.HEA_campaign_search")}
                  </NavLink>
                </li>
                <li className={this.getNavLinkClass(routeName.FISTAR_QA)}>
                  <NavLink to={routeName.FISTAR_QA}>
                    {t("HEADER.HEA_qa")}
                  </NavLink>
                </li>
                {/*<li className="list-item">
                                    <a href="javascript:void(0)">fiStar Search</a>
                                </li>
                                <li className="list-item">
                                    <a href="javascript:void(0)">My fiStar</a>
                                </li>
                                <li className="list-item">
                                    <a href="javascript:void(0)">Campaign Tracking</a>
                                </li>
                                <li className="list-item">
                                    <NavLink to={routeName.FISTAR_QA}>Q&amp;A</NavLink>
                                </li>*/}
              </ul>
              <button className="logout-btn fistar-logout" onClick={this.logout}>
                            {t("HEADER.HEA_button_logout")}
                          </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};

const mapDispatchToProps = dispatch => {
  return {
    logout: data => dispatch(logoutAction()),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(withRouter(SideBar)));
