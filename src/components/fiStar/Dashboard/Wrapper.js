import React, { Component } from 'react';
import { withTranslation, Trans } from 'react-i18next';
import { connect } from 'react-redux'
import SideBar from './../Sidebar/index'
import BackToTop from './../../partial/BackToTop'

class FistarDashboardWrapper extends Component {

    constructor(props) {
        super(props)
        this.state = {
            data: {},
            isToggleOnSibar:true
        }
    }

    onToggleOnSibars = (data) => {
        // e.preventDefault()
        console.log(data)
        this.setState(state => ({
            isToggleOnSibar:!data
        }));

    }


  render() {
    const { t } = this.props;
    const {dataTogle}=this.state.isToggleOnSibar
    return (
        <main className="main-container">
            <div className={`fistar-content ${this.state.isToggleOnSibar == false ? 'dashboard-open' : ' '}`}>
                <SideBar toggleOnSibar={this.onToggleOnSibars} />
                {this.props.children}
            </div>
            <BackToTop />
        </main>
    );
  }
}

function mapStateToProps (state) {
  return {}
}

export default connect(mapStateToProps)(withTranslation('translations')(FistarDashboardWrapper))
