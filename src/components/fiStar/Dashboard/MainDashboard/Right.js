import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import InfiniteScroll from "react-infinite-scroll-component";
import imgaeIdol from "./../../../../images/dash-02.png";
import imageFace from "./../../../../images/dashboard/face.png";
import imageIntar from "./../../../../images/dashboard/intar.png";
import imageYoutube from "./../../../../images/dashboard/toutube.png";
import imageFime from "./../../../../images/dashboard/fime.png";
import imageLogo from "./../../../../images/dashboard/Layer 6.png";
import QAComponent from "./QA";
import { getCampaignReviewAction } from "./../../../../store/actions/campaign";
import { getImageLink } from "./../../../../common/helper";
import { IMAGE_SIZE, IMAGE_TYPE } from "./../../../../constants/index";
import {calcAge} from "../../../../common/helper";

class RightComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      campaigns: [],
      matching: 0,
      scrap: 0,
      request: 0,
      apply: 0,
      recommended: 0,
      total: 0,
      isLoadingFistar: false
    };
  }

  componentDidMount() {
    this.setState(
      {
        isLoadingFistar: true
      },
      () => {
        this.props.getCampaignReview().then(response => {
          this.setState({
            campaigns: response.data,
            matching: response.matching,
            scrap: response.scrap,
            request: response.request,
            apply: response.apply,
            recommended: response.recommended,
            total: response.total,
            isLoadingFistar: false
          });
        });
      }
    );
  }

  renderCampaign = campaign => {
    if (!campaign) {
      return null;
    }
    let imageSns = "";
    switch (campaign.sns.sns_id) {
      case 4: {
        imageSns = imageIntar;
        break;
      }
      case 3: {
        imageSns = imageYoutube;
        break;
      }
      case 2: {
        imageSns = imageFace;
        break;
      }
      case 1: {
        imageSns = imageFime;
        break;
      }
      default:
        break;
    }

    let yearCurrent = new Date().getFullYear();
    let oldCurrent =
      yearCurrent - campaign.matching.influencer.dob.split("-")[0];
    return (
      <div className="card" key={campaign.m_ch_id}>
        <div className="image-covers">
          <a href="javascript:void(0)">
            <img
              src={getImageLink(
                campaign.matching.campaign.cp_image,
                IMAGE_TYPE.CAMPAIGNS,
                IMAGE_SIZE.ORIGINAL
              )}
              alt={campaign.matching.campaign.cp_name}
            />
          </a>
        </div>
        <div className="content-card">
          <div className="information-user">
            <a href="javascript:void(0)">
              <img
                src={getImageLink(
                  campaign.matching.influencer.picture,
                  IMAGE_TYPE.FISTARS,
                  IMAGE_SIZE.ORIGINAL
                )}
                alt={campaign.matching.influencer.fullname}
                className="thumnail"
              />
            </a>
            <div className="user-name">
              <h4>
                <span className={"mr-2"}>
                  {campaign.matching.influencer.gender &&
                  campaign.matching.influencer.gender.cd_id === 10 ? (
                    <i className="fas fa-venus" />
                  ) : (
                    <i className="fas fa-mars" />
                  )}
                </span>

                {campaign.matching.influencer.fullname}
              </h4>
              <div className="address-old">
                <p>
                  <span>
                    {campaign.matching.influencer.dob.split("-")[0]}({calcAge(campaign.matching.influencer.dob)})
                  </span>
                  <span className="address">
                    {campaign.matching.influencer.location.cd_label}
                  </span>
                </p>
              </div>
            </div>
          </div>
          <div className="sns-title">{campaign.m_ch_title}</div>
          <div className="text">
            <div dangerouslySetInnerHTML={{ __html: campaign.m_ch_content }} />
          </div>
          <div className="date-more">
            <p className="date">
              {campaign.created_at.split(" ")[0]}
              <span>{campaign.sns.sns_name}</span>
            </p>
            <a href={campaign.m_ch_url} target={"_blank"}>
              More
            </a>
          </div>
        </div>
        <div className="footer-card">
          <div className="flow-solical">
            <div className="icon-solical">
              <a href="javascript:void(0)">
                <img src={imageSns} alt="" />
              </a>
            </div>
            <ul className="list-footer">
              <li className="item">
                <a href="javascript:void(0)">
                  <i className="far fa-heart" />
                </a>
                <span>{campaign.m_ch_like}</span>
              </li>
              <li className="item">
                <a href="javascript:void(0)">
                  <i className="far fa-comment-dots" />
                </a>
                <span>{campaign.m_ch_comment}</span>
              </li>
              <li className="item">
                <a href="javascript:void(0)">
                  <i className="fas fa-share-alt" />
                </a>
                <span>{campaign.m_ch_share}</span>
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  };

  render() {
    const { t, height } = this.props;
    const {
      campaigns,
      isLoadingCampaign,
      matching,
      ready,
      ongoing,
      closed,
      total
    } = this.state;

    return (
      <Fragment>
        <div className="q-a-fistar">
          <div className="left">
            <QAComponent />
            <div className="content-q-a">
              <div className="top">
                <h4>{t("DASHBOARD_FISTAR.DFR_campaign_review")}</h4>
                {/*{!(campaigns.length === 0 && !isLoadingCampaign) && (*/}
                {/*<button>{t("DASHBOARD_FISTAR.DFR_button_view_all")}<</button>*/}
                {/*)}*/}
              </div>
              <div className="card-group">
                {campaigns.length === 0 && !isLoadingCampaign ? (
                  <div className={"text-center mt-5"}>
                    You don't have any reviews
                  </div>
                ) : (
                  campaigns.map((camp, i) => {
                    if (i <= 4) {
                      return this.renderCampaign(campaigns[i]);
                    }
                  })
                )}
              </div>
            </div>
          </div>
          <div className="right">
            <div className="content-q-a">
              <div className="card-group">
                {campaigns.length > 0 &&
                  campaigns.map((camp, i) => {
                    if (i > 4) {
                      return this.renderCampaign(campaigns[i]);
                    }
                  })}
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};
const mapDispatchToProps = dispatch => {
  return {
    getCampaignReview: () => dispatch(getCampaignReviewAction())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(RightComponent));
