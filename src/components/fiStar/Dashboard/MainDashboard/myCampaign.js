import React, { Component, Fragment } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { getMyCampaignDashBoardAction } from "./../../../../store/actions/campaign";
import InfiniteScroll from "react-infinite-scroll-component";
import * as routeName from "./../../../../routes/routeName";
import {
  SNS_STATE,
  CAMPAIGN_STATUS
} from "./../../../../constants/index";
import CampaignMainImage from "./../../../../components/CampaignMainImage";
import {FISTER_FINISH_STEP} from "../../../../constants";
import { STATUS_STEP_FITAR } from "./../../../../constants";
import { nextStepAction } from "./../../../../store/actions/campaign";
import imageFace from "../../../../images/dashboard/face.png";
import imageIntar from "../../../../images/dashboard/intar.png";
import imageToutube from "../../../../images/dashboard/toutube.png";
import imageRead from "./../../../../images/mk.png";
import { getCodeAction } from "./../../../../store/actions/auth";
import { withTranslation } from "react-i18next";
import ModalStatusCampaign from "../MyCampaign/ModalStatusCampaign";
import FistarApply from "../MyCampaign/FistarApply";
import { Modal } from "react-bootstrap";
import Scrap from "./../../../../components/Scrap";
import "../MyCampaign/campaign.scss";

const MATCHING = {
  REQUEST: [1, 2, 3, 4, 5, 6, 7, 8],
  APPLY: [9, 10, 11, 12, 13, 14, 15, 16]
};

const MATCHING_STATUS = {
  MARCHED: [8, 16],
  RECOMMENDEDED: [],
  CONSIDERING: [1, 9],
  REJECTED: [3, 11, 13],
  CANCEL: [2, 5, 7, 10, 15]
};

class FistarMyCampaign extends Component {
  constructor(props) {
    super(props);
    this.state = {
      campaigns: [],
      matching: 0,
      ready: 0,
      ongoing: 0,
      closed: 0,
      total: 0,
      isLoadingCampaign: false,
      last_page: 1,
      page: 1,
      hasMore: true,
      isRedirectMyCamPaign: false,
      loading: false,
      show: false,
      step: null,

    };
  }

  componentDidMount() {
    this.setState(
      {
        isLoadingCampaign: true
      },
      () => {
        this.props.actions.getMyCampaignDashBoard(this.state.page).then(response => {
          this.setState({
            campaigns: response.data,
            matching: response.matching,
            ready: response.ready,
            ongoing: response.ongoing,
            closed: response.closed,
            total: response.total,
            isLoadingCampaign: false,
            last_page: response.last_page
          });
        });
      }
    );
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.reset) {
      this.props.actions.getMyCampaignDashBoard(1).then(response => {
        this.setState({
          campaigns: response.data,
          matching: response.matching,
          ready: response.ready,
          ongoing: response.ongoing,
          closed: response.closed,
          total: response.total,
          isLoadingCampaign: false,
          last_page: response.last_page
        });
      });
      this.props.changeReset();
    }
  }

  fetchMoreData = () => {
    if (this.state.page >= this.state.last_page) {
      this.setState({ hasMore: false });
      return;
    }
    this.props.actions.getMyCampaignDashBoard(this.state.page + 1).then(response => {
      this.setState({
        campaigns: [...this.state.campaigns, ...response.data],
        page: response.current_page
      });
    });
  };

  clickMore = () => {
    this.setState({
      isRedirectMyCamPaign: true
    });
  };

  renderCampaign = () => {
    const { campaigns } = this.state;
    return (campaigns.slice(0, 5) || []).map((campaign, key) => {

         
            
      return (
        
        <Fragment key={key}>
          {console.log(campaign,'test code')
          }
        <Campaign
          {...this.props}
          campaign={campaign}
          auth={this.props.auth}
        />
      </Fragment>
      );
    });
  };

  render() {
    const { t, height, count } = this.props;
    const {
      campaigns,
      isLoadingCampaign,
      isRedirectMyCamPaign
    } = this.state;
    const {auth} = this.props;
    if (isRedirectMyCamPaign) {
      return <Redirect to={routeName.FISTAR_MY_CAMPAIGN} />;
    }
    this.props.actions.getCode("location");
    return (
      <Fragment>
        <div className="header-track">
          <div className="top">
            <h4>fistar {auth.name}'s {t("DASHBOARD_FISTAR.DFR_campaign")}</h4>
            {!(campaigns.length === 0 && !isLoadingCampaign) && (
              <button onClick={this.clickMore}>
                {t("DASHBOARD_FISTAR.DFR_button_more")}
              </button>
            )}
          </div>
          <div className="amount-notification">
            <h2>
              {count
                ? count.matching + count.ready + count.ongoing + count.closed
                : ""}
            </h2>
          </div>
          <div className="list-amount">
            <div className="item">
              <p>
                <span>{count ? count.matching : ""}</span>
              </p>
              <span>{t("DASHBOARD_FISTAR.DFR_status_matching")}</span>
            </div>
            <div className="item">
              <p>
                <span>{count ? count.ready : ""}</span>
              </p>
              <span>{t("DASHBOARD_FISTAR.DFR_status_ready")}</span>
            </div>
            <div className="item">
              <p>
                <span>{count ? count.ongoing : ""}</span>
              </p>
              <span>{t("DASHBOARD_FISTAR.DFR_status_ongoing")}</span>
            </div>
            <div className="item closed">
              <p>
                <span>{count ? count.closed : ""}</span>
              </p>
              <span>{t("DASHBOARD_FISTAR.DFR_status_close")}</span>
            </div>
          </div>
        </div>
        <div className="">
          {campaigns.length === 0 && !isLoadingCampaign ? (
            <Fragment>
              <div className={"text-center mt-5"}>No campaign</div>
            </Fragment>
          ) : (
            <InfiniteScroll
              dataLength={this.state.campaigns.length}
              next={this.fetchMoreData}
              loader={<h4>{t("LOADING.LOADING")}</h4>}
              hasMore={this.state.hasMore}
              height={height}
              className="new-card-matching"
            >
              {this.renderCampaign()}
            </InfiniteScroll>
          )}
        </div>
      </Fragment>
    );
  }
}

class Campaign extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      step: null,
      loading: false,
      fime: "",
      instagram: "",
      facebook: "",
      youtube: ""
    };

  }

  onClickNextStep = (m_id, stt_id) => {
    if (!this.state.loading) {
      this.setState(
        {
          loading: true
        },
        () => {
          this.props.actions
            .nextStep(m_id, stt_id)
            .then(response => {
              this.setState({
                step: stt_id,
                loading: false
              });
            })
            .catch(er => {
              this.setState({
                loading: false
              });
            });
        }
      );
    }
  };

  renderButtonNextStep = (m_id, step) => {
    let nextSteps = STATUS_STEP_FITAR[step];
    return (
      <Fragment>
        {nextSteps.button.map((nextStep, key) => (
          <button
            key={key}
            type={"button"}
            onClick={() => this.onClickNextStep(m_id, nextStep.id)}
          >
            {this.state.loading ? (
              <div className="spinner-border" role="status">
                {/*<span className="sr-only">Loading...</span>*/}
              </div>
            ) : (
              <Fragment>{nextStep.text}</Fragment>
            )}
          </button>
        ))}
      </Fragment>
    );
  };

  renderStatus = () => {
    const {
      t,
      auth,
      campaign
    } = this.props;
    const { matchings } = campaign
    if (matchings.length === 0) {
      return <div className="process-campaign progress-mycampaign" />;
    }
    let matchingWithFistarCurrent = matchings.filter(
      matching_id => matching_id.influencer.email == auth.email
    );
    if (matchingWithFistarCurrent.length === 0) {
      return <div className="process-campaign progress-mycampaign" />;
    }
    const { label, m_id } = matchingWithFistarCurrent[0].matching_status;
    let { stt_id, fistar_status } = label;
    const { step } = this.state;
    if (step) {
      fistar_status = STATUS_STEP_FITAR[step].display;
      stt_id = step;
    }
    return (
      <div className="process-campaign">
        <span className="icon-start" />
        {fistar_status.split("→").map((status, key) => {
          if (fistar_status.split("→").length === key + 1) {
            const final_status = status.replace(" ",'');
            final_status == "Matched" ? this.matchings = true : this.matchings = false;
            return (
              <Fragment key={key}>
                <p className="font-weight-bold text-white">{status}</p>
              </Fragment>
            );
          }
          return (
            <Fragment key={`${key}`}>
              <p className="text">{status}</p>
              {fistar_status.split("→").length - 1 !== key && <span />}
            </Fragment>
          );
        })}
        {!FISTER_FINISH_STEP.includes(stt_id) && campaign.cp_status !== CAMPAIGN_STATUS.CLOSED && (
          <i className="fas fa-long-arrow-alt-right" />
        )}
        {campaign.cp_status !== CAMPAIGN_STATUS.CLOSED && this.renderButtonNextStep(m_id, stt_id)}
      </div>
    );
  };

  OpenModal = e => {
    e.preventDefault();
    this.setState({ show: true });
  };

  handleClose = () => {
    this.setState({ show: false });
  };

  onCloseModal = () => {
    this.setState({ show: !this.state.show });
  };

  getReviewStatusText = rv_status => {
    switch (rv_status) {
      case SNS_STATE.READY:
      case SNS_STATE.REQUEST:
      case SNS_STATE.MODIFY:
        return "Preparing";
      case SNS_STATE.CHECKING:
        return "Considering";
      case SNS_STATE.COMPLETE:
        return "Completed";
      default:
        return "";
    }
  };

  renderStatusCampaign = () => {
    const {
      t,
      auth,
      campaign: { matchings }
    } = this.props;
    var cp_status = this.props.campaign.cp_status;
    var fime = "";
    var urlFime = "";
    var facebook = "";
    var urlFacebook = "";
    var youtube = "";
    var urlYoutube = "";
    var instagram = "";
    var urlInstagram = "";

    if (!matchings || !matchings[0]) {
      fime = "";
      facebook = "";
      youtube = "";
      instagram = "";
    }
    let matchingsFistar = matchings.filter(
      matching => matching.influencer.email == auth.email
    );

    let channel =
      matchingsFistar && matchingsFistar[0]
        ? matchingsFistar[0].matching_channel
        : "";

    channel &&
      channel.map(channel => {
        if (
          channel.sns_id == 1 &&
          channel.m_ch_selected == 1 &&
          channel.review_status
        ) {
          if (channel.m_ch_url && !channel.m_ch_active_url) {
            fime = this.getReviewStatusText(103);
          } else {
            fime = this.getReviewStatusText(channel.review_status.rv_status);
          }
          urlFime= channel.m_ch_url;
        }
        if (
          channel.sns_id == 2 &&
          channel.m_ch_selected == 1 &&
          channel.review_status
        ) {
          if (channel.m_ch_url && !channel.m_ch_active_url) {
            facebook = this.getReviewStatusText(103);
          } else {
            facebook = this.getReviewStatusText(channel.review_status.rv_status);
          }
          urlFacebook= channel.m_ch_url;
        }
        if (
          channel.sns_id == 3 &&
          channel.m_ch_selected == 1 &&
          channel.review_status
        ) {
          if (channel.m_ch_url && !channel.m_ch_active_url) {
            youtube = this.getReviewStatusText(103);
          } else {
            youtube = this.getReviewStatusText(channel.review_status.rv_status);
          }
          urlYoutube= channel.m_ch_url;
        }
        if (
          channel.sns_id == 4 &&
          channel.m_ch_selected == 1 &&
          channel.review_status
        ) {
          if (channel.m_ch_url && !channel.m_ch_active_url) {
            instagram = this.getReviewStatusText(103);
          } else {
            instagram = this.getReviewStatusText(channel.review_status.rv_status);
          }
          urlInstagram= channel.m_ch_url;
        }
      });

    return (
      <div className="price-matching">
        <div className="item-price">
          <div className="icon-solical">
            <span>
              <img src={imageRead} alt="" />
            </span>
          </div>
          {fime ? (cp_status == 62 ? <a className="item-link-complete" target="_blank" href={urlFime ? urlFime : "javascript:void(0)"}>Open Review</a> :(
            <button>{fime}</button>
          )) : (
            <h6 className="texts text-center">-</h6>
          )}
        </div>
        <div className="item-price">
          <div className="icon-solical">
            <span>
              <img src={imageFace} alt="" />
            </span>
          </div>
          {facebook ? (cp_status == 62 ? <a className="item-link-complete" target="_blank" href={urlFacebook ? urlFacebook : "javascript:void(0)"}>Open Review</a> :(
            <button>{facebook}</button>
          )) : (
            <h6 className="texts text-center">-</h6>
          )}
        </div>
        <div className="item-price">
          <div className="icon-solical">
            <span>
              <img src={imageIntar} alt="" />
            </span>
          </div>
          {instagram ? (cp_status == 62 ? <a className="item-link-complete" target="_blank" href={urlInstagram ? urlInstagram : "javascript:void(0)"}>Open Review</a> : (
            <button>{instagram}</button>
          )) : (
            <h6 className="texts text-center">-</h6>
          )}
        </div>
        <div className="item-price">
          <div className="icon-solical">
            <span>
              <img src={imageToutube} alt="" />
            </span>
          </div>
          {youtube ? (cp_status == 62 ? <a className="item-link-complete" target="_blank" href={urlInstagram ? urlInstagram : "javascript:void(0)"}>Open Review</a> : (
            <button>{youtube}</button>
          )) : (
            <h6 className="texts text-center">- </h6>
          )}
        </div>
      </div>
    );
  };

  renderAlet(){
    if (this.matchings) {
      return (
        <div className="matched_status">
          <p className="matched">Matched</p>
          <p className="note">Writing fistar review</p>
        </div>
      )
    } else {
      return (
        <div className="matched_status">
          <p className="matched">Not Matched</p>
          <p className="note">Waiting Partner Approve</p>
        </div>
      )
    }
  }

  render() {
    const { t, campaign } = this.props;
    let status = "";
    switch (campaign.cp_status) {
      case 59: {
        status = `${t("CAMPAIGN_SEARCH.CSH_matching")}`;
        break;
      }
      case 60: {
        status = `${t("CAMPAIGN_SEARCH.CSH_ready")}`;
        break;
      }
      case 61: {
        status = `${t("CAMPAIGN_SEARCH.CSH_on-going")}`;
        break;
      }
      case 62: {
        status = "Closed";
        break;
      }
      default: {
        break;
      }
    }
    let apply = (campaign.matchings || []).map(fistar => {
      let status = fistar.matching_status.m_status;
      if (MATCHING.APPLY.includes(status)) {
        return fistar;
      }
      return null;
    });
    apply = apply.filter(e => e !== null);

    let applyMarched = (campaign.matchings || []).map(fistar => {
      let status = fistar.matching_status.m_status;
      if (MATCHING_STATUS.MARCHED.includes(status)) {
        return fistar;
      }
      return null;
    });
    applyMarched = applyMarched.filter(e => e !== null);

    let recruit = campaign.cp_total_influencer || 0;
    let dataFistar = "";
    if (campaign.cp_status == 59) {
      dataFistar = campaign.matchings;
    } else {
      dataFistar = applyMarched;
    }
    return (
      <div className="change-new-dashboard">
      <div className="list-campaign-track">
      <div className="item">
        <div className="img-matching">
          <Link to={`/fi-star/my-campaign-detail/${campaign.cp_slug}`}>
            <CampaignMainImage campaign={campaign} />
          </Link>
          <Scrap
            scraps_count={campaign.scraps_count}
            is_scrap={campaign.is_scrap}
            id={campaign.cp_id}
            className="btn over-flow"
            iconClassName={"fa-heart"}
          />
        </div>
        <div className="status-campaign">
          <Link to={`/fi-star/my-campaign-detail/${campaign.cp_slug}`}>
            <div className="name">
              <div className="left">
                <span className="wrap-matching">{status}</span>
                <h4>{campaign.cp_name}</h4>
              </div>
              <div className="right">
                <a href="javascript:void(0)">
                  {t("CAMPAIGN_SEARCH.CSH_open")}
                  <span>{campaign.cp_period_start.split(" ")[0]}</span>
                </a>
              </div>
            </div>
          </Link>
          {this.renderStatus()}
          <div className="fistar-apply">
            <h4>fiStar {campaign.cp_total_influencer}</h4>
            <div className="user-apply" onClick={this.OpenModal}>
              <p>
                <span>{(dataFistar || []).length}</span>{" "}
                {t("CAMPAIGN_SEARCH.CSH_applied")}
              </p>
              <FistarApply matchingData={dataFistar} />
            </div>
            {this.renderAlet()}

          </div>
          {this.renderStatusCampaign()}
        </div>

        <Modal
          size="lg"
          show={this.state.show}
          onHide={this.handleClose}
          aria-labelledby="example-modal-sizes-title-lg"
          dialogClassName="popup-history"
        >
          <ModalStatusCampaign
            onClose={this.onCloseModal}
            recruit={recruit}
            apply={dataFistar}
            scraps={campaign.scraps}
          />
        </Modal>
      </div>
      </div>
      </div>
    );
  }
}


const mapStateToProps = state => {
  return {
    auth:state.auth,
    // getCode: type => state(getCodeAction(type)),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    actions: {
      getCode: (type) => dispatch(getCodeAction(type)),
      getMyCampaignDashBoard: (page = 1) => dispatch(getMyCampaignDashBoardAction(page)),
      nextStep: (m_id, stt_id) => dispatch(nextStepAction(m_id, stt_id))
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(FistarMyCampaign));
