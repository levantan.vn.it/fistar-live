import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import { withTranslation, Trans } from "react-i18next";
import { connect } from "react-redux";
import { IMAGE_SIZE, IMAGE_TYPE } from "../../../../constants";
import { getImageLink } from "../../../../common/helper";

class FistarApply extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { t } = this.props;
    let dataApply = this.props.matchingData;
    return (
      <Fragment>
        <div className="gr-user">
          {(dataApply || []).map((fistar, key) => (
            <div className="user" key={key}>
              <a href="javascript:void(0)">
                <img
                  src={getImageLink(
                    fistar.influencer.picture,
                    IMAGE_TYPE.FISTARS,
                    IMAGE_SIZE.ORIGINAL
                  )}
                  alt={fistar.influencer.fullname}
                />
              </a>
            </div>
          ))}
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(FistarApply));
