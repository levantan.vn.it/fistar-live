import React, { Component, Fragment } from "react";
import ReactDOM from "react-dom";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect, Route } from "react-router-dom";
import { connect } from "react-redux";

// import calendar style
// You can customize style by copying asset folder.
import "@y0c/react-datepicker/assets/styles/calendar.scss";
import { format } from "date-fns";
// import PushMessageComponent from './../../../components/PushMessage'

import imgaeIdol from "../../../../images/dash-02.png";
import imageClose from "../../../../images/close-popup.svg";
import imageFb from "../../../../images/face.png";
import imageYt from "../../../../images/toutube.png";
import imageInstar from "../../../../images/intar.png";
import imageMk from "../../../../images/mk.png";
import imageThum from "../../../../images/thumnaildetail.png";

import { Modal, Tabs, Tab, Nav } from "react-bootstrap";
import "./ModalStatusCampaign.css";
import { getImageLink, nFormatter } from "./../../../../common/helper";
import { IMAGE_SIZE, IMAGE_TYPE } from "./../../../../constants/index";

const GENDER = {
  MALE: 9,
  FEMALE: 10
};

class ModalStatusCampaign extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false
    };
  }

  handleClose = () => {
    this.setState({ show: !this.props.onClose() });
  };

  renderFistar = (fistar, location) => {
    var locationAddress = '';
    var locationChange = '';
    if (location) {
       locationAddress = location.code.filter(
        location => location.cd_id == (fistar.location.cd_id ? fistar.location.cd_id : fistar.location)
      );
       locationChange =
        locationAddress.length > 0
          ? locationAddress[0].cd_label
          : fistar.location;
  
    }
   
    var fime = "";
    var facebook = "";
    var youtube = "";
    var instagram = "";
    fistar &&
      fistar.channels &&
      fistar.channels.map(channel => {
        if (channel.sns_id == 1) {
          fime = channel.usn_follower;
        }
        if (channel.sns_id == 2) {
          facebook = channel.usn_follower;
        }
        if (channel.sns_id == 3) {
          youtube = channel.usn_follower;
        }
        if (channel.sns_id == 4) {
          instagram = channel.usn_follower;
        }
      });

    return <div className="card">
      <div className="top">
        <div className="left-card">
          <div className="male">
            {fistar.gender === GENDER.MALE ? (
              <i className="fas fa-mars" aria-hidden="true" />
            ) : (
              <i className="fa fa-venus" aria-hidden="true" />
            )}
          </div>
          <div className="address-old">
            <h4>
              <a  className="disabled-hover">
                {fistar.fullname}
              </a>
            </h4>
            <span>{fistar.dob.split("-")[0]}</span>
            <span className="address">{locationChange ? locationChange: '' }</span>
          </div>
        </div>
      </div>
      <div className="img-partner">
        <a >
          {" "}
          <img
            src={getImageLink(
              fistar.picture,
              IMAGE_TYPE.FISTARS,
              IMAGE_SIZE.ORIGINAL
            )}
            alt={fistar.fullname}
          />
        </a>
        <div className="btn over-flow">
          <i className="fas fa-star" />
          <span>
            {(fistar.scraps_count || 0).toLocaleString()}
          </span>
        </div>
      </div>
      <div className="card-footer">
        <ul className="list-footer">
          <li className="item">
            <a>
              <i className="fab fa-facebook-f" />
            </a>
            <span>{nFormatter(facebook, 0)}</span>
          </li>
          <li className="item">
            <a>
              <i className="fab fa-instagram" />
            </a>
            <span>{nFormatter(instagram, 0)}</span>
          </li>
          <li className="item">
            <a>
              <i className="fab fa-youtube-square" />
            </a>
            <span>{nFormatter(youtube, 0)}</span>
          </li>
        </ul>
      </div>
    </div>
  }

  renderTabFiStar = fistars => {
    const {
      code: {
        data: { location }
      }
    } = this.props;
    return (
      <div className="information-user">
        <div className="card-groups">
          {fistars.map((fistar, key) => {
            return !fistar.influencer ? null : (
              <Fragment key={key}>
                {this.renderFistar(fistar.influencer, location)}
              </Fragment>
            );
          })}
        </div>
      </div>
    );
  };

  renderTabScrap = () => {
  
    const {
      code: {
        data: { location }
      },
      scraps
    } = this.props;

    return (
      <div className="information-user">
        <div className="card-groups">
          {scraps.map((fistar, key) => {
            return !fistar ? null : (
              <Fragment key={key}>
                {this.renderFistar(fistar, location)}
              </Fragment>
            );
          })}
         </div>
      </div>
    );
  };

  render() {
    const { t, recruit, apply, scraps } = this.props;
    const { height } = this.state;
    return (
      <Fragment>
        <div className="popup-fistar-status">
          <div className="modal-header">
            <h4 id="exampleModalLabel">{t("POPUP_FISTAR_STATUS.PFS_title")}</h4>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
              onClick={this.handleClose}
            >
              <img src={imageClose} alt="" />
            </button>
          </div>
       
          <div className="body-popup">
            <Tab.Container defaultActiveKey={"second"}>
              <div className="content-proceed">
                <div className="tab-content">
                  <Nav variant="pills" className="list-tab">
                    <Nav.Item className="item">
                      <Nav.Link className="link-tab recruiting p-0">
                        <p className="tab-text-title">
                          {t("POPUP_FISTAR_STATUS.PFS_tab_recruiting")}
                        </p>
                        <p className="amount recruiting">{recruit}</p>
                      </Nav.Link>
                    </Nav.Item>
                    <Nav.Item className="item">
                      <Nav.Link eventKey="second" className="link-tab p-0">
                        <p className="tab-text-title">
                          {t("POPUP_FISTAR_STATUS.PFS_tab_fistar_applied")}
                        </p>
                        <p className="amount">{apply.length}</p>
                      </Nav.Link>
                    </Nav.Item>
                    <Nav.Item className="item">
                      <Nav.Link eventKey="third" className="link-tab p-0">
                        <p className="tab-text-title">
                          <i className="fas fa-star" />{" "}
                          {t("POPUP_FISTAR_STATUS.PFS_tab_scrap_fistar")}
                        </p>
                        <p className="amount">{scraps.length}</p>
                      </Nav.Link>
                    </Nav.Item>
                  </Nav>
                </div>
              </div>
              <Tab.Content>
                <div className="containModal">

                <Tab.Pane eventKey="second">
                  <div className="1">
                  {this.renderTabFiStar(apply)}

                   </div>
                </Tab.Pane>
                <Tab.Pane eventKey="third">
                <div className="1">

                  {this.renderTabScrap()}
                  </div>
                  </Tab.Pane>
                </div>

              </Tab.Content>
            </Tab.Container>
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    code: state.code
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(ModalStatusCampaign));
