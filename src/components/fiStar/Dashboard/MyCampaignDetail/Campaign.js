import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "./Campaign.css";
import { nextStepAction } from "./../../../../store/actions/campaign";

import imgaeIdol from "./../../../../images/dash-02.png";
import imageFace from "./../../../../images/dashboard/face.png";
import imageIntar from "./../../../../images/dashboard/intar.png";
import imageToutube from "./../../../../images/toutube.png";
import imageRead from "./../../../../images/mk.png";
import { CAMPAIGN_STATUS } from "./../../../../constants";
import { getImageLink } from "./../../../../common/helper";
import { IMAGE_SIZE, IMAGE_TYPE, FISTER_FINISH_STEP,} from "./../../../../constants/index";
import { applyNewCampaignAction } from "../../../../store/actions/campaign";
import { getId, DateFormatYMDDatePickerPlus7Day } from "../../../../common/helper";
import { STATUS_STEP_FITAR } from "./../../../../constants";
import moment from 'moment'

const TYPE = {
  ALL: "",
  MATCHING: "matching",
  READY: "ready",
  ONGOING: "ongoing",
  CLOSED: "closed"
};

const MATCHING_STATUS = {
  MARCHED: [8, 16],
  RECOMMENDEDED: [],
  CONSIDERING: [1, 9],
  REJECTED: [3, 11, 13],
  CANCEL: [2, 5, 7, 10, 15]
};

const MATCHING = {
  REQUEST: [1, 2, 3, 4, 5, 6, 7, 8],
  APPLY: [9, 10, 11, 12, 13, 14, 15, 16]
};

class CampaignDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isApplied: false,
      loading: false
    };
  }

  applyNewCampaign = campaign => {
    this.setState(
      {
        loading: true
      },
      () => {
        this.props.actions
          .applyNewCampaign(campaign.cp_id)
          .then(response => {
            this.setState({
              isApplied: true
              // loading: false
            });
            this.props.reloadCampaign(campaign.cp_slug);
          })
          .catch(e => {
            alert("You already applied for this campaign !");
          });
      }
    );
  };

  renderStatus = () => {
    const {  t, auth, campaign } = this.props;
    let matchings = campaign.matchings;
    if (matchings) {
      if (matchings.length === 0) {
        return <div className="process-campaign progress-mycampaign" />;
      }
      let matchingWithFistarCurrent = matchings.filter(
        matching_id => matching_id.influencer.email == auth.email
      );
      if (matchingWithFistarCurrent.length === 0) {
        return <div className="process-campaign progress-mycampaign" />;
      }
      const { label, m_id } = matchingWithFistarCurrent[0].matching_status;
      let { stt_id, fistar_status } = label;
      const { step } = this.state;
      if (step) {
        fistar_status = STATUS_STEP_FITAR[step].display;
        stt_id = step;
      }
      return (
        <div className="process-campaign">
          {/* <span className="icon-start" />
          {fistar_status.split("→").map((status, key) => {
            if (fistar_status.split("→").length === key + 1) {
              return (
                <Fragment key={key}>
                  <p className="font-weight-bold text-white">{status}</p>
                </Fragment>
              );
            }
            return (
              <Fragment key={`${key}`}>
                <p className="text">{status}</p>
                {fistar_status.split("→").length - 1 !== key && <span />}
              </Fragment>
            );
          })} */}
          {/* {!FISTER_FINISH_STEP.includes(stt_id) && campaign.cp_status !== CAMPAIGN_STATUS.CLOSED && (
            <i className="fas fa-long-arrow-alt-right" />
          )} */}
          {campaign.cp_status !== CAMPAIGN_STATUS.CLOSED && this.renderButtonNextStep(m_id, stt_id)}
        </div>
      );
    };
  }

  renderButtonNextStep = (m_id, step) => {
    let nextSteps = STATUS_STEP_FITAR[step];
    return (
      <Fragment>
        {nextSteps.button.map((nextStep, key) => (
          <button
            key={key}
            type={"button"}
            onClick={() => this.onClickNextStep(m_id, nextStep.id)}
          >
            {this.state.loading ? (
              <div className="spinner-border" role="status">
                {/*<span className="sr-only">Loading...</span>*/}
              </div>
            ) : (
              <Fragment>{nextStep.text}</Fragment>
            )}
          </button>
        ))}
      </Fragment>
    );
  };

  onClickNextStep = (m_id, stt_id) => {
    if (!this.state.loading) {
      this.setState(
        {
          loading: true
        },
        () => {
          this.props.actions
            .nextStep(m_id, stt_id)
            .then(response => {
              this.setState({
                step: stt_id,
                loading: false
              });
            })
            .catch(er => {
              this.setState({
                loading: false
              });
            });
        }
      );
    }
  };

  renderApply = () => {
    const { t, campaign, auth } = this.props;
    let isApplied = false;
    let a = "";

    if (campaign) {
      console.log(campaign.matchings, auth);
      (campaign.matchings || []).map(fistar => {
        let emailFistar = fistar.influencer.email;
        if (auth.email.trim() == emailFistar.trim()) {
          isApplied = true;
        }
      });
    } else {
      isApplied = true;
    }

    return (
      campaign.cp_status === 59 &&
      isApplied == false && (
        <button
          className="ok-campaign"
          onClick={() => this.applyNewCampaign(campaign)}
        >
          {this.state.loading ? (
            <div className="spinner-border" role="status">
              <span className="sr-only">Loading...</span>
            </div>
          ) : (
            "Apply"
          )}
        </button>
      )
    );
  };

  renderSchedule(campaign){
    let activeSchedule = null;
    let createDate = moment(campaign.created_at).format('YYYY-MM-DD');
    let startDate = moment(campaign.cp_period_start).format('YYYY-MM-DD');
    let threeDateAfter = moment(startDate).add(3, 'day').format('YYYY-MM-DD');
    let endDate = moment(campaign.cp_period_end).format('YYYY-MM-DD');
    let schedule = [
      ['Matching',createDate, startDate],
      ['Ready',startDate, threeDateAfter],
      ['Ongoing',threeDateAfter, endDate],
      ['Closed',endDate, null]];
    if(campaign.cp_status == 59 ) { activeSchedule = 0 };
    if(campaign.cp_status == 60 ) { activeSchedule = 1 };
    if(campaign.cp_status == 61 ) { activeSchedule = 2 };
    if(campaign.cp_status == 62  ) { activeSchedule = 3 };
    return (
      <table className="schedule">
        <td className="first_col" valign="middle">Schedule</td>
        <td className="last_col">
          {schedule.map((sche, index) => {
              if (activeSchedule == index) {
                return( 
                <tr className="activeSchedule">
                    <td className="name_col">{sche[0]}</td>
                    <td className="sche_col">{sche[1]} ~ {sche[2]}</td>
                </tr> );
              } else {
                return(  
                <tr className="nor_schedule">
                    <td className="name_col">{sche[0]}</td>
                    <td className="sche_col">{sche[1]} ~ {sche[2]}</td>
                </tr> 
                );
              }
            })
          }
        </td>
      </table>   
    )
  }

  render() {
    const { t, campaign, auth } = this.props;
    let status = "";
    switch (campaign.cp_status) {
      case CAMPAIGN_STATUS.MATCHING: {
        status = "Matching";
        break;
      }
      case CAMPAIGN_STATUS.READY: {
        status = "Ready";
        break;
      }
      case CAMPAIGN_STATUS.ONGOING: {
        status = "On-Going";
        break;
      }
      case CAMPAIGN_STATUS.CLOSED: {
        status = "Closed";
        break;
      }
      default: {
        break;
      }
    }
    let matching = [];

    let apply = (campaign.matchings || []).map(fistar => {
      let status = fistar.matching_status.m_status;

      matching.push(fistar);
      if (MATCHING.APPLY.includes(status)) {
        return fistar;
      }
      return null;
    });
    apply = apply.filter(e => e !== null);

    let applyMarched = (campaign.matchings || []).map(fistar => {
      let status = fistar.matching_status.m_status;

      matching.push(fistar);
      if (MATCHING_STATUS.MARCHED.includes(status)) {
        return fistar;
      }
      return null;
    });
    applyMarched = applyMarched.filter(e => e !== null);

    let dataFistar = "";
    if (campaign.cp_status == 59) {
      dataFistar = campaign.matchings;
    } else {
      dataFistar = applyMarched;
    }

    let attachments = (campaign.attachments || [])
      .filter(e => e != null)
      .map(e => ({
        url: getImageLink(
          e.cp_attachment_url,
          IMAGE_TYPE.ATTACHMENTS,
          IMAGE_SIZE.ORIGINAL
        ),
        thumb: getImageLink(
          e.cp_attachment_url,
          IMAGE_TYPE.ATTACHMENTS,
          IMAGE_SIZE.THUMBNAIL
        ),
        type: e.cp_attachment_type
      }));
    let previewAttachments = [];
    if (!!campaign) {
      previewAttachments = [
        {
          url: getImageLink(
            campaign.cp_main_image,
            IMAGE_TYPE.CAMPAIGNS,
            campaign.cp_main_image_type == 3
              ? IMAGE_TYPE.ORIGINAL
              : IMAGE_SIZE.ORIGINAL
          ),
          thumb:
            campaign.cp_main_image_type == 3
              ? getImageLink(campaign.cp_video_thumb, IMAGE_TYPE.CAMPAIGNS)
              : getImageLink(
                  campaign.cp_main_image,
                  IMAGE_TYPE.CAMPAIGNS,
                  IMAGE_SIZE.THUMBNAIL
                ),
          type: campaign.cp_main_image_type
        },
        ...attachments
      ];
    }

    const settings = {
      customPaging: function(i) {
        let preview = "";
        switch (+previewAttachments[i].type) {
          case 1: {
            preview = <img src={previewAttachments[i].thumb} />;
            break;
          }
          case 2: {
            preview = (
              <img
                src={
                  "//img.youtube.com/vi/" +
                  getId(previewAttachments[i].thumb) +
                  "/0.jpg"
                }
              />
            );
            break;
          }
          case 3: {
            preview = <img src={previewAttachments[i].thumb} />;
            break;
          }
          default:
            break;
        }
        return <a>{preview}</a>;
      },
      dots: true,
      dotsClass: "slick-dots slick-thumb",
      infinite: true,
      // lazyLoad: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      adaptiveHeight: true
    };

    return (
      <div className="wrapper-information">
        <div className="left">
          <div className="photo-main">
            <Slider {...settings}>
              {previewAttachments.map((attachment, key) => {
                let preview = "";
                switch (+attachment.type) {
                  case 1: {
                    preview = <img src={attachment.url} className="w-100" />;
                    break;
                  }
                  case 2: {
                    preview = (
                      <div className="text-center">
                        <iframe
                          id="myIframe"
                          width="500"
                          className="react-player"
                          height="420"
                          frameBorder="0"
                          src={
                            attachment.url
                              ? "//www.youtube.com/embed/" +
                                getId(attachment.url)
                              : "//www.youtube.com/embed/"
                          }
                          allowFullScreen
                        />
                      </div>
                    );
                    break;
                  }
                  case 3: {
                    preview = !this.state.reloadVideo ? (
                      <div className="text-center">
                        <video
                          ref={this.previewVideo}
                          className="w-100"
                          controls
                        >
                          <source
                            ref={this.previewVideoSource}
                            src={attachment.url}
                            type="video/mp4"
                          />
                          Your browser does not support HTML5 video.
                        </video>
                      </div>
                    ) : null;
                    break;
                  }
                  default:
                    break;
                }
                return <div key={key}>{preview}</div>;
              })}
            </Slider>
            {/*<a href="javascript:void(0)">
              <img src={campaign.cp_image} alt={campaign.cp_name} title={campaign.cp_image_title} />
            </a>*/}
            <div className="btn over-flow">
              <i className="fas fa-heart" />{" "}
              <span>{(campaign.scraps_count || 0).toLocaleString()}</span>
            </div>
          </div>
          {/*<div className="image-click">
            <div className="item">
              <img src="https://images.unsplash.com/photo-1464863979621-258859e62245?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80" alt="" className="active" />
            </div>
            <div className="item">
              <img src="https://images.unsplash.com/photo-1464863979621-258859e62245?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80" alt="" />
            </div>
          </div>*/}
        </div>
        <div className="right">
          <div className="top top-campaign-detail">
            <div className="name left-campai-detail">
              <span className="wrap-matching">{status}</span>
              <div className="text-campaign">
                <p>{!campaign.branch ? campaign.cp_brand : campaign.branch.CODE_NM}</p>
                <h4>{campaign.cp_name || ""}</h4>
              </div>
            </div>
            <div className="date-open">
              <span>Open day</span>
              <p>{(campaign.cp_period_start || "").split(" ")[0]}</p>
            </div>
          </div>
          <div
            className="text"
            dangerouslySetInnerHTML={{ __html: campaign.cp_description }}
          />
          <div className="seleted">
            {(campaign.keywords || []).map((keyword, key) => (
              <a key={key}>{keyword.code.cd_label}</a>
            ))}
          </div>
          <div className="information-review">
            <div className="item-review">
              <div className="key-name">
                <p className="key">{t("MY_CAMPAIGN_DETAIL.MCD_category")}</p>
                <p className="name text-center">
                  {campaign.catalog
                    ? campaign.catalog.CODE_NM
                    : ""}
                </p>
              </div>
              <div className="key-name">
                <p className="key">{t("MY_CAMPAIGN_DETAIL.MCD_period")}</p>
                <p className="name text-center pl-2 pr-2">
                  {/*{new Date() < new Date(campaign.cp_period_start) &&*/}
                  {/*campaign.cp_status == 59*/}
                    {/*? (campaign.cp_period_start || "").split(" ")[0] +*/}
                      {/*" ~ " +*/}
                      {/*(campaign.cp_period_end || "").split(" ")[0]*/}
                    {/*: new Date() > new Date(campaign.cp_period_end)*/}
                      {/*? "Complete"*/}
                      {/*: "Going"}*/}


                    {campaign.cp_status == 59 ? (
                        <span className="item-period">
                      <label>
                        {(campaign.cp_period_start || "")}{" "}
                      </label>
                      {/*<label> ~ </label>*/}
                      <label>
                        {(campaign.cp_period_end || "")}
                      </label>{" "}
                    </span>
                    ) : campaign.cp_status == 62 ? (
                        "Complete"
                    ) : (
                        "Going"
                    )}

                </p>
              </div>
              <div className="amount-campaign">
                <p className="key">
                  {t("MY_CAMPAIGN_DETAIL.MCD_recruiting_numer")}
                </p>
                <p className="name ">
                  fiStar {campaign.cp_total_influencer} people
                  <span className="name-applied">
                    / {(dataFistar || []).length}{" "}
                    {t("MY_CAMPAIGN_DETAIL.MCD_applied")}
                  </span>
                </p>
              </div>
              <div className="amount-campaign">
                <p className="key">
                  {[CAMPAIGN_STATUS.READY, CAMPAIGN_STATUS.ONGOING].includes(
                    campaign.cp_status
                  )
                    ? `${t("MY_CAMPAIGN_DETAIL.MCD_matching_fiStar")}`
                    : `${t("MY_CAMPAIGN_DETAIL.MCD_applied")}`}
                </p>
                <div className="user-apply name">
                  <div className="gr-user">
                    {[CAMPAIGN_STATUS.READY, CAMPAIGN_STATUS.ONGOING].includes(
                      campaign.cp_status
                    ) ? (
                      <Fragment>
                        {(dataFistar || []).map((fistar, key) => (
                          <div className="user" key={key}>
                            <a href="javascript:void(0)">
                              <img
                                src={getImageLink(
                                  fistar.influencer.picture,
                                  IMAGE_TYPE.FISTARS,
                                  IMAGE_SIZE.ORIGINAL
                                )}
                                alt={fistar.influencer.fullname}
                              />
                            </a>
                          </div>
                        ))}
                      </Fragment>
                    ) : (
                      <Fragment>
                        {(dataFistar || []).map((fistar, key) => (
                          <div className="user" key={key}>
                            <a href="javascript:void(0)">
                              <img
                                src={getImageLink(
                                  fistar.influencer.picture,
                                  IMAGE_TYPE.FISTARS,
                                  IMAGE_SIZE.ORIGINAL
                                )}
                                alt={fistar.influencer.fullname}
                              />
                            </a>
                          </div>
                        ))}
                      </Fragment>
                    )}
                  </div>
                </div>
              </div>
              <div className="amount-campaign">
                <p className="key">
                  {t("MY_CAMPAIGN_DETAIL.MCD_product_price")}
                </p>
                <p className="name">
                  <span className={campaign.cp_campaign_price ? "price" : ""}>
                    {(+campaign.cp_product_price || 0).toLocaleString()} VND{" "}
                  </span>
                  {campaign.cp_campaign_price ? (
                    <span>
                      {(+campaign.cp_campaign_price || 0).toLocaleString()} VND{" "}
                    </span>
                  ) : (
                    ""
                  )}
                </p>
              </div>
            </div>
          </div>
          {this.renderSchedule(campaign)}
          {this.renderApply()}
        </div>
          <div className="status-campaign-detail">
            {this.renderStatus()}
          </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth
  };
}

const mapDispatchToProps = dispatch => {
  return {
    actions: {
      applyNewCampaign: id => dispatch(applyNewCampaignAction(id)),
      nextStep: (m_id, stt_id) => dispatch(nextStepAction(m_id, stt_id))
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(CampaignDetail));
