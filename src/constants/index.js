export const CAMPAIGN_STATUS = {
  MATCHING: 59,
  READY: 60,
  ONGOING: 61,
  CLOSED: 62
};
export const TYPE = {
  ALL: "",
  MATCHING: "matching",
  READY: "ready",
  ONGOING: "ongoing",
  CLOSED: "closed"
};
export const SNS_CHANNEL = {
  FIME: 1,
  FACEBOOK: 2,
  YOUTUBE: 3,
  INSTAGRAM: 4
};
export const FISTAR_TYPE = {
  ALL: "",
  MATCHING: "matching",
  SCRAP: "scrap",
  REQUEST: "request",
  APPLY: "apply",
  RECOMMENDED: "recommended"
};
export const GENDER_TYPE = {
  MALE: 9,
  FEMALE: 10
};
export const SNS = {
  FIME: 1,
  FACEBOOK: 2,
  YOUTUBE: 3,
  INSTAGRAM: 4
};
export const MATCHING_STATUS = {
  MARCHED: [8, 16],
  RECOMMENDEDED: [],
  CONSIDERING: [1, 9],
  REJECTED: [3, 11, 13],
  CANCEL: [2, 5, 7, 10, 15]
};
export const SNS_STATE = {
  READY: 101, // (Write reviews -> 102)
  REQUEST: 102, // (Request to Check -> 103)
  CHECKING: 103, // (check | Request to modify -> 104 | Approve reviews -> 105)
  MODIFY: 104, // (Request to Check -> 103)
  COMPLETE: 105 // (Save SNS URL)
};
export const DATE_FORMAT = "YYYY-MM-DD HH:mm:ss";

export const IMAGE_TYPE = {
  CAMPAIGNS: "campaigns",
  FISTARS: "fistars",
  PARTNERS: "partners",
  ATTACHMENTS: "attachments",
  BANNER: "banners",
  REVIEWS: "reviews"
};

export const IMAGE_SIZE = {
  ORIGINAL: "original",
  THUMBNAIL: "thumbnail",
  MEDIUM: "medium",
  MEDIUM_LARGE: "medium_large",
  LARGE: "large"
};

export const VALIDATION = {
  MAX_LENGTH: 200,
  MAX_NAME_LENGTH: 100,
}

const REDIRECT_FB = 'https://www.facebook.com/v2.3/dialog/oauth?&client_id='
const JOIN_STEP_1 = `${process.env.REACT_APP_URL}/fi-star/join?tab=1`
const JOIN_STEP_2 = `${process.env.REACT_APP_URL}/fi-star/join?tab=2`
const LOGIN = `${process.env.REACT_APP_URL}/login`
const REGISTER = `${process.env.REACT_APP_URL}/fi-star/register`
const FISTAR_INFOMATION = `${process.env.REACT_APP_URL}/fi-star/profile/information`
const SCOPE = `scope=public_profile,user_photos,user_friends,user_posts`
export const REDIRECT_JOIN_STEP_1 =  `${REDIRECT_FB}${process.env.REACT_APP_FACEBOOK_FISTAR_ID}&redirect_uri=` + JOIN_STEP_1
export const REDIRECT_LOGIN =  `${REDIRECT_FB}${process.env.REACT_APP_FACEBOOK_FIME_ID}&redirect_uri=${LOGIN}`
export const REDIRECT_LOGIN_FISTAR =  `${REDIRECT_FB}${process.env.REACT_APP_FACEBOOK_FISTAR_ID}&redirect_uri=${LOGIN}?t=1&${SCOPE}`
export const REDIRECT_UPDATE_FISTAR =  `${REDIRECT_FB}${process.env.REACT_APP_FACEBOOK_FISTAR_ID}&redirect_uri=${FISTAR_INFOMATION}&${SCOPE}`

export const REDIRECT_JOIN_STEP_2 =      `${REDIRECT_FB}${process.env.REACT_APP_FACEBOOK_FISTAR_ID}&redirect_uri=${JOIN_STEP_2}&${SCOPE}`
export const REDIRECT_REGISTER_FISTAR =  `${REDIRECT_FB}${process.env.REACT_APP_FACEBOOK_FISTAR_ID}&redirect_uri=${REGISTER}`


const REJECT = "Reject";
const APPROVE = "Approve";
const RE_REQUEST = "Re-request";
const CANCEL = "Cancel";
const CONFIRM = "Confirm";
export const FISTER_FINISH_STEP = [2, 7, 8, 13, 15, 16]
export const PARTNER_FINISH_STEP = [5, 7, 8, 10, 15, 16]
export const STATUS_STEP_FITAR = {
  1: {
    display: "Partner Request → considering",
    button: [{ id: 3, text: REJECT }, { id: 6, text: APPROVE }]
  },
  2: {
    display: "Partner Request → fiStar considering → partner cancel",
    button: []
  },
  3: {
    display: "Partner Request → reject",
    button: [{ id: 9, text: RE_REQUEST }]
  },
  4: {
    display: "Partner Request → considering",
    button: [{ id: 3, text: REJECT }, { id: 6, text: APPROVE }]
  },
  5: {
    display: "Partner Request → reject",
    button: [{ id: 9, text: RE_REQUEST }]
  },
  6: {
    display: "Partner Request → Approve",
    button: [{ id: 7, text: CANCEL }]
  },
  7: {
    display: "Partner Request → Approve → Cancel",
    button: []
  },
  8: {
    display: "Partner Request → Approve → Confirm → Matched",
    button: []
  },
  9: {
    display: "Apply → Partner considering",
    button: [{ id: 10, text: CANCEL }]
  },
  10: {
    display: "Apply → Partner considering → Cancel",
    button: [{ id: 9, text: RE_REQUEST }]
  },
  11: {
    display: "Apply → Partner reject",
    button: [{ id: 12, text: RE_REQUEST }]
  },
  12: {
    display: "Apply → Partner reject → Re-request",
    button: [{ id: 13, text: CANCEL }]
  },
  13: {
    display: "Apply → Partner reject → Re-request → Cancel",
    button: []
  },
  14: {
    display: "Apply → Partner confirm",
    button: [{ id: 15, text: CANCEL }]
  },
  15: {
    display: "Apply → Partner confirm → cancel",
    button: []
  },
  16: {
    display: "Apply → Partner confirm → Matched",
    button: []
  }
};
export const STATUS_STEP_PARTNER = {
  1: {
    display: "Request → fiStar considering",
    button: [{ id: 2, text: CANCEL }]
  },
  2: {
    display: "Request → fiStar considering → Cancel",
    button: [{ id: 1, text: RE_REQUEST }]
  },
  3: {
    display: "Request → fiStar reject",
    button: [{ id: 4, text: RE_REQUEST }]
  },
  4: {
    display: "Request → fiStar reject → Re-request",
    button: [{ id: 5, text: CANCEL }]
  },
  5: {
    display: "Request → fiStar reject → Re-request → Cancel",
    button: []
  },
  6: {
    display: "Request → fiStar approve",
    button: [{ id: 7, text: CANCEL }, { id: 8, text: CONFIRM }]
  },
  7: {
    display: "Request → fiStar approve → Cancel",
    button: []
  },
  8: {
    display: "Request → fiStar approve → Confirm → Matched",
    button: []
  },
  9: {
    display: "fiStar Apply → Considering",
    button: [{ id: 11, text: REJECT }, { id: 14, text: CONFIRM }]
  },
  10: {
    display: "fiStar Apply → Considering → fistar cancel",
    button: []
  },
  11: {
    display: "fiStar Apply → Reject",
    button: [{ id: 1, text: RE_REQUEST }]
  },
  12: {
    display: "fiStar Apply → Considering",
    button: [{ id: 11, text: REJECT }, { id: 14, text: CONFIRM }]
  },
  13: {
    display: "fiStar Apply → Reject",
    button: [{ id: 1, text: RE_REQUEST }]
  },
  14: {
    display: "fiStar Apply → Confirm",
    button: [{ id: 15, text: CANCEL }, { id: 16, text: APPROVE }]
  },
  15: {
    display: "fiStar Apply → Confirm → cancel",
    button: []
  },
  16: {
    display: "fiStar Apply → Confirm → Matched",
    button: []
  }
};
