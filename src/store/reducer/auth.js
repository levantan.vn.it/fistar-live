import { actionTypes } from "./../actions/actionTypes";

const initialState = {
  token: null,
  isAuthenticated: false,
  user: null,
  type: null,
  name: null,
  avatar: null,
  pc_name: null,
  phone: null,
  email: null,
  id: null,
  first_login: null,
  error: null,
  loading: false
};
// const count = (state = initialState, action) => {
export default function auth(state = initialState, action) {
  switch (action.type) {
    case actionTypes.LOGIN_SUCCESS: {
      return {
        ...state,
        loading: false,
        isAuthenticated: true,
        token: action.payload.token,
        user: action.payload.user,
        type: action.payload.type,
        name: action.payload.name ? action.payload.name : state.name,
        avatar: action.payload.avatar ? action.payload.avatar : state.avatar,
        pc_name: action.payload.pc_name
          ? action.payload.pc_name
          : state.pc_name,
        email: action.payload.email ? action.payload.email : state.email,
        phone: action.payload.phone ? action.payload.phone : state.phone,
        id: action.payload.id ? action.payload.id : state.id,
        first_login: action.payload.first_login
          ? action.payload.first_login
          : state.first_login
      };
    }
    case actionTypes.UPDATE_PROFILE: {
      return {
        ...state,
        loading: false,
        isAuthenticated: true,
        // token: action.payload.token,
        user: action.payload.user,
        type: action.payload.type,
        name: action.payload.name ? action.payload.name : state.name,
        avatar: action.payload.avatar ? action.payload.avatar : state.avatar,
        pc_name: action.payload.pc_name
          ? action.payload.pc_name
          : state.pc_name,
        email: action.payload.email ? action.payload.email : state.email,
        phone: action.payload.phone ? action.payload.phone : state.phone,
        id: action.payload.id ? action.payload.id : state.id,
        first_login: action.payload.first_login
          ? action.payload.first_login
          : state.first_login
      };
    }
    case actionTypes.LOGOUT_SUCCESS: {
      return {
        token: null,
        isAuthenticated: false,
        user: null,
        type: null,
        name: null,
        avatar: null,
        pc_name: null,
        email: null,
        phone: null,
        first_login: null,
        error: null,
        loading: false
      };
    }
    default:
      return state;
  }
}

// export default auth;
