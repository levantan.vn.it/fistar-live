import { actionTypes } from './../actions/actionTypes'

const initialState = {
  data: [],
  loading: false,
}
// const count = (state = initialState, action) => {
export default function code(state = initialState, action) {
  switch (action.type) {
    case actionTypes.FETCH_CHANNEL_SUCCESS: {
      return {
        ...state,
        data: action.payload.data,
      }
    };
    default:
      return state
  }
}

// export default code;
