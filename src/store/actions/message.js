import { actionTypes } from './actionTypes'
import api from '../../api'
// import setAuthorizationHeader from './../../utils/setAuthorizationHeader'

export const getPushMessageCampaignSuccess = (data, reset) => ({
  type: actionTypes.FETCH_MESSAGE_CAMPAIGN_SUCCESS,
  payload: {
    data: data,
    reset: reset,
  }
})

export const getPushMessageCampaignAction = (page = 1, by_relate = '') => dispatch => {
  return api.campaign.getPushMessageCampaign(page, by_relate).then(response => {
    dispatch(getPushMessageCampaignSuccess(response.data, page == 1))
    return Promise.resolve(response.data);
  }).catch((e) => {
    return Promise.reject(e.response)
  })
}

export const getPushMessageFistarSuccess = (data, reset) => ({
  type: actionTypes.FETCH_MESSAGE_FISTAR_SUCCESS,
  payload: {
    data: data,
    reset: reset,
  }
})

export const getPushMessageFistarAction = (page = 1) => dispatch => {
  return api.campaign.getPushMessageFistar(page).then(response => {
    dispatch(getPushMessageFistarSuccess(response.data, page == 1))
    return Promise.resolve(response.data);
  }).catch((e) => {
    return Promise.reject(e.response)
  })
}

export const getPushMessageNoticeSuccess = (data, reset) => ({
  type: actionTypes.FETCH_MESSAGE_NOTICE_SUCCESS,
  payload: {
    data: data,
    reset: reset,
  }
})

export const getPushMessageNoticeAction = (page = 1) => dispatch => {
  return api.campaign.getPushMessageNotice(page).then(response => {
    dispatch(getPushMessageNoticeSuccess(response.data, page == 1))
    return Promise.resolve(response.data);
  }).catch((e) => {
    return Promise.reject(e.response)
  })
}

export const getPushMessageCountAction = () => dispatch => {
  return api.campaign.getPushMessageCount().then(response => {
    console.log('response.data',response.data);
    return Promise.resolve(response.data);
  }).catch((e) => {
    return Promise.reject(e.response)
  })
}
