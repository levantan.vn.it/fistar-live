import { actionTypes } from './actionTypes'
import api from '../../api'
// import setAuthorizationHeader from './../../utils/setAuthorizationHeader'

export const getChannelSuccess = (type, data) => ({
  type: actionTypes.FETCH_CODE_SUCCESS,
  payload: {
    data: data,
    type: type,
  }
})
export const getChannelAction = (isDispatch = true) => dispatch => {
  return api.channel.getAll().then(response => {
    if (isDispatch) {
      let channel = response.data.map((channel) => ({
        ...channel,
        cd_id: channel.sns_id,
        cd_label: channel.sns_name,
        "cdg_id": 1000,
      }))
      dispatch(getChannelSuccess('channel', {
        "cdg_id": 1000,
        "cdg_name": "Channel",
        "code": channel
      }))
    }
    return Promise.resolve(response.data);
  }).catch((e) => {
    return Promise.reject(e.response)
  })
};

export const getRecentSNSAction = (uid, channel) => dispatch => {
  return api.channel.getRecentSNS(uid, channel).then(response => {
    return Promise.resolve(response.data);
  }).catch((e) => {
    return Promise.reject(e.response)
  })
};
