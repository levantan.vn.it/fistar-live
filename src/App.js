// The basics
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Switch, Redirect, BrowserRouter } from 'react-router-dom';
import * as routeName from './routes/routeName'
import UserRoute from "./routes/UserRoute";
import GuestRoute from "./routes/GuestRoute";
import Route from "./routes/Route";
import HttpsRedirect from 'react-https-redirect';

// Action creators and helpers
import { getProfileLoggedInAction } from './store/actions/auth';
import { isServer } from './store';

import routes from './routes';

import './styles/main.scss';

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isInit: false
    }
  }
  componentWillMount() {
    if (typeof window !== 'undefined' && window.document && window.document.createElement) {
      if (!this.props.isAuthenticated) {
        this.props.getProfileLoggedIn()
          .then(() => {
            this.setState({
              isInit: true
            })
          })
          .catch(() => {
            this.setState({
              isInit: true
            })
          })
      } else {
        this.setState({
          isInit: true
        })
      }
    }
  }

  render() {
    if (typeof window !== 'undefined' && window.document && window.document.createElement) {
      if (!this.state.isInit) {
        return null
      }
    }
    return (
          <Switch>
            {routes.map((route, key) => {
              if (!route.path) {
                return <Redirect key={key} {...route} />
              }
              if (route.type === 'GuestRoute') {
                console.log(route.type);
                return <GuestRoute key={route.path} {...route} />
              } else if (route.type === 'UserRoute') {
                console.log(route.type);
                return <UserRoute key={route.path} {...route} />
              }
              return <Route key={route.path} {...route} />
            })}
          </Switch>
    );
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated
});

const mapDispatchToProps = dispatch => ({
  getProfileLoggedIn: () => dispatch(getProfileLoggedInAction())
})

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(App)
);
