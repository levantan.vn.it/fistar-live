import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';

class Home extends Component {

  componentDidMount() {
    console.log(123213);
  }

  render() {
    return <div id="homepage">
    <Helmet key={Math.random()}>
            <title>Home</title>
            <meta property="og:title" content="Home"/>
            <meta
                name="description"
                content="Breaking news,latest articles, popular articles from most popular news websites of the world"
            />
            <meta name="robots" content="index, follow"/>
            <link rel="canonical" href="https://react-ssr-ilker.herokuapp.com"/>



        </Helmet>
    <p>Here's our homepage. All are welcome.</p>
    <img src={'logo'} alt="Homepage" style={{ width: '400px' }} />
  </div>
  }
}

const mapDispatchToProps = dispatch => {}

export default connect(
  null,
  mapDispatchToProps
)(Home);
