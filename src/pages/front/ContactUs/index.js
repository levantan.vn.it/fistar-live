import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, NavLink } from "react-router-dom";
import { connect } from "react-redux";
import api from "../../../api";
import cancelImage from "../../../images/cancel.svg";
import { Modal } from "react-bootstrap";
import PageHead from './../../../components/PageHead'

const defaultValidate = {
  name: false,
  email: false,
  location: false,
  phone: false,
  content: false
};

const defaultFormData = {
  type: 1,
  name: "",
  email: "",
  location: "",
  phone: "",
  content: ""
};

class ContactUs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form_data: defaultFormData,
      form_validate: defaultValidate,
      loading: false,
      success: false
    };
  }

  componentDidMount() {}

  validateEmail = email => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  };

  __handleChangeInput = e => {
    this.setState({
      form_data: {
        ...this.state.form_data,
        [e.target.name]: e.target.value
      }
    });
  };

  handleChangeInputNumberOnly = e => {
    const name = e.target.name;
    const value = e.target.value;
    if (/^\d*$/.test(value)) {
      this.setState(prevState => ({
        form_data: {
          ...prevState.form_data,
          [name]: value
        }
      }));
    }
  };

  __handleSubmit = e => {
    this.setState({
      loading: true
    });
    const { form_data } = this.state;
    if (this.validateInput(form_data)) {
      api.contact
        .sendContact(form_data)
        .then(res => {
          this.handleShow();
        })
        .catch(e => {
        });
    } else {
    }
    e.preventDefault();
  };

  validateInput = formData => {
    let validate = { ...defaultValidate };
    let status = 0;
    if (!formData.name || formData.name == "") {
      validate.name = "The name field is required";
      status++;
    }
    if (!formData.email) {
      validate.email = "The email field is required";
      status++;
    } else if (formData.email && !this.validateEmail(formData.email)) {
      validate.email = `${this.props.t("FISTAR_LOGIN.FL_toptip_email")}`;
      status++;
    }
    // if (!formData.location || formData.location == "") {
    //   validate.location = "The location field is required";
    //   status++;
    // }
    if (!formData.phone || formData.phone == "") {
      validate.phone = "The phone field is required";
      status++;
    } else if (formData.phone && formData.phone.length < 9) {
      validate.phone = "The phone field is min 9 numbers.";
      status++;
    } else if (formData.phone && formData.phone.length > 15) {
      validate.phone = "The phone field is maximums 15 numbers.";
      status++;
    }
    if (!formData.phone || formData.content == "") {
      validate.content = "Please enter your question.";
      status++;
    }
    this.setState({
      form_validate: validate,
      loading: false
    });
    if (status > 0) return false;
    else return true;
  };

  __handleForcus = () => {
    this.setState({
      form_validate: defaultValidate,
      loading: false
    });
  };

  handleClose = () => {
    this.setState({
      success: false,
      form_data: defaultFormData,
      form_validate: defaultValidate
    });
  };

  handleShow = () => {
    this.setState({ success: true });
  };

  onExited = () => {
    this.setState({
      isRedirect: true
    });
  };

  render() {
    const { form_data, loading, form_validate } = this.state;
    const { t } = this.props;
    return (
      <React.Fragment>
        <PageHead title="Contact Us | Fistar" />
        <div className="form-site">
          <div className="header-form">
            <div className="logo logo-faq">
              <h1>
                <a>{t("CONTACT_US.CUS_contact_us")}</a>
              </h1>
            </div>
          </div>
          <div className="body-form">
            <div className="content">
              <div className="form-contact">
                <div className="top">
                  <p>{t("CONTACT_US.CUS_text_contact")}</p>
                </div>
                <form onSubmit={this.__handleSubmit}>
                  <div className="form-group row">
                    <label
                      htmlFor="colFormLabelSm"
                      className="col-sm-3 col-form-label col-form-label-sm label-campaign"
                    >
                      {t("CONTACT_US.CUS_label_type_ select")}
                      <span>*</span>
                    </label>
                    <div className="col-sm-9 services-select">
                      <i className="fas fa-angle-down" />
                      <select
                        className="form-control form-control-sm"
                        name="type"
                        value={form_data.type}
                        onChange={this.__handleChangeInput}
                        onFocus={this.__handleForcus}
                      >
                        <option value="1">
                          {t("CONTACT_US.CUS_option_about_fistar")}
                        </option>
                        <option value="2">
                          {t("CONTACT_US.CUS_option_about_partner")}
                        </option>
                        <option value="3">
                          {t("CONTACT_US.CUS_option_etc")}
                        </option>
                      </select>
                    </div>
                  </div>
                  <div className="form-group row">
                    <label
                      htmlFor="colFormLabelSm"
                      className="col-sm-3 col-form-label col-form-label-sm label-campaign"
                    >
                      {t("CONTACT_US.CUS_label_name")}
                      <span>*</span>
                    </label>
                    <div className="col-sm-9 ">
                      <input
                        type="text"
                        className="form-control"
                        id="examfpleInputtext1"
                        aria-describedby="textHelp"
                        name="name"
                        value={form_data.name}
                        onChange={this.__handleChangeInput}
                        onFocus={this.__handleForcus}
                      />
                      {form_validate.name && (
                        <div
                          className="tooptip-text-error"
                          style={{ width: "250px" }}
                        >
                          <p>{form_validate.name}</p>
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="form-group row">
                    <label
                      htmlFor="colFormLabelSm"
                      className="col-sm-3 col-form-label col-form-label-sm label-campaign"
                    >
                      {t("CONTACT_US.CUS_label_email")}
                      <span>*</span>
                    </label>
                    <div className="col-sm-9 ">
                      <input
                        type="text"
                        className="form-control"
                        id="examgplefgInputtext1"
                        aria-describedby="textHelp"
                        name="email"
                        value={form_data.email}
                        onChange={this.__handleChangeInput}
                        onFocus={this.__handleForcus}
                      />
                      {form_validate.email && (
                        <div
                          className="tooptip-text-error"
                          style={{ width: "250px" }}
                        >
                          <p>{form_validate.email}</p>
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="form-group row">
                    <label
                      htmlFor="colFormLabelSm"
                      className="col-sm-3 col-form-label col-form-label-sm label-campaign"
                    >
                      {t("CONTACT_US.CUS_location")}
                      {/*<span>*</span>*/}
                    </label>
                    <div className="col-sm-9 ">
                      <input
                        type="text"
                        className="form-control"
                        id="exagfmpleInputtext1"
                        aria-describedby="textHelp"
                        name="location"
                        value={form_data.location}
                        onChange={this.__handleChangeInput}
                        onFocus={this.__handleForcus}
                      />
                      {form_validate.location && (
                        <div
                          className="tooptip-text-error"
                          style={{ width: "250px" }}
                        >
                          <p>{form_validate.location}</p>
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="form-group row">
                    <label
                      htmlFor="colFormLabelSm"
                      className="col-sm-3 col-form-label col-form-label-sm label-campaign"
                    >
                      {t("CONTACT_US.CUS_phone")}
                      <span>*</span>
                    </label>
                    <div className="col-sm-9 ">
                      <input
                        type="text"
                        className="form-control"
                        id="exfamspleInputtext1"
                        aria-describedby="textHelp"
                        name="phone"
                        value={form_data.phone}
                        onChange={this.handleChangeInputNumberOnly}
                        onFocus={this.__handleForcus}
                      />
                      {form_validate.phone && (
                        <div
                          className="tooptip-text-error"
                          style={{ width: "250px" }}
                        >
                          <p>{form_validate.phone}</p>
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="text-area position-relative">
                    <textarea
                      className="form-control"
                      id="exampleFormControlTextarea1"
                      rows={6}
                      placeholder={t("CONTACT_US.CUS_textarea")}
                      name="content"
                      value={form_data.content}
                      onChange={this.__handleChangeInput}
                      onFocus={this.__handleForcus}
                    />
                    {form_validate.content && (
                      <div
                        className="tooptip-text-error"
                        style={{ width: "250px", height: "auto" }}
                      >
                        <p>{form_validate.content}</p>
                      </div>
                    )}
                  </div>
                  <div className="btn-send">
                    <button className={"btn-submit"} type={"submit"}>
                      {" "}
                      {t("CONTACT_US.CUS_button_send")}
                      {loading ? (
                        <div
                          className="spinner-border"
                          role="status"
                          style={{
                            marginLeft: "15px",
                            verticalAlign: "middle"
                          }}
                        >
                          <span className="sr-only">
                            {t("LOADING.LOADING")}
                          </span>
                        </div>
                      ) : (
                        ""
                      )}
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <Modal
          show={this.state.success}
          onHide={this.handleClose}
          onExited={this.onExited}
          dialogClassName="howto-register modal-dialog-centered"
        >
          <div className="modal-content">
            <div className="register">
              <div className="top modal-header">
                <h4>{t("HEADER.HED_fistar")}</h4>
                <button
                  type="button"
                  className="btn btn-close close"
                  onClick={this.handleClose}
                >
                  <img src={cancelImage} alt="close" />
                </button>
              </div>
              <div className="body-popup">
                <p>{t("CONTACT_US.CTU_sent_successfully")}</p>
                <button type="button" onClick={this.handleClose}>
                  {t("POPUP_RESET_PASS.PRP_button_ok")}
                </button>
              </div>
            </div>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(ContactUs));
