import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { findEmailAction } from "./../../../../store/actions/auth";
import { Modal, Button } from "react-bootstrap";
import cancelImage from "./../../../../images/cancel.svg";
import * as routeName from "./../../../../routes/routeName";

class FistarFindEmail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        id: "",
        email: ""
      },
      errors: {
        id: "",
        email: "",
        message: ""
      },
      success: false,
      loading: false,
      redirectLogin: false
    };
  }

  handleChangeInput = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevState => ({
      data: {
        ...prevState.data,
        [name]: value
      }
    }));
  };

  onFocusInput = e => {
    this.setState(prevState => ({
      errors: {
        id: "",
        email: "",
        message: ""
      }
    }));
  };

    validateEmail = email => {
        // var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var re = /^[a-zA-Z0-9\.]{2,}@[a-zA-Z0-9]{2}(?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z](?:[a-zA-Z-]{0,61}[a-zA-Z])?)(?=.*[\.])*$/;
        return re.test(String(email).toLowerCase());
    };

    validateEmailCustomer = email => {
        var re = /^[a-zA-Z0-9]{2,}@[a-zA-Z0-9]{2,}.[a-zA-Z](?:[a-zA-Z-]{0,61}[a-zA-Z])*$/;
        return re.test(String(email).toLowerCase());
    };

    validateForm = () => {
        const errors = {
            id: "",
            email: "",
            message: ""
        };
        const { data } = this.state;
        // if (!data.pm_name) {
        //   errors.pm_name = "The name field is required.";
        // }
        // if (!data.pm_phone) {
        //   errors.pm_phone = "The phone field is required.";
        // }
        //
        // if (!data.email) {
        //   errors.email = "The email field is required.";
        // }
        if (data.email && !this.validateEmail(data.email)) {
            errors.email = "The email must be a valid email address.";
        }


        return errors;
    };

  submitForm = e => {
    e.preventDefault();
    const { t } = this.props;
    const { data } = this.state;

      const validate = this.validateForm();
      console.log(validate);
      if (!validate.email) {

          this.setState(
              {
                  loading: true
              },
              () => {
                  this.props
                      .findEmail(data)
                      .then(response => {
                          this.setState({
                              data: {
                                  id: "",
                                  email: ""
                              },
                              success: true,
                              loading: false
                          });
                      })
                      .catch(e => {
                          const errors = {
                              id: "",
                              email: "",
                              message: ""
                          };
                          if (e.data.errors) {
                              errors.id = e.data.errors.id ? e.data.errors.id[0] : "";
                              errors.email = e.data.errors.email ? e.data.errors.email[0] : "";
                          }
                          errors.message =
                              e.data.message == false
                                  ? t("FISTAR_RESETPW.FRP_id_incorrect")
                                  : "";
                          this.setState({
                              errors,
                              loading: false
                          });
                      });
              }
          );

      }else {
          this.setState(
              {
                  errors: validate
              },
              () => {}
          );
      }


  };

  handleClose = () => {
    this.setState({ success: false });
  };
  redirectLogin = () => {
    this.setState({ redirectLogin: true });
  };

  handleShow = () => {
    this.setState({ success: true });
  };

  onExited = () => {
    this.setState({
      isRedirect: true
    });
  };

  render() {
    const { t } = this.props;
    const { data, errors, success, loading, redirectLogin } = this.state;
    if (redirectLogin) {
      return <Redirect to={routeName.FISTAR_LOGIN} />;
    }

    return (
      <Fragment>
        <form
          className="wf-form-content search-login"
          onSubmit={this.submitForm}
        >
          <div className="row">
            <div className="col-md-12 mb-3">
              <a className="text-login-form" href="javascript:void(0)">
                {t("FISTAR_FINDID.FFI_fistar_find_email")}
              </a>
            </div>
          </div>
          <div className="row row-eq-height">
            <div className="col-md-12">
              <div className="form-input">
                <div className="input-group mb-3">
                  <div className="input-group-prepend">
                    <span className="input-group-text">
                      <i className="fas fa-user-alt" />
                    </span>
                  </div>
                  <input
                    placeholder={t("FISTAR_FINDID.FFI_id")}
                    type="text"
                    name="id"
                    id="id"
                    value={data.id}
                    className="empty"
                    onChange={this.handleChangeInput}
                    onFocus={this.onFocusInput}
                  />
                  {(errors.id || errors.message) && (
                    <div className="tooptip-search-login tooptip-find-id">
                      <p>{errors.message ? errors.message : errors.id}</p>
                    </div>
                  )}
                </div>
                <div className="input-group">
                  <div className="input-group-prepend">
                    <span className="input-group-text">
                      <i className="fas fa-envelope" />
                    </span>
                  </div>
                  <input
                    placeholder={t("FISTAR_FINDID.FFI_email")}
                    type="text"
                    name="email"
                    id="email"
                    value={data.email}
                    className="empty mb-0"
                    onChange={this.handleChangeInput}
                    onFocus={this.onFocusInput}
                  />
                  {errors.email && (
                    <div className="tooptip-search-login tooptip-find-id">
                      <p>{errors.email}</p>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12">
              <div className="text-search-login">
                <p className="text-right mt-2 mb-4">
                  <a href="javascript:void(0)" className="disabled-hover">
                    {t("FISTAR_FINDID.FFI_email_you_can")}
                  </a>
                </p>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12">
              <button
                className="btn btn-submit-search mt-3"
                onClick={this.submitForm}
              >
                {loading ? (
                  <div className="spinner-border" role="status">
                    <span className="sr-only">{t("LOADING.LOADING")}</span>
                  </div>
                ) : (
                  t("FISTAR_FINDID.FFI_send")
                )}
              </button>
            </div>
          </div>
        </form>
        <Modal
          show={this.state.success}
          onHide={this.handleClose}
          onExited={this.onExited}
          dialogClassName="howto-register modal-dialog-centered"
        >
          <div className="modal-content">
            <div className="register">
              <div className="top modal-header">
                <h4>{t("HEADER.HED_fistar")}</h4>
                <button
                  type="button"
                  className="btn btn-close close"
                  onClick={this.handleClose}
                >
                  <img src={cancelImage} alt="close" />
                </button>
              </div>
              <div className="body-popup">
                <p className="mb-3">{t("FISTAR_FINDID.FFI_popup_title")}</p>
                <p>{t("FISTAR_FINDID.FFI_popup_description")}</p>
                <button type="button" onClick={this.redirectLogin}>
                  {t("FISTAR_FINDID.FFI_popup_bt_ok")}
                </button>
              </div>
            </div>
          </div>
        </Modal>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    findEmail: data => dispatch(findEmailAction(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(FistarFindEmail));
