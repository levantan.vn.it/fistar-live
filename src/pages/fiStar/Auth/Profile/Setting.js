import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import {
  updateProfileFistarAction,
  logoutAction,
  getProfileFistarAction
} from "./../../../../store/actions/auth";
import { Modal, Button } from "react-bootstrap";
import * as routeName from "./../../../../routes/routeName";
import cancelImage from "./../../../../images/cancel.svg";


class FistarProfileSetting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        fb_posts: false,
        fb_photos: false,
      },
      isLoading: false,
      success: false,
    };
    this.handleChangeInput = this.handleChangeInput.bind(this);
  }

  componentDidMount() {
    this.props.getProfile().then(response => {
      const {
        data: {
          status: { influencer }
        }
      } = response;
      console.log(response);
      this.setState({
        data: {
          fb_photos: influencer.fb_photos === 1 ? true : false,
          fb_posts: influencer.fb_posts === 1 ? true : false,
        },
      });
    });
  }

  handleChangeInput(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    console.log(value);
    this.setState(prevState => ({
        data: {
          ...prevState.data,
          [name]: value
        }
      }));
  }


  submitForm = e => {
    e.preventDefault();
    const { data, errors } = this.state;
    this.setState(
      {
        isLoading: true
      },
      () => {
        this.props
          .updateProfile(data)
          .then(response => {
            this.setState({
              isLoading: false,
              success: true,
            });
          })
          .catch((er, code) => {
            this.setState({
              isLoading: false,
              errors: {
                ...this.state.errors,
                image:
                  this.props.t("VALIDATE.VLD_img_size_big") +
                  process.env.REACT_APP_MAX_IMAGE_SIZE +
                  "MB."
              }
            });
          });
      }
    );
    console.log(this.setState.success)
  };

  handleClose = () => {
    this.setState({ success: false });
  };

  handleShow = () => {
    this.setState({ success: true });
  };

  onExited = () => {
    this.props.logout();
    this.props.history.push("/login");
    this.setState({
      isRedirect: true
    });
  };

  render() {
    const { t } = this.props;
    const { data, errors, genders, isLoading, isRedirect,success} = this.state;
    console.log(success)
    if (success) {
      return  <Redirect to={routeName.FISTAR_DASHBOARD} />;
    }

    return (
      <Fragment>
        <div className="content-mypage-partner">
          <div className="container-change">
            <form className="myfistar-form" onSubmit={this.submitForm}>
              <div className="setting">
                <div className="form-group row">
                  <div className="group-icon-pass">
                    <input
                        name="fb_posts"
                        type="checkbox"
                        checked={data.fb_posts}
                        onChange={this.handleChangeInput} />
                    <span>
                        Share your FB posts with fiStar
                    </span>
                  </div>
                </div>
                <div className="form-group row">
                  <div className="group-icon-pass">
                    <input
                        name="fb_photos"
                        type="checkbox"
                        checked={data.fb_photos}
                        onChange={this.handleChangeInput} />
                    <span>
                        Share your FB photos with fiStar
                    </span>
                  </div>
                </div>
              </div>
              <div className="btn-ok">
                <button onClick={this.submitForm} type="submit">
                  {isLoading ? (
                    <div className="spinner-border" role="status">
                      <span className="sr-only">{t("LOADING.LOADING")}</span>
                    </div>
                  ) : (
                    t("PROFILE_FISTAR.PFR_button_ok")
                  )}
                </button>
              </div>
            </form>
          </div>
        </div>
        {/* <Modal
          show={this.state.success}
          onHide={this.handleClose}
          onExited={this.onExited}
          dialogClassName="howto-register modal-dialog-centered"
        >
          <div className="modal-content">
            <div className="register">
              <div className="top modal-header">
                <h4>{t("PROFILE_FISTAR.PFR_popup_fistar")}</h4>
                <button
                  type="button"
                  className="btn btn-close close"
                  onClick={this.handleClose}
                >
                  <img src={cancelImage} alt="close" />
                </button>
              </div>
              <div className="body-popup">
                <p className="mb-3 text-center">
                  {t("POPUP_CHANGE_PASSWORD.PCP_text_1")}
                </p>
                <p className="text-center">
                  <small>{t("POPUP_CHANGE_PASSWORD.PCP_text_2")}</small>
                </p>
                <button type="button" onClick={this.onExited}>
                  {t("PROFILE_FISTAR.PFR_button_ok")}
                </button>
              </div>
            </div>
          </div>
        </Modal> */}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    getProfile: () => dispatch(getProfileFistarAction()),
    updateProfile: data =>
      dispatch(updateProfileFistarAction("setting", data)),
    logout: () => dispatch(logoutAction())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(FistarProfileSetting));
