import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { resetPasswordAction } from "./../../../../store/actions/auth";
import { forgotPasswordAction } from "../../../../store/actions/auth";

class FistarForgotPasswordStep2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        code: ""
      },
      errors: {
        code: "",
        message: ""
      },
      success: false,
      loading: false,
      successSendCode: false
    };
  }

  handleChangeInput = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevState => ({
      data: {
        ...prevState.data,
        [name]: value
      }
    }));
  };

  onFocusInput = e => {
    this.setState(prevState => ({
      errors: {
        code: "",
        message: ""
      }
    }));
  };

  validateForm = () => {
    const errors = {
      code: ""
    };
    const { data } = this.state;
    if (!data.code) {
      errors.code = this.props.t("FISTAR_RESETPW.FRP_erro_code");
    } else if (data.code.length !== 6) {
      errors.code = this.props.t("FISTAR_RESETPW.FRP_erro_code_limit_6");
    }

    return errors;
  };

  submitForm = e => {
    e.preventDefault();
    const validate = this.validateForm();
    if (!validate.code) {
      if (!this.state.loading) {
        const { data } = this.state;
        const { email } = this.props;
        this.setState(
          {
            loading: true
          },
          () => {
            data.email = email;
            this.props
              .resetPassword(data)
              .then(response => {
                console.log(response);
                this.setState({
                  success: true,
                  loading: false
                });
                this.props.onNextStep(3, { code: data.code });
              })
              .catch(e => {
                const errors = {
                  code: "",
                  message: ""
                };
                if (e.data.errors) {
                  errors.code = e.data.errors.code ? e.data.errors.code[0] : "";
                }
                errors.message =
                  e.data.status == false
                    ? this.props.t("FISTAR_RESETPW.FRP_erro_code_wrong")
                    : "";
                this.setState({
                  errors,
                  loading: false
                });
              });
          }
        );
      }
    } else {
      this.setState({
        errors: validate
      });
    }
  };

  onClickBack = () => {
    console.log(this.props);
    let dataStep1 = {};
    if (this.props.email && this.props.id) {
      dataStep1.id = this.props.id;
      dataStep1.email = this.props.email;
    }
    console.log(dataStep1);

    this.props
      .forgotPassword(dataStep1)
      .then(response => {
        this.setState({
          successSendCode: true,
          loading: false
        });

        setTimeout(() => {
          this.setState({
            successSendCode: false,
            loading: false
          });
        }, 7000);
      })
      .catch(e => {
        const errors = {
          id: "",
          email: "",
          message: ""
        };

        this.setState({
          errors,
          loading: false
        });
      });

    // this.props.onNextStep(1);
  };

  render() {
    const { t, email } = this.props;
    const { data, errors, success, loading, successSendCode } = this.state;

    return (
      <form className="wf-form-content search-login" onSubmit={this.submitForm}>
        <div className="row">
          <div className="col-md-12 mb-3">
            <a href="javascript:void(0)" className="text-login-form">
              {t("FISTAR_RESETPW.FRP_fiStar_find_password")}
            </a>
            {/*<div className="alert alert-success" role="alert">
              We send an email to your email.<br />
            </div>*/}
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <p className="code-test mb-2 mt-3">
              {t("FISTAR_RESETPW.FRP_text_forget_1")}{" "}
              <span>
                <a href="javascript:void(0)">{email}</a>
              </span>
              .<br />
              {t("FISTAR_RESETPW.FRP_text_forget_2")}
            </p>
          </div>
        </div>
        <div className="row row-eq-height">
          <div className="col-md-12">
            <div className="form-input">
              <div className="input-group input-test">
                <input
                  placeholder={t("FISTAR_RESETPW.FRP_input_veryfica")}
                  type="text"
                  className="empty mb-0"
                  id="code"
                  name="code"
                  onChange={this.handleChangeInput}
                />
                {(errors.code || errors.message) && (
                  <div className="tooptip-search-login">
                    <p>{errors.message ? errors.message : errors.code}</p>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
        <div className="row btn-reset-pass">
          <div className="col-md-12">
            <button className="btn btn-reset mt-3" onClick={this.submitForm}>
              {loading ? (
                <div className="spinner-border text-primary" role="status">
                  <span className="sr-only">{t("LOADING.LOADING")}</span>
                </div>
              ) : (
                <Fragment>{t("FISTAR_RESETPW.FRP_button_very_next")}</Fragment>
              )}
            </button>
          </div>
          <div className="col-md-12 mt-1">
            <a href="javascript:void(0)" onClick={this.onClickBack}>
              {t("FISTAR_RESETPW.FRP_do_you_receive")}
            </a>
            {successSendCode && (
              <div className="col-md-12 m-0 P-0">
                <div className="alert alert-success" role="alert">
                  {t("FISTAR_RESETPW.FRP_text_code_again")}
                </div>
              </div>
            )}
          </div>
        </div>
      </form>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    resetPassword: data => dispatch(resetPasswordAction(data)),
    forgotPassword: data => dispatch(forgotPasswordAction(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(FistarForgotPasswordStep2));
