import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { loginAction, logoutAction } from "./../../../../store/actions/auth";
import FiStarAuthFooter from "./../../../../components/fiStar/Auth/Footer";
import * as routeName from "./../../../../routes/routeName";

class FiStarRegisterStep2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        email: "",
        password: ""
      },
      errors: {
        email: "",
        password: "",
        message: ""
      },
      isGoToLogin: false
    };
  }

  // componentDidMount() {
  //   this.props.completeStep();
  // }

  handleChangeInput = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevState => ({
      data: {
        ...prevState.data,
        [name]: value
      }
    }));
  };

  onFocusInput = e => {
    this.setState(prevState => ({
      errors: {
        email: "",
        password: "",
        message: ""
      }
    }));
  };

  componentWillUnmount() {
    this.props.logout()
  }

  // onClickGoToLogin = () => {
  //   this.setState({
  //     isGoToLogin: true
  //   })
  // }

  render() {
    const { t } = this.props;
    const { data, errors, isGoToLogin } = this.state;

    // if (isGoToLogin) {
    //   return <Redirect to={'/login'}/>
    // }

    return (
      <Fragment>
        <div className="body-form body-form-thanks">
          <div className="container-form-join">
            <div className="row">
              <div className="col-md-12 thanks-for">
                <h2 className="text-center">{t("FISTAR_JOIN.PJN_thanks")}</h2>
                <p className="mt-2 text-center">{t("FISTAR_JOIN.PJN_text")}</p>
              </div>
            </div>
            <div className="row">
              <div className="time-approvals col-md-12">
                <p>
                  {t("FISTAR_JOIN.PJN_text_01")}{" "}
                  <a href="javascript:void(0)">
                    {t("FISTAR_JOIN.PJN_text_bold")}
                  </a>{" "}
                  {t("FISTAR_JOIN.PJN_text_02")}
                </p>
                <p className="d-flex">
                  {t("FISTAR_JOIN.PJN_text_04")}
                  <br />
                  {t("FISTAR_JOIN.PJN_text_05")}
                </p>
              </div>
            </div>
            <div className="row">
              <div className="col-md-12">
                <div className="button-group-thanks">
                  <Link to={routeName.FISTAR_FRONT}>
                    {t("FISTAR_JOIN.PJN_about_fistar")}
                  </Link>
                  <Link to={routeName.CAMPAIGN}>
                    {t("FISTAR_JOIN.PJN_about_campaign")}
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    login: data => dispatch(loginAction(data)),
    logout: data => dispatch(logoutAction())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(FiStarRegisterStep2));
