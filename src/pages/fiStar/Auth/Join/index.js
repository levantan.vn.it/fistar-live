import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import FacebookLogin from "react-facebook-login";
import {
  loginFacebookAction,
  joinAction
} from "./../../../../store/actions/auth";
import FistarJoinTab1 from "./Tab1";
import FistarJoinTab2 from "./Tab2";
import "./join.scss";
import { convertLocationSearchToObject, convertObjectToLocationSearch } from "./../../../../common/helper";

class FistarJoin extends Component {
  constructor(props) {
    super(props);
    let search = convertLocationSearchToObject(props.location.search)
    let tab = search.tab || 1
    this.state = {
      data: {
        id: "",
        email: ""
      },
      errors: {
        id: "",
        email: "",
        message: ""
      },
      success: false,
      loading: false,
      tab: +tab
    };
  }


  handleChangeInput = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevState => ({
      data: {
        ...prevState.data,
        [name]: value
      }
    }));
  };

  onFocusInput = e => {
    this.setState(prevState => ({
      errors: {
        id: "",
        email: "",
        message: ""
      }
    }));
  };

  submitForm = e => {
    e.preventDefault();
  };

  responseFacebook = (accessToken, tab) => {
    return this.props.loginFacebook({ accessToken: accessToken, tab: tab });
  };

  changeTab = tab => {
    const { location } = this.props
    let path = location.path
    let search = location.search
    search = convertLocationSearchToObject(search)
    search.tab = tab
    search = convertObjectToLocationSearch(search)
    this.props.history.push({
      pathname: path,
      search: '?' + search,
    })
    this.setState({
      tab
    });
  };

  join = (
    data,
    type,
    isChangeID,
    isChangeEmail,
    isNewPass,
    isSkipStep = false
  ) => {
    this.props.join(
      data,
      type,
      isChangeID,
      isChangeEmail,
      isNewPass,
      isSkipStep
    );
    // this.props.history.push({ pathname: "/fi-star/join-fb" });
    this.props.history.push({ pathname: "/fi-star/register" });
  };

  clearCode = () => {
    const { location } = this.props
    let path = location.path
    let search = location.search
    search = convertLocationSearchToObject(search)
    search.code = ''
    search = convertObjectToLocationSearch(search)
    this.props.history.push({
      pathname: path,
      search: '?' + search,
    })
  }

  renderTab = () => {
    const { tab } = this.state;
    switch (tab) {
      case 1:
        return (
          <FistarJoinTab1
            responseFacebook={this.responseFacebook}
            join={this.join}
            clearCode={this.clearCode}
          />
        );
        break;
      case 2:
        return (
          <FistarJoinTab2
            responseFacebook={this.responseFacebook}
            join={this.join}
            clearCode={this.clearCode}
          />
        );
        break;

      default:
        // code...
        break;
    }
  };

  render() {
    const { t } = this.props;
    const { data, errors, success, loading, tab } = this.state;

    return (
      <div className="form-register">
        <div className="row">
          <div className="col-md-12 mb-4">
            {/*<a href="#" className="text-left text-register">fiStar는<span> fi :me account</span>You can apply to join!</a>*/}
            <a
              href="javascript:void(0)"
              className="text-left text-register title-join-fistar"
            >
              {t("FISTAR_JOIN.FJN_title")}{" "}
              <span> {t("FISTAR_JOIN.FJN_title_bold")}</span>
            </a>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <div className="form-register-content">
              <ul className="tabs-register">
                <li
                  className={tab == 1 ? "item activce" : "item"}
                  onClick={() => this.changeTab(1)}
                >
                  <label>{t("FISTAR_JOIN.FJN_fime_member")}</label>
                </li>
                <li
                  className={tab == 2 ? "item activce" : "item"}
                  onClick={() => this.changeTab(2)}
                >
                  <label>{t("FISTAR_JOIN.FJN_fime_not_member")}</label>
                </li>
              </ul>
              <div className="body">{this.renderTab()}</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    loginFacebook: data => dispatch(loginFacebookAction(data)),
    join: (
      data,
      type,
      isChangeID = null,
      isChangeEmail = null,
      isNewPass = false,
      isSkipStep = false
    ) =>
      dispatch(
        joinAction(data, type, isChangeID, isChangeEmail, isNewPass, isSkipStep)
      )
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(FistarJoin));
