import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import { loginAction } from "./../../../../store/actions/auth";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";
import api from "../../../../api";
import { REDIRECT_JOIN_STEP_1 } from "../../../../constants/index";
import { validUrl } from './../../../../common/helper'

class FistarJoinTab1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        email: "",
        password: "",
        join: true
      },
      errors: {
        email: "",
        password: "",
        facebook: "",
        message: ""
      },
      success: false,
      loading: false,
      isLoadingFB: false
    };

    // if (window.FB) {
    //   window.FB.init({
    //     appId: process.env.REACT_APP_FACEBOOK_FIME_ID,
    //     status: true,
    //     xfbml: true,
    //     version: "v2.3" // or v2.6, v2.5, v2.4, v2.3
    //   });
    // }
  }

  componentDidMount() {
    let v = this.props.location.search.match(new RegExp("(?:[?&]code=)([^&]+)"));
    let code = v ? v[1] : null;
    if (code) {
      this.setState({isLoadingFB: true})
      api.getAccessToken("fistar", "/fi-star/join?tab=1", code).then(res => {
        this.props.responseFacebook(res.data.access_token, 'tab1').catch(error => {
          let isChangeID = false
          let isChangeEmail = false
          let isNewPass = false
          let isSkipStep = false
          if (error.status === 403) {
            if (error.data.data === false) {
              this.props.clearCode()
              this.setState({
                errors: {
                  ...this.state.errors,
                  facebook: this.props.t(
                    "FISTAR_JOIN.FJN_erro_facebook_register"
                  )
                },
                isLoadingFB: false
              });
              return;
            }
            if (!error.data.data.EMAIL) {
              isChangeEmail = true
              isNewPass = true
              this.props.clearCode()
              this.setState({
                // errors: {
                //   ...this.state.errors,
                //   facebook: this.props.t("FISTAR_JOIN.FB_not_registered")
                // },
                isLoadingFB: false
              });
              // return;
            }
            let result = error.data.data;
            let data = {
              name: result.REG_NAME,
              id: result.ID,
              uid: result.USER_NO,
              contact: result.CELLPHONE,
              address: result.HOME_ADDR1,
              email: result.EMAIL
            };
            if (result.facebook_url !== 'null') {
              console.log(validUrl(result.facebook_url, fbId));
              let fbId = 2
              data.channels = {
                facebook: {
                  id: fbId,
                  value: result.facebook_url,
                  disable: validUrl(result.facebook_url, 'facebook')
                }
              }
            }
            isChangeID = !result.ID
            this.props.join(data, "fistar", isChangeID, isChangeEmail, isNewPass, isSkipStep);
          }
          if (error.status == 402) {
            this.props.clearCode()
            this.setState({
              errors: {
                ...this.state.errors,
                facebook: this.props.t("FISTAR_JOIN.FJN_erro_facebook_register_lock")
              },
              isLoadingFB: false
            });
          }
        });
      });
    }
  }

  responseFacebook = e => {};

  handleChangeInput = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevState => ({
      data: {
        ...prevState.data,
        [name]: value
      }
    }));
  };

  onFocusInput = e => {
    this.setState(prevState => ({
      errors: {
        email: "",
        password: "",
        facebook: "",
        message: ""
      }
    }));
  };
  validateEmail = email => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  };

  validateForm = () => {
    const errors = {
      email: "",
      password: "",
      facebook: "",
      message: ""
    };
    const { data, validateEmail } = this.state;
    const { t } = this.props;

    if (!data.password) {
      errors.password = t("FISTAR_JOIN.FJN_erro_pw");
    } else if (data.password && data.password.length < 6) {
      errors.password = t("FISTAR_JOIN.FJN_erro_pw_limit_6");
    }

    if (!data.email) {
      errors.email = t("FISTAR_JOIN.FJN_erro_email");
    } else if (data.email && !this.validateEmail(data.email)) {
      errors.email = t("FISTAR_JOIN.FJN_erro_email_valid");
    }
    return errors;
  };

  submitForm = e => {
    e.preventDefault();
    const { data } = this.state;
    const { t } = this.props;
    const validate = this.validateForm();
    if (!validate.email && !validate.password) {
      this.setState(
        {
          loading: true
        },
        () => {
          this.props.login(data).catch(e => {
          console.log(e, '-------');

            if (e.status === 403) {
              let result = e.data.data;
              let data = {
                name: result.REG_NAME,
                id: result.ID,
                email: result.EMAIL,
                uid: result.USER_NO,
              };
              if (result.facebook_url !== 'null') {
                console.log(validUrl(result.facebook_url, fbId));
                let fbId = 2
                data.channels = {
                  facebook: {
                    id: fbId,
                    value: result.facebook_url,
                    disable: validUrl(result.facebook_url, 'facebook')
                  }
                }
              }
              this.props.join(data, "fistar", false, false);
            }
            let errors = {
              email: "",
              password: "",
              message: ""
            };
            if (e.data.errors) {
              errors.email = e.data.errors.email ? e.data.errors.email[0] : "";
              errors.password = e.data.errors.password
                ? e.data.errors.password[0]
                : "";
            }
            if (e.data.message == "Unauthorized") {
              errors.message = t("FISTAR_JOIN.FJN_erro_email_pw_wrong");
            } else if (e.data.message == "Exist fi:star account") {
              errors.message = t("FISTAR_JOIN.FJN_erro_existed");
            }
            this.setState({
              errors,
              loading: false
            });
          });
        }
      );
    } else {
      this.setState(
        {
          errors: validate
        },
        () => {}
      );
    }
  };

  renderLoginFB = () => (
    <a href={REDIRECT_JOIN_STEP_1}>
      <button className="btn btn-submit-facebook text-center">
        <i className="fab fa-facebook-f" />
        {this.props.t("FISTAR_JOIN.FJN_sign_facebook")}
      </button>
    </a>
  );

  renderLoginFBLoading = () => (
    <button className="btn btn-submit-facebook text-center">
      <i className="fab fa-facebook-f" />
      <div className="spinner-border" role="status">
        <span className="sr-only">Loading...</span>
      </div>
    </button>
  );

  render() {
    const { t } = this.props;
    const { data, errors, success, loading, isLoadingFB } = this.state;

    return (
      <Fragment>
        <div className="submit-face">
          {/*<FacebookLogin*/}
          {/*appId={process.env.REACT_APP_FACEBOOK_FIME_ID}*/}
          {/*autoLoad={false}*/}
          {/*fields="name,email,picture,id"*/}
          {/*callback={this.responseFacebook}*/}
          {/*render={renderProps => (*/}
          {/*<button*/}
          {/*className="btn btn-submit-facebook text-center"*/}
          {/*onClick={renderProps.onClick}*/}
          {/*>*/}
          {/*<i className="fab fa-facebook-f" />*/}
          {/*{t("FISTAR_JOIN.FJN_sign_facebook")}*/}
          {/*</button>*/}
          {/*)}*/}
          {/*/>*/}
          {isLoadingFB && this.renderLoginFBLoading()}
          {!isLoadingFB && this.renderLoginFB()}
          {errors.facebook && <p className="text-danger">{errors.facebook}</p>}
        </div>
        <div className="form form-sub-register mt-4">
          <div className="row">
            <div className="col-md-9">
              <div className="text-register">
                <p className="text-left mt-2 mb-4">
                  <a href="javascript:viod(0)">
                    <span>{t("FISTAR_JOIN.FJN_subscribed")}</span>
                    {/*account subscribed*/}
                  </a>
                </p>
              </div>
            </div>
          </div>
          <form className="row row-eq-height">
            <div className="col-md-9">
              <div className="form-input">
                <div
                  className={`input-group mb-3${
                    errors.email || errors.message ? " invalid" : ""
                  }`}
                >
                  <div className="input-group-prepend">
                    <span className="input-group-text">
                      <i className="fas fa-envelope" />
                    </span>
                  </div>
                  <input
                    placeholder={t("FISTAR_JOIN.FJN_input_email")}
                    type="text"
                    id="email"
                    name="email"
                    className="empty"
                    value={data.email}
                    onChange={this.handleChangeInput}
                    onFocus={this.onFocusInput}
                  />
                  {errors.email && (
                    <div
                      className="tooptip-text-error"
                      style={{ width: "260px" }}
                    >
                      <p>{errors.email}</p>
                    </div>
                  )}
                </div>
                <div className="input-group">
                  <div className="input-group-prepend">
                    <span className="input-group-text">
                      <i className="fas fa-lock-open" />
                    </span>
                  </div>
                  <input
                    placeholder={t("FISTAR_JOIN.FJN_password")}
                    type="password"
                    id="password"
                    name="password"
                    className="empty mb-0"
                    value={data.password}
                    onChange={this.handleChangeInput}
                    onFocus={this.onFocusInput}
                  />
                  {(errors.password || errors.message) && (
                    <div
                      className="tooptip-text-error"
                      style={{ width: "260px" }}
                    >
                      <p>{errors.message ? errors.message : errors.password}</p>
                    </div>
                  )}
                </div>
              </div>
            </div>
            <div className="col-md-3 sub-face">
              <button
                className="btn btn-submit"
                type="submit"
                onClick={this.submitForm}
              >
                {loading ? (
                  <div className="spinner-border" role="status">
                    <span className="sr-only">{t("LOADING.LOADING")}</span>
                  </div>
                ) : (
                  t("FISTAR_JOIN.FJN_button_signup")
                )}
              </button>
            </div>
          </form>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    login: data => dispatch(loginAction(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(withRouter(FistarJoinTab1)));
