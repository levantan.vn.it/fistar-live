// import React, { PureComponent } from "react";
// import { withTranslation, Trans } from "react-i18next";
// import { Link, Redirect } from "react-router-dom";
// import { connect } from "react-redux";
// // import RichTextEditor from 'react-rte';
// import { getCampaignDetailAction } from "./../../../../store/actions/campaign";
// import { getCodeAction } from "./../../../../store/actions/code";
// import {Editor, EditorState, RichUtils} from 'draft-js';

// // Custom overrides for "code" style.
//       const styleMap = {
//         CODE: {
//           backgroundColor: 'rgba(0, 0, 0, 0.05)',
//           fontFamily: '"Inconsolata", "Menlo", "Consolas", monospace',
//           fontSize: 16,
//           padding: 2,
//         },
//       };

//       function getBlockStyle(block) {
//         switch (block.getType()) {
//           case 'blockquote': return 'RichEditor-blockquote';
//           default: return null;
//         }
//       }

//       class StyleButton extends React.Component {
//         constructor() {
//           super();
//           this.onToggle = (e) => {
//             e.preventDefault();
//             this.props.onToggle(this.props.style);
//           };
//         }

//         render() {
//           let className = 'RichEditor-styleButton';
//           if (this.props.active) {
//             className += ' RichEditor-activeButton';
//           }

//           return (
//             <span className={className} onMouseDown={this.onToggle}>
//               {this.props.label}
//             </span>
//           );
//         }
//       }

//       const BLOCK_TYPES = [
//         {label: 'H1', style: 'header-one'},
//         {label: 'H2', style: 'header-two'},
//         {label: 'H3', style: 'header-three'},
//         {label: 'H4', style: 'header-four'},
//         {label: 'H5', style: 'header-five'},
//         {label: 'H6', style: 'header-six'},
//         {label: 'Blockquote', style: 'blockquote'},
//         {label: 'UL', style: 'unordered-list-item'},
//         {label: 'OL', style: 'ordered-list-item'},
//         {label: 'Code Block', style: 'code-block'},
//       ];

//       const BlockStyleControls = (props) => {
//         const {editorState} = props;
//         const selection = editorState.getSelection();
//         const blockType = editorState
//           .getCurrentContent()
//           .getBlockForKey(selection.getStartKey())
//           .getType();

//         return (
//           <div className="RichEditor-controls">
//             {BLOCK_TYPES.map((type) =>
//               <StyleButton
//                 key={type.label}
//                 active={type.style === blockType}
//                 label={type.label}
//                 onToggle={props.onToggle}
//                 style={type.style}
//               />
//             )}
//           </div>
//         );
//       };

//       var INLINE_STYLES = [
//         {label: 'Bold', style: 'BOLD'},
//         {label: 'Italic', style: 'ITALIC'},
//         {label: 'Underline', style: 'UNDERLINE'},
//         {label: 'Monospace', style: 'CODE'},
//       ];

//       const InlineStyleControls = (props) => {
//         var currentStyle = props.editorState.getCurrentInlineStyle();
//         return (
//           <div className="RichEditor-controls">
//             {INLINE_STYLES.map(type =>
//               <StyleButton
//                 key={type.label}
//                 active={currentStyle.has(type.style)}
//                 label={type.label}
//                 onToggle={props.onToggle}
//                 style={type.style}
//               />
//             )}
//           </div>
//         );
//       };

// class EditorReview extends PureComponent {
//   constructor(props) {
//     super(props)
//     this.state = {
//       sns: '',
//       editorState: EditorState.createEmpty(),
//       // value: RichTextEditor.createEmptyValue(),
//     }

//     this.focus = () => this.refs.editor.focus();
//     this.onChange = (editorState) => this.setState({editorState});

//     this.handleKeyCommand = (command) => this._handleKeyCommand(command);
//     this.onTab = (e) => this._onTab(e);
//     this.toggleBlockType = (type) => this._toggleBlockType(type);
//     this.toggleInlineStyle = (style) => this._toggleInlineStyle(style);
//   }

//   static getDerivedStateFromProps(props, state) {
//     if (+props.sns !== +state.sns) {
//       return {
//         sns: props.sns,
//         // value: RichTextEditor.createValueFromString(props.value, 'html')
//       }
//     }
//     return null
//   }

//   onChange = (value) => {
//     this.setState({value});
//     if (this.props.onChange) {
//       // Send the changes up to the parent component as an HTML string.
//       // This is here to demonstrate using `.toString()` but in a real app it
//       // would be better to avoid generating a string on each change.
//       this.props.onChange(
//         value.toString('html')
//       );
//     }
//   };

//   // onFocus = () => {
//   //   if (this.props.onFocus) {
//   //     // Send the changes up to the parent component as an HTML string.
//   //     // This is here to demonstrate using `.toString()` but in a real app it
//   //     // would be better to avoid generating a string on each change.
//   //     this.props.onFocus();
//   //   }
//   // };

//   _handleKeyCommand(command) {
//     const {editorState} = this.state;
//     const newState = RichUtils.handleKeyCommand(editorState, command);
//     if (newState) {
//       this.onChange(newState);
//       return true;
//     }
//     return false;
//   }

//   _onTab(e) {
//     const maxDepth = 4;
//     this.onChange(RichUtils.onTab(e, this.state.editorState, maxDepth));
//   }

//   _toggleBlockType(blockType) {
//     this.onChange(
//       RichUtils.toggleBlockType(
//         this.state.editorState,
//         blockType
//       )
//     );
//   }

//   _toggleInlineStyle(inlineStyle) {
//     this.onChange(
//       RichUtils.toggleInlineStyle(
//         this.state.editorState,
//         inlineStyle
//       )
//     );
//   }

//   render() {
//     const {editorState} = this.state;

//     // If the user changes block type before entering any text, we can
//     // either style the placeholder or hide it. Let's just hide it now.
//     let className = 'RichEditor-editor';
//     var contentState = editorState.getCurrentContent();
//     if (!contentState.hasText()) {
//       if (contentState.getBlockMap().first().getType() !== 'unstyled') {
//         className += ' RichEditor-hidePlaceholder';
//       }
//     }
//     // const toolbarConfig = {
//     //   // Optionally specify the groups to display (displayed in the order listed).
//     //   display: ['INLINE_STYLE_BUTTONS', 'BLOCK_TYPE_BUTTONS', 'BLOCK_TYPE_DROPDOWN', 'HISTORY_BUTTONS'],
//     //   INLINE_STYLE_BUTTONS: [
//     //     {label: 'Bold', style: 'BOLD', className: 'custom-css-class'},
//     //     {label: 'Italic', style: 'ITALIC'},
//     //     {label: 'Underline', style: 'UNDERLINE'}
//     //   ],
//     //   BLOCK_TYPE_DROPDOWN: [
//     //     {label: 'Normal', style: 'unstyled'},
//     //     {label: 'Heading Large', style: 'header-one'},
//     //     {label: 'Heading Medium', style: 'header-two'},
//     //     {label: 'Heading Small', style: 'header-three'}
//     //   ],
//     //   BLOCK_TYPE_BUTTONS: [
//     //     {label: 'UL', style: 'unordered-list-item'},
//     //     {label: 'OL', style: 'ordered-list-item'}
//     //   ]
//     // };

//     return <div className="RichEditor-root">
//       <BlockStyleControls
//         editorState={editorState}
//         onToggle={this.toggleBlockType}
//       />
//       <InlineStyleControls
//         editorState={editorState}
//         onToggle={this.toggleInlineStyle}
//       />
//       <div className={className} onClick={this.focus}>
//         <Editor
//           blockStyleFn={getBlockStyle}
//           customStyleMap={styleMap}
//           editorState={editorState}
//           handleKeyCommand={this.handleKeyCommand}
//           onChange={this.onChange}
//           onTab={this.onTab}
//           placeholder="Tell a story..."
//           ref="editor"
//           spellCheck={true}
//         />
//       </div>
//     </div>

//     return <Editor editorState={this.state.value} onChange={this.onChange} onFocus={this.onFocus} />

//     // return (
//     //   <RichTextEditor
//     //     value={this.state.value}
//     //     onChange={this.onChange}
//     //     onFocus={this.onFocus}
//     //     toolbarConfig={toolbarConfig}
//     //     // editorClassName="demo-editor"
//     //   />
//     // );
//   }
// }

// export default withTranslation("translations")(EditorReview)

import React from "react";
import Draft from "draft-js";
import "./rich.scss";
import { stateToHTML } from "draft-js-export-html";

const {
  Editor,
  EditorState,
  RichUtils,
  getDefaultKeyBinding,
  convertFromHTML,
  createFromBlockArray,
  ContentState
} = Draft;

export default class RichEditorExample extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sns: "",
      editorState: EditorState.createEmpty()
    };
    this.focus = () => this.refs.editor.focus();
    this.onChange = editorState => {
      props.onChange(stateToHTML(editorState.getCurrentContent()));
      this.setState({ editorState });
    };
    this.handleKeyCommand = this._handleKeyCommand.bind(this);
    this.mapKeyToEditorCommand = this._mapKeyToEditorCommand.bind(this);
    this.toggleBlockType = this._toggleBlockType.bind(this);
    this.toggleInlineStyle = this._toggleInlineStyle.bind(this);
  }
  static getDerivedStateFromProps(props, state) {
    if (+props.sns !== +state.sns) {
      if (!props.value) {
        return {
          sns: props.sns,
          editorState: EditorState.createEmpty()
        };
      }
      const blocksFromHTML = convertFromHTML(props.value);
      const state = ContentState.createFromBlockArray(
        blocksFromHTML.contentBlocks,
        blocksFromHTML.entityMap
      );
      return {
        sns: props.sns,
        editorState: EditorState.createWithContent(state)
      };
    }
    return null;
  }
  _handleKeyCommand(command, editorState) {
    const newState = RichUtils.handleKeyCommand(editorState, command);
    if (newState) {
      this.onChange(newState);
      return true;
    }
    return false;
  }
  _mapKeyToEditorCommand(e) {
    if (e.keyCode === 9 /* TAB */) {
      const newEditorState = RichUtils.onTab(
        e,
        this.state.editorState,
        4 /* maxDepth */
      );
      if (newEditorState !== this.state.editorState) {
        this.onChange(newEditorState);
      }
      return;
    }
    return getDefaultKeyBinding(e);
  }
  _toggleBlockType(blockType) {
    this.onChange(RichUtils.toggleBlockType(this.state.editorState, blockType));
  }
  _toggleInlineStyle(inlineStyle) {
    this.onChange(
      RichUtils.toggleInlineStyle(this.state.editorState, inlineStyle)
    );
  }
  render() {
    const { editorState } = this.state;
    // If the user changes block type before entering any text, we can
    // either style the placeholder or hide it. Let's just hide it now.
    let className = "RichEditor-editor";
    var contentState = editorState.getCurrentContent();
    if (!contentState.hasText()) {
      if (
        contentState
          .getBlockMap()
          .first()
          .getType() !== "unstyled"
      ) {
        className += " RichEditor-hidePlaceholder";
      }
    }
    return (
      <div className="RichEditor-root">
        <BlockStyleControls
          editorState={editorState}
          onToggle={this.toggleBlockType}
        />
        <InlineStyleControls
          editorState={editorState}
          onToggle={this.toggleInlineStyle}
        />
        <div className={className} onClick={this.focus}>
          <Editor
            blockStyleFn={getBlockStyle}
            customStyleMap={styleMap}
            editorState={editorState}
            handleKeyCommand={this.handleKeyCommand}
            keyBindingFn={this.mapKeyToEditorCommand}
            onChange={this.onChange}
            placeholder=""
            ref="editor"
            spellCheck={true}
          />
        </div>
      </div>
    );
  }
}
// Custom overrides for "code" style.
const styleMap = {
  CODE: {
    backgroundColor: "rgba(0, 0, 0, 0.05)",
    fontFamily: '"Inconsolata", "Menlo", "Consolas", monospace',
    fontSize: 16,
    padding: 2
  }
};
function getBlockStyle(block) {
  switch (block.getType()) {
    case "blockquote":
      return "RichEditor-blockquote";
    default:
      return null;
  }
}
class StyleButton extends React.Component {
  constructor() {
    super();
    this.onToggle = e => {
      e.preventDefault();
      this.props.onToggle(this.props.style);
    };
  }
  render() {
    let className = "RichEditor-styleButton";
    if (this.props.active) {
      className += " RichEditor-activeButton";
    }
    return (
      <span className={className} onMouseDown={this.onToggle}>
        {this.props.label}
      </span>
    );
  }
}
const BLOCK_TYPES = [
  { label: "H1", style: "header-one" },
  { label: "H2", style: "header-two" },
  { label: "H3", style: "header-three" },
  { label: "H4", style: "header-four" },
  { label: "H5", style: "header-five" },
  { label: "H6", style: "header-six" },
  // { label: "Blockquote", style: "blockquote" },
  { label: "UL", style: "unordered-list-item" },
  { label: "OL", style: "ordered-list-item" }
  // { label: "Code Block", style: "code-block" }
];
const BlockStyleControls = props => {
  const { editorState } = props;
  const selection = editorState.getSelection();
  const blockType = editorState
    .getCurrentContent()
    .getBlockForKey(selection.getStartKey())
    .getType();
  return (
    <div className="RichEditor-controls">
      {BLOCK_TYPES.map(type => (
        <StyleButton
          key={type.label}
          active={type.style === blockType}
          label={type.label}
          onToggle={props.onToggle}
          style={type.style}
        />
      ))}
    </div>
  );
};
var INLINE_STYLES = [
  { label: "Bold", style: "BOLD" },
  { label: "Italic", style: "ITALIC" },
  { label: "Underline", style: "UNDERLINE" },
  { label: "Monospace", style: "CODE" }
];
const InlineStyleControls = props => {
  const currentStyle = props.editorState.getCurrentInlineStyle();

  return (
    <div className="RichEditor-controls">
      {INLINE_STYLES.map(type => (
        <StyleButton
          key={type.label}
          active={currentStyle.has(type.style)}
          label={type.label}
          onToggle={props.onToggle}
          style={type.style}
        />
      ))}
    </div>
  );
};
