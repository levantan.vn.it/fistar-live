import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import {
  getCampaignDetailAction,
  updateReviewAction
} from "./../../../../store/actions/campaign";
import { getCodeAction } from "./../../../../store/actions/code";
import { DateFormatYMD } from "./../../../../common/helper";
import EditorReview from "./EditorReview";
import { Modal, Button } from "react-bootstrap";
import cancelImage from "./../../../../images/cancel.svg";
import {
  getImageLink,
} from "./../../../../common/helper";
import {
  IMAGE_SIZE,
  IMAGE_TYPE
} from "./../../../../constants/index";

const SNS_STATE = {
  READY: 101,
  REQUEST: 102,
  CHECKING: 103,
  MODIFY: 104,
  COMPLETE: 105
};

class WriteReview extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        m_ch_title: "",
        m_ch_category: "",
        m_ch_content: "",
        m_ch_url: "",
        sns_id: "",
      },
      errors: {
        m_ch_title: "",
        m_ch_category: "",
        m_ch_content: "",
        m_ch_url: "",
        sns_id: ""
      },
      isLoadingCampaign: false,
      isCancel: false,
      isLoadingSubmit: false,
      reset: true,
      isShowModal: false,
      isSubmit: false,
      images: [],
      imagePreviewUrl: [],
      remove_id: [],
      bigSizeMultiple: false,
    };
  }

  componentDidMount() {
    if (Object.keys(this.props.campaign.data).length === 0) {
      const {
        match: { params }
      } = this.props;
      const { id } = params;
      this.initData(id);
    }
    if (!this.props.code.data.catalog) {
      this.props.getCode("catalog");
    }
  }

  static getDerivedStateFromProps(props, state) {
    if (state.reset) {
      let channels = [];
      const { auth, campaign } = props;
      const { isLoadingCampaign } = state;
      if (
        !isLoadingCampaign &&
        campaign &&
        Object.keys(campaign.data).length > 0
      ) {
        let sns = [];
        campaign.data.matchings.map(fistar => {
          if (fistar.influencer.email == auth.email) {
            sns = fistar.matching_channel;
          }
        });
        channels = sns;
      }
      let sns = null;
      channels.map(channel => {
        if (+channel.sns_id === +props.match.params.sns_id) {
          sns = channel;
        }
      });
      if (!sns) {
        return null;
      }
      let imagePreviewUrl = sns.galery.map(e => 
        getImageLink(
          e.image,
          IMAGE_TYPE.REVIEWS,
          IMAGE_SIZE.MEDIUM
        )
      )
      // if (sns.review_status.rv_status === SNS_STATE.READY) {
      //   return {
      //     data: {
      //       ...state.data,
      //       m_ch_category: campaign.data.cp_category,
      //       sns_id: props.match.params.sns_id,
      //     },
      //     reset: false,
      //   }
      // }
      return {
        data: {
          ...state.data,
          m_ch_title: sns.m_ch_title,
          m_ch_category: sns.m_ch_category
            ? sns.m_ch_category
            : props.campaign.data.cp_category,
          m_ch_content: sns.m_ch_content,
          m_ch_url: sns.m_ch_url,
          sns_id: props.match.params.sns_id
        },
        images: sns.galery,
        imagePreviewUrl,
        reset: false,
      };
      //   return {
      //     data: {
      //       ...state.data,
      //       m_ch_title: props.campaign.data,
      //       m_ch_category: '',
      //       m_ch_content: '',
      //       m_ch_url: '',
      //     }
      //   },
    }
    return null;
  }

  handleChange = e => {
    this.setState({
      data: {
        ...this.state.data,
        [e.target.name]: e.target.value
      }
    });
  };

  handleChangeSNS = e => {
    let channels = this.getSNSReview();
    let sns = null;
    channels.map(channel => {
      if (+channel.sns_id === +e.target.value) {
        sns = channel;
      }
    });
    this.setState({
      data: {
        ...this.state.data,
        [e.target.name]: e.target.value,
        m_ch_title: sns.m_ch_title,
        m_ch_category: sns.m_ch_category,
        m_ch_content: sns.m_ch_content,
        m_ch_url: sns.m_ch_url
      }
    });
  };

  handleFocus = e => {
    this.setState({
      errors: {
        m_ch_title: "",
        m_ch_category: "",
        m_ch_content: "",
        m_ch_url: "",
        sns_id: "",
        bigSizeMultiple: this.state.errors.bigSizeMultiple
      }
    });
  };

  onChangeTextEdtitor = value => {
    this.setState({
      data: {
        ...this.state.data,
        m_ch_content: value.toString("html")
      }
    });
  };

  initData = id => {
    this.setState(
      {
        isLoadingCampaign: true
      },
      () => {
        this.props
          .getCampaign(id)
          .then(response => {
            this.setState({
              isLoadingCampaign: false,
              reset: true
            });
          })
          .catch(() => {
            this.setState({
              isLoadingCampaign: false
            });
          });
      }
    );
  };

  removeImage = (imgRemove, key) => {
    let {
      imagePreviewUrl,
      images,
    } = this.state;
    imagePreviewUrl[key] = undefined;
    images[key] = undefined;
    imagePreviewUrl = imagePreviewUrl.filter(el => el != null);
    images = images.filter(el => el != null);
    let bigSizeMultiple = false;
    images.map(img => {
      if (
        img &&
        img.size &&
        img.size > process.env.REACT_APP_MAX_IMAGE_SIZE * 1000000
      ) {
        bigSizeMultiple = true;
      }
    });
    this.setState(
      {
        imagePreviewUrl: imagePreviewUrl,
        images: images,
        remove_id: imgRemove.id ? [...this.state.remove_id, imgRemove.id] : [...this.state.remove_id],
        errors: {
          ...this.state.errors,
          bigSizeMultiple
        }
      });
  };

  // setImage = (files, index = 0) => {
  //   if (files.length - 1 === index) {
  //     return
  //   }
  //   let reader = new FileReader();
  //   let file = files[index];
  //   const fileType = file["type"];
  //   const validImageTypes = ["image/gif", "image/jpeg", "image/png"];
  //   if (validImageTypes.includes(fileType)) {
  //     reader.onloadend = () => {
  //       this.setState({
  //         images: [...this.state.images, file],
  //         imagePreviewUrl: [...this.state.imagePreviewUrl, reader.result]
  //       }, () => {
  //         this.setImage(files, index + 1)
  //       });
  //     };

  //     reader.readAsDataURL(file);
  //   }
  // }

  handleImageChange = e => {
    e.preventDefault();
    const { images } = this.state
    if (images.length >= 7) {
      return
    }
    let emptyImage = 7 - images.length
    let length = emptyImage
    if (emptyImage > e.target.files.length) {
      length = e.target.files.length
    }
    // this.setImage(e.target.files)
    for (let i = 0; i < length; i++) {
      if (this.state.images.length < 7) {
        let reader = new FileReader();
        let file = e.target.files[i];
        const fileType = file["type"];
        const validImageTypes = ["image/gif", "image/jpeg", "image/png"];
        if (validImageTypes.includes(fileType)) {
          let bigSizeMultiple = false;
          if (this.state.bigSizeMultiple) {
            bigSizeMultiple = this.state.bigSizeMultiple;
          } else {
            bigSizeMultiple =
              file &&
              file.size &&
              file.size > process.env.REACT_APP_MAX_IMAGE_SIZE * 1000000
                ? true
                : false;
          }
          reader.onloadend = () => {
            this.setState({
              images: [...this.state.images, file],
              imagePreviewUrl: [...this.state.imagePreviewUrl, reader.result],
              errors: {
                ...this.state.errors,
                bigSizeMultiple,
              }
            });
          };

          reader.readAsDataURL(file);
        }
      }
    }
  };

  onClickCancel = () => {
    this.setState({
      isCancel: true
    });
  };

  validateForm = () => {
    const errors = {
      m_ch_title: "",
      m_ch_category: "",
      m_ch_content: "",
      m_ch_url: "",
      sns_id: "",
      bigSizeMultiple: this.state.errors.bigSizeMultiple || ''
    };
    const { data } = this.state;

    if (!data.m_ch_title) {
      errors.m_ch_title = "The title field is required.";
    }
    if (!data.m_ch_category) {
      errors.m_ch_category = "The category field is required.";
    }
    if (!data.m_ch_content) {
      errors.m_ch_content = "The content field is required.";
    }
    if (!data.sns_id) {
      errors.sns_id = "The channel field is required.";
    }

    return errors;
  };

  callrequest = (id, data, rv_status) => {
    console.log(this.state, id, data, rv_status, '----------');
    this.props
      .updateReview(id, data)
      .then(() => {
        this.setState(
          {
            isLoadingSubmit: false
          },
          () => {
            if (rv_status === SNS_STATE.CHECKING) {
              this.onClickCancel();
            } else {
              this.initData(this.props.match.params.id);
            }
          }
        );
      })
      .catch(er => {
        const {
          data: { errors }
        } = er;
        this.setState({
          isLoadingSubmit: false,
          errors: {
            m_ch_title:
              errors.m_ch_title && errors.m_ch_title[0]
                ? errors.m_ch_title[0]
                : "",
            m_ch_category:
              errors.m_ch_category && errors.m_ch_category[0]
                ? errors.m_ch_category[0]
                : "",
            m_ch_content:
              errors.m_ch_content && errors.m_ch_content[0]
                ? errors.m_ch_content[0]
                : "",
            m_ch_url:
              errors.m_ch_url && errors.m_ch_url[0] ? errors.m_ch_url[0] : "",
            sns_id: errors.sns_id && errors.sns_id[0] ? errors.sns_id[0] : ""
          }
        });
      });
  };

  handleSubmit = (e, rv_status) => {
    e.preventDefault();
    if (rv_status === SNS_STATE.CHECKING) {
      this.setState({
        data: {
          ...this.state.data,
          rv_status
        },
        isShowModal: true
      });
      return;
    }
    const { data, errors } = this.state;
    const validate = this.validateForm();
    this.setState(
      {
        isLoadingSubmit: true
      },
      () => {
        if (
          validate.m_ch_title == "" &&
          validate.m_ch_category == "" &&
          validate.m_ch_content == "" &&
          validate.m_ch_url == "" &&
          validate.sns_id == "" &&
          validate.bigSizeMultiple == false
        ) {
          let channels = this.getSNSReview();
          let m_id = channels[0].m_id;
          // (this.props.campaign.data.influencers || []).map((fistar) => {
          //   if (fistar.email === this.props.auth.email) {
          //     m_id = fistar.v_review_status[0] ? fistar.v_review_status[0].m_id : ''
          //   }
          // })
          let images = this.state.images
          images = images
            .map((image) => !image.id ? image : null)
            .filter(e => e !== null)
          let dataSubmit = {
            ...this.state.data,
            rv_status,
            'remove_id': this.state.remove_id
          }
          if (images.length > 0) {
            dataSubmit.images = images
          }
          let formData = new FormData();
            Object.keys(dataSubmit)
            .map(key => {
              if (!!dataSubmit[key]) {
                if (dataSubmit[key].constructor === Array) {
                  dataSubmit[key].map(item => {
                    formData.append(key + "[]", item);
                  });
                } else {
                  formData.append(key, dataSubmit[key]);
                }
              } else if (dataSubmit[key] === 0) {
                formData.append(key, dataSubmit[key]);
              }
            });
          this.callrequest(m_id, formData, rv_status);
        } else {
          this.setState({
            isLoadingSubmit: false,
            errors: validate
          });
        }
      }
    );
  };

  getSNSReview = () => {
    const { auth, campaign } = this.props;
    const { isLoadingCampaign } = this.state;
    if (
      !isLoadingCampaign &&
      campaign &&
      Object.keys(campaign.data).length > 0
    ) {
      let sns = [];
      campaign.data.matchings.map(fistar => {
        if (fistar.influencer.email == auth.email) {
          sns = fistar.matching_channel;
        }
      });
      return sns;
    }
    return [];
  };

  handleClose = (isSubmit = false) => {
    this.setState({
      isShowModal: false,
      isSubmit
    });
  };

  handleShow = () => {
    this.setState({ isShowModal: true });
  };

  onExited = () => {
    if (this.state.isSubmit) {
      this.setState(
        {
          isLoadingSubmit: true
        },
        () => {
          let channels = this.getSNSReview();
          let m_id = channels[0].m_id;

          let images = this.state.images
          images = images
            .map((image) => !image.id ? image : null)
            .filter(e => e !== null)
          let dataSubmit = {
            ...this.state.data,
            'remove_id': this.state.remove_id
          }
          if (images.length > 0) {
            dataSubmit.images = images
          }
          let formData = new FormData();
          Object.keys(dataSubmit)
          .map(key => {
            if (!!dataSubmit[key]) {
              if (dataSubmit[key].constructor === Array) {
                dataSubmit[key].map(item => {
                  formData.append(key + "[]", item);
                });
              } else {
                formData.append(key, dataSubmit[key]);
              }
            } else if (dataSubmit[key] === 0) {
              formData.append(key, dataSubmit[key]);
            }
          });
          this.callrequest(m_id, formData, SNS_STATE.CHECKING);
        }
      );
    }
  };

  renderSNSChannel = () => {
    const { errors } = this.state;
    let channels = this.getSNSReview();

    let rv_status = "";
    let sns_name = "";
    channels.map(channel => {
      if (+channel.sns_id === +this.state.data.sns_id) {
        rv_status = channel.review_status.rv_status;
        sns_name = channel.sns.sns_name;
      }
    });

    return (
      <Fragment>
        {![SNS_STATE.REQUEST, SNS_STATE.CHECKING, SNS_STATE.COMPLETE].includes(
          +rv_status
        ) ? (
          <Fragment>
            <select
              className={
                errors.sns_id ? "form-control is-invalid" : "form-control"
              }
              id="sns_id"
              name="sns_id"
              value={this.state.data.sns_id}
              onChange={this.handleChangeSNS}
              onFocus={this.handleFocus}
            >
              {channels.map(
                (channel, key) =>
                  channel.m_ch_selected && (
                    <option value={channel.sns_id} key={key}>
                      {channel.sns ? channel.sns.sns_name : ""}
                    </option>
                  )
              )}
            </select>
            {errors.sns_id && (
              <div className="invalid-feedback">{errors.sns_id}</div>
            )}
          </Fragment>
        ) : (
          <Fragment>{sns_name}</Fragment>
        )}
      </Fragment>
    );
  };

  renderCategory = () => {
    const { data, errors } = this.state;

    let channels = this.getSNSReview();
    let rv_status = "";
    let category = "";
    channels.map(channel => {
      if (+channel.sns_id === +this.state.data.sns_id) {
        rv_status = channel.review_status.rv_status;
      }
    });

    if (
      this.props.code.data &&
      this.props.code.data.catalog &&
      this.props.code.data.catalog.code
    ) {
      if (
        [SNS_STATE.REQUEST, SNS_STATE.CHECKING, SNS_STATE.COMPLETE].includes(
          +rv_status
        )
      ) {
        this.props.code.data.catalog.code.map((cate, key) => {
          if (cate.cd_id === this.state.data.m_ch_category) {
            category = cate.cd_label;
          }
        });
        return category;
      }

      return (
        <Fragment>
          <select
            className={
              errors.m_ch_category ? "form-control is-invalid" : "form-control"
            }
            id="m_ch_category"
            name="m_ch_category"
            value={data.m_ch_category}
            onChange={this.handleChange}
            onFocus={this.handleFocus}
          >
            <option />
            {this.props.code.data.catalog.code.map((cate, key) => (
              <option value={cate.cd_id} key={key}>
                {cate.cd_label}
              </option>
            ))}
          </select>
          {errors.m_ch_category && (
            <div className="invalid-feedback">{errors.m_ch_category}</div>
          )}
        </Fragment>
      );
    }
    return null;
  };

  renderButton = () => {
    let channels = this.getSNSReview();
    let rv_status = "";
    const { t } = this.props;
    channels.map(channel => {
      if (+channel.sns_id === +this.state.data.sns_id) {
        rv_status = channel.review_status.rv_status;
      }
    });
    let button = "";
    switch (rv_status) {
      case SNS_STATE.READY: {
        button = (
          <div className="gr-btn-review">
            <button type="button" onClick={this.onClickCancel}>
              {t("WRITE_REVIEW.WRW_button_cancel")}
            </button>
            <button
              type="submit"
              onClick={e => this.handleSubmit(e, SNS_STATE.REQUEST)}
            >
              {this.state.isLoadingSubmit ? (
                <div className="spinner-border" role="status">
                  <span className="sr-only">{t("LOADING.LOADING")}</span>
                </div>
              ) : (
                `${t("WRITE_REVIEW.WRW_button_save")}`
              )}
            </button>
          </div>
        );
        break;
      }
      case SNS_STATE.REQUEST: {
        button = (
          <div className="gr-btn-review">
            <button type="button" onClick={this.onClickCancel}>
              {t("QA_DETAIL.QDL_list")}
            </button>
            <button
              type="button"
              onClick={e => this.handleSubmit(e, SNS_STATE.READY)}
            >
              {this.state.isLoadingSubmit ? (
                <div className="spinner-border" role="status">
                  <span className="sr-only">{t("LOADING.LOADING")}</span>
                </div>
              ) : (
                `${t("WRITE_REVIEW.WRW_button_edit")}`
              )}
            </button>
            <button
              type="button"
              onClick={e => this.handleSubmit(e, SNS_STATE.CHECKING)}
            >
              {this.state.isLoadingSubmit ? (
                <div className="spinner-border" role="status">
                  <span className="sr-only">{t("LOADING.LOADING")}</span>
                </div>
              ) : (
                `${t("WRITE_REVIEW.WRW_button_request")}`
              )}
            </button>
          </div>
        );
        break;
      }
      case SNS_STATE.CHECKING: {
        button = (
          <div className="gr-btn-review">
            <button type="button" onClick={this.onClickCancel}>
              {t("WRITE_REVIEW.WRW_button_cancel")}
            </button>
          </div>
        );
        break;
      }
      case SNS_STATE.MODIFY: {
        button = (
          <div className="gr-btn-review">
            <button type="button" onClick={this.onClickCancel}>
              List
            </button>
            <button
              type="submit"
              onClick={e => this.handleSubmit(e, SNS_STATE.REQUEST)}
            >
              {this.state.isLoadingSubmit ? (
                <div className="spinner-border" role="status">
                  <span className="sr-only">Loading...</span>
                </div>
              ) : (
                "Edit"
              )}
            </button>
          </div>
        );
        break;
      }
      case SNS_STATE.COMPLETE: {
        button = (
          <div className="gr-btn-review">
            <button type="button" onClick={this.onClickCancel}>
              {t("WRITE_REVIEW.WRW_button_cancel")}
            </button>
            {/*<button type="button" onClick={e => this.handleSubmit(e, SNS_STATE.COMPLETE)}>
              {this.state.isLoadingSubmit ? (
                <div className="spinner-border" role="status">
                  <span className="sr-only">{t("LOADING.LOADING")}</span>
                </div>
              ) : (
                "Save"
              )}
            </button>*/}
          </div>
        );
        break;
      }
      default:
        break;
    }
    return button;
  };

  renderFormSaveSNS = () => {
    const { t } = this.props;
    return (
      <div className="save-sns-url">
        <h4 className="title">{t("WRITE_REVIEW.registered_SNS")}</h4>
        <div className="content-sns">
          <div className="left-save-sns">
            <textarea
              className="form-control"
              id="m_ch_url"
              name="m_ch_url"
              rows="5"
              onChange={this.handleChange}
              value={this.state.data.m_ch_url}
            />
          </div>
          <div className="right-save-sns">
            <button
              type="button"
              className="btn-save-sns"
              onClick={e => this.handleSubmit(e, SNS_STATE.COMPLETE)}
            >
              {t("WRITE_REVIEW.save_SNS_URL")}
            </button>
          </div>
        </div>
      </div>
    );
  };

  render() {
    const { t, campaign, code } = this.props;
    const { data, isCancel, errors, isLoadingSubmit, imagePreviewUrl } = this.state;

    if (isCancel) {
      return (
        <Redirect to={`/fi-star/my-campaign-detail/${campaign.data.cp_slug}`} />
      );
    }

    let channels = this.getSNSReview();
    let rv_status = "";
    let time = "";
    channels.map(channel => {
      if (+channel.sns_id === +this.state.data.sns_id) {
        rv_status = channel.review_status.rv_status;
        time = DateFormatYMD(channel.review_status.updated_at, ".");
      }
    });

    console.log(campaign);

    return (
      <Fragment>
        <div className="content myfistar-mycampaign fime-review-write">
          <div className="container">
            <div className="top-fime-review d-flex justify-content-between">
              <div className="name left-fimereview">
                <span className="wrap-matching">
                  {t("POPUP_REVIEW_MODIFY.PRM_ready")}
                </span>
                <div className="text-campaign">
                  <p className="font-weight-bold text-uppercase">{!campaign.data.branch ? campaign.data.cp_brand : campaign.data.branch.CODE_NM}</p>
                  <h4>{campaign.data.cp_name}</h4>
                </div>
              </div>
              <div className="d-flex">
                <span className="d-flex align-items-end justify-content-around">
                  {time}
                </span>
              </div>
            </div>
            <form onSubmit={this.handleSubmit}>
              <div className="form-fimereview">
                <div className="row">
                  <div className="col-md-8">
                    <div className="row">
                      <div className="col-md-6">
                        <div className="row pb-3">
                          <label className="col-3">
                            {t("POPUP_REVIEW_MODIFY.PRM_channel")}
                          </label>
                          <div className="col-9 text-category font-weight-bold">
                            {this.renderSNSChannel()}
                          </div>
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="row pb-3">
                          <label className="col-3">
                            {t("POPUP_REVIEW_MODIFY.PRM_category")}
                          </label>
                          <div className="col-9 text-category text-category-key font-weight-bold">
                            {this.renderCategory()}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row">
                  {/*<label className="col-3 col-md-1">*/}
                  {/*{t("POPUP_REVIEW_MODIFY.PRM_title")}*/}
                  {/*</label>*/}
                  <div className="col-md-12 text-title-review">
                    {![
                      SNS_STATE.REQUEST,
                      SNS_STATE.CHECKING,
                      SNS_STATE.COMPLETE
                    ].includes(+rv_status) ? (
                      <Fragment>
                        <input
                          type="text"
                          className={
                            errors.m_ch_title
                              ? "form-control is-invalid"
                              : "form-control"
                          }
                          id="m_ch_title"
                          name="m_ch_title"
                          value={data.m_ch_title}
                          onChange={this.handleChange}
                          onFocus={this.handleFocus}
                        />
                        {errors.m_ch_title && (
                          <div className="invalid-feedback">
                            {errors.m_ch_title}
                          </div>
                        )}
                      </Fragment>
                    ) : (
                      <p className="h5 mb-0 mt-2 font-weight-bold">{data.m_ch_title}</p>
                    )}
                  </div>
                </div>
                <div className="row">
                  {/*<label className="col-3 col-md-1">*/}
                  {/*{t("POPUP_REVIEW_MODIFY.PRM_title")}*/}
                  {/*</label>*/}
                  <div className="col-md-12 image-review justify-content-start">
                    {imagePreviewUrl.map((photo, key) =>
                      <div className="item-img" key={key}>
                        <img src={photo} className="mt-0 w-100 h-100" />
                        {![
                          SNS_STATE.REQUEST,
                          SNS_STATE.CHECKING,
                          SNS_STATE.COMPLETE
                        ].includes(+rv_status) && (
                          <div className="icon-delete-review">
                            <i
                              className="fa fa-times"
                              onClick={() => this.removeImage(this.state.images[key], key)}
                            />
                          </div>
                        )}
                      </div>
                    )}
                    {![
                      SNS_STATE.REQUEST,
                      SNS_STATE.CHECKING,
                      SNS_STATE.COMPLETE
                    ].includes(+rv_status) && this.state.images.length < 7 && (
                      <div className="item add">
                        <div className="icon-item icon-add">
                          <i className="fas fa-camera" />
                          <input
                            className="fileInput"
                            type="file"
                            accept="image/*"
                            multiple="multiple"
                            onChange={this.handleImageChange}
                            value=""
                          />
                        </div>
                      </div>
                    )}
                  </div>
                    {errors.bigSizeMultiple && (
                      <p className="text-danger pt-3">{'max file size'}</p>
                    )}
                </div>
              </div>
              <div className="content-review">
                {/*<textarea
                  className="form-control"
                  rows="15"
                  id="m_ch_content"
                  name="m_ch_content"
                  value={data.m_ch_content}
                  onChange={this.handleChange}
                />*/}
                {![
                  SNS_STATE.REQUEST,
                  SNS_STATE.CHECKING,
                  SNS_STATE.COMPLETE
                ].includes(+rv_status) || this.state.reset ? (
                  <Fragment>
                    <EditorReview
                      value={data.m_ch_content}
                      sns={data.sns_id}
                      onChange={this.onChangeTextEdtitor}
                      onFocus={this.handleFocus}
                    />
                    {errors.m_ch_content && (
                      <p className="text-danger">{errors.m_ch_content}</p>
                    )}
                  </Fragment>
                ) : (
                  <div className="review">
                    <div
                      dangerouslySetInnerHTML={{ __html: data.m_ch_content }}
                    />
                  </div>
                )}
              </div>
              {this.renderButton()}
            </form>
            {/*SNS_STATE.COMPLETE === rv_status && this.renderFormSaveSNS()*/}
          </div>
        </div>
        <Modal
          show={this.state.isShowModal}
          onHide={() => this.handleClose(false)}
          onExited={this.onExited}
          dialogClassName="howto-register modal-dialog-centered"
        >
          <div className="modal-content">
            <div className="register">
              <div className="top modal-header">
                <h4>{t("POPUP_MODIFY.PMY_fistar")}</h4>
                <button
                  type="button"
                  className="btn btn-close close"
                  onClick={() => this.handleClose(false)}
                >
                  <img src={cancelImage} alt="close" />
                </button>
              </div>
              <div className="body-popup">
                <p className="mb-3">
                  {t("POPUP_MODIFY.PMY_request_review")}{" "}
                  <span className="text-primary">
                    {campaign.data.partner ? campaign.data.partner.pc_name : ""}
                  </span>{" "}
                  {t("POPUP_MODIFY.PMY_manager")}
                </p>
                <p>{t("POPUP_MODIFY.PMY_partner_considering")}</p>
                <button type="button" onClick={() => this.handleClose(true)}>
                  {t("POPUP_MODIFY.PMY_button_ok")}
                </button>
              </div>
            </div>
          </div>
        </Modal>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth,
    campaign: state.campaign,
    code: state.code
  };
};
const mapDispatchToProps = dispatch => {
  return {
    getCampaign: id => dispatch(getCampaignDetailAction(id)),
    getCode: type => dispatch(getCodeAction(type)),
    updateReview: (id, data) => dispatch(updateReviewAction(id, data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(WriteReview));
