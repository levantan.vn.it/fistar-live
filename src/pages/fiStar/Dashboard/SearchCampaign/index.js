import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import imgaeIdol from "./../../../../images/dash-02.png";
import imageFace from "./../../../../images/dashboard/face.png";
import imageIntar from "./../../../../images/dashboard/intar.png";
import imageToutube from "./../../../../images/toutube.png";
import imageRead from "./../../../../images/mk.png";
import imageLogo from "./../../../../images/dashboard/Layer 6.png";
import Campaign from "./../../../../components/fiStar/Dashboard/MyCampaign/Campaign";
import { convertVi } from "./../../../../common/helper";
import {
  getSearchCampaignAction,
  getCountCampaignAction
} from "./../../../../store/actions/campaign";
import InfiniteScroll from "react-infinite-scroll-component";
import "./searchCampaign.scss";
import { getCodeAction } from "../../../../store/actions/code";

const SEARCH_TYPE = {
  ALL: "",
  REQUEST: "request",
  APPLY: "apply",
  SCRAP: "scrap",
  RECOMENDED: "recommended"
};

class FistarSearchCampaign extends Component {
  constructor(props) {
    super(props);
    this.state = {
      type: "",
      campaigns: [],
      total: 0,
      request: 0,
      apply: 0,
      scrap: 0,
      recommended: 0,
      textTitle: `${this.props.t("MY_CAMPAIGN.MCN_tab_allcampaign")}`,
      last_page: 1,
      page: 1,
      hasMore: true,
      isLoadingCampaign: false,
      search: "",
      campaign: "",
      keyword: "",
      keywordSearch: "",
      reset: true,
      all: 0,
      by_relate: "",
      total_result: 0,
    };
  }

  componentDidMount() {
    this.setState(
      {
        isLoadingCampaign: true,
        reset: true
      },
      () => {
        this.initData();
        this.props.getCode("location");
      }
    );
  }

  initData = () => {
    console.log(this.state, "state init data");
    this.state.keyword = this.state.keywordSearch
      ? convertVi(this.state.keywordSearch)
      : this.state.keywordSearch;
    console.log(this.state.keyword);
    this.props
      .getCampaign(
        this.state.page,
        this.state.type,
        this.state.search,
        this.state.campaign,
        this.state.keyword,
        this.state.by_relate
      )
      .then(response => {
        this.setState({
          campaigns: response.data,
          total:
            // !this.state.reset && this.state.type !== SEARCH_TYPE.ALL
            this.state.total
              ? this.state.total
              : response.total,
          isLoadingCampaign: false,
          last_page: response.last_page,
          reset: false,
          total_result: response.total
        });
      })
      .catch(() => {
        this.setState({
          isLoadingCampaign: false,
          reset: false
        });
      });
    this.props
      .getCountCampaign()
      .then(response => {
        this.setState({
          request: response.request,
          apply: response.apply,
          scrap: response.scrap,
          recommended: response.recommended,
          all: response.matching + response.ongoing + response.ready
        });
      })
      .catch(() => {
        this.setState({
          isLoadingCampaign: false,
          reset: false
        });
      });
  };

  clickChangeTab = type => {
    const { t } = this.props;
    let textTitle = "";
    let by_relate = "";

    switch (type) {
      case SEARCH_TYPE.SCRAP: {
        textTitle = `${t("CAMPAIGN_SEARCH.CSH_amount_scrap")}`;
        by_relate = "";
        break;
      }
      case SEARCH_TYPE.RECOMENDED: {
        textTitle = `${t("CAMPAIGN_SEARCH.CSH_amount_recommend")}`;
        by_relate = "";
        break;
      }
      case SEARCH_TYPE.REQUEST: {
        textTitle = `${t("CAMPAIGN_SEARCH.CSH_amount_request")}`;
        by_relate = 1;
        break;
      }
      case SEARCH_TYPE.APPLY: {
        textTitle = `${t("CAMPAIGN_SEARCH.CSH_amount_apply")}`;
        by_relate = 1;
        break;
      }
      default: {
        textTitle = `${t("MY_CAMPAIGN.MCN_tab_allcampaign")}`;
        break;
      }
    }

    this.setState(
      {
        type,
        page: 1,
        isLoadingCampaign: true,
        textTitle,
        by_relate: by_relate,
        hasMore: true,
        campaign: '',
      },
      () => {
        this.initData();
      }
    );
  };

  fetchMoreData = () => {
    if (this.state.page >= this.state.last_page) {
      this.setState({ hasMore: false });
      return;
    }

    this.props
      .getCampaign(
        this.state.page + 1,
        this.state.type,
        this.state.search,
        this.state.campaign,
        this.state.keyword,
        this.state.by_relate
      )
      .then(response => {
        this.setState({
          campaigns: [...this.state.campaigns, ...response.data],
          page: this.state.page + 1
        });
      });
  };

  handleChangeInput = e => {
    this.setState({
      // search: e.target.value
      campaign: e.target.name,
      keywordSearch: e && e.target.value ? e.target.value : ""
    });
  };

  clickSearch = e => {
    e.preventDefault();
    this.setState(
      {
        page: 1,
        isLoadingCampaign: true,
        reset: true
      },
      () => {
        this.initData();
      }
    );
  };

  render() {
    const { t, auth } = this.props;
    const {
      type,
      campaigns,
      total,
      request,
      apply,
      scrap,
      recommended,
      textTitle,
      last_page,
      page,
      hasMore,
      keyword,
      isLoadingCampaign,
      search,
      reset,
      keywordSearch,
      all,
      campaign,
      total_result
    } = this.state;

    let totalCam = "";
    if (campaign !== "") {
      totalCam = total;
    } else {
      switch (type) {
        case SEARCH_TYPE.SCRAP: {
          totalCam = scrap;
          break;
        }
        case SEARCH_TYPE.RECOMENDED: {
          totalCam = recommended;
          break;
        }
        case SEARCH_TYPE.REQUEST: {
          totalCam = request;
          break;
        }
        case SEARCH_TYPE.APPLY: {
          totalCam = apply;
          break;
        }
        default: {
          totalCam = total;
          break;
        }
      }
    }
    console.log(totalCam, total, 'ffffffffffff');

    return (
      <div className="content myfistar-campaign mypage-partner-campaign-tracking search-campaign-main">
        <div className="top w-100">
          <div className="title d-flex">
            <h4>
              <a href="javascript:void(0)">
                {t("CAMPAIGN_SEARCH.CSH_campaign_search")}
              </a>
            </h4>
          </div>
          <form
            className="form-inline search-campaign my-2 my-lg-0"
            onSubmit={this.clickSearch}
          >
            <input
              className="form-control mr-sm-2 input-search"
              type="search"
              aria-label="Search"
              id="campaign_name"
              name="campaign_name"
              value={keywordSearch}
              onChange={this.handleChangeInput}
            />
            <button
              className="btn btn-outline-success my-2 my-sm-0"
              type="submit"
              onClick={this.clickSearch}
            >
              <i className="fas fa-search" />
            </button>
          </form>
        </div>
        <div className="information-tracking">
          <ul className="list-information list-myfistar-title wf-list-fistar">
            <li
              className={`item item-myfistar${
                type === SEARCH_TYPE.ALL ? " active" : ""
              }`}
              onClick={() => this.clickChangeTab(SEARCH_TYPE.ALL)}
            >
              <a href="javascript:void(0)" className="text">
                <p className="number">{!total ? "0" : total}</p>
                <p>{t("CAMPAIGN_SEARCH.CSH_amount_all")}</p>
              </a>
            </li>
            <li
              className={`item item-myfistar${
                type === SEARCH_TYPE.SCRAP ? " active" : ""
              }`}
              onClick={() => this.clickChangeTab(SEARCH_TYPE.SCRAP)}
            >
              <a href="javascript:void(0)" className="text">
                <p className="number">{!scrap ? "0" : scrap}</p>
                <p>{t("CAMPAIGN_SEARCH.CSH_amount_scrap")}</p>
              </a>
            </li>
            <li
              className={`item item-myfistar${
                type === SEARCH_TYPE.RECOMENDED ? " active" : ""
              }`}
              onClick={() => this.clickChangeTab(SEARCH_TYPE.RECOMENDED)}
            >
              <a href="javascript:void(0)" className="text">
                <p className="number">{!recommended ? "0" : recommended}</p>
                <p>{t("CAMPAIGN_SEARCH.CSH_amount_recommend")}</p>
              </a>
            </li>
            <li
              className={`item item-myfistar${
                type === SEARCH_TYPE.REQUEST ? " active" : ""
              }`}
              onClick={() => this.clickChangeTab(SEARCH_TYPE.REQUEST)}
            >
              <a href="javascript:void(0)" className="text">
                <p className="number">{!request ? "0" : request}</p>
                <p>{t("CAMPAIGN_SEARCH.CSH_amount_request")}</p>
              </a>
            </li>
            <li
              className={`item item-myfistar${
                type === SEARCH_TYPE.APPLY ? " active" : ""
              }`}
              onClick={() => this.clickChangeTab(SEARCH_TYPE.APPLY)}
            >
              <a href="javascript:void(0)" className="text">
                <p className="number">{!apply ? "0" : apply}</p>
                <p>{t("CAMPAIGN_SEARCH.CSH_amount_apply")}</p>
              </a>
            </li>
          </ul>
        </div>

        <div className="list-campaign-track">
          <div className="title">
            <h4>{`${textTitle} ${isLoadingCampaign ? '' : total_result}`}</h4>
          </div>
          {isLoadingCampaign ? (
            <div className="list-campaign-track text-center">
              <div className="spinner-border" role="status">
                <span className="sr-only">{t("LOADING.LOADING")}</span>
              </div>
            </div>
          ) : (
            <Fragment>
              {campaigns.length === 0 && !isLoadingCampaign ? (
                <Fragment>
                  <div className={"text-center mt-5"}>
                    {t("CAMPAIGN.CAMPAIGN_no_campaign")}
                  </div>
                </Fragment>
              ) : (
                <InfiniteScroll
                  dataLength={campaigns.length}
                  next={this.fetchMoreData}
                  loader={
                    <div>
                      <div id="loading" />
                    </div>
                  }
                  hasMore={this.state.hasMore}
                  className="box-item search-campaign-page"
                >
                  {campaigns.map((campaign, key) => (
                    <Fragment key={key}>
                      <Campaign campaign={campaign} />
                    </Fragment>
                  ))}
                </InfiniteScroll>
              )}
            </Fragment>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getCampaign: (
      page = 1,
      type = "",
      search = "",
      campaign,
      keyword,
      by_relate = ""
    ) =>
      dispatch(
        getSearchCampaignAction(
          page,
          type,
          search,
          campaign,
          keyword,
          by_relate
        )
      ),
    getCountCampaign: () => dispatch(getCountCampaignAction()),
    getCode: type => dispatch(getCodeAction(type))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(FistarSearchCampaign));
