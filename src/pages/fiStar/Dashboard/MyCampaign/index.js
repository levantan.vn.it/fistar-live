import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import Campaign from "./../../../../components/fiStar/Dashboard/MyCampaign/Campaign";
import {
  getMyCampaignAction,
  getCountCampaignAction
} from "./../../../../store/actions/campaign";
import InfiniteScroll from "react-infinite-scroll-component";
import "./my-campaign.scss";
import { getCodeAction } from "../../../../store/actions/code";

import { convertVi } from "./../../../../common/helper";

const TYPE = {
  ALL: "",
  READY: "ready",
  ONGOING: "ongoing",
  CLOSED: "closed"
};

class FistarMyCampaign extends Component {
  constructor(props) {
    super(props);
    this.state = {
      type: TYPE.ALL,
      campaigns: [],
      total: 0,
      ready: 0,
      onGoing: 0,
      closed: 0,
      all: 0,
      textTitle: `${this.props.t("MY_CAMPAIGN.MCN_tab_allcampaign")}`,
      last_page: 1,
      page: 1,
      hasMore: true,
      isLoadingCampaign: false,
      search: "",
      keyword: "",
      keywordSearch: "",
      campaign: "",
      reset: true,
      searchClick: false
    };
  }

  componentDidMount() {
    this.setState(
      {
        isLoadingCampaign: true,
        reset: true
      },
      () => {
        this.initData();
        this.props.getCode("location");
      }
    );
  }

  initData = () => {
    console.log(this.state, "state init data");
    this.state.keyword = this.state.keywordSearch
      ? convertVi(this.state.keywordSearch)
      : this.state.keywordSearch;
    console.log(this.state.keyword);
    this.props
      .getMyCampaign(
        this.state.page,
        this.state.type,
        this.state.search,
        this.state.campaign,
        this.state.keyword
      )
      .then(response => {
        console.log(response);
        this.setState({
          campaigns: response.data,
          total: !this.state.reset ? this.state.total : response.total,
          isLoadingCampaign: false,
          last_page: response.last_page,
          reset: false
        });
      })
      .catch(() => {
        this.setState({
          isLoadingCampaign: false,
          reset: false
        });
      });
    this.props
      .getCountCampaign()
      .then(response => {
        this.setState({
          ready: response.ready,
          onGoing: response.ongoing,
          closed: response.closed,
          all:
            response.matching +
            response.closed +
            response.ongoing +
            response.ready
        });
      })
      .catch(() => {
        this.setState({
          isLoadingCampaign: false,
          reset: false
        });
      });
  };

  clickChangeTab = type => {
    let textTitle = "";
    const { t } = this.props;
    switch (type) {
      case TYPE.READY: {
        textTitle = `${t("MY_CAMPAIGN.MCN_tab_ready_campaign")}`;
        break;
      }
      case TYPE.ONGOING: {
        textTitle = `${t("MY_CAMPAIGN.MCN_tab_ongoing")}`;
        break;
      }
      case TYPE.CLOSED: {
        textTitle = `${t("MY_CAMPAIGN.MCN_tab_close")}`;
        break;
      }
      default: {
        textTitle = `${t("MY_CAMPAIGN.MCN_tab_allcampaign")}`;
        break;
      }
    }
    this.setState(
      {
        type,
        page: 1,
        isLoadingCampaign: true,
        hasMore: true,
        textTitle
      },
      () => {
        this.initData();
      }
    );
  };

  fetchMoreData = () => {
    if (this.state.page >= this.state.last_page) {
      this.setState({ hasMore: false });
      return;
    }

    this.props
      .getMyCampaign(
        this.state.page + 1,
        this.state.type,
        this.state.search,
        this.state.campaign,
        this.state.keyword
      )
      .then(response => {
        this.setState({
          campaigns: [...this.state.campaigns, ...response.data],
          page: this.state.page + 1
        });
      });
  };

  handleChangeInput = e => {
    this.setState({
      campaign: e.target.name,
      // keyword: e && e.target.value ? e.target.value : ""
      keywordSearch: e && e.target.value ? e.target.value : ""
    });
  };

  clickSearch = e => {
    e.preventDefault();
    console.log(e);
    this.setState(
      {
        page: 1,
        isLoadingCampaign: true,
        reset: true,
        searchClick: true
      },
      () => {
        this.initData();
      }
    );
  };

  render() {
    const { t } = this.props;
    const {
      type,
      campaigns,
      total,
      ready,
      onGoing,
      closed,
      textTitle,
      hasMore,
      isLoadingCampaign,
      search,
      all,
      keyword,
      campaign,
      keywordSearch,
      reset
    } = this.state;

    let totalCam = "";
    if (campaign !== "") {
      totalCam = total;
    } else {
      totalCam = total;
      switch (type) {
        case TYPE.READY: {
          totalCam = ready;
          break;
        }
        case TYPE.ONGOING: {
          totalCam = onGoing;
          break;
        }
        case TYPE.CLOSED: {
          totalCam = closed;
          break;
        }
        default: {
          totalCam = all;
          break;
        }
      }
    }

    const campaignNotMatching =
      campaigns.length > 0 &&
      campaigns.filter(campaign => campaign.cp_status !== 59);

    return (
      <div className="content myfistar-campaign mypage-partner-campaign-tracking">
        <div className="top w-100">
          <div className="title">
            <h4>
              <a href="javascript:void(0)">
                {t("MY_CAMPAIGN.MCN_my_campaign")}
              </a>
            </h4>
          </div>
          <form
            className="form-inline search-campaign my-2 my-lg-0"
            onSubmit={this.clickSearch}
          >
            <input
              className="form-control mr-sm-2 input-search"
              type="search"
              aria-label="Search"
              id="campaign_name"
              name="campaign_name"
              value={keywordSearch}
              onChange={this.handleChangeInput}
            />
            <button
              className="btn btn-outline-success my-2 my-sm-0"
              type="submit"
              onClick={this.clickSearch}
            >
              <i className="fas fa-search" />
            </button>
          </form>
        </div>
        <div className="information-tracking">
          <ul className="list-information list-myfistar-title">
            <li
              className={`item item-myfistar${
                type === TYPE.ALL ? " active" : ""
              }`}
              onClick={() => this.clickChangeTab(TYPE.ALL)}
            >
              <a href="javascript:void(0)" className="text">
                <p className="number">{reset ? "0" : all}</p>
                <p>{t("MY_CAMPAIGN.MCN_tab_allcampaign")}</p>
              </a>
            </li>
            <li
              className={`item item-myfistar${
                type === TYPE.READY ? " active" : ""
              }`}
              onClick={() => this.clickChangeTab(TYPE.READY)}
            >
              <a href="javascript:void(0)" className="text">
                <p className="number">{reset ? "0" : ready}</p>
                <p>{t("MY_CAMPAIGN.MCN_tab_ready_campaign")}</p>
              </a>
            </li>
            <li
              className={`item item-myfistar${
                type === TYPE.ONGOING ? " active" : ""
              }`}
              onClick={() => this.clickChangeTab(TYPE.ONGOING)}
            >
              <a href="javascript:void(0)" className="text">
                <p className="number">{reset ? "0" : onGoing}</p>
                <p>{t("MY_CAMPAIGN.MCN_tab_ongoing")}</p>
              </a>
            </li>
            <li
              className={`item item-myfistar${
                type === TYPE.CLOSED ? " active" : ""
              }`}
              onClick={() => this.clickChangeTab(TYPE.CLOSED)}
            >
              <a href="javascript:void(0)" className="text">
                <p className="number">{reset ? "0" : closed}</p>
                <p>{t("MY_CAMPAIGN.MCN_tab_close")}</p>
              </a>
            </li>
          </ul>
        </div>
        <div className="list-campaign-track my-fistar-main">
          <div className="title">
            {/*<h4>{`${textTitle} ${totalCam}`}</h4>*/}
            <h4>
              {`${textTitle}`} {this.state.searchClick ? "Search" : ""}{" "}
              {this.state.searchClick ? totalCam : ""}
            </h4>
          </div>
          {isLoadingCampaign ? (
            <div id="loading" />
          ) : (
            <div className="box-item">
              {/*type === TYPE.ALL ? (
                <Fragment>
                  {campaigns.length > 0 &&
                    campaigns.map((campaign, key) => (
                      <Fragment key={key}>
                        <Campaign campaign={campaign} />
                      </Fragment>
                    ))}
                </Fragment>
              ) : */campaigns.length > 0 ? (
                <InfiniteScroll
                  dataLength={campaigns.length}
                  next={this.fetchMoreData}
                  loader={
                    <div className="w-100 text-center">
                      <div id="loading" />
                    </div>
                  }
                  hasMore={this.state.hasMore}
                  className="box-item my-campaign-scroll w-100"
                >
                  {campaigns.length > 0 &&
                    campaigns.map((campaign, key) => (
                      <Fragment key={key}>
                        <Campaign campaign={campaign} />
                      </Fragment>
                    ))}
                </InfiniteScroll>
              ) : (
                "No campaign"
              )}
            </div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    getMyCampaign: (
      page = 1,
      type = "",
      search = "",
      campaign = "",
      keyword = ""
    ) => dispatch(getMyCampaignAction(page, type, search, campaign, keyword)),
    getCountCampaign: () => dispatch(getCountCampaignAction()),
    getCode: type => dispatch(getCodeAction(type))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(FistarMyCampaign));
