import React, { Component } from 'react';
import { withTranslation, Trans } from 'react-i18next';
import { Link } from 'react-router-dom'
import { incrementCount, decrementCount } from './../../store/actions/count'

import { connect } from 'react-redux'

class Private extends Component {
  clickIncrease = () => {
    this.props.incrementCount()
  }
  clickDecrease = () => {
    this.props.decrementCount()
  }
  render() {
    const { t, i18n } = this.props;
    const changeLanguage = lng => {
      i18n.changeLanguage(lng);
    };
    console.log(this.props);
    return (
      <main className="main-container">
        <section className="form-login">
          <div className="container">
            <div className="form">
              <div className="header-form">
                <div className="logo">
                  <h1>
                    <a href="">fiStar 계정</a>
                  </h1>
                </div>
                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="toggler-icon"><i className="fas fa-bars"></i></span>
                  </button>
                  <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div className="navbar-nav">
                      <a className="nav-item nav-link active" href="#">로그인 <span className="sr-only">(current)</span></a>
                      <a className="nav-item nav-link" href="#">계정찾기</a>
                      <a className="nav-item nav-link" href="#">비밀번호 재설정</a>
                      <a className="nav-item nav-link" href="#">가입신청</a>
                    </div>
                  </div>
                </nav>
              </div>
              <div className="body-form">
                <div className="container-form">
                  <h1>Private</h1>
                  <div className="card-content">
                    <div className="row">
                      <div className="col-md-5">
                        <div className="card">
                          <a href="#">
                            <img src="./images/card-01.png" alt="" />
                          </a>
                        </div>
                      </div>
                      <div className="col-md-7 text-card">
                        <div className="text">
                          <p className="quoite text-center">❝</p>
                          <h3 className="text-center mb-2">지금, fiStar에 도전하세요!</h3>
                          <p className="text-center">당신의 특별한 재능을 fiStar에서 발휘해보세요.</p>
                          <p className="quoite text-center">❠</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section>
          <div className="drop-up btn">
            <i className="fas fa-long-arrow-alt-up"></i>
          </div>
        </section>
      </main>
    );
  }
}

function mapStateToProps (state) {
  const { count } = state

  return {count}
}

export default connect(mapStateToProps, {incrementCount, decrementCount})(withTranslation('translations')(Private))
