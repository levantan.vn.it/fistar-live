import React, { Component } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";

import { connect } from "react-redux";

class Auth extends Component {
  render() {
    const { t } = this.props;
    return (
      <div className="wrapper">
        <header className="header-container">
          <div className="row ml-0 mr-0">
            <div className="col-md-12">
              <div className="header-top">
                <ul className="list-language">
                  <li className="language-item">
                    <a href="" className="language-link">
                      KOggg
                    </a>
                  </li>
                  <li className="language-item">
                    <a href="" className="language-link">
                      EN
                    </a>
                  </li>
                  <li className="language-item active">
                    <a href="" className="language-link">
                      VN
                    </a>
                  </li>
                </ul>
              </div>
              <div className="menu-top">
                <div className="logo">
                  <a href="">
                    <img src=" https://clubpiaf.vn/assets/images/logo.png" alt="" />
                  </a>
                </div>
                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                  <button
                    className="navbar-toggler"
                    type="button"
                    data-toggle="collapse"
                    data-target="#navbarNavAltMarkups"
                    aria-controls="navbarNavAltMarkup"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
                  >
                    <span className="navbar-toggler-icon" />
                  </button>
                  <div
                    className="collapse navbar-collapse"
                    id="navbarNavAltMarkups"
                  >
                    <div className="navbar-nav">
                      <a className="nav-item nav-link" href="#">
                        {t("HEADER.HED_service")}
                      </a>
                      <a className="nav-item nav-link" href="#">
                        {t("HEADER.HED_campaign")}
                      </a>
                      <a className="nav-item nav-link" href="#">
                        {t("HEADER.HED_fistar")}
                      </a>
                      <a className="nav-item nav-link" href="#">
                        {t("HEADER.HED_faq")}
                      </a>
                      <a className="nav-item nav-link" href="#">
                        {t("HEADER.HED_contact_us")}
                      </a>
                    </div>
                  </div>
                </nav>
                <div className="login-info">
                  <ul className="list-item">
                    <li className="item">
                      <a href=""> {t("HEADER.HED_login")}</a>
                    </li>
                    <li className="item">
                      <a href=""> {t("HEADER.HEA_button_logout")}</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </header>
        <section className="banner-container">
          <div className="row">
            <div className="col-md-12" />
          </div>
        </section>

        {this.props.children}

        <footer className="footer-container">
          <div className="container">
            <div className="content-footer">
              <div className="row">
                <div className="col-md-1 ">
                  <div className="logo">
                    <h1>FiStar</h1>
                  </div>
                </div>
                <div className="col-md-8">
                  <div className="address">
                    <p>
                      473 Dien Bien Phu, Ward 25, Binh Thanh District, Ho Chi
                      Minh City l fimevn@gmail.com l Tel 0903029423
                    </p>
                  </div>
                </div>
                <div className="col-md-3">
                  <div className="copyright">
                    <p>{t("FOOTER.FTR_privacy_policy")}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </footer>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps)(withTranslation("translations")(Auth));
