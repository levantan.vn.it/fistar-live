import React, { Component } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { findEmailActionPartner } from "./../../../../store/actions/auth";

class PartnerFindEmail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        pm_name: "",
        email: "",
        pm_phone: ""
      },
      errors: {
        pm_name: "",
        pm_phone: "",
        email: "",
        message: ""
      },
      success: false,
      loading: false
    };
  }

  handleChangeInput = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevState => ({
      data: {
        ...prevState.data,
        [name]: value
      }
    }));
    console.log(this.state);
  };

  onFocusInput = e => {
    this.setState(prevState => ({
      errors: {
        pm_name: "",
        pm_phone: "",
        email: "",
        message: ""
      },
      success: false
    }));
  };

  validateEmail = email => {
    // var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    //   var re = /^[a-zA-Z0-9\.]{2,}@[a-zA-Z0-9]{2}(?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z](?:[a-zA-Z-]{0,61}[a-zA-Z])?)(?=.*[\.])*$/;
      var re = /([a-z]+[a-z0-9]*[_\.]?[a-z0-9]+)@(([a-z0-9]{2,}\.)*[a-z0-9]{2,}\.)+[a-z]{2,}/;
    return re.test(String(email).toLowerCase());
  };

    validateEmailCustomer = email => {
        var re = /^[a-zA-Z0-9]{2,}@[a-zA-Z0-9]{2,}.[a-zA-Z](?:[a-zA-Z-]{0,61}[a-zA-Z])*$/;
        return re.test(String(email).toLowerCase());
    };

  validateForm = () => {
    const errors = {
      pm_name: "",
      pm_phone: "",
      email: "",
      message: ""
    };
    const { data } = this.state;
    // if (!data.pm_name) {
    //   errors.pm_name = "The name field is required.";
    // }
    // if (!data.pm_phone) {
    //   errors.pm_phone = "The phone field is required.";
    // }
    //
    // if (!data.email) {
    //   errors.email = "The email field is required.";
    // }
     if (data.email && !this.validateEmail(data.email)) {
      errors.email = "The email must be a valid email address.";
    }

    return errors;
  };

  submitForm = e => {
    console.log("asdsadsa");
    e.preventDefault();
    const { data } = this.state;
    const validate = this.validateForm();
    console.log(validate);
    if (!validate.pm_name && !validate.pm_phone && !validate.email) {
      if (!this.state.loading) {
        this.setState(
          {
            loading: true
          },
          () => {
            this.props
              .findEmail(data)
              .then(response => {
                console.log("zozozoz");
                this.setState({
                  success: true,
                  loading: false
                });
              })
              .catch(e => {
                console.log("catch");
                const errors = {
                  pm_name: "",
                  pm_phone: "",
                  email: "",
                  empty_value: "",
                  message: ""
                };
                console.log(e);

                if (e.data && e.data.errors) {
                  errors.pm_name =
                    e.data.errors && e.data.errors.pm_name
                      ? e.data.errors.pm_name[0]
                      : "";
                  errors.email =
                    e.data.errors && e.data.errors.email
                      ? e.data.errors.email[0]
                      : "";
                  errors.pm_phone =
                    e.data.errors && e.data.errors.pm_phone
                      ? e.data.errors.pm_phone[0]
                      : "";
                }

                this.setState({
                  errors,
                  loading: false
                });
              });
          }
        );
      }
    } else {
      this.setState(
        {
          errors: validate
        },
        () => {}
      );
    }
  };

  validateNumber(event) {
    const { data } = this.state;
    console.log(this.state.data);
    console.log(data);

    var invalidChars = ["-", "+", "e", "."];

    if (invalidChars.includes(event.key)) {
      event.preventDefault();
    }
  }

  render() {
    const { t } = this.props;
    const { data, errors, success, loading } = this.state;
    console.log(errors);

    return (
      <form className="forgot-form" onSubmit={this.submitForm}>
        <div className="row">
          <div className="col-md-12 mb-3">
            <a href="" className="text-forgot-form">
              {t("PARTNER_FINDID.PFI_title")}
            </a>
          </div>
        </div>
        <div className="row row-eq-height info-address">
          <div className="col-md-12">
            <div className="form-input">
              <div className="input-group mb-3">
                <div className="input-group-prepend">
                  <span className="input-group-text">
                    <i className="fa fa-address-card" />
                  </span>
                </div>
                <input
                  placeholder={t("PARTNER_FINDID.PFI_input_name")}
                  type="text"
                  name="pm_name"
                  id="pm_name"
                  value={data.pm_name}
                  className="form-input empty"
                  onChange={this.handleChangeInput}
                  onFocus={this.onFocusInput}
                />
                {(errors.pm_name || errors.message) && (
                  <div className="tooptip-text-find">
                    <p>{errors.message ? errors.message : errors.pm_name}</p>
                  </div>
                )}
              </div>
              <div className="input-group">
                <div className="input-group-prepend">
                  <span className="input-group-text">
                    <i className="fa fa-phone" />
                  </span>
                </div>
                <input
                  placeholder={t("PARTNER_FINDID.PFI_input_contact")}
                  name="pm_phone"
                  id="pm_phone"
                  value={data.pm_phone}
                  onKeyPress={event => this.validateNumber(event)}
                  type="text"
                  className="form-input empty mb-0"
                  onChange={this.handleChangeInput}
                  onFocus={this.onFocusInput}
                />
                {errors.pm_phone && (
                  <div className="tooptip-text-find">
                    <p>{errors.pm_phone}</p>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
        <div className="row row-eq-height infor-email">
          <div className="col-md-12">
            <div className="form-input">
              <div className="input-group">
                <div className="input-group-prepend">
                  <span className="input-group-text">
                    <i className="fa fa-envelope" />
                  </span>
                </div>
                <input
                  placeholder={t("PARTNER_FINDID.PFI_input_email")}
                  type="email"
                  name="email"
                  id="email"
                  value={data.email}
                  className="form-input empty mb-0"
                  onChange={this.handleChangeInput}
                  onFocus={this.onFocusInput}
                />
                {errors.email && (
                  <div className="tooptip-text-find">
                    <p>{errors.email}</p>
                  </div>
                )}
              </div>
              <div className="input-group attention">
                <span>
                  <i className="fas fa-exclamation-circle" />
                  <label>{t("PARTNER_FINDID.PFI_verifies_account")}</label>
                </span>
              </div>
            </div>
          </div>
        </div>
        <div className="row main-submit">
          {success && (
            <div className="col-md-12">
              <div className="alert alert-success" role="alert">
                {t("PARTNER_FINDID.PFI_send_email")}
                <br /> {t("FISTAR_JOIN.PJN_thanks")}
              </div>
            </div>
          )}
          <div className="col-md-12 content-find-id">
            <button className="btn find-id" onClick={this.submitForm}>
              {loading ? (
                <div className="spinner-border" role="status">
                  <span className="sr-only">{t("LOADING.LOADING")}</span>
                </div>
              ) : (
                `${t("PARTNER_FINDID.PFI_button_find_id")}`
              )}
            </button>
            {/*{(errors.pm_name ||errors.pm_phone || errors.email || errors.message) && (*/}
            {/*<div className="tooptip-text">*/}
            {/*<p>{errors.message ? errors.message : errors.empty_value}</p>*/}
            {/*</div>*/}
            {/*)}*/}
          </div>
        </div>
      </form>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    findEmail: data => dispatch(findEmailActionPartner(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(PartnerFindEmail));
