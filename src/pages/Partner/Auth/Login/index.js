import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, NavLink, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { loginActionPartner } from "./../../../../store/actions/auth";
import PartnerAuthFooter from "./../../../../components/partner/Auth/Footer";
import "./login.scss";
import * as routeName from "../../../../routes/routeName";

class PartnerLogin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        email: "",
        password: ""
      },
      errors: {
        email: "",
        password: "",
        message: ""
      },
      loading: false
    };
  }

  handleChangeInput = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevState => ({
      data: {
        ...prevState.data,
        [name]: value
      }
    }));
  };

  onFocusInput = e => {
    this.setState(prevState => ({
      errors: {
        email: "",
        password: "",
        message: ""
      }
    }));
  };

  validateForm = () => {
    const errors = {
      email: "",
      password: "",
      message: ""
    };
    const { data } = this.state;

    // if (!data.email) {
    //   errors.email = "The email field is required.";
    // }
    // if (!data.password) {
    //   errors.password = "The password field is required.";
    // }

    return errors;
  };

  submitForm = e => {
    e.preventDefault();
    const { data, errors } = this.state;
    const validate = this.validateForm();
    if (validate.email === "" && validate.password === "") {
      if (!this.state.loading) {
        this.setState(
          {
            loading: true
          },
          () => {
            this.props
              .login(data)
              .then(response => {
                // return <Redirect to="/" />;
                console.log(response);
              })
              .catch(e => {
                const errors = {
                  email: "",
                  password: "",
                  message: ""
                };
                console.log(e);
                if (e.data.errors) {
                  console.log(e.data.message);
                  errors.email =
                    e.data.errors && e.data.errors.email
                      ? e.data.errors.email
                      : "";
                  errors.password =
                    e.data.errors && e.data.errors.password
                      ? e.data.errors.password
                      : "";
                } else if (e.data && e.data.message) {
                  if (e.data.message == "Unauthorized") {
                    errors.password = this.props.t(
                      "PARTNER_LOGIN.PLG_erro_login_approval"
                    );
                  } else {
                    errors.password =
                      e.data && e.data.message ? e.data.message : "";
                  }
                }
                // e.data.message ? errors.message = e.data.message : ''
                this.setState({
                  errors,
                  loading: false
                });
              });
          }
        );
      }
    } else {
      this.setState({
        errors: validate
      });
    }
  };

  render() {
    const { t } = this.props;
    const { data, errors, loading } = this.state;
    console.log(errors);
    return (
      <Fragment>
        <h3 className="partner-login">{t("PARTNER_LOGIN.FLC_account")}</h3>
        <form className="form-login-group" onSubmit={this.submitForm}>
          <div className="row row-eq-height form-login-content">
            <div className="col-md-9">
              <div className="form-input">
                <div className="input-group mb-3">
                  <div className="input-group-prepend">
                    <span className="input-group-text">
                      <i className="fas fa-envelope" />
                    </span>
                  </div>
                  <input
                    placeholder={t("PARTNER_JOIN.PJN_label_email")}
                    type="text"
                    id="email"
                    name="email"
                    className="form-input empty"
                    value={data.email}
                    onChange={this.handleChangeInput}
                    onFocus={this.onFocusInput}
                  />
                  {(errors.email || errors.message) && (
                    <div className="tooptip-text-error">
                      <p>{errors.message ? errors.message : errors.email}</p>
                    </div>
                  )}
                </div>
                <div className="input-group">
                  <div className="input-group-prepend">
                    <span className="input-group-text">
                      <i className="fa fa-lock-open" />
                    </span>
                  </div>
                  <input
                    placeholder={t("PARTNER_JOIN.PJN_label_password")}
                    type="password"
                    id="password"
                    name="password"
                    className="form-input empty mb-0"
                    value={data.password}
                    onChange={this.handleChangeInput}
                    onFocus={this.onFocusInput}
                  />
                  {errors.password && (
                    <div
                      className="tooptip-text-error tooptip-text-error-pass"
                      style={{ width: "260px" }}
                    >
                      <p>{errors.password}</p>
                    </div>
                  )}
                </div>
              </div>
            </div>
            <div className="col-md-3">
              <button
                type="submit"
                className="btn btn-submit submit-login"
                onClick={this.submitForm}
              >
                {loading ? (
                  <div className="spinner-border" role="status">
                    <span className="sr-only">{t("LOADING.LOADING")}</span>
                  </div>
                ) : (
                  `${t("PARTNER_LOGIN.PLG_login")}`
                )}
              </button>
            </div>
          </div>
          <div className="row lost-account">
            <div className="col-md-9">
              <div className="text">
                <p className="text-left mt-4 mb-4">
                  <NavLink to={routeName.PARTNER_FIND_EMAIL}>
                    {t("PARTNER_LOGIN.PLG_lost_account")}?
                  </NavLink>
                </p>
              </div>
            </div>
          </div>
        </form>
        <PartnerAuthFooter />
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    login: data => dispatch(loginActionPartner(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(PartnerLogin));
