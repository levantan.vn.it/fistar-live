import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { forgotPasswordActionPartner } from "../../../../store/actions/auth";

class PartnerForgotPasswordStep1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        pm_name: "",
        email: ""
      },
      errors: {
        pm_name: "",
        email: "",
        message: ""
      },
      success: false,
      loading: false
    };
  }

  handleChangeInput = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevState => ({
      data: {
        ...prevState.data,
        [name]: value
      }
    }));
  };

  onFocusInput = e => {
    this.setState(prevState => ({
      errors: {
        pm_name: "",
        email: "",
        message: ""
      }
    }));
  };

  validateEmail = email => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  };

  validateForm = () => {
    const errors = {
      pm_name: "",
      email: "",
      message: ""
    };
    const { data } = this.state;
    // if (!data.pm_name) {
    //   errors.pm_name = "The Name field is required.";
    // }
    // if (!data.email) {
    //   errors.email = "The Email field is required";
    // }
    // else if (!this.validateEmail(data.email)) {
    //   errors.email = "The email must be a valid email address.";
    // }
    return errors;
  };

  submitForm = e => {
    e.preventDefault();
    if (!this.state.loading) {
      const { data } = this.state;

      const validate = this.validateForm();
      console.log(validate);
      if (!validate.pm_name && !validate.email) {
        if (!this.state.loading) {
          this.setState(
            {
              loading: true
            },
            () => {
              this.props
                .forgotPassword(data)
                .then(response => {
                  console.log(response);
                  this.setState({
                    success: true,
                    loading: false
                  });
                  // setTimeout(() => {
                  // body...
                  this.props.onNextStep(2, { email: data.email });
                  // }, 1000);
                })
                .catch(e => {
                  const errors = {
                    pm_name: "",
                    email: "",
                    message: ""
                  };
                  console.log(e);
                  if (e.data && e.data.errors) {
                    errors.pm_name =
                      e.data.errors && e.data.errors.pm_name
                        ? e.data.errors.pm_name
                        : "";
                    errors.email =
                      e.data.errors && e.data.errors.email
                        ? e.data.errors.email
                        : "";
                  } else if (e.data.message == false) {
                    errors.pm_name = "name or email is wrong!";
                  }
                  // if (e.data.errors) {
                  //   e.data.errors.pm_name ? errors.pm_name = e.data.errors.pm_name[0] : ''
                  //   e.data.errors.email ? errors.email = e.data.errors.email[0] : ''
                  // }
                  // (e.data.message == false) ? errors.message = 'Could not find this id' : ''
                  this.setState({
                    errors,
                    loading: false
                  });
                });
            }
          );
        }
      } else {
        this.setState(
          {
            errors: validate
          },
          () => {}
        );
      }
    }
  };

  render() {
    const { t } = this.props;
    const { data, errors, success, loading } = this.state;
    console.log(errors);
    return (
      <form className="form reset-pass-form" onSubmit={this.submitForm}>
        <div className="row">
          <div className="col-md-12 mb-3">
            <a href="" className="text-reset-form">
              {t("PARTNER_RESETPW.PRWP_title")}
            </a>
          </div>
        </div>

        <div className="row row-eq-height infor-email">
          <div className="col-md-12">
            <div className="form-input">
              <div className="input-group mb-3">
                <div className="input-group-prepend">
                  <span className="input-group-text">
                    <i className="fa fa-address-card" />
                  </span>
                </div>
                <input
                  placeholder={t("PARTNER_RESETPW.PRW_input_name")}
                  type="text"
                  name="pm_name"
                  id="pm_name"
                  value={data.pm_name}
                  className="form-input empty mb-0"
                  onChange={this.handleChangeInput}
                  onFocus={this.onFocusInput}
                />
                {(errors.pm_name || errors.message) && (
                  <div className="tooptip-text">
                    <p>{errors.message ? errors.message : errors.pm_name}</p>
                  </div>
                )}
              </div>
              <div className="input-group">
                <div className="input-group-prepend">
                  <span className="input-group-text">
                    <i className="fa fa-envelope" />
                  </span>
                </div>

                <input
                  placeholder={t("PARTNER_RESETPW.PRW_input_email")}
                  type="text"
                  name="email"
                  id="email"
                  value={data.email}
                  className="form-input empty mb-0"
                  onChange={this.handleChangeInput}
                  onFocus={this.onFocusInput}
                />
                {errors.email && (
                  <div className="tooptip-text">
                    <p>{errors.email}</p>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
        <div className="row btn-submit-pass main-submit">
          <div className="col-md-12">
            <button className="btn submit-next" onClick={this.submitForm}>
              {loading ? (
                <div className="spinner-border text-primary" role="status">
                  <span className="sr-only">{t("LOADING.LOADING")}</span>
                </div>
              ) : (
                <Fragment>{t("PARTNER_RESETPW.PRW_button_next")}</Fragment>
              )}
            </button>
          </div>
        </div>
      </form>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    forgotPassword: data => dispatch(forgotPasswordActionPartner(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(PartnerForgotPasswordStep1));
