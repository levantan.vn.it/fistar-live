import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { resetPasswordAction } from "./../../../../store/actions/auth";
import { resetPasswordActionPartner } from "../../../../store/actions/auth";
import * as routeName from "./../../../../routes/routeName";

class PartnerForgotPasswordStep2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        code: ""
      },
      errors: {
        code: "",
        message: ""
      },
      success: false,
      loading: false
    };
  }

  handleChangeInput = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevState => ({
      data: {
        ...prevState.data,
        [name]: value
      }
    }));
  };

  onFocusInput = e => {
    this.setState(prevState => ({
      errors: {
        code: "",
        message: ""
      }
    }));
  };

  validateForm = () => {
    const errors = {
      code: "",
      message: ""
    };
    const { t } = this.props;
    const { data } = this.state;
    console.log(data.code);
    if (!data.code) {
      errors.code = this.props.t("PARTNER_RESETPW.PRW_erro_forget_code");
    }

    return errors;
  };

  submitForm = e => {
    e.preventDefault();

    const validate = this.validateForm();
    console.log(validate);
    const { t } = this.props;
    if (!validate.code) {
      if (!this.state.loading) {
        const { data } = this.state;
        const { email } = this.props;
        this.setState(
          {
            loading: true
          },
          () => {
            data.email = email;

            this.props
              .resetPassword(data)
              .then(response => {
                console.log(response);
                this.setState({
                  success: true,
                  loading: false
                });
                this.props.onNextStep(3, { code: data.code });
              })
              .catch(e => {
                const errors = {
                  code: "",
                  message: ""
                };
                console.log(e);
                if (e.data) {
                  errors.code =
                    e.data.status == false
                      ? `${t("PARTNER_RESETPW.PRW_erro_forget_swift_code")}`
                      : "";
                }
                // (e.data.status == false) ? errors.message = 'Code is incorrect' : ''
                this.setState({
                  errors,
                  loading: false
                });
              });
          }
        );
      }
    } else {
      this.setState(
        {
          errors: validate
        },
        () => {}
      );
    }
  };

  onBackForm = () => {
    this.props.onNextStep(1);
  };

  render() {
    const { t, email } = this.props;
    const { data, errors, success, loading } = this.state;
    console.log(errors);
    return (
      <form className="form reset-pass-form" onSubmit={this.submitForm}>
        <div className="row">
          <div className="col-md-12 mb-3">
            <a href="" className="text-reset-form">
              {t("PARTNER_RESETPW.PRWP_title")}
            </a>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <p className="code-test mb-2 mt-3">
              {t("PARTNER_RESETPW.PRW_text_1")}{" "}
              <span>
                <a href="">{email} </a>
              </span>{" "}
              {t("PARTNER_RESETPW.PRW_text")}
            </p>
          </div>
        </div>
        <div className="row row-eq-height infor-email">
          <div className="col-md-12">
            <div className="form-input">
              <div className="input-group mb-3">
                <div className="input-group-prepend">
                  <span className="input-group-text">
                    <i className="fa fa-address-card" />
                  </span>
                </div>
                <input
                  placeholder={t("PARTNER_RESETPW.PRW_input_forget")}
                  type="text"
                  name="code"
                  id="code"
                  className="form-input empty mb-0"
                  onFocus={this.onFocusInput}
                  onChange={this.handleChangeInput}
                />
                {(errors.message || errors.code) && (
                  <div className="tooptip-text">
                    <p>{errors.message ? errors.message : errors.code}</p>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
        <div className="row btn-submit-pass main-submit">
          <div className="col-md-12">
            <button className="btn submit-next" onClick={this.submitForm}>
              {loading ? (
                <div className="spinner-border text-primary" role="status">
                  <span className="sr-only">{t("LOADING.LOADING")}</span>
                </div>
              ) : (
                <Fragment>{t("PARTNER_RESETPW.PRW_button_next")}</Fragment>
              )}
            </button>
            <button className="back-to-form btn p-0" onClick={this.onBackForm}>
              {t("PARTNER_RESETPW.PRW_do_you_receive")}
            </button>
          </div>
        </div>
      </form>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    resetPassword: data => dispatch(resetPasswordActionPartner(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(PartnerForgotPasswordStep2));
