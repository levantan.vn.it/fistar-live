import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { changePasswordAction } from "./../../../../store/actions/auth";
import { changePasswordActionPartner } from "../../../../store/actions/auth";
import { Modal, Tabs, Tab, Nav } from "react-bootstrap";
import closePopup from "./../../../../images/close-popup.svg";
import * as routeName from "../../../../routes/routeName";

class PartnerForgotPasswordStep3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        password: "",
        password_confirmation: ""
      },
      errors: {
        password: "",
        password_confirmation: "",
        message: ""
      },
      success: false,
      loading: false,
      show: false,
      isRedirect: false
    };
  }

  handleChangeInput = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevState => ({
      data: {
        ...prevState.data,
        [name]: value
      }
    }));
  };

  onFocusInput = e => {
    this.setState(prevState => ({
      errors: {
        password: "",
        password_confirmation: "",
        message: ""
      }
    }));
  };

  validateForm = () => {
    const errors = {
      password: "",
      password_confirmation: "",
      message: ""
    };
    const { data } = this.state;
    console.log(data.code);
    const { t } = this.props;
    if (!data.password) {
      errors.password = `${t("PARTNER_RESETPW.PRW_erro_forget_new_pw")}`;
    } else if (data.password.length < 6) {
      errors.password = `${t("PARTNER_RESETPW.PRW_input_password_limit")}`;
    }
    if (!data.password_confirmation) {
      errors.password_confirmation = `${t(
        "PARTNER_RESETPW.PRW_erro_forget_confirm_new_pw"
      )}`;
    } else if (data.password_confirmation.length < 6) {
      errors.password_confirmation = `${t(
        "PARTNER_RESETPW.PRW_erro_forget_confirm_new_pw_limit"
      )}`;
    } else if (data.password_confirmation !== data.password) {
      errors.password_confirmation = `${t(
        "PARTNER_RESETPW.PRW_erro_forget_confirm_pw_not_matched"
      )}`;
    }

    return errors;
  };

  submitForm = e => {
    e.preventDefault();

    const validate = this.validateForm();
    console.log(validate);
    if (!validate.password && !validate.password_confirmation) {
      if (!this.state.loading) {
        const { data } = this.state;
        const { email, code } = this.props;
        this.setState(
          {
            loading: true
          },
          () => {
            data.email = email;
            data.code = code;
            this.props
              .changePassword(data)
              .then(response => {
                console.log(response);
                this.setState({
                  success: true,
                  loading: false,
                  show: true
                });
                this.props.onNextStep(3);
              })
              .catch(e => {
                const errors = {
                  password: "",
                  password_confirmation: "",
                  message: ""
                };
                console.log(e);
                if (e.data.errors) {
                  errors.password = e.data.errors.password
                    ? e.data.errors.password[0]
                    : "";
                  errors.password_confirmation = e.data.errors.password
                    ? e.data.errors.password[0]
                    : "";
                } else if (e.data.message) {
                  errors.password =
                    e.data && e.data.message ? e.data.message : "";
                }
                // (e.data.status == false) ? errors.message = 'Confirm Password is incorrect' : ''
                this.setState({
                  errors,
                  loading: false
                });
              });
          }
        );
      }
    } else {
      this.setState(
        {
          errors: validate
        },
        () => {}
      );
    }
  };

  handleClose = () => {
    this.setState({ show: false });
  };
  handleShow = () => {
    this.setState({ show: true });
  };

  loadLogin = () => {
    this.setState({
      isRedirect: true
    });
  };

  render() {
    const { t } = this.props;
    const { data, errors, success, loading, isRedirect } = this.state;
    console.log(errors);
    if (isRedirect) {
      return <Redirect to={routeName.PARTNER_LOGIN} />;
    }

    return (
      <form className="form reset-pass-form" onSubmit={this.submitForm}>
        <div className="row">
          <div className="col-md-12 mb-3">
            <a href="" className="text-reset-form">
              {t("PARTNER_RESETPW.PRWP_title")}
            </a>
          </div>
        </div>

        <div className="row row-eq-height infor-email">
          <div className="col-md-12">
            <div className="form-input">
              <div className="input-group mb-3">
                <div className="input-group-prepend">
                  <span className="input-group-text">
                    <i className="fa fa-lock-open" />
                  </span>
                </div>
                <input
                  placeholder={t("PARTNER_RESETPW.PRW_input_new_password")}
                  type="password"
                  className="form-input empty mb-0"
                  id="password"
                  name="password"
                  onChange={this.handleChangeInput}
                  onFocus={this.onFocusInput}
                />
                {(errors.message || errors.password) && (
                  <div className="tooptip-text">
                    <p>{errors.message ? errors.message : errors.password}</p>
                  </div>
                )}
              </div>
              <div className="input-group">
                <div className="input-group-prepend">
                  <span className="input-group-text">
                    <i className="fa fa-lock-open" />
                  </span>
                </div>
                <input
                  placeholder={t("PARTNER_RESETPW.PRW_input_confirm_password")}
                  type="password"
                  className="form-input empty mb-0"
                  id="password_confirmation"
                  name="password_confirmation"
                  onChange={this.handleChangeInput}
                  onFocus={this.onFocusInput}
                />

                {(errors.message || errors.password_confirmation) && (
                  <div className="tooptip-text">
                    <p>
                      {errors.message
                        ? errors.message
                        : errors.password_confirmation}
                    </p>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>

        <Modal
          size="lg"
          show={this.state.show}
          onHide={this.handleClose}
          aria-labelledby="example-modal-sizes-title-lg"
          dialogClassName="howto-register modal-dialog-centered"
        >
          <div className="register">
            <div className="top modal-header">
              <h4>{t("POPUP_RESET_PASS.PRP_title")}</h4>
              <button
                type="button"
                className="btn btn-close close"
                data-dismiss="modal"
                aria-label="Close"
                onClick={this.handleClose}
              >
                <img src={closePopup} alt="" />
              </button>
            </div>
            <div className="body-popup">
              <p className="text-center">{t("POPUP_RESET_PASS.PRP_text")}</p>
              <button onClick={this.loadLogin}>
                {t("POPUP_RESET_PASS.PRP_button_ok")}
              </button>
            </div>
          </div>
        </Modal>

        <div className="row btn-submit-pass main-submit">
          {success && (
            <div className="col-md-12">
              <div className="alert alert-success" role="alert">
                {t("POPUP_RESET_PASS.PRP_updated_sucessed")} <br />{" "}
                {t("POPUP_RESET_PASS.PRP_you_can_login")}
              </div>
            </div>
          )}
          <div className="col-md-12">
            <button className="btn submit-confirm" onClick={this.submitForm}>
              {loading ? (
                <div className="spinner-border text-primary" role="status">
                  <span className="sr-only">{t("LOADING.LOADING")}</span>
                </div>
              ) : (
                <Fragment>{t("POPUP_RESET_PASS.PRP_button_ok")}</Fragment>
              )}
            </button>
          </div>
        </div>
      </form>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    changePassword: data => dispatch(changePasswordActionPartner(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(PartnerForgotPasswordStep3));
