import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { findEmailAction } from "./../../../../store/actions/auth";
import PartnerForgotPasswordStep1 from "./Step1";
import PartnerForgotPasswordStep2 from "./Step2";
import PartnerForgotPasswordStep3 from "./Step3";
import { findEmailActionPartner } from "../../../../store/actions/auth";
import "./index.scss";

class PartnerForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      step: 1,
      email: "",
      code: ""
    };
  }

  clickNextStep = (step, data = {}) => {
    if (step == 1) {
      this.setState(prevState => ({
        step: step,
        data: {
          ...prevState.data,
          ...data
        }
      }));
    } else {
      this.setState(prevState => ({
        step: step,
        ...data
      }));
    }
  };

  renderStep = () => {
    const { step } = this.state;
    switch (step) {
      case 1:
        return <PartnerForgotPasswordStep1 onNextStep={this.clickNextStep} />;
        break;
      case 2:
        return (
          <PartnerForgotPasswordStep2
            onNextStep={this.clickNextStep}
            email={this.state.email}
          />
        );
        break;
      case 3:
        return (
          <PartnerForgotPasswordStep3
            onNextStep={this.clickNextStep}
            email={this.state.email}
            code={this.state.code}
          />
        );
        break;

      default:
        return null;
        break;
    }
  };

  render() {
    return <Fragment>{this.renderStep()}</Fragment>;
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    findEmail: data => dispatch(findEmailActionPartner(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(PartnerForgotPassword));
