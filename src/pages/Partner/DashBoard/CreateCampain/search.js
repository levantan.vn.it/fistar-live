import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { registerActionPartner } from "./../../../../store/actions/auth";
import { getCodeAction } from "../../../../store/actions/code";
import { getChannelAction } from "../../../../store/actions/channel";
import { searchFistarAction } from "../../../../store/actions/fistar";
import "./search.css";
import CancelSvg from "./../../../../images/cancel.svg";
import ModalDetailFistar from "./../../../../components/partner/Dashboard/DetailFistar/detailFistar";
import { Modal, Tabs, Tab, Nav } from "react-bootstrap";
import closePopup from "./../../../../images/close-popup.svg";
import { GENDER_TYPE } from "./../../../../constants";
import * as routeName from "./../../../../routes/routeName";
import { withRouter } from "react-router";
import { Accordion, Card } from "react-bootstrap";
import { nFormatter } from "./../../../../common/helper";
import { getImageLink, convertVi } from "./../../../../common/helper";
import { IMAGE_SIZE, IMAGE_TYPE } from "./../../../../constants/index";
import Scrap from "./../../../../components/Scrap";
import { scrapAction } from "./../../../../store/actions/scrap";
import InfiniteScroll from "react-infinite-scroll-component";

const TYPES = [
  "fistar_type",
  "gender",
  "age_range",
  "location",
  "follower_range",
  "post_range",
  "channel",
  "keyword"
];

class PartnerCreateCampain extends Component {
  constructor(props) {
    super(props);
    this.state = {
      step: 1,
      selectedTypes: [],
      allSelected: [],
      fiStars: [],
      last_page: 1,
      page: 0,
      hasMore: true,
      isInfinity: false,
      selectedFiStar: [],
      isLoadingSearch: false,
      show: false,
      search: "",
      isSearchLink: props.location.pathname === routeName.PARTNER_SEARCH_FISTAR,
      isRedirectToFistar: false,
      uid: "",
      fime: "",
      stateStatus: 2,
      activeState: 1,
      facebook: "",
      youtube: "",
      instagram: "",
      reset: true,
      total: 0
    };
  }

  componentDidMount() {
    window.scrollTo(0, 0);
    const types = TYPES.join(",");
    this.props.getCode(types);
    this.props.getChannel();
  }

  onSelectedType = (item, codeLength = null) => {
    const { code } = this.props;
    let { selectedTypes, allSelected } = this.state;
    let selectedTypesCdgId = [...allSelected];
    // click all checkbox
    if (Array.isArray(item)) {
      // if have items in array unchecked
      // check all item
      let selectedIds = selectedTypes.map(e => e.cd_id + "-" + e.cdg_id);
      let itemIds = item.map(e => e.cd_id + "-" + e.cdg_id);
      let checked = false;
      let intersection = selectedIds.filter(x => itemIds.includes(x));
      if (intersection.length === itemIds.length) {
        allSelected = allSelected.filter(e => e !== item[0].cdg_id);
      } else {
        allSelected = [...allSelected, item[0].cdg_id];
      }
      if (intersection.length === 0 || intersection.length === itemIds.length) {
        // if all item in array checked or unchecked
        for (let i = 0; i < item.length; i++) {
          if (
            selectedTypes.some(
              e =>
                e.cd_id + "-" + e.cdg_id ===
                item[i].cd_id + "-" + item[i].cdg_id
            )
          ) {
            selectedTypes = selectedTypes.filter(
              e =>
                e.cd_id + "-" + e.cdg_id !==
                item[i].cd_id + "-" + item[i].cdg_id
            );
          } else {
            selectedTypes = [...selectedTypes, item[i]];
          }
        }
      } else {
        let itemUnchecked = item.filter(
          e => !intersection.includes(e.cd_id + "-" + e.cdg_id)
        );
        selectedTypes = [...selectedTypes, ...itemUnchecked];
      }
      // click item checkbox
    } else {
      selectedTypesCdgId = selectedTypes
        .map(e => e.cdg_id)
        .filter(e => e === item.cdg_id);
      if (
        selectedTypes.some(
          e => e.cd_id + "-" + e.cdg_id === item.cd_id + "-" + item.cdg_id
        )
      ) {
        // remove select all checkbox
        allSelected = allSelected.filter(e => e !== item.cdg_id);
        // remove item
        selectedTypes = selectedTypes.filter(
          e => e.cd_id + "-" + e.cdg_id !== item.cd_id + "-" + item.cdg_id
        );
      } else {
        // if all item checked
        if (codeLength === selectedTypesCdgId.length + 1) {
          // trigger checked to all checkbox
          allSelected = [...allSelected, item.cdg_id];
        }
        // add item
        selectedTypes = [...selectedTypes, item];
      }
    }

    this.setState({
      selectedTypes,
      allSelected
    });
  };

  resetSelectTypes = () => {
    this.setState({
      selectedTypes: [],
      allSelected: []
    });
  };

  submit = () => {
    this.setState(
      {
        isLoadingSearch: true,
        reset: true,
        page: 0,
        hasMore: true
      },
      () => {
        this.callRequest();
      }
    );
  };

  fetchMoreData = () => {
    console.log('fetchhhhhhhhhhhhhhhhhhhhhhhhhhhhhh');
    if (this.state.isInfinity && this.state.page >= this.state.last_page) {
      this.setState({ hasMore: false });
      return;
    }
    this.setState(
      {
        reset: false
      },
      () => {
        this.callRequest();
      }
    );
    // this.props.getCampaign(this.state.page + 1).then(response => {
    //   this.setState({
    //     campaigns: [...this.state.campaigns, ...response.data],
    //     page: response.current_page
    //   });
    // });
  };

  callRequest = () => {
    const { selectedTypes } = this.state;
    const {
      code: { data: codeData }
    } = this.props;
    let search = "";
    let condition = {};
    Object.keys(codeData).map(group => {
      condition[group] = [];
      selectedTypes.map(type => {
        if (type.cdg_id === codeData[group].cdg_id) {
          search += group + "[]=";
          search += type.cd_id;
          condition[group].push(type.cd_id);
          search += "&";
        }
      });
    });
    if (this.state.search) {
      search += "&search=" + convertVi(this.state.search);
    }
    const fields =
      "uid,id,name,email,avatar,gender,gender_label,location,location_label&relation=keywords,channel";
    this.props
      // .searchFistar(fields, JSON.stringify(condition))
      .searchFistar(
        search,
        this.state.page + 1,
        this.state.stateStatus,
        this.state.activeState
      )
      .then(response => {
        this.setState({
          fiStars: this.state.reset
            ? response.data
            : [...this.state.fiStars, ...response.data],
          page: response.current_page,
          last_page: response.last_page,
          total: response.total,
          isInfinity: true,
          isLoadingSearch: false
        });
      })
      .catch(er => {
        // body...
        this.setState({
          isLoadingSearch: false
        });
      });
  };

  onClickSearch = e => {
    e.preventDefault();
    // if (!this.state.isLoadingSearch && this.state.selectedTypes.length > 0) {
    this.submit();
    // }
  };

  selectFiStar = fistar => {
    let { selectedFiStar } = this.state;
    let exist = false;
    selectedFiStar.map(e => {
      if (e.uid === fistar.uid) {
        exist = true;
      }
    });
    if (exist) {
      selectedFiStar = selectedFiStar.filter(e => e.uid !== fistar.uid);
      this.setState({
        selectedFiStar
      });
    } else {
      this.setState(prevState => ({
        selectedFiStar: [...prevState.selectedFiStar, fistar]
      }));
    }
  };

  getFiStarsToCreate = () => {
    if (this.state.selectedFiStar.length > 0) {
      this.props.getFiStars(this.state.selectedFiStar);
      this.props.closeModal();
    }
  };

  clickScrap = () => {
    let { selectedFiStar, fiStars } = this.state;
    if (selectedFiStar.length > 0) {
      let uid = selectedFiStar.map(e => e.uid);
      this.props.scrap(uid).then(() => {
        selectedFiStar = selectedFiStar.map(fistar => ({
          ...fistar,
          is_scrap: !fistar.is_scrap,
          scraps_count: fistar.is_scrap
            ? fistar.scraps_count - 1
            : fistar.scraps_count + 1
        }));
        fiStars = fiStars.map(fistar => {
          if (uid.includes(fistar.uid)) {
            return {
              ...fistar,
              is_scrap: !fistar.is_scrap,
              scraps_count: fistar.is_scrap
                ? fistar.scraps_count - 1
                : fistar.scraps_count + 1
            };
          }
          return fistar;
        });
        this.setState({
          fiStars,
          selectedFiStar
        });
        // this.submit();
      });
    }
  };

  callback = () => {
    this.submit();
  };

  backToCreate = () => {
    this.props.closeModal();
  };

  hanldeChangeSearch = e => {
    this.setState({
      search: e.target.value
    });
  };

  handleSubmitSearch = e => {
    e.preventDefault();
    if (this.state.search) {
      this.submit();
    }
  };

  onClickToMyFistar = () => {
    this.setState({
      isRedirectToFistar: true
    });
  };

  renderFilterItem = () => {
    const { code } = this.props;
    const { selectedTypes, allSelected } = this.state;

    return TYPES.map((type, typeKey) => {
      if (!code.data[type]) {
        return;
      }
      let cdg_name = code.data[type].cdg_name;
      if (cdg_name.toLowerCase() == "Age range".toLowerCase()) {
        cdg_name = cdg_name.split(" ")[0];
      }
      return (
        <div className="item item-value" key={typeKey}>
          <div className="title">
            <h4>{cdg_name}</h4>
          </div>
          <div className="check-box-group">
            <div className="check-detail-custom check-box mb-3">
              <input
                type="checkbox"
                name={`all-${type}`}
                id={`all-${type}`}
                onChange={() => this.onSelectedType(code.data[type].code)}
                checked={allSelected.includes(code.data[type].cdg_id)}
              />
              <label htmlFor={`all-${type}`} className="label-box">
                All
              </label>
            </div>
            {code.data[type].code.map((item, itemKey) => (
              <div className="check-detail-custom check-box mb-3" key={itemKey}>
                <input
                  type="checkbox"
                  name={`${type}-${item.cd_label}`}
                  id={`${type}-${item.cd_label}`}
                  onChange={() =>
                    this.onSelectedType(item, code.data[type].code.length)
                  }
                  checked={
                    selectedTypes.some(
                      e =>
                        `${e.cd_id}-${e.cdg_id}` ===
                        `${item.cd_id}-${item.cdg_id}`
                    )
                      ? true
                      : false
                  }
                />
                <label
                  htmlFor={`${type}-${item.cd_label}`}
                  className="label-box"
                >
                  {item.cd_label}
                </label>
              </div>
            ))}
          </div>
        </div>
      );
    });
  };

  renderSelectedType = () => {
    const { selectedTypes } = this.state;
    return (
      <div className="wf-new-all">
        {selectedTypes.map((type, key) => {
          return (
            <div className="item" key={key}>
              <span>{type.cd_label}</span>
              <button type="button" onClick={() => this.onSelectedType(type)}>
                <i className="fas fa-times" />
              </button>
            </div>
          );
        })}
      </div>
    );
  };

  renderSearchFilters = () => (
    <Accordion defaultActiveKey="0">
      <Accordion.Toggle as={Card.Header} eventKey="0">
        <button>123</button>
      </Accordion.Toggle>
      <Accordion.Collapse eventKey="0">
        <div>Hello! I'm the body</div>
      </Accordion.Collapse>
    </Accordion>
  );

  renderSearchFilter = () => (
    <div className="container content-fistarsearch">
      <div className="top-fistar-search">
        <form className="form-search" onSubmit={this.handleSubmitSearch}>
          <input
            type="text"
            className="text-white"
            onChange={this.hanldeChangeSearch}
            value={this.state.search}
          />
          <div
            className={`btn icon-search${this.state.search ? "" : " disable"}`}
            onClick={this.handleSubmitSearch}
          >
            <i className="fas fa-search" />
          </div>
          <div className="btn-fistar">
            <button type="button" onClick={this.onClickToMyFistar}>
              {this.props.t("FISTAR_SEARCH.FSH_button_myfistar")}
              <i className="fas fa-long-arrow-alt-right" />
            </button>
          </div>
        </form>
      </div>
      <form onSubmit={this.onClickSearch} className="form-search-fistar">
        <div className="content-search content w-100">
          <Accordion defaultActiveKey="0">
            <Accordion.Toggle eventKey="0" className="wf-btn-drop">
              <div className="dropdown-search">
                <div className="dropdown">
                  <button
                    className="btn btn-secondary dropdown-toggle"
                    type="button"
                  >
                    {this.props.t("FISTAR_SEARCH.FSH_search_filter")}
                  </button>
                </div>
              </div>
            </Accordion.Toggle>
            <Accordion.Collapse eventKey="0">
              <Fragment>
                <div className="list-tick-value">{this.renderFilterItem()}</div>
                <div className="reset-search">
                  <div className="left">
                    <button
                      className="reset"
                      type="button"
                      onClick={this.resetSelectTypes}
                    >
                      <i className="fas fa-sync-alt" />
                      <span>{this.props.t("FISTAR_SEARCH.FSH_reset")}</span>
                    </button>
                    {this.renderSelectedType()}
                  </div>
                  <div className="right">
                    <button
                      onClick={this.onClickSearch}
                      // disabled={this.state.isLoadingSearch ? true : false}
                    >
                      {this.props.t("FISTAR_SEARCH.FSH_button_fistar_search")}{" "}
                      {this.state.isLoadingSearch && (
                        <span
                          className="spinner-border spinner-border-sm"
                          role="status"
                          aria-hidden="true"
                        />
                      )}
                    </button>
                  </div>
                </div>
              </Fragment>
            </Accordion.Collapse>
          </Accordion>
        </div>
      </form>
    </div>
  );

  renderFistar = () => {
    const { fiStars, selectedFiStar } = this.state;
    console.log(this.state.isInfinity, this.state.hasMore, 'isInfinityisInfinityisInfinity');

    if (!this.state.isInfinity) {
      return null;
    }

    if (!fiStars.length) {
      return null;
    }
    // if(fiStars.length > 0)
    return (
      <div id="ss" style={{ height: '100vh', overflow: "auto" }}>
      <InfiniteScroll
        dataLength={fiStars.length}
        next={this.fetchMoreData}
        loader={<h4>{this.props.t("LOADING.LOADING")}</h4>}
        hasMore={this.state.hasMore}
        className="card-matching"
        scrollableTarget="ss"
      >
        <div className="list-item row">
          {fiStars.map((fistar, key) => {
            let fime = 0;
            let facebook = 0;
            let youtube = 0;
            let instagram = 0;
            let year = new Date(fistar.dob);
            let yearConver = year.toDateString();
            let yearFistar = yearConver.split(" ").pop();

            fistar.channel &&
              fistar.channel.map(channel => {
                if (channel.sns_id == 1) {
                  fime = channel.usn_follower;
                }
                if (channel.sns_id == 2) {
                  facebook = channel.usn_follower;
                }
                if (channel.sns_id == 3) {
                  youtube = channel.usn_follower;
                }
                if (channel.sns_id == 4) {
                  instagram = channel.usn_follower;
                }
              });

            return (
              <div className="item col-md-3 col-xs-6" key={key}>
                <div className="card">
                  <div className="top">
                    <div className="left">
                      <div className="male">
                        {fistar.gender === GENDER_TYPE.MALE ? (
                          <i className="fas fa-mars" aria-hidden="true" />
                        ) : (
                          <i className="fa fa-venus" aria-hidden="true" />
                        )}
                      </div>
                      <div className="address-old">
                        <h4>
                          <a href="javascript:void(0)">{fistar.name}</a>
                        </h4>
                        <span>{yearFistar}</span>
                        <span className="address">{fistar.location_label}</span>
                      </div>
                    </div>
                    <div className="right">
                      <div className="check-detail-custom checkbox-add-partner check-box mb-3">
                        <input
                          type="checkbox"
                          name={fistar.name}
                          id={fistar.uid}
                          readOnly
                          onChange={() => this.selectFiStar(fistar)}
                          checked={
                            selectedFiStar.some(e => e.uid === fistar.uid)
                              ? true
                              : false
                          }
                        />
                        <label htmlFor={fistar.uid} className="label-box" />
                      </div>
                    </div>
                  </div>
                  <div className="img-partner">
                    <a
                      href="javascript:void(0)"
                      className="item-fistar-select"
                      onClick={() => this.OpenModal(fistar.uid)}
                    >
                      <img
                        src={getImageLink(
                          fistar.avatar,
                          IMAGE_TYPE.FISTARS,
                          IMAGE_SIZE.ORIGINAL
                        )}
                        alt="..."
                      />
                    </a>
                    <Scrap
                      scraps_count={fistar.scraps_count}
                      is_scrap={fistar.is_scrap}
                      id={fistar.uid}
                      className="btn over-flow"
                      callback={this.callback}
                    />
                  </div>
                  <div className="card-footer">
                    <ul className="list-footer">
                      <li className="item">
                        <a href="javascript:void(0)">
                          <i className="fab fa-facebook-f" />
                        </a>
                        <span>{nFormatter(facebook, 0)}</span>
                      </li>
                      <li className="item">
                        <a href="javascript:void(0)">
                          <i className="fab fa-instagram" />
                        </a>
                        <span>{nFormatter(instagram, 0)}</span>
                      </li>
                      <li className="item">
                        <a href="javascript:void(0)">
                          <i className="fab fa-youtube-square" />
                        </a>
                        <span>{nFormatter(youtube, 0)}</span>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </InfiniteScroll>
      </div>
    );
  };

  OpenModal = uid => {
    this.setState({
      show: true,
      uid: uid
    });
  };

  handleClose = () => {
    this.setState({ show: false });
  };
  onCloseModal = () => {
    this.setState({ show: !this.state.show });
  };

  renderSelectedFiStar = () => {
    const { selectedFiStar } = this.state;

    return selectedFiStar.map((fistar, key) => {
      return (
        <div className="item" key={key}>
          <a href="javascript:void(0)">
            {" "}
            <img
              src={getImageLink(
                fistar.avatar,
                IMAGE_TYPE.FISTARS,
                IMAGE_SIZE.ORIGINAL
              )}
              alt=""
            />
          </a>
          <div className="name">
            <span>{fistar.name}</span>
            <button
              type="button"
              className="close"
              onClick={() => this.selectFiStar(fistar)}
            >
              <i className="fas fa-times" />
            </button>
          </div>
        </div>
      );
    });
  };

  render() {
    const { t, code } = this.props;
    const {
      fiStars,
      selectedFiStar,
      show,
      isSearchLink,
      isRedirectToFistar
    } = this.state;
    if (isRedirectToFistar) {
      return <Redirect to={routeName.PARTNER_MY_FISTAR} />;
    }
    return (
      <Fragment>
        <div className="content-fistarsearch modal-fistar-search fistar-search-template wf-search-fistars">
          {!isSearchLink && (
            <button
              className="close icon-close mt-3"
              onClick={this.backToCreate}
            >
              <img src={CancelSvg} alt="" />
            </button>
          )}

          {this.renderSearchFilter()}

          <div className="container">
            <div className="list-username-partner">
              <div className="amount-partner">
                <p>
                  {t("FISTAR_SEARCH.FSH_result")}:{" "}
                  <span>{this.state.total}</span> {t("HEADER.HED_fistar")}
                </p>
              </div>
              
              {this.renderFistar()}
            </div>
          </div>
        </div>
        <div className="add-partner">
          <div className="container pr-3 pl-3">
            <div className="row-col">
              <div className="col-md-6-flex">
                <div className="list-thumnailS">
                  {this.renderSelectedFiStar()}
                </div>
              </div>
              <div className="col-md-6-flex">
                <div className="group-btn-partner">
                  <div className="select-add">
                    <p>
                      {t("PROFILE_FISTAR.PFR_select ")} {selectedFiStar.length}{" "}
                      <span>
                        {t("PROFILE_FISTAR.PFR_out_of")} {fiStars.length}
                      </span>
                    </p>
                  </div>
                  <div className="btn-group mt-2 mb-2">
                    {this.props.showSelectButton && (
                      <button
                        className={`btn-matching${
                          selectedFiStar.length === 0 ? " disable" : ""
                        }`}
                        type="button"
                        onClick={this.getFiStarsToCreate}
                      >
                        {t("HEADER.HEA_campaign_matching")}
                      </button>
                    )}
                    <button
                      className={`btn-scap${
                        selectedFiStar.length === 0 ? " disable" : ""
                      }`}
                      type="button"
                      onClick={this.clickScrap}
                    >
                      <i className="fas fa-star" />
                      {t("DASHBOARD_FISTAR.DFR_status_scrap")}
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/*<Modal*/}
        {/*size="lg"*/}
        {/*show={this.state.show}*/}
        {/*onHide={this.handleClose}*/}
        {/*aria-labelledby="example-modal-sizes-title-lg"*/}
        {/*dialogClassName="fistar-search-template"*/}
        {/*>*/}

        {this.state.show &&
          this.state.uid && (
            <ModalDetailFistar
              show={this.state.show}
              onCloseModal={this.onCloseModal}
              uid={this.state.uid}
            />
          )}

        {/*</Modal>*/}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    code: state.code,
    channel: state.channel
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getCode: type => dispatch(getCodeAction(type)),
    getChannel: type => dispatch(getChannelAction(type)),
    searchFistar: (search, page, state, active) =>
      dispatch(searchFistarAction(search, page, state, active)),
    scrap: id => dispatch(scrapAction(id))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(withRouter(PartnerCreateCampain)));
