import React, { Component, Fragment } from "react";
import ReactDOM from "react-dom";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect, Route } from "react-router-dom";
import { connect } from "react-redux";
import { getCampaignDetailAction, getFistarOfCampaignDetailAction } from "./../../../../store/actions/campaign";
import { Tab, Nav } from "react-bootstrap";
import "./CampaignTrackingDetail.css";
import backImage from "./../../../../images/left.svg";
import imageFime from "./../../../../images/mk.png";
import imageFb from "./../../../../images/face.png";
import imageInsta from "./../../../../images/intar.png";
import imageYt from "./../../../../images/toutube.png";
import imageIdol from "./../../../../images/Layer 6.png";
import * as routeName from "./../../../../routes/routeName";
import { STATUS_STEP_PARTNER, CAMPAIGN_STATUS } from "./../../../../constants";
import FistarItem from "./FistarItem";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { nFormatter } from "./../../../../common/helper";
import PieChart from "./../../../../components/charts/PieChart";
import ModalReviewSNS from "./ModalReviewSNS";
import ModalModifyCampaign from "./ModalModifyCampaign";
import CampaignStatusAction from "./../../../../components/CampaignStatusAction";
import { getImageLink } from "./../../../../common/helper";
import Scrap from "./../../../../components/Scrap/index";
import CampaignMainImage from "./../../../../components/CampaignMainImage/index"
import {
  IMAGE_SIZE,
  IMAGE_TYPE,
  SNS_STATE
} from "./../../../../constants/index";
import { SNS_CHANNEL } from "../../../../constants";
import { DateFormatYMD } from "../../../../common/helper";
import SearchFiStar from "./../CreateCampain/search";
import {DateFormatYMDDatePickerPlus7Day} from "./../../../../common/helper"
import moment from 'moment'
// import {AmCharts} from "@amcharts/amcharts3-react";

function getId(url) {
  if (typeof url !== "string") {
    return null;
  }
  var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
  var match = url.match(regExp);

  if (match && match[2].length == 11) {
    return match[2];
  } else {
    return "error";
  }
}

const TYPE = {
  ALL: "all",
  MATCHING: "matching",
  READY: "ready",
  ONGOING: "ongoing",
  CLOSED: "closed"
};

const MATCHING_STATUS = {
  MARCHED: [8, 16],
  RECOMMENDEDED: [],
  CONSIDERING: [1, 6, 9],
  REJECTED: [3,6,7,10,11,13,15],
  CANCEL: [2, 5, 7, 10, 15]
};

const SNS = {
  FIME: 1,
  FACEBOOK: 2,
  YOUTUBE: 3,
  INSTAGRAM: 4
};

class CampaignTrackingDetail extends Component {
  constructor(props) {
    super(props);
    console.log(this.props);
    this.state = {
      campaign: {},
      isLoadingCampaign: false,
      isBack: false,
      show: false,
      showModify: false,
      fistar: null,
      channel: null,
      reloadVideo: false,
      statusType: "all",
      isSearchFiStar: false,
      fistars: []
    };
    this.previewVideo = React.createRef();
    this.previewVideoSource = React.createRef();
    if (typeof window !== 'undefined' && window.document && window.document.createElement) {
      window.scrollTo(0, 0);
    }
  }

  componentDidMount() {
    const {
      match: { params }
    } = this.props;
    const { id } = params;
    this.setState(
      {
        isLoadingCampaign: true
      },
      () => {
        this.props
          .getCampaign(id)
          .then(response => {
            this.setState({
              campaign: response,
              isLoadingCampaign: false
            });
            return this.props.getFistarOfCampaign(response.cp_id, TYPE.ALL)
          })
          .then((response) => {
            this.setState({
              fistars: response
            })
          })
          .catch(e => {
            this.setState({
              isLoadingCampaign: false
            });
            this.props.history.push(routeName.PARTNER_DASHBOARD);
          });
      }
    );
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.match.params.id !== prevProps.match.params.id) {
      this.reloadData(this.props.match.params.id);
    }
    //   if (
    //     this.state.campaign
    //     && this.state.campaign.cp_main_image_type == 3
    //     && this.state.campaign.cp_main_image == prevState.campaign.cp_main_image
    //   ) {
    //     if (this.previewVideo.current) {
    //       this.previewVideo.current.pause();
    //     }
    //   }
    //   if (
    //     this.state.campaign
    //     && this.state.campaign.cp_main_image_type == 3
    //     && this.state.campaign.cp_main_image != prevState.campaign.cp_main_image
    //   ) {
    //     if (this.previewVideo.current) {
    //       this.previewVideo.current.load();
    //       this.previewVideo.current.play();
    //     }
    //   }
  }

  reloadData = (loading = false) => {
    const {
      match: { params }
    } = this.props;
    const { id } = params;
    this.setState({
      isLoadingCampaign: loading
    })
    this.props
      .getCampaign(id)
      .then(response => {
        this.setState({
          campaign: response,
          reloadVideo: false
        });
        return this.props.getFistarOfCampaign(this.state.campaign.cp_id, this.state.statusType)
      })
      .then((response) => {
        this.setState({
          fistars: response,
          isLoadingCampaign: false
        })
      })
      .catch(e => {
        this.props.history.push(routeName.PARTNER_DASHBOARD);
      });
  };

  onClickBack = () => {
    this.setState({
      isBack: true
    });
  };

  onClickReviewSNS = (campaign, fistar, channel) => {
    if (campaign && fistar && channel) {
      this.setState({
        show: true,
        campaign,
        fistar,
        channel
      });
    }
  };

  handleClose = () => {
    this.setState({ show: false });
  };

  onCloseModal = () => {
    this.setState({ show: !this.state.show });
  };

  onClickModify = campaign => {
    this.setState({
      showModify: true,
      campaign
    });
  };

  handleCloseModify = () => {
    this.setState({ showModify: false });
  };

  onCloseModalModify = () => {
    this.setState({ showModify: !this.state.showModify });
  };

  resetData = (setLoading = true) => {
    this.setState(
      {
        reloadVideo: true,
        isLoadingCampaign: setLoading ? true : false,
      },
      () => {
        this.props.getCampaign(this.props.match.params.id).then(response => {
          this.setState({
            campaign: response,
            reloadVideo: false,
            isLoadingCampaign: false
          });
          return this.props.getFistarOfCampaign(response.cp_id, TYPE.ALL)
        })
        .then((response) => {
          this.setState({
            fistars: response
          })
        })
      }
    );
  };

  renderTabMatchingFistar = () => {
    const { fistars } = this.state
    const { campaign } = this.state;
    let pid = campaign && campaign.p_id ? campaign.p_id : "";
    let status = campaign && campaign.cp_status ? campaign.cp_status : "";
    if (fistars.length === 0) {
      return <div>No fistar</div>;
    }
    return (
      <Fragment>
        {fistars.map((fistar, key) => {
          return (
            <Fragment key={key}>
              <FistarItem
                fistar={fistar}
                reloadData={this.reloadData}
                cp_status={this.state.campaign.cp_status}
                cp_name={this.state.campaign.cp_name}
                pid={pid}
                status={status}
                clickisSearchFiStar={this.clickisSearchFiStar}
                callbackScrap={() => this.resetData(false)}
              />
            </Fragment>
          );
        })}
      </Fragment>
    );
  };

  changeTab = (type) => {
    this.props.getFistarOfCampaign(this.state.campaign.cp_id, type)
      .then((response) => {
        this.setState({
          fistars: response
        })
      })
  }

  checkClick = type => {
    let statusType = "";
    switch (type) {
      case "matched": {
        statusType = "matched";
        break;
      }
      case "considering": {
        statusType = "considering";
        break;
      }
      case "recommended": {
        statusType = "recommended";
        break;
      }
      case "reject": {
        statusType = "reject";
        break;
      }
      default: {
        statusType = "all";
        break;
      }
    }
    this.changeTab(type)
    this.setState({
      statusType: statusType
    });
  };

  clickisSearchFiStar = () => {
    this.setState({ isSearchFiStar: true });
  };

  closeModalSearchFistar = () => {
    this.setState({ isSearchFiStar: false });
  };

  renderTabMatching = () => {
    const { campaign } = this.state;
    const { t } = this.props;
    if (!campaign || Object.keys(campaign).length === 0) {
      return null;
    }

    const { matchings } = campaign;
    let cost = 0;
    let matched = [];
    let recomended = [];
    let considering = [];
    let rejected = [];
    let fistars_matching = [];

    let fimeSelected = {};
    let facebookSelected = {};
    let youtubeSelected = {};
    let instagramSelected = {};
    let arrayChannel = [];
    let testChannel = [];

    // matchings.map(status => {
    //   if (
    //     !fistars_matching
    //       .map(s => s.influencer.uid)
    //       .includes(status.influencer.uid)
    //   ) {
    //     fistars_matching.push(status);
    //   } else {
    //     fistars_matching[fistars_matching.length - 1] = status;
    //   }
    // });

    this.state.fistars.map((fistar) => {
      let lastMatching = null
      if (fistar.fistar_campaign && fistar.fistar_campaign.length > 0) {
        lastMatching = fistar.fistar_campaign[fistar.fistar_campaign.length - 1]
        let id_selectd = lastMatching.matching_channel
            .map((channel) => channel.m_ch_selected ? channel.sns_id : null)
            .filter(e => e !== null)
        fistar.channels.map((channel) => {
            if (id_selectd.includes(channel.sns_id)) {
                cost += +channel.cost
    
            }
        })
      }
    })

    return (
      <Fragment>
        <Tab.Container id="tabs-matching" defaultActiveKey={"tabFistarAll"}>
          <div className="information-tracking">
            <Nav variant="pills">
              <Nav.Item className="list-information w-100">
                <Nav.Link
                  className="item"
                  eventKey="tabFistarAll"
                  onClick={() => this.checkClick("all")}
                >
                  <div className="text">
                    <p className="number">{+campaign.cp_total_influencer}</p>
                    <p>{t("PARTNER_INFORMATION.PNI_all_people")}</p>
                  </div>
                </Nav.Link>
                <Nav.Link
                  className="item"
                  eventKey="tabFistarMatched"
                  onClick={() => this.checkClick("matched")}
                >
                  <div className="text">
                    <p className="number">{campaign.statitics ? +campaign.statitics.matched : 0}</p>
                    <p>{t("PARTNER_INFORMATION.PNI_amount_matche")}</p>
                  </div>
                </Nav.Link>
                <Nav.Link
                  className="item"
                  eventKey="tabFistarRcommended"
                  onClick={() => this.checkClick("recommended")}
                >
                  <div className="text">
                    <p className="number">{+campaign.recommendeds_count}</p>
                    <p>{t("PARTNER_INFORMATION.PNI_amount_recommended")}</p>
                  </div>
                </Nav.Link>
                <Nav.Link
                  className="item"
                  eventKey="tabFistarConsidering"
                  onClick={() => this.checkClick("considering")}
                >
                  <div className="text">
                    <p className="number">{campaign.statitics ? +campaign.statitics.considering : 0}</p>
                    <p>{t("PARTNER_INFORMATION.PNI_amount_considering")}</p>
                  </div>
                </Nav.Link>
                <Nav.Link
                  className="item"
                  eventKey="tabFistarRejected"
                  onClick={() => this.checkClick("reject")}
                >
                  <div className="text">
                    <p className="number">{campaign.statitics ? +campaign.statitics.rejected : 0}</p>
                    <p>{t("PARTNER_INFORMATION.PNI_amount_rejected")}</p>
                  </div>
                </Nav.Link>

                <div className="item">
                  <div className="text">
                    <p className="number">
                      <span>VND</span> {cost.toLocaleString()}
                    </p>
                    <p>{t("PARTNER_INFORMATION.PNI_expected_cost")} </p>
                    <p>({t("PARTNER_INFORMATION.PNI_base_completion")})</p>
                  </div>
                </div>
              </Nav.Item>
            </Nav>
          </div>
          <div className="box-partner">
            <Tab.Content>
              <Tab.Pane eventKey="tabFistarAll">
                {this.renderTabMatchingFistar()}
              </Tab.Pane>
              <Tab.Pane eventKey="tabFistarMatched">
                {this.renderTabMatchingFistar()}
              </Tab.Pane>
              <Tab.Pane eventKey="tabFistarRcommended">
                {this.renderTabMatchingFistar()}
              </Tab.Pane>
              <Tab.Pane eventKey="tabFistarConsidering">
                {this.renderTabMatchingFistar()}
              </Tab.Pane>
              <Tab.Pane eventKey="tabFistarRejected">
                {this.renderTabMatchingFistar()}
              </Tab.Pane>
            </Tab.Content>
          </div>
        </Tab.Container>
      </Fragment>
    );
  };

  renderFistarInfo = influencer => {
    let snsFime = {};
    let snsFB = {};
    let snsIG = {};
    let snsYT = {};
    (influencer.channels || []).map(channel => {
      let sns = {
        follow: channel.usn_follower
        // channel.usn_comment +
        // channel.usn_follower +
        // channel.usn_like +
        // channel.usn_share +
        // channel.usn_view
      };
      if (channel.sns_id === SNS.FIME) {
        snsFime = sns;
      } else if (channel.sns_id === SNS.FACEBOOK) {
        snsFB = sns;
      } else if (channel.sns_id === SNS.YOUTUBE) {
        snsYT = sns;
      } else if (channel.sns_id === SNS.INSTAGRAM) {
        snsIG = sns;
      }
    });
    return (
      <Fragment>
        <div className="image-box ">
          <img
            src={getImageLink(
              influencer.picture,
              IMAGE_TYPE.FISTARS,
              IMAGE_SIZE.ORIGINAL
            )}
            alt={influencer.fullname}
          />

          <Scrap
            scraps_count={influencer.scraps_count}
            is_scrap={influencer.is_scrap}
            id={influencer.uid}
            callback={() => this.resetData(false)}
          />
        </div>
        <div className="information-user">
          <div className="name">
            <h4>
              <i className="fas fa-mars" />
              {influencer.fullname}
            </h4>
            <div className="address-old">
              <span>
                {influencer && influencer.dob
                  ? influencer.dob.split("-")[0]
                  : ""}
              </span>
              <span className="address">{influencer.location.cd_label}</span>
            </div>
          </div>
          <div className="solical-flow">
            <div className="item">
              <a href="javascript:void(0)" className="disabled-hover">
                <i className="fab fa-facebook-f" />
              </a>
              <span>{snsFB.follow ? snsFB.follow.toLocaleString() : 0}</span>
            </div>
            <div className="item">
              <a href="javascript:void(0)" className="disabled-hover">
                <i className="fab fa-instagram" />
              </a>
              <span>{snsIG.follow ? snsIG.follow.toLocaleString() : 0}</span>
            </div>
            <div className="item">
              <a href="javascript:void(0)" className="disabled-hover">
                <i className="fab fa-youtube-square" />
              </a>
              <span>{snsYT.follow ? snsYT.follow.toLocaleString() : 0}</span>
            </div>
          </div>
        </div>
      </Fragment>
    );
  };

  getTextFromSNSStatus = (campaign, fistar, sns, status) => {
    switch (status) {
      case SNS_STATE.READY:
      case SNS_STATE.REQUEST:
      case SNS_STATE.MODIFY: {
        return (
          <button type="button" className="btn">
            fistar Writing
          </button>
        );
        break;
      }
      case SNS_STATE.CHECKING: {
        return (
          <button
            type="button"
            className="btn"
            onClick={() => this.onClickReviewSNS(campaign, fistar, sns)}
          >
            Partner Checking
          </button>
        );
        break;
      }
      case SNS_STATE.COMPLETE: {
        if (sns.m_ch_active_url) {
            let url_https=sns.m_ch_url.includes("https://");
            let url_http=sns.m_ch_url.includes("http://");
          if(!url_https && !url_http) {
              sns.m_ch_url="https://" + sns.m_ch_url
          }
          return (
            <a href={sns.m_ch_url} className="btn" target="_blank">
              Completed
            </a>
          );
        } else {
          return (
            <button
              type="button"
              className="btn"
              onClick={() => this.onClickReviewSNS(campaign, fistar, sns)}
            >
              Partner Checking
            </button>
          );
        }
        break;
      }
      default: {
        break;
      }
    }
  };

  renderTabReview = campaign => {
    if (!campaign || Object.keys(campaign).length === 0) {
      return <div className="box-partner" />;
    }

    const { matchings } = campaign;
    let review = 0;
    matchings.map(fistar => {
      review += fistar.matching_channel ? fistar.matching_channel.length : 0;
    });

    if (!review) {
      return <div className="box-partner" />;
    }

    let DMC = {
      "fullname": "DM&C",
      "picture": "https://x.kinja-static.com/assets/images/logos/placeholders/default.png",
      "gender":{},
      "dob": "2001-09-07",
      "location": {cd_label: 'Ho Chi Minh'},
      "created_at": "2019-09-03 08:33:13",
      "updated_at": "2019-09-16 17:09:08",
      "scraps_count": 0,
      "history_from_partner": null,
      "count_campaign_relate": {},
      "is_scrap": null,
      "channels": [
      {
          "sns_id": 1,
          "sns_url": "",
          "usn_follower": 0,
          "usn_like": 0,
          "usn_post": 0,
          "usn_comment": 0,
          "usn_share": 0,
          "usn_view": 0,
          "cost": 0,
      },
      {
          "sns_id": 2,
          "sns_url": "",
          "usn_follower": 0,
          "usn_like": 0,
          "usn_post": 0,
          "usn_comment": 0,
          "usn_share": 0,
          "usn_view": 0,
          "cost": 0,
      },
      {
          "sns_id": 3,
          "sns_url": "",
          "usn_follower": 0,
          "usn_like": 0,
          "usn_post": 0,
          "usn_comment": 0,
          "usn_share": 0,
          "usn_view": 0,
          "cost": 0,
      },
      {
          "sns_id": 4,
          "sns_url": "",
          "usn_follower": 0,
          "usn_like": 0,
          "usn_post": 0,
          "usn_comment": 0,
          "usn_share": 0,
          "usn_view": 0,
          "cost": 0,
      }]
    }
    DMC.influencer = DMC

    let snsFime = "";
    let snsFB = "";
    let snsIG = "";
    let snsYT = "";
    (campaign.admin_reviews || []).map(channel => {
      if (channel.sns_id === SNS.FIME) {
        snsFime = channel;
      } else if (channel.sns_id === SNS.FACEBOOK) {
        snsFB = channel;
      } else if (channel.sns_id === SNS.YOUTUBE) {
        snsYT = channel;
      } else if (channel.sns_id === SNS.INSTAGRAM) {
        snsIG = channel;
      }
    });

    return (
      <div className="box-partner">
        {campaign.admin_reviews && campaign.admin_reviews.length > 0 && (
          <Fragment>
          <div className="box-item">
            <div className="left">
              {this.renderFistarInfo(DMC)}
            </div>
            <div className="group-solical suggested-price">
              <div className="item-solical">
                <div className="icon-solical">
                  <img
                    src={imageFime}
                    alt=""
                    className={`${
                      snsFime.m_ch_selected === 1 ? "" : "fitter-gray"
                    }`}
                  />
                </div>
                {snsFime.admin_review_status &&
                  snsFime.m_ch_selected === 1 && (
                    <div className="btn-solical">
                      {this.getTextFromSNSStatus(
                        campaign,
                        DMC,
                        snsFime,
                        snsFime.admin_review_status.rv_status
                      )}
                    </div>
                  )}
              </div>
              <div className="item-solical">
                <div className="icon-solical">
                  <img
                    src={imageFb}
                    alt=""
                    className={`${
                      snsFB.m_ch_selected === 1 ? "" : "fitter-gray"
                    }`}
                  />
                </div>
                {snsFB.admin_review_status &&
                  snsFB.m_ch_selected === 1 && (
                    <div className="btn-solical">
                      {this.getTextFromSNSStatus(
                        campaign,
                        DMC,
                        snsFB,
                        snsFB.admin_review_status.rv_status
                      )}
                    </div>
                  )}
              </div>
              <div className="item-solical">
                <div className="icon-solical">
                  <img
                    src={imageInsta}
                    alt=""
                    className={`${
                      snsIG.m_ch_selected === 1 ? "" : "fitter-gray"
                    }`}
                  />
                </div>
                {snsIG.admin_review_status &&
                  snsIG.m_ch_selected === 1 && (
                    <div className="btn-solical">
                      {this.getTextFromSNSStatus(
                        campaign,
                        DMC,
                        snsIG,
                        snsIG.admin_review_status.rv_status
                      )}
                    </div>
                  )}
              </div>
              <div className="item-solical">
                <div className="icon-solical">
                  <img
                    src={imageYt}
                    alt=""
                    className={`${
                      snsYT.m_ch_selected === 1 ? "" : "fitter-gray"
                    }`}
                  />
                </div>
                {snsYT.admin_review_status &&
                  snsYT.m_ch_selected === 1 && (
                    <div className="btn-solical">
                      {this.getTextFromSNSStatus(
                        campaign,
                        DMC,
                        snsYT,
                        snsYT.admin_review_status.rv_status
                      )}
                    </div>
                  )}
              </div>
            </div>
          </div>
          
          </Fragment>
        )}
        {matchings.map((fistar, key) => {
          let snsFime = "";
          let snsFB = "";
          let snsIG = "";
          let snsYT = "";
          (fistar.matching_channel || []).map(channel => {
            if (channel.sns_id === SNS.FIME) {
              snsFime = channel;
            } else if (channel.sns_id === SNS.FACEBOOK) {
              snsFB = channel;
            } else if (channel.sns_id === SNS.YOUTUBE) {
              snsYT = channel;
            } else if (channel.sns_id === SNS.INSTAGRAM) {
              snsIG = channel;
            }
          });
          return (
            <Fragment key={key}>
              {MATCHING_STATUS.MARCHED.includes(fistar.matching_status.m_status) ? <div className="box-item">
                <div className="left">
                  {this.renderFistarInfo(fistar.influencer)}
                </div>
                <div className="group-solical suggested-price">
                  <div className="item-solical">
                    <div className="icon-solical">
                      <img
                        src={imageFime}
                        alt=""
                        className={`${
                          snsFime.m_ch_selected === 1 ? "" : "fitter-gray"
                        }`}
                      />
                    </div>
                    {snsFime.review_status &&
                      snsFime.review_status.label.partner_status &&
                      snsFime.m_ch_selected === 1 && (
                        <div className="btn-solical">
                          {this.getTextFromSNSStatus(
                            campaign,
                            fistar,
                            snsFime,
                            snsFime.review_status.rv_status
                          )}
                        </div>
                      )}
                  </div>
                  <div className="item-solical">
                    <div className="icon-solical">
                      <img
                        src={imageFb}
                        alt=""
                        className={`${
                          snsFB.m_ch_selected === 1 ? "" : "fitter-gray"
                        }`}
                      />
                    </div>
                    {snsFB.review_status &&
                      snsFB.review_status.label.partner_status &&
                      snsFB.m_ch_selected === 1 && (
                        <div className="btn-solical">
                          {this.getTextFromSNSStatus(
                            campaign,
                            fistar,
                            snsFB,
                            snsFB.review_status.rv_status
                          )}
                        </div>
                      )}
                  </div>
                  <div className="item-solical">
                    <div className="icon-solical">
                      <img
                        src={imageInsta}
                        alt=""
                        className={`${
                          snsIG.m_ch_selected === 1 ? "" : "fitter-gray"
                        }`}
                      />
                    </div>
                    {snsIG.review_status &&
                      snsIG.review_status.label.partner_status &&
                      snsIG.m_ch_selected === 1 && (
                        <div className="btn-solical">
                          {this.getTextFromSNSStatus(
                            campaign,
                            fistar,
                            snsIG,
                            snsIG.review_status.rv_status
                          )}
                        </div>
                      )}
                  </div>
                  <div className="item-solical">
                    <div className="icon-solical">
                      <img
                        src={imageYt}
                        alt=""
                        className={`${
                          snsYT.m_ch_selected === 1 ? "" : "fitter-gray"
                        }`}
                      />
                    </div>
                    {snsYT.review_status &&
                      snsYT.review_status.label.partner_status &&
                      snsYT.m_ch_selected === 1 && (
                        <div className="btn-solical">
                          {this.getTextFromSNSStatus(
                            campaign,
                            fistar,
                            snsYT,
                            snsYT.review_status.rv_status
                          )}
                        </div>
                      )}
                  </div>
                </div>
              </div> : ""}
              
            </Fragment>
          );
        })}
      </div>
    );
  };

  renderReviewSNS = campaign => {
    const { t } = this.props;
    return campaign.matchings.map(matching => {
      return matching.matching_channel.map((channel, key) => {
        if (channel.m_ch_selected === 1 && channel.m_ch_url != null && channel.m_ch_url != '' && channel.m_ch_active_url === 1) {
          let dateObj = new Date(channel.updated_at);
          let month = dateObj.getUTCMonth() + 1;
          let day = dateObj.getUTCDate();
          let year = dateObj.getUTCFullYear();
          // let newdate = year + "/" + month + "/" + day;
          let newdate = DateFormatYMD(channel.updated_at);
          let imageSNS = "";
          switch (channel.sns_id) {
            case SNS.FIME:
              imageSNS = imageFime;
              break;
            case SNS.FACEBOOK:
              imageSNS = imageFb;
              break;
            case SNS.YOUTUBE:
              imageSNS = imageYt;
              break;
            case SNS.INSTAGRAM:
              imageSNS = imageInsta;
              break;
            default:
              break;
          }
          return (
            <div className="card" key={key}>
              <div className="image-covers">
                <a href="javascript:void(0)">
                  {/*<img*/}
                    {/*src={getImageLink(*/}
                      {/*campaign.cp_main_image,*/}
                      {/*IMAGE_TYPE.CAMPAIGNS,*/}
                      {/*IMAGE_SIZE.LARGE*/}
                    {/*)}*/}
                    {/*alt={campaign.cp_name}*/}
                  {/*/>*/}

                  <CampaignMainImage campaign ={campaign} />

                </a>
              </div>
              <div className="content-card">
                <div className="information-user">
                  <a href="javascript:void(0)">
                    <img
                      src={getImageLink(
                        matching.influencer.picture,
                        IMAGE_TYPE.FISTARS,
                        IMAGE_SIZE.ORIGINAL
                      )}
                      alt={matching.influencer.fullname}
                      className="thumnail"
                    />
                  </a>
                  <div className="user-name">
                    <h4>
                      <i className="fas fa-mars" />
                      {matching.influencer.fullname}
                    </h4>
                    <div className="address-old">
                      <p>
                        <span>
                          {new Date(matching.influencer.dob).getFullYear()}
                        </span>
                        <span className="address">
                          {matching.influencer.location.cd_label}
                        </span>
                      </p>
                    </div>
                  </div>
                </div>
                <div className="text">
                  <h3 className="mb-3">{channel.m_ch_title}</h3>
                  <div
                    dangerouslySetInnerHTML={{ __html: channel.m_ch_content }}
                  />{" "}
                  {channel.m_ch_url && (
                    <a href={channel.m_ch_url} target="_blank">
                      {t("POPUP_MESSEAGE.PME_button_more")}
                    </a>
                  )}
                </div>
                <p className="date">
                  {newdate}
                  <span>{channel.sns.sns_name}</span>
                </p>
              </div>
              <div className="footer-card">
                <div className="flow-solical">
                  <div className="icon-solical">
                    <a href="javscript:void(0)">
                      <img src={imageSNS} alt={channel.sns.sns_name} />
                    </a>
                  </div>
                  <ul className="list-footer">
                    <li className="item">
                      <a href="javascript:void(0)">
                        <i className="far fa-heart" />
                      </a>{" "}
                      <span>{(channel.m_ch_like || 0).toLocaleString()}</span>
                    </li>
                    <li className="item">
                      <a href="javascript:void(0)">
                        <i className="far fa-comment-dots" />
                      </a>{" "}
                      <span>
                        {(channel.m_ch_comment || 0).toLocaleString()}
                      </span>
                    </li>
                    <li className="item">
                      <a href="javascript:void(0)">
                        <i className="fas fa-share-alt" />
                      </a>{" "}
                      <span>{(channel.m_ch_share || 0).toLocaleString()}</span>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          );
        }
      });
    });
  };

  renderReviewDMCSNS = campaign => {
    const { t } = this.props;
    return campaign.admin_reviews.map((channel, key) => {
      if (channel.m_ch_selected === 1 && channel.m_ch_url != null && channel.m_ch_url != '' && channel.m_ch_active_url === 1) {
        let dateObj = new Date(channel.updated_at);
        let month = dateObj.getUTCMonth() + 1;
        let day = dateObj.getUTCDate();
        let year = dateObj.getUTCFullYear();
        // let newdate = year + "/" + month + "/" + day;
        let newdate = DateFormatYMD(channel.updated_at);
        let imageSNS = "";
        switch (channel.sns_id) {
          case SNS.FIME:
            imageSNS = imageFime;
            break;
          case SNS.FACEBOOK:
            imageSNS = imageFb;
            break;
          case SNS.YOUTUBE:
            imageSNS = imageYt;
            break;
          case SNS.INSTAGRAM:
            imageSNS = imageInsta;
            break;
          default:
            break;
        }
        return (
          <div className="card" key={key}>
            <div className="image-covers">
              <a href="javascript:void(0)">
                {/*<img*/}
                  {/*src={getImageLink(*/}
                    {/*campaign.cp_main_image,*/}
                    {/*IMAGE_TYPE.CAMPAIGNS,*/}
                    {/*IMAGE_SIZE.LARGE*/}
                  {/*)}*/}
                  {/*alt={campaign.cp_name}*/}
                {/*/>*/}

                <CampaignMainImage campaign ={campaign} />

              </a>
            </div>
            <div className="content-card">
              <div className="information-user">
                <a href="javascript:void(0)">
                  <img
                    src={'DM&C'}
                    alt={'DM&C'}
                    className="thumnail"
                  />
                </a>
                <div className="user-name">
                  <h4>
                    <i className="fas fa-mars" />
                    DM&C
                  </h4>
                  <div className="address-old">
                    <p>
                      <span>
                        DM&C
                      </span>
                      <span className="address">
                        DM&C
                      </span>
                    </p>
                  </div>
                </div>
              </div>
              <div className="text">
                <h3 className="mb-3">{channel.m_ch_title}</h3>
                <div
                  dangerouslySetInnerHTML={{ __html: channel.m_ch_content }}
                />{" "}
                {channel.m_ch_url && (
                  <a href={channel.m_ch_url} target="_blank">
                    {t("POPUP_MESSEAGE.PME_button_more")}
                  </a>
                )}
              </div>
              <p className="date">
                {newdate}
                <span>{channel.sns.sns_name}</span>
              </p>
            </div>
            <div className="footer-card">
              <div className="flow-solical">
                <div className="icon-solical">
                  <a href="javscript:void(0)">
                    <img src={imageSNS} alt={channel.sns.sns_name} />
                  </a>
                </div>
                <ul className="list-footer">
                  <li className="item">
                    <a href="javascript:void(0)">
                      <i className="far fa-heart" />
                    </a>{" "}
                    <span>{(channel.m_ch_like || 0).toLocaleString()}</span>
                  </li>
                  <li className="item">
                    <a href="javascript:void(0)">
                      <i className="far fa-comment-dots" />
                    </a>{" "}
                    <span>
                      {(channel.m_ch_comment || 0).toLocaleString()}
                    </span>
                  </li>
                  <li className="item">
                    <a href="javascript:void(0)">
                      <i className="fas fa-share-alt" />
                    </a>{" "}
                    <span>{(channel.m_ch_share || 0).toLocaleString()}</span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        );
      }
    });
  };

  renderTabReport = campaign => {
    const { t } = this.props;
    if (!campaign || !campaign.matchings) {
      return null;
    }
    let totalLike = 0;
    let totalShare = 0;
    let totalComment = 0;
    let totalPost = 0;
    let fime = {
      post: 0,
      like: 0,
      share: 0,
      comment: 0
    };
    let facebook = {
      post: 0,
      like: 0,
      share: 0,
      comment: 0
    };
    let youtube = {
      post: 0,
      like: 0,
      share: 0,
      comment: 0
    };
    let instagram = {
      post: 0,
      like: 0,
      share: 0,
      comment: 0
    };
    let dataFime = [];
    let dataSNS = [];
    campaign.matchings.map(matching => {
      let matching_channel = matching.matching_channel.map(e => {
        if (e.m_ch_selected === 1 && e.m_ch_url != null && e.m_ch_url != '' && e.m_ch_active_url) {
          totalLike += e.m_ch_like;
          totalShare += e.m_ch_share;
          totalComment += e.m_ch_comment;
          switch (e.sns_id) {
            case SNS.FIME: {
              fime = {
                post: fime.post + 1,
                like: fime.like + e.m_ch_like,
                share: fime.share + e.m_ch_share,
                comment: fime.comment + e.m_ch_comment
              };
              totalPost += 1;
              break;
            }
            case SNS.FACEBOOK: {
              facebook = {
                post: facebook.post + 1,
                like: facebook.like + e.m_ch_like,
                share: facebook.share + e.m_ch_share,
                comment: facebook.comment + e.m_ch_comment
              };
              totalPost += 1;
              break;
            }
            case SNS.YOUTUBE: {
              youtube = {
                post: youtube.post + 1,
                like: youtube.like + e.m_ch_like,
                share: youtube.share + e.m_ch_share,
                comment: youtube.comment + e.m_ch_comment
              };
              totalPost += 1;
              break;
            }
            case SNS.INSTAGRAM: {
              instagram = {
                post: instagram.post + 1,
                like: instagram.like + e.m_ch_like,
                share: instagram.share + e.m_ch_share,
                comment: instagram.comment + e.m_ch_comment
              };
              totalPost += 1;
              break;
            }
            default:
              break;
          }
        }
        // return e.m_ch_selected === 1 ? e.sns_id : null
      });
      //   .filter(e => e !== null)
      // matching.influencer.channels.map((channel) => {
      //   matching_channel.includes(channel.sns_id)
      // })
    });
    campaign.admin_reviews.map(e => {
      if (e.m_ch_selected === 1 && e.m_ch_url != null && e.m_ch_url != '' && e.m_ch_active_url) {
        totalLike += e.m_ch_like;
        totalShare += e.m_ch_share;
        totalComment += e.m_ch_comment;
        switch (e.sns_id) {
          case SNS.FIME: {
            fime = {
              post: fime.post + 1,
              like: fime.like + e.m_ch_like,
              share: fime.share + e.m_ch_share,
              comment: fime.comment + e.m_ch_comment
            };
            totalPost += 1;
            break;
          }
          case SNS.FACEBOOK: {
            facebook = {
              post: facebook.post + 1,
              like: facebook.like + e.m_ch_like,
              share: facebook.share + e.m_ch_share,
              comment: facebook.comment + e.m_ch_comment
            };
            totalPost += 1;
            break;
          }
          case SNS.YOUTUBE: {
            youtube = {
              post: youtube.post + 1,
              like: youtube.like + e.m_ch_like,
              share: youtube.share + e.m_ch_share,
              comment: youtube.comment + e.m_ch_comment
            };
            totalPost += 1;
            break;
          }
          case SNS.INSTAGRAM: {
            instagram = {
              post: instagram.post + 1,
              like: instagram.like + e.m_ch_like,
              share: instagram.share + e.m_ch_share,
              comment: instagram.comment + e.m_ch_comment
            };
            totalPost += 1;
            break;
          }
          default:
            break;
        }
      }
    });
    if (!(!fime.like && !fime.comment && !fime.share)) {
      dataFime = [
        { name: "Like", value: fime.like, color: "#FC3667" },
        { name: "Reply", value: fime.comment, color: "#FF9FB6" },
        { name: "Share", value: fime.share, color: "#FFDEE6" }
      ];
    }
    if (!(!facebook.like && !youtube.like && !instagram.like)) {
      dataSNS = [
        { name: "Facebook", value: facebook.like, color: "#48BAFD" },
        { name: "Youtube", value: youtube.like, color: "#7FC4FD" },
        { name: "Instagram", value: instagram.like, color: "#BCE0FD" }
      ];
    }
    return (
      <Fragment>
        <div className="matching-fistar-track">
          <div className="total-campaign">
            <div className="item-total">
              <h4 className="title-total">{this.props.t("PARTNER_INFORMATION.PNI_label_total_like")}</h4>
              <h4 className="amount-total">
                {nFormatter(totalLike)}
                {/*<span>Ma</span>*/}
              </h4>
            </div>
            <div className="item-total">
              <h4 className="title-total">{this.props.t("PARTNER_INFORMATION.PNI_label_total_share")}</h4>
              <h4 className="amount-total">{nFormatter(totalShare)}</h4>
            </div>
            <div className="item-total">
              <h4 className="title-total">{this.props.t("PARTNER_INFORMATION.PNI_label_total_comment")}</h4>
              <h4 className="amount-total">{nFormatter(totalComment)}</h4>
            </div>
          </div>
          <div className="chart-campaign">
            <div className="left-chart w-40">
              <h4 className="title-post">
                {t("SEARCH_DETAIL.SDL_posts_campaign")}
              </h4>
              <h4 className="amount-post">{totalPost}</h4>
              <div className="amount-solical">
                <div className="item-solical">
                  <div className="img">
                    <img src={imageInsta} alt="" />
                  </div>
                  <div className="amount">
                    <span className="name">{this.props.t("PARTNER_INFORMATION.PNI_post")}</span>
                    <span>{instagram.post}</span>
                  </div>
                  <div className="amount">
                    <span className="name">{this.props.t("PARTNER_INFORMATION.PNI_like")}</span>
                    <span>{nFormatter(instagram.like)}</span>
                  </div>
                  <div className="amount">
                    <span className="name">{this.props.t("PARTNER_INFORMATION.PNI_share")}</span>
                    <span>{nFormatter(instagram.share)}</span>
                  </div>
                  <div className="amount">
                    <span className="name">{this.props.t("PARTNER_INFORMATION.PNI_comments")}</span>
                    <span>{nFormatter(instagram.comment)}</span>
                  </div>
                </div>
                <div className="item-solical">
                  <div className="img">
                    <img src={imageFb} alt="" />
                  </div>
                  <div className="amount">
                    <span className="name">{this.props.t("PARTNER_INFORMATION.PNI_post")}</span>
                    <span>{facebook.post}</span>
                  </div>
                  <div className="amount">
                    <span className="name">{this.props.t("PARTNER_INFORMATION.PNI_like")}</span>
                    <span>{nFormatter(facebook.like)}</span>
                  </div>
                  <div className="amount">
                    <span className="name">{this.props.t("PARTNER_INFORMATION.PNI_share")}</span>
                    <span>{nFormatter(facebook.share)}</span>
                  </div>
                  <div className="amount">
                    <span className="name">{this.props.t("PARTNER_INFORMATION.PNI_comments")}</span>
                    <span>{nFormatter(facebook.comment)}</span>
                  </div>
                </div>
                <div className="item-solical">
                  <div className="img">
                    <img src={imageFime} alt="" />
                  </div>
                  <div className="amount">
                    <span className="name">{this.props.t("PARTNER_INFORMATION.PNI_post")}</span>
                    <span>{fime.post}</span>
                  </div>
                  <div className="amount">
                    <span className="name">{this.props.t("PARTNER_INFORMATION.PNI_like")}</span>
                    <span>{nFormatter(fime.like)}</span>
                  </div>
                  <div className="amount">
                    <span className="name">{this.props.t("PARTNER_INFORMATION.PNI_share")}</span>
                    <span>{nFormatter(fime.share)}</span>
                  </div>
                  <div className="amount">
                    <span className="name">{this.props.t("PARTNER_INFORMATION.PNI_comments")}</span>
                    <span>{nFormatter(fime.comment)}</span>
                  </div>
                </div>
                <div className="item-solical">
                  <div className="img">
                    <img src={imageYt} alt="" />
                  </div>
                  <div className="amount">
                    <span className="name">{this.props.t("PARTNER_INFORMATION.PNI_post")}</span>
                    <span>{youtube.post}</span>
                  </div>
                  <div className="amount">
                    <span className="name">{this.props.t("PARTNER_INFORMATION.PNI_like")}</span>
                    <span>{nFormatter(youtube.like)}</span>
                  </div>
                  <div className="amount">
                    <span className="name">{this.props.t("PARTNER_INFORMATION.PNI_share")}</span>
                    <span>{nFormatter(youtube.share)}</span>
                  </div>
                  <div className="amount">
                    <span className="name">{this.props.t("PARTNER_INFORMATION.PNI_comments")}</span>
                    <span>{nFormatter(youtube.comment)}</span>
                  </div>
                </div>
              </div>
            </div>
            <div className="right-chart w-60">
              <div className="chart">
                <div className="item-chart text-center">
                  <h4 className="title-chart">
                    {t("SEARCH_DETAIL.SDL_fi_me_engagement")}
                  </h4>
                  <div>
                    {dataFime.length > 0 ? (
                      <PieChart data={dataFime} />
                    ) : (
                      <div className="chart-empty">
                        {this.props.t("SEARCH_DETAIL.SDL_data_chart")}
                      </div>
                    )}
                  </div>
                </div>
                <div className="item-chart text-center">
                  <h4 className="title-chart">
                    {t("SEARCH_DETAIL.SDL_sns_channel")}
                  </h4>
                  <div>
                    {dataSNS.length > 0 ? (
                      <PieChart data={dataSNS} />
                    ) : (
                      <div className="chart-empty">
                        {this.props.t("SEARCH_DETAIL.SDL_data_chart")}
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="group-card-campaign">
            {this.renderReviewDMCSNS(campaign)}
            {this.renderReviewSNS(campaign)}
          </div>
        </div>
      </Fragment>
    );
  };

  renderTabFistars = () => {
    return (
      <Fragment>
        <div className="matching-fistar-track">
          <Tab.Container id="tabs-fistar" defaultActiveKey={"tabMatching"}>
            <div className="tab">
              <Nav variant="pills" className="list-tab">
                <Nav.Item className="tab-item">
                  <Nav.Link className="tab-link" eventKey="tabMatching">
                    {this.props.t("PARTNER_INFORMATION.PNI_tab_matching")}
                  </Nav.Link>
                </Nav.Item>
                {this.state.campaign.cp_status !== CAMPAIGN_STATUS.MATCHING && (
                  <Nav.Item className="tab-item">
                    <Nav.Link className="tab-link" eventKey="tabReview">
                      {this.props.t("PARTNER_INFORMATION.PNI_tab_review")}
                    </Nav.Link>
                  </Nav.Item>
                )}
                {[CAMPAIGN_STATUS.ONGOING, CAMPAIGN_STATUS.CLOSED].includes(
                  this.state.campaign.cp_status
                ) && (
                  <Nav.Item className="tab-item">
                    <Nav.Link className="tab-link" eventKey="tabReport">
                        {this.props.t("PARTNER_INFORMATION.PNI_tab_report")}
                    </Nav.Link>
                  </Nav.Item>
                )}
              </Nav>
            </div>
            <Tab.Content>
              <Tab.Pane eventKey="tabMatching">
                {this.renderTabMatching()}
              </Tab.Pane>
              {this.state.campaign.cp_status !== CAMPAIGN_STATUS.MATCHING && (
                <Tab.Pane eventKey="tabReview">
                  {this.renderTabReview(this.state.campaign)}
                </Tab.Pane>
              )}
              {[CAMPAIGN_STATUS.ONGOING, CAMPAIGN_STATUS.CLOSED].includes(
                this.state.campaign.cp_status
              ) && (
                <Tab.Pane eventKey="tabReport">
                  {this.renderTabReport(this.state.campaign)}
                </Tab.Pane>
              )}
            </Tab.Content>
          </Tab.Container>
        </div>
      </Fragment>
    );
  };

  renderGeneral = () => {
    const { campaign } = this.state;
    let status = "";
    switch (campaign.cp_status) {
      case 59: {
        status = "Matching";
        break;
      }
      case 60: {
        status = "Ready";
        break;
      }
      case 61: {
        status = "On-Going";
        break;
      }
      case 62: {
        status = "Closed";
        break;
      }
      default: {
        break;
      }
    }
    let fistarCount = campaign.matchings ? campaign.matchings.length : 0;
    // if (campaign.mcatching) {
    //   fistarCount += campaign.v_matching_status.length;
    // }
    // if (campaign.v_review_status) {
    //   fistarCount += campaign.v_review_status.length;
    // }

    let attachments = (campaign.attachments || [])
      .filter(e => e != null)
      .map(e => ({
        url: getImageLink(
          e.cp_attachment_url,
          IMAGE_TYPE.ATTACHMENTS,
          IMAGE_SIZE.ORIGINAL
        ),
        thumb: getImageLink(
          e.cp_attachment_url,
          IMAGE_TYPE.ATTACHMENTS,
          IMAGE_SIZE.THUMBNAIL
        ),
        type: e.cp_attachment_type
      }));
    let previewAttachments = [];
    if (!!campaign) {
      previewAttachments = [
        {
          url: getImageLink(
            campaign.cp_main_image,
            IMAGE_TYPE.CAMPAIGNS,
            campaign.cp_main_image_type == 3
              ? IMAGE_TYPE.ORIGINAL
              : IMAGE_SIZE.ORIGINAL
          ),
          thumb:
            campaign.cp_main_image_type == 3
              ? getImageLink(campaign.cp_video_thumb, IMAGE_TYPE.CAMPAIGNS)
              : getImageLink(
                  campaign.cp_main_image,
                  IMAGE_TYPE.CAMPAIGNS,
                  IMAGE_SIZE.THUMBNAIL
                ),
          type: campaign.cp_main_image_type
        },
        ...attachments
      ];
    }

    const settings = {
      customPaging: i => {
        let preview = "";
        switch (+previewAttachments[i].type) {
          case 1: {
            preview = <img src={previewAttachments[i].thumb} />;
            break;
          }
          case 2: {
            preview = (
              <img
                src={
                  "//img.youtube.com/vi/" +
                  getId(previewAttachments[i].thumb) +
                  "/0.jpg"
                }
              />
            );
            break;
          }
          case 3: {
            preview = <img src={previewAttachments[i].thumb} />;
            break;
          }
          default:
            break;
        }
        return <a>{preview}</a>;
      },
      dots: true,
      dotsClass: "slick-dots slick-thumb",
      infinite: true,
      // lazyLoad: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      adaptiveHeight: true
    };
    const { t } = this.props;

    return (
      <div className="wrapper-information">
        <div className="left">
          <div className="photo-main">
            <Slider {...settings}>
              {previewAttachments.map((attachment, key) => {
                let preview = "";
                switch (+attachment.type) {
                  case 1: {
                    preview = <img src={attachment.url} className="w-100" />;
                    break;
                  }
                  case 2: {
                    preview = (
                      <div className="text-center">
                        <iframe
                          id="myIframe"
                          width="500"
                          className="react-player"
                          height="420"
                          frameBorder="0"
                          src={
                            attachment.url
                              ? "//www.youtube.com/embed/" +
                                getId(attachment.url)
                              : "//www.youtube.com/embed/"
                          }
                          allowFullScreen
                        />
                      </div>
                    );
                    break;
                  }
                  case 3: {
                    preview = !this.state.reloadVideo ? (
                      <div className="text-center">
                        <video
                          ref={this.previewVideo}
                          className="w-100"
                          controls
                        >
                          <source
                            ref={this.previewVideoSource}
                            src={attachment.url}
                            type="video/mp4"
                          />
                          Your browser does not support HTML5 video.
                        </video>
                      </div>
                    ) : null;
                    break;
                  }
                  default:
                    break;
                }
                return <div key={key}>{preview}</div>;
              })}
            </Slider>
          </div>
        </div>
        <div className="right">
          <div className="top">
            <div className="name">
              <span className="wrap-matching">{status}</span>
              <h4>{campaign.cp_name}</h4>
            </div>
            <div className="date-open">
              <span>{t("PARTNER_INFORMATION.PNI_open")}</span>
              <p>

                  {(campaign.cp_period_start || "").split(" ")[0]}
              </p>
            </div>
          </div>
          <div
            className="text custom-description"
            dangerouslySetInnerHTML={{ __html: campaign.cp_description }}
          />
          <div className="seleted">
            {(campaign.keywords || []).map((keyword, key) => (
              <a className="mb-2" key={key}>
                {keyword.code.cd_label}
              </a>
            ))}
          </div>
          <div className="information-review">
            <div className="item-review">
              <div className="key-name">
                <p className="key">{t("PARTNER_INFORMATION.PNI_category")}</p>
                <p className="name">
                  {campaign.catalog ? campaign.catalog.CODE_NM : ""}
                </p>
              </div>
              <div className="key-name">
                <p className="key">{t("PARTNER_INFORMATION.PNI_tryfree")}</p>
                <p className="name">{campaign.cp_total_free}</p>
              </div>
              <div className="key-name">
                <p className="key">{t("PARTNER_INFORMATION.PNI_fistar")}</p>
                <p className="name red">{campaign.cp_total_influencer}</p>
              </div>
              <div className="key-name">
                <p className="key">
                  {t("PARTNER_INFORMATION.PNI_fistar_period")}
                </p>
                <p className="name pl-2 pr-2 period-date">
                  {/*{campaign.cp_period_start*/}
                  {/*? campaign.cp_period_start.split(" ")[0]*/}
                  {/*: ""}{" "}*/}
                  {/*~{" "}*/}
                  {/*{campaign.cp_period_end*/}
                  {/*? campaign.cp_period_end.split(" ")[0]*/}
                  {/*: ""}*/}

                    {campaign.cp_status == 59 ? (
                    <span className="item-period">
                      <label>
                        {(campaign.cp_period_start || "")}{" "}
                      </label>
                      <label>
                        {(campaign.cp_period_end || "")}
                      </label>{" "}
                    </span>
                  ) : campaign.cp_status == 62 ? (
                    "Complete"
                  ) : (
                    "Going"
                  )}
                </p>
              </div>
              <div className="key-name w-100">
                <p className="key key-20">
                  {t("PARTNER_INFORMATION.PNI_price_product")}
                </p>
                <p className="name">
                  <span className={campaign.cp_campaign_price ? "price" : ""}>
                    {campaign.cp_product_price
                      ? (+campaign.cp_product_price).toLocaleString()
                      : 0}{" "}
                    VND{" "}
                  </span>
                  {/*-*/}
                  <span className="pl-3">
                    {campaign.cp_campaign_price
                      ? (+campaign.cp_campaign_price).toLocaleString()
                      : ""}{" "}
                    {campaign.cp_campaign_price ? "VND" : ""}{" "}
                  </span>
                </p>
              </div>
            </div>
            {this.renderSchedule(campaign)}
          </div>
          {this.props.auth.id === campaign.p_id &&
            campaign.cp_status === CAMPAIGN_STATUS.MATCHING && (
              <button
                className="modify"
                type="button"
                onClick={() => this.onClickModify(campaign)}
              >
                {t("MENU_LEFT_PARTNER.MLP_button_modify")}
              </button>
            )}
        </div>
      </div>
    );
  };

 renderSchedule(campaign){
    let activeSchedule = null;
    let createDate = moment(campaign.created_at).format('YYYY-MM-DD');
    let startDate = moment(campaign.cp_period_start).format('YYYY-MM-DD');
    let threeDateAfter = moment(startDate).add(3, 'day').format('YYYY-MM-DD');
    let endDate = moment(campaign.cp_period_end).format('YYYY-MM-DD');
    let schedule = [
      ['Matching',createDate, startDate],
      ['Ready',startDate, threeDateAfter],
      ['Ongoing',threeDateAfter, endDate],
      ['Closed',endDate, null]];
    if(campaign.cp_status == 59 ) { activeSchedule = 0 };
    if(campaign.cp_status == 60 ) { activeSchedule = 1 };
    if(campaign.cp_status == 61 ) { activeSchedule = 2 };
    if(campaign.cp_status == 62  ) { activeSchedule = 3 };
    return (
      <table className="schedule">
        <td className="first_col" valign="middle">Schedule</td>
        <td className="last_col">
          {schedule.map((sche, index) => {
              if (activeSchedule == index) {
                return( 
                <tr className="activeSchedule">
                    <td className="name_col">{sche[0]}</td>
                    <td className="sche_col">{sche[1]} ~ {sche[2]}</td>
                </tr> );
              } else {
                return(  
                <tr className="nor_schedule">
                    <td className="name_col">{sche[0]}</td>
                    <td className="sche_col">{sche[1]} ~ {sche[2]}</td>
                </tr> 
                );
              }
            })
          }
        </td>
      </table>   
    )
  }

  render() {
    const { campaign, isLoadingCampaign, isBack, fistar, channel } = this.state;
    const { t } = this.props;
    const loading = (
      <div className="text-center mt-5" role="status">
        <div className="spinner-border" role="status">
          <span className="sr-only">{t("LOADING.LOADING")}</span>
        </div>
      </div>
    );

    if (isBack) {
      return <Redirect to={routeName.PARTNER_CAMPAIGN_TRACKING} />;
    }

    return (
      <Fragment>
        <div
          className="content campaign-tracking-campaign-01"
          style={{ display: this.state.isSearchFiStar ? "none" : "block" }}
        >
          <div className="container-campaign-tracking">
            <div className="top-camppaign-tracking">
              <div className="left">
                <h4>{t("PARTNER_INFORMATION.PNI_cp_information")}</h4>
              </div>
              <div className="right">
                <button type="button" onClick={this.onClickBack}>
                  <img src={backImage} alt="Back" />{" "}
                  {t("PARTNER_INFORMATION.PNI_button_list")}
                </button>
              </div>
            </div>
            {process.env.REACT_APP_NODE_ENV === "development" && (
              <CampaignStatusAction id={campaign.cp_id} />
            )}
            {isLoadingCampaign ? (
                <div id="loading"></div>
            ) : (
              <div className="content-tracking w-100">
                {this.renderGeneral()}
                {this.renderTabFistars()}
              </div>
            )}
          </div>
        </div>
        {this.state.show && (
          <ModalReviewSNS
            show={this.state.show}
            onCloseModal={this.onCloseModal}
            resetData={this.resetData}
            campaign={campaign}
            fistar={fistar}
            channel={channel}
          />
        )}
        {this.state.showModify && (
          <ModalModifyCampaign
            show={this.state.showModify}
            onCloseModal={this.onCloseModalModify}
            reloadData={this.resetData}
            campaign={campaign}
          />
        )}
        {this.state.isSearchFiStar && (
          <SearchFiStar closeModal={this.closeModalSearchFistar} />
        )}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};
const mapDispatchToProps = dispatch => {
  return {
    getCampaign: id => dispatch(getCampaignDetailAction(id)),
    getFistarOfCampaign: (id, type) => dispatch(getFistarOfCampaignDetailAction(id, type)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(CampaignTrackingDetail));
