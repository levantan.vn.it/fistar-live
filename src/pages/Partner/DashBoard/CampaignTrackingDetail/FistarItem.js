import React, { Component, Fragment } from "react";
import ReactDOM from "react-dom";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect, Route } from "react-router-dom";
import { connect } from "react-redux";
import { getCampaignDetailAction } from "./../../../../store/actions/campaign";
import { Tab, Nav } from "react-bootstrap";
import "./CampaignTrackingDetail.css";
import backImage from "./../../../../images/left.svg";
import imageFime from "./../../../../images/mk.png";
import imageFb from "./../../../../images/face.png";
import imageInsta from "./../../../../images/intar.png";
import imageYt from "./../../../../images/toutube.png";
import imageIdol from "./../../../../images/Layer 6.png";
import * as routeName from "./../../../../routes/routeName";
import { STATUS_STEP_PARTNER } from "./../../../../constants";
import {
  nextStepAction,
  clickSelectSNSAction
} from "./../../../../store/actions/campaign";
import ModalHistory from "./ModalHistory";
import { getImageLink } from "./../../../../common/helper";
import {
  IMAGE_SIZE,
  IMAGE_TYPE,
  PARTNER_FINISH_STEP,
  CAMPAIGN_STATUS
} from "./../../../../constants/index";
import Scrap from "./../../../../components/Scrap";

// import {AmCharts} from "@amcharts/amcharts3-react";

const TYPE = {
  ALL: "",
  MATCHING: "Matching",
  READY: "Ready",
  ONGOING: "ongoing",
  CLOSED: "Closed"
};

const MATCHING_STATUS = {
  MARCHED: [8, 16],
  RECOMMENDEDED: [],
  CONSIDERING: [1, 9],
  REJECTED: [3, 11, 13],
  CANCEL: [2, 5, 7, 10, 15]
};

const SNS = {
  FIME: 1,
  FACEBOOK: 2,
  YOUTUBE: 3,
  INSTAGRAM: 4
};

class FistarItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      step: null,
      loading: false,
      show: false,
      redirectSearchFistar: false
    };
  }

  onClickNextStep = (m_id, stt_id) => {
    if (!this.state.loading) {
      this.setState(
        {
          loading: true
        },
        () => {
          this.props
            .nextStep(m_id, stt_id)
            .then(response => {
              this.setState({
                step: stt_id,
                loading: false
              });
              this.props.reloadData();
            })
            .catch(er => {
              this.setState({
                loading: false
              });
            });
        }
      );
    }
  };

  showHistory = uid => {
    this.setState({
      show: true,
      uid: uid
    });
  };

  handleClose = () => {
    this.setState({ show: false });
  };
  onCloseModal = () => {
    this.setState({ show: !this.state.show });
  };

  clickSelectSNS = (m_id, sns_id) => {
    const { pid, auth, cp_status } = this.props;
    if (pid == auth.id) {
      if (cp_status == 59 || cp_status == 60 || cp_status == 61) {
        return this.props.clickSelectSNS(m_id, sns_id).then(() => {
          this.props.reloadData();
        });
      } else {
        return null;
      }
    } else {
      return null;
    }
  };

  callbackScrap = () => {
    this.props.callbackScrap()
  }

  renderButtonNextStep = (m_id, step) => {
    let nextSteps = STATUS_STEP_PARTNER[step];

    return (
      <Fragment>
        {nextSteps.button.map((nextStep, key) => (
          <button
            key={key}
            type={"button"}
            onClick={() => this.onClickNextStep(m_id, nextStep.id)}
          >
            {this.state.loading ? (
              <div className="spinner-border" role="status">
                {/*<span className="sr-only">Loading...</span>*/}
              </div>
            ) : (
              <Fragment>{nextStep.text}</Fragment>
            )}
          </button>
        ))}
      </Fragment>
    );
  };

  renderFistarInfo = influencer => {
    let snsFime = {};
    let snsFB = {};
    let snsIG = {};
    let snsYT = {};
    (influencer.channels || []).map(channel => {
      let sns = {
        follow:
          channel.usn_comment +
          channel.usn_follower +
          channel.usn_like +
          channel.usn_share +
          channel.usn_view
      };
      if (channel.sns_id === SNS.FIME) {
        snsFime = sns;
      } else if (channel.sns_id === SNS.FACEBOOK) {
        snsFB = sns;
      } else if (channel.sns_id === SNS.YOUTUBE) {
        snsYT = sns;
      } else if (channel.sns_id === SNS.INSTAGRAM) {
        snsIG = sns;
      }
    });

    return (
      <Fragment>
        <div className="image-box h-100">
          <img
            src={getImageLink(
              influencer.picture,
              IMAGE_TYPE.FISTARS,
              IMAGE_SIZE.ORIGINAL
            )}
            alt={influencer.fullname}
          />
          <Scrap
            scraps_count={influencer.scrap_total}
            is_scrap={influencer.is_scrap}
            id={influencer.uid}
            callback={this.callbackScrap}
          />
        </div>
        <div className="information-user">
          <div className="name">
            <h4>
              <i className="fas fa-mars" />
              {influencer.fullname}
            </h4>
            <div className="address-old">
              <span>
                {influencer && influencer.dob
                  ? influencer.dob.split("-")[0]
                  : ""}
              </span>
              <span className="address">{influencer.location_label}</span>
            </div>
          </div>
          <div className="solical-flow">
            <div className="item">
              <a href="">
                <i className="fab fa-facebook-f" />
              </a>
              <span>{snsFB.follow ? snsFB.follow.toLocaleString() : 0}</span>
            </div>
            <div className="item">
              <a href="">
                <i className="fab fa-instagram" />
              </a>
              <span>{snsIG.follow ? snsIG.follow.toLocaleString() : 0}</span>
            </div>
            <div className="item">
              <a href="">
                <i className="fab fa-youtube-square" />
              </a>
              <span>{snsYT.follow ? snsYT.follow.toLocaleString() : 0}</span>
            </div>
          </div>
        </div>
      </Fragment>
    );
  };

  redirectSearchFistar = () => {
    this.props.clickisSearchFiStar();
  };

  render() {
    const { fistar, cp_status, cp_name, pid, auth } = this.props;
    const { step, redirectSearchFistar } = this.state;
      // return null
    if (redirectSearchFistar) {
      return <Redirect to={routeName.PARTNER_SEARCH_FISTAR} />;
    }
    if (!fistar) {
      return null
    }
    let lastMatching = null
    let status = "";
    if (fistar.fistar_campaign && fistar.fistar_campaign.length > 0) {
      lastMatching = fistar.fistar_campaign[fistar.fistar_campaign.length - 1]
      switch (lastMatching.cp_status) {
        case 59: {
          status = `${this.props.t("DASHBOARD_FISTAR.DFR_status_matching")}`;
          break;
        }
        case 60: {
          status = `${this.props.t("DASHBOARD_FISTAR.DFR_status_ready")}`;
          break;
        }
        case 61: {
          status = `${this.props.t("DASHBOARD_FISTAR.DFR_status_ongoing")}`;
          break;
        }
        case 62: {
          status = `${this.props.t("DASHBOARD_FISTAR.DFR_status_close")}`;
          break;
        }
        default: {
          break;
        }
      }
    }
    let stt_id = null
    let snsFime = {}
    let snsFB = {}
    let snsIG = {}
    let snsYT = {}
    let partner_status = null
    let matching_channel = []
    if (lastMatching) {
      stt_id = lastMatching.m_status
      partner_status = lastMatching.partner_status;
      matching_channel = lastMatching.matching_channel
    }
    // let {
    //   m_status: stt_id,
    //   label: { partner_status }
    // } = matching_status;
    (fistar.channels || []).map(channel => {
      let sns = {
        sns_id: "",
        m_ch_id: "",
        cost: +channel.cost,
        selected: false
      };
      if (channel.sns_id === SNS.FIME) {
        sns.sns_id = SNS.FIME;
        matching_channel.map(matching => {
          if (matching.sns_id === SNS.FIME) {
            sns.m_ch_id = matching.m_ch_id;
            sns.selected = matching.m_ch_selected;
          }
        });
        snsFime = sns;
      } else if (channel.sns_id === SNS.FACEBOOK) {
        sns.sns_id = SNS.FACEBOOK;
        matching_channel.map(matching => {
          if (matching.sns_id === SNS.FACEBOOK) {
            sns.m_ch_id = matching.m_ch_id;
            sns.selected = matching.m_ch_selected;
          }
        });
        snsFB = sns;
      } else if (channel.sns_id === SNS.YOUTUBE) {
        sns.sns_id = SNS.YOUTUBE;
        matching_channel.map(matching => {
          if (matching.sns_id === SNS.YOUTUBE) {
            sns.m_ch_id = matching.m_ch_id;
            sns.selected = matching.m_ch_selected;
          }
        });
        snsYT = sns;
      } else if (channel.sns_id === SNS.INSTAGRAM) {
        sns.sns_id = SNS.INSTAGRAM;
        matching_channel.map(matching => {
          if (matching.sns_id === SNS.INSTAGRAM) {
            sns.m_ch_id = matching.m_ch_id;
            sns.selected = matching.m_ch_selected;
          }
        });
        snsIG = sns;
      }
    });
    if (step && lastMatching) {
      partner_status = STATUS_STEP_PARTNER[step].display;
      stt_id = step;
    }

    return (
      <Fragment>
        <div className="box-item">
          <div className="left">{this.renderFistarInfo(fistar)}</div>
          <div className="suggested-price">
            {lastMatching && 
              <Fragment>
                <div className="top-matching">
                  <p>{status}</p>
                  {/*<h4>{cp_name}</h4>*/}
                </div>
                <div className="status-matching">
                  <span className="icon-start" />
                  {partner_status.split("→").map((status, key) => {
                    if (partner_status.split("→").length === key + 1) {
                      return (
                        <Fragment key={key}>
                          <p className="font-weight-bold step-partner-active">
                            {status}
                          </p>
                        </Fragment>
                      );
                    }
                    return (
                      <Fragment key={key}>
                        <p className="reject">{status}</p>
                        {partner_status.split("→").length - 1 !== key && <span />}
                      </Fragment>
                    );
                  })}
                  {!PARTNER_FINISH_STEP.includes(stt_id) && cp_status !== CAMPAIGN_STATUS.CLOSED && (
                    <i className="fas fa-long-arrow-alt-right" />
                  )}
                  {pid == auth.id && cp_status !== CAMPAIGN_STATUS.CLOSED
                    ? this.renderButtonNextStep(lastMatching.m_id, stt_id)
                    : ""}

                  {pid == auth.id && stt_id == 11 && cp_status !== CAMPAIGN_STATUS.CLOSED ? (
                    <button type="button" onClick={this.redirectSearchFistar}>
                      Search fiStar
                    </button>
                  ) : (
                    ""
                  )}
                </div>
              </Fragment>
            }
            <div className="price">
              <div
                className={`item cursor-pointer${snsFime.selected ? "" : " no-results"}`}
                onClick={() => lastMatching ? this.clickSelectSNS(lastMatching.m_id, snsFime.sns_id) : ''}
              >
                <img
                  src={imageFime}
                  alt="Fime"
                  title={snsFime.selected ? "selected" : "unselect"}
                />
                <p>
                  {snsFime.selected ? (
                    <Fragment>
                      {snsFime.cost.toLocaleString()} <span>VND</span>
                    </Fragment>
                  ) : (
                    "-"
                  )}
                </p>
              </div>
              <div
                className={`item cursor-pointer${snsFB.selected ? "" : " no-results"}`}
                onClick={() => lastMatching ? this.clickSelectSNS(lastMatching.m_id, snsFB.sns_id) : ''}
              >
                <img
                  src={imageFb}
                  alt="Facebook"
                  title={snsFB.selected ? "selected" : "unselect"}
                />
                <p>
                  {snsFB.cost && snsFB.selected ? (
                    <Fragment>
                      {snsFB.cost.toLocaleString()} <span>VND</span>
                    </Fragment>
                  ) : (
                    "-"
                  )}
                </p>
              </div>
              <div
                className={`item cursor-pointer${snsIG.selected ? "" : " no-results"}`}
                onClick={() => lastMatching ? this.clickSelectSNS(lastMatching.m_id, snsIG.sns_id) : ''}
              >
                <img
                  src={imageInsta}
                  alt="Instagram"
                  title={snsIG.selected ? "selected" : "unselect"}
                />
                <p>
                  {snsIG.cost && snsIG.selected ? (
                    <Fragment>
                      {snsIG.cost.toLocaleString()} <span>VND</span>
                    </Fragment>
                  ) : (
                    "-"
                  )}
                </p>
              </div>
              <div
                className={`item cursor-pointer${snsYT.selected ? "" : " no-results"}`}
                onClick={() => lastMatching ? this.clickSelectSNS(lastMatching.m_id, snsYT.sns_id) : ''}
              >
                <img
                  src={imageYt}
                  alt="Youtube"
                  title={snsYT.selected ? "selected" : "unselect"}
                />
                <p>
                  {snsYT.cost && snsYT.selected ? (
                    <Fragment>
                      {snsYT.cost.toLocaleString()} <span>VND</span>
                    </Fragment>
                  ) : (
                    "-"
                  )}
                </p>
              </div>
            </div>
          </div>
          <div
            className="history-information"
            onClick={() => this.showHistory(fistar.uid)}
          >
            <div className="text">
              <p>History</p>
              <h4>{fistar.history_from_partner}</h4>
            </div>
          </div>
        </div>
        {this.state.show &&
          this.state.uid && (
            <ModalHistory
              show={this.state.show}
              onCloseModal={this.onCloseModal}
              uid={this.state.uid}
            />
          )}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};
const mapDispatchToProps = dispatch => {
  return {
    nextStep: (m_id, stt_id) => dispatch(nextStepAction(m_id, stt_id)),
    clickSelectSNS: (m_id, sns_id) =>
      dispatch(clickSelectSNSAction(m_id, sns_id))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(FistarItem));
