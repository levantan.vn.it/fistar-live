import React, { Component, Fragment } from "react";
import ReactDOM from "react-dom";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect, Route } from "react-router-dom";
import { connect } from "react-redux";

// import calendar style
// You can customize style by copying asset folder.
import "@y0c/react-datepicker/assets/styles/calendar.scss";
import { format } from "date-fns";
// import PushMessageComponent from './../../../components/PushMessage'

import CampaignTracking from "./../../../components/partner/Dashboard/Partial/campaignTracking";
import MyFistar from "./../../../components/partner/Dashboard/Partial/myFistar";
import PartnerDashboardRight from "./../../../components/partner/Dashboard/Partial/Right";
import "./dashboard.scss";
import {
  getCountCampaignAction,
  getTotalStatusAction
} from "./../../../store/actions/campaign";
import SearchFiStar from "./CreateCampain/search";

const COMPONENT_HEIGHT = "129vh";

class DashboardPartner extends Component {
  constructor(props) {
    super(props);
    this.state = {
      height: COMPONENT_HEIGHT,
      count: {},
      isSearchFiStar: false
    };
  }

  componentDidMount() {
    if (typeof window !== 'undefined' && window.document && window.document.createElement) {
      window.scrollTo(0, 0);
    }
    this.props.getCountCampaign().then(response => {
      this.setState({
        count: {
          ...this.state.count,
          closed: response.closed,
          matched: response.matched,
          matching: response.matching,
          ongoing: response.ongoing,
          ready: response.ready
        }
      });
    });
    const { auth } = this.props;
    if (auth) {
      this.props.totalStatus(auth.id).then(response => {
        this.setState({
          count: {
            ...this.state.count,
            fistar_total: response.all,
            fistar_scrap: response.scrap,
            fistar_apply: response.apply,
            fistar_matching: response.matching,
            fistar_recommended: response.recommended,
            fistar_request: response.request,
          }
        });
      });
    }
  }

  callback = () => {
    const { auth } = this.props;
    this.props.totalStatus(auth.id).then(response => {
      this.setState({
        count: {
          ...this.state.count,
          fistar_scrap: response.scrap,
          fistar_apply: response.apply,
          fistar_matching: response.matching,
          fistar_recommended: response.recommended,
          fistar_request: response.request,
        }
      });
    });
  };

  clickisSearchFiStar = () => {
    this.setState({ isSearchFiStar: true });
  };

  closeModalSearchFistar = () => {
    this.setState({ isSearchFiStar: false });
  };

  render() {
    const { t } = this.props;
    const { height, count } = this.state;

    return (
      <Fragment>
        <div
          className="content fistar-dashbord"
          style={{ display: this.state.isSearchFiStar ? "none" : "flex" }}
        >
          <div className="campaign-track">
            <CampaignTracking height={height} count={count} />
          </div>
          <div className="my-fistar">
            <MyFistar
              height={height}
              count={count}
              clickisSearchFiStar={this.clickisSearchFiStar}
            />
          </div>
          <div className="q-a-fistar">
            <PartnerDashboardRight callback={this.callback} />
          </div>
        </div>

        {this.state.isSearchFiStar && (
          <SearchFiStar closeModal={this.closeModalSearchFistar} />
        )}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getCountCampaign: () => dispatch(getCountCampaignAction()),
    totalStatus: pid => dispatch(getTotalStatusAction(pid))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(DashboardPartner));
