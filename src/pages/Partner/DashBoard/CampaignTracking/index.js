import React, { Component, Fragment } from "react";
import ReactDOM from "react-dom";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect, Route } from "react-router-dom";
import { connect } from "react-redux";
import {
  getCampaignAction,
  getCountCampaignAction
} from "./../../../../store/actions/campaign";
import InfiniteScroll from "react-infinite-scroll-component";
import { CAMPAIGN_STATUS, TYPE, SNS_CHANNEL } from "./../../../../constants";
import * as routeName from "./../../../../routes/routeName";
import imageFB from "./../../../../images/face.png";
import imageIG from "./../../../../images/intar.png";
import imageFime from "./../../../../images/mk.png";
import imageYT from "./../../../../images/toutube.png";
import { getImageLink } from "./../../../../common/helper";
import { IMAGE_SIZE, IMAGE_TYPE } from "./../../../../constants/index";
import CampaignMainImage from "./../../../../components/CampaignMainImage";
import "./campaignTracking.scss";

const MATCHING = {
  REQUEST: [1, 2, 3, 4, 5, 6, 7, 8],
  APPLY: [9, 10, 11, 12, 13, 14, 15, 16]
};

const ListFimes = ({ fimes }) => (
  <Fragment>
    <div className="user-fime">
      {fimes[0] && (
        <div className="thumnail-user">
          <a href="javascript:viod(0)">
            <img
              src={fimes[0].profile ? process.env.REACT_APP_FIME_URL + fimes[0].profile.PIC : ''}
              alt={fimes[0].profile ? fimes[0].profile.REG_NAME : ''}
            />
          </a>
        </div>
      )}
      {fimes[1] && (
        <div className="thumnail-user">
          <a href="javascript:viod(0)">
            <img
              src={fimes[1].profile ? process.env.REACT_APP_FIME_URL + fimes[1].profile.PIC : ''}
              alt={fimes[1].profile ? fimes[1].profile.REG_NAME : ''}
            />
          </a>
        </div>
      )}
      {fimes.length > 2 && (
        <div className="thumnail-user">
          <a href="javascript:viod(0)">
            <div className="all-user">
              <p>+{fimes.length - 2}</p>
            </div>
          </a>
        </div>
      )}
    </div>
  </Fragment>
);

const ListFistars = ({ fistars }) => (
  <Fragment>
    <div className="user-fime">
      {fistars[0] && (
        <div className="thumnail-user">
          <a href="javascript:viod(0)">
            <img
              src={getImageLink(
                fistars[0].picture,
                IMAGE_TYPE.FISTARS,
                IMAGE_SIZE.THUMBNAIL
              )}
              alt={fistars[0].fullname}
            />
          </a>
        </div>
      )}
      {fistars[1] && (
        <div className="thumnail-user">
          <a href="javascript:viod(0)">
            <img
              src={getImageLink(
                fistars[1].picture,
                IMAGE_TYPE.FISTARS,
                IMAGE_SIZE.THUMBNAIL
              )}
              alt={fistars[1].fullname}
            />
          </a>
        </div>
      )}
      {fistars.length > 2 && (
        <div className="thumnail-user">
          <a href="javascript:viod(0)">
            <div className="all-user">
              <p>+{fistars.length - 2}</p>
            </div>
          </a>
        </div>
      )}
    </div>
  </Fragment>
);

const Campaign = props => {
  const { campaign, t } = props;
  let status = "";
  let request = [];
  let apply = [];
  let fimes = [];
  let moneyFime = 0;
  let moneyFacebook = 0;
  let moneyInstagram = 0;
  let moneyYoutube = 0;
  let moneyTotal = 0;

  let fimeSelected = {};
  let facebookSelected = {};
  let youtubeSelected = {};
  let instagramSelected = {};
  switch (campaign.cp_status) {
    case CAMPAIGN_STATUS.MATCHING: {
      status = "Matching";
      campaign.matchings.map(status => {
        // fistar
        fimes.push(status.influencer);
        if (MATCHING.REQUEST.includes(status.matching_status.label.stt_id)) {
          request.push(status.influencer);
        }
        if (MATCHING.APPLY.includes(status.matching_status.label.stt_id)) {
          apply.push(status.influencer);
        }
        status.matching_channel.map(channelMatching => {
          switch (channelMatching.sns_id) {
            case SNS_CHANNEL.FIME: {
              fimeSelected.m_ch_selected = channelMatching.m_ch_selected;
              fimeSelected.sns_id = channelMatching.sns_id;
              break;
            }
            case SNS_CHANNEL.FACEBOOK: {
              facebookSelected.m_ch_selected = channelMatching.m_ch_selected;
              facebookSelected.sns_id = channelMatching.sns_id;
              break;
            }
            case SNS_CHANNEL.YOUTUBE: {
              youtubeSelected.m_ch_selected = channelMatching.m_ch_selected;
              youtubeSelected.sns_id = channelMatching.sns_id;
              break;
            }
            case SNS_CHANNEL.INSTAGRAM: {
              instagramSelected.m_ch_selected = channelMatching.m_ch_selected;
              instagramSelected.sns_id = channelMatching.sns_id;
              break;
            }
          }
        });

        // money sns channel
        status.influencer.channels.map(channel => {
          switch (channel.sns_id) {
            case SNS_CHANNEL.FIME: {
              if (
                fimeSelected.sns_id == SNS_CHANNEL.FIME &&
                fimeSelected.m_ch_selected == 1
              ) {
                moneyFime += +channel.cost;
              } else {
                moneyFime += 0;
              }

              break;
            }
            case SNS_CHANNEL.FACEBOOK: {
              if (
                facebookSelected.sns_id == SNS_CHANNEL.FACEBOOK &&
                facebookSelected.m_ch_selected == 1
              ) {
                moneyFacebook += +channel.cost;
              } else {
                moneyFacebook += 0;
              }
              break;
            }
            case SNS_CHANNEL.YOUTUBE: {
              if (
                youtubeSelected.sns_id == SNS_CHANNEL.YOUTUBE &&
                youtubeSelected.m_ch_selected == 1
              ) {
                moneyYoutube += +channel.cost;
              } else {
                moneyYoutube += 0;
              }
              break;
            }
            case SNS_CHANNEL.INSTAGRAM: {
              if (
                instagramSelected.sns_id == SNS_CHANNEL.INSTAGRAM &&
                instagramSelected.m_ch_selected == 1
              ) {
                moneyInstagram += +channel.cost;
              } else {
                moneyInstagram += 0;
              }
              break;
            }
          }
        });
      });
      break;
    }
    case CAMPAIGN_STATUS.READY: {
      status = `${t("DASHBOARD_FISTAR.DFR_status_ready")}`;

      campaign.matchings.map(status => {
        // fistar
        fimes.push(status.influencer);
        if (MATCHING.REQUEST.includes(status.matching_status.label.stt_id)) {
          request.push(status.influencer);
        }
        if (MATCHING.APPLY.includes(status.matching_status.label.stt_id)) {
          apply.push(status.influencer);
        }

        status.matching_channel.map(channelMatching => {
          switch (channelMatching.sns_id) {
            case SNS_CHANNEL.FIME: {
              fimeSelected.m_ch_selected = channelMatching.m_ch_selected;
              fimeSelected.sns_id = channelMatching.sns_id;
              break;
            }
            case SNS_CHANNEL.FACEBOOK: {
              facebookSelected.m_ch_selected = channelMatching.m_ch_selected;
              facebookSelected.sns_id = channelMatching.sns_id;
              break;
            }
            case SNS_CHANNEL.YOUTUBE: {
              youtubeSelected.m_ch_selected = channelMatching.m_ch_selected;
              youtubeSelected.sns_id = channelMatching.sns_id;
              break;
            }
            case SNS_CHANNEL.INSTAGRAM: {
              instagramSelected.m_ch_selected = channelMatching.m_ch_selected;
              instagramSelected.sns_id = channelMatching.sns_id;
              break;
            }
          }
        });

        // money sns channel
        status.influencer.channels.map(channel => {
          switch (channel.sns_id) {
            case SNS_CHANNEL.FIME: {
              if (
                fimeSelected.sns_id == SNS_CHANNEL.FIME &&
                fimeSelected.m_ch_selected == 1
              ) {
                moneyFime += +channel.cost;
              } else {
                moneyFime += 0;
              }

              break;
            }
            case SNS_CHANNEL.FACEBOOK: {
              if (
                facebookSelected.sns_id == SNS_CHANNEL.FACEBOOK &&
                facebookSelected.m_ch_selected == 1
              ) {
                moneyFacebook += +channel.cost;
              } else {
                moneyFacebook += 0;
              }
              break;
            }
            case SNS_CHANNEL.YOUTUBE: {
              if (
                youtubeSelected.sns_id == SNS_CHANNEL.YOUTUBE &&
                youtubeSelected.m_ch_selected == 1
              ) {
                moneyYoutube += +channel.cost;
              } else {
                moneyYoutube += 0;
              }
              break;
            }
            case SNS_CHANNEL.INSTAGRAM: {
              if (
                instagramSelected.sns_id == SNS_CHANNEL.INSTAGRAM &&
                instagramSelected.m_ch_selected == 1
              ) {
                moneyInstagram += +channel.cost;
              } else {
                moneyInstagram += 0;
              }
              break;
            }
          }
        });
      });
      // campaign.v_review_status.map((status) => {
      //   fimes.push(status.influencer)
      //   if (MATCHING.REQUEST.includes(status.label_status.stt_id)) {
      //     request.push(status.influencer)
      //   }
      //   if (MATCHING.APPLY.includes(status.label_status.stt_id)) {
      //     apply.push(status.influencer)
      //   }
      // })
      break;
    }
    case CAMPAIGN_STATUS.ONGOING: {
      status = `${t("DASHBOARD_FISTAR.DFR_status_ongoing")}`;

      campaign.matchings.map(status => {
        // fistar
        fimes.push(status.influencer);
        if (MATCHING.REQUEST.includes(status.matching_status.label.stt_id)) {
          request.push(status.influencer);
        }
        if (MATCHING.APPLY.includes(status.matching_status.label.stt_id)) {
          apply.push(status.influencer);
        }
        status.matching_channel.map(channelMatching => {
          switch (channelMatching.sns_id) {
            case SNS_CHANNEL.FIME: {
              fimeSelected.m_ch_selected = channelMatching.m_ch_selected;
              fimeSelected.sns_id = channelMatching.sns_id;
              break;
            }
            case SNS_CHANNEL.FACEBOOK: {
              facebookSelected.m_ch_selected = channelMatching.m_ch_selected;
              facebookSelected.sns_id = channelMatching.sns_id;
              break;
            }
            case SNS_CHANNEL.YOUTUBE: {
              youtubeSelected.m_ch_selected = channelMatching.m_ch_selected;
              youtubeSelected.sns_id = channelMatching.sns_id;
              break;
            }
            case SNS_CHANNEL.INSTAGRAM: {
              instagramSelected.m_ch_selected = channelMatching.m_ch_selected;
              instagramSelected.sns_id = channelMatching.sns_id;
              break;
            }
          }
        });

        // money sns channel
        status.influencer.channels.map(channel => {
          switch (channel.sns_id) {
            case SNS_CHANNEL.FIME: {
              if (
                fimeSelected.sns_id == SNS_CHANNEL.FIME &&
                fimeSelected.m_ch_selected == 1
              ) {
                moneyFime += +channel.cost;
              } else {
                moneyFime += 0;
              }

              break;
            }
            case SNS_CHANNEL.FACEBOOK: {
              if (
                facebookSelected.sns_id == SNS_CHANNEL.FACEBOOK &&
                facebookSelected.m_ch_selected == 1
              ) {
                moneyFacebook += +channel.cost;
              } else {
                moneyFacebook += 0;
              }
              break;
            }
            case SNS_CHANNEL.YOUTUBE: {
              if (
                youtubeSelected.sns_id == SNS_CHANNEL.YOUTUBE &&
                youtubeSelected.m_ch_selected == 1
              ) {
                moneyYoutube += +channel.cost;
              } else {
                moneyYoutube += 0;
              }
              break;
            }
            case SNS_CHANNEL.INSTAGRAM: {
              if (
                instagramSelected.sns_id == SNS_CHANNEL.INSTAGRAM &&
                instagramSelected.m_ch_selected == 1
              ) {
                moneyInstagram += +channel.cost;
              } else {
                moneyInstagram += 0;
              }
              break;
            }
          }
        });
      });

      break;
    }
    case CAMPAIGN_STATUS.CLOSED: {
      status = `${t("DASHBOARD_FISTAR.DFR_status_close")}`;

      campaign.matchings.map(status => {
        // fistar
        fimes.push(status.influencer);
        if (MATCHING.REQUEST.includes(status.matching_status.label.stt_id)) {
          request.push(status.influencer);
        }
        if (MATCHING.APPLY.includes(status.matching_status.label.stt_id)) {
          apply.push(status.influencer);
        }
        status.matching_channel.map(channelMatching => {
          switch (channelMatching.sns_id) {
            case SNS_CHANNEL.FIME: {
              fimeSelected.m_ch_selected = channelMatching.m_ch_selected;
              fimeSelected.sns_id = channelMatching.sns_id;
              break;
            }
            case SNS_CHANNEL.FACEBOOK: {
              facebookSelected.m_ch_selected = channelMatching.m_ch_selected;
              facebookSelected.sns_id = channelMatching.sns_id;
              break;
            }
            case SNS_CHANNEL.YOUTUBE: {
              youtubeSelected.m_ch_selected = channelMatching.m_ch_selected;
              youtubeSelected.sns_id = channelMatching.sns_id;
              break;
            }
            case SNS_CHANNEL.INSTAGRAM: {
              instagramSelected.m_ch_selected = channelMatching.m_ch_selected;
              instagramSelected.sns_id = channelMatching.sns_id;
              break;
            }
          }
        });

        // money sns channel
        status.influencer.channels.map(channel => {
          switch (channel.sns_id) {
            case SNS_CHANNEL.FIME: {
              if (
                fimeSelected.sns_id == SNS_CHANNEL.FIME &&
                fimeSelected.m_ch_selected == 1
              ) {
                moneyFime += +channel.cost;
              } else {
                moneyFime += 0;
              }

              break;
            }
            case SNS_CHANNEL.FACEBOOK: {
              if (
                facebookSelected.sns_id == SNS_CHANNEL.FACEBOOK &&
                facebookSelected.m_ch_selected == 1
              ) {
                moneyFacebook += +channel.cost;
              } else {
                moneyFacebook += 0;
              }
              break;
            }
            case SNS_CHANNEL.YOUTUBE: {
              if (
                youtubeSelected.sns_id == SNS_CHANNEL.YOUTUBE &&
                youtubeSelected.m_ch_selected == 1
              ) {
                moneyYoutube += +channel.cost;
              } else {
                moneyYoutube += 0;
              }
              break;
            }
            case SNS_CHANNEL.INSTAGRAM: {
              if (
                instagramSelected.sns_id == SNS_CHANNEL.INSTAGRAM &&
                instagramSelected.m_ch_selected == 1
              ) {
                moneyInstagram += +channel.cost;
              } else {
                moneyInstagram += 0;
              }
              break;
            }
          }
        });
      });
      break;
    }
    default: {
      break;
    }
  }
  moneyTotal = moneyFime + moneyFacebook + moneyInstagram + moneyYoutube;
  console.log(fimes.fimelike);
  return (
    <div className="item">
      <div className="img-matching card-campaign-matching">
        <Link to={`/partner/campaign/${campaign.cp_slug}`}>
          <CampaignMainImage campaign={campaign} />
        </Link>
        <button className="overlay-box disnabled-scrap">
          <i className="fas fa-heart m-0" />{" "}
          <span>{campaign.scraps_count}</span>
        </button>
      </div>
      <div className="status-campaign">
        <div className="name">
          <div className="left">
            <span className="wrap-matching">{status}</span>
            <h4>
              <Link to={`/partner/campaign/${campaign.cp_slug}`}>
                {campaign.cp_name}
              </Link>
            </h4>
          </div>
          <p>
            <Link to={`/partner/campaign/${campaign.cp_slug}`}>
              Open
              <span>{campaign.cp_period_start.split(" ")[0]}</span>
            </Link>
          </p>
        </div>
        <div className="situation-matching">
          <div className="matching fime">
            <div className="title-user">
              <h4>
                Club Piaf <span>{campaign.cp_total_free}</span>
              </h4>
            </div>
            <ListFimes fimes={campaign.fimelike} />
          </div>
          <div className="matching fistar">
            <div className="title-user">
              <h4>
                {/*fistar <span>{request.length + apply.length}</span>*/}
                fistar{" "}
                <span>
                  {/*{campaign.total_request_count + campaign.total_applied_count}*/}
                  {campaign.cp_total_influencer}
                </span>
              </h4>
            </div>
            <div className="group-wf-item">
              <div
                className={
                  [CAMPAIGN_STATUS.MATCHING].includes(campaign.cp_status)
                    ? "wf-item"
                    : "wf-item w-50"
                }
              >
                <div className="status">
                  {/*<h4>{campaign.total_request_count} Request</h4>*/}
                  <h4>{request.length} Request</h4>
                </div>
                <ListFistars fistars={request} />
              </div>
              <div
                className={
                  [CAMPAIGN_STATUS.MATCHING].includes(campaign.cp_status)
                    ? "wf-item-second"
                    : "wf-item-second w-50"
                }
              >
                <div className="status">
                  {/*<h4>{campaign.total_applied_count} Apply </h4>*/}
                  <h4>
                    {/*{[CAMPAIGN_STATUS.MATCHING].includes(campaign.cp_status)*/}
                    {/*? apply.lengh : apply.length}*/}
                    {apply.length} {t("CAMPAI_TRACKING.CTG_apply")}{" "}
                  </h4>
                </div>
                <ListFistars fistars={apply} />
              </div>
              {[CAMPAIGN_STATUS.MATCHING].includes(campaign.cp_status) && (
                <div className="wf-item-last">
                  <div className="status">
                    <h4>0 {t("CAMPAI_TRACKING.CTG_recommended")}</h4>
                  </div>
                  <ListFistars fistars={[]} />
                </div>
              )}
            </div>
          </div>
        </div>
        <div className="price-matching">
          <div className="item-price">
            <div className="icon-solical">
              <a href="javascript:viod(0)">
                <img src={imageFime} alt="Fime" />
              </a>
            </div>
            <p>
              {moneyFime.toLocaleString()} <span>VND</span>
            </p>
          </div>
          <div className="item-price">
            <div className="icon-solical">
              <a href="javascript:viod(0)">
                <img src={imageFB} alt="Facebook" />
              </a>
            </div>
            <p>
              {moneyFacebook.toLocaleString()} <span>VND</span>
            </p>
          </div>
          <div className="item-price">
            <div className="icon-solical">
              <a href="javascript:viod(0)">
                <img src={imageIG} alt="Instagram" />
              </a>
            </div>
            <p>
              {moneyInstagram.toLocaleString()} <span>VND</span>
            </p>
          </div>
          <div className="item-price">
            <div className="icon-solical">
              <a href="javascript:viod(0)">
                <img src={imageYT} alt="Youtube" />
              </a>
            </div>
            <p>
              {moneyYoutube.toLocaleString()} <span>VND</span>
            </p>
          </div>
        </div>
        <div className="price-total">
          <p>
            Expected Cost{" "}
            <span>
              {" "}
              {moneyTotal.toLocaleString()} <b>VND</b>
            </span>
          </p>
        </div>
      </div>
    </div>
  );
};

class CampaignTracking extends Component {
  constructor(props) {
    super(props);
    this.state = {
      type: TYPE.ALL,
      campaigns: [],
      matching: 0,
      ready: 0,
      onGoing: 0,
      closed: 0,
      total: 0,
      isLoadingCampaign: false,
      textTitle: `${this.props.t("MY_CAMPAIGN.MCN_tab_allcampaign")}`,
      last_page: 1,
      page: 1,
      hasMore: true
    };
  }

  componentDidMount() {
    this.setState(
      {
        isLoadingCampaign: true
      },
      () => {
        this.initData();
        this.props.getCountCampaign().then(response => {
          this.setState({
            matching: response.matching,
            ready: response.ready,
            onGoing: response.ongoing,
            closed: response.closed
          });
        });
      }
    );
  }

  initData = () => {
    this.props
      .getCampaign(this.state.page, this.state.type)
      .then(response => {
        this.setState({
          campaigns: response.data,
          total: this.state.total ? this.state.total : response.total,
          // matching: response.matching,
          // ready: response.ready,
          // onGoing: response.ongoing,
          // closed: response.closed,
          isLoadingCampaign: false,
          last_page: response.last_page
        });
      })
      .catch(() => {
        this.setState({
          isLoadingCampaign: false
        });
      });
  };

  clickChangeTab = type => {
    let textTitle = "";
    const { t } = this.props;
    switch (type) {
      case TYPE.MATCHING: {
        textTitle = `${t("CAMPAI_TRACKING.CTG_amount_matching")}`;
        break;
      }
      case TYPE.READY: {
        textTitle = `${t("CAMPAI_TRACKING.CTG_amount_ready")}`;
        break;
      }
      case TYPE.ONGOING: {
        textTitle = `${t("CAMPAI_TRACKING.CTG_amount_on_going")}`;
        break;
      }
      case TYPE.CLOSED: {
        textTitle = `${t("CAMPAI_TRACKING.CTG_amount_close")}`;
        break;
      }
      default: {
        textTitle = `${t("CAMPAI_TRACKING.CTG_all_campaign")}`;
        break;
      }
    }
    this.setState(
      {
        type,
        page: 1,
        isLoadingCampaign: true,
        hasMore: true,
        textTitle
      },
      () => {
        this.initData();
      }
    );
  };

  fetchMoreData = () => {
    if (this.state.page >= this.state.last_page) {
      this.setState({ hasMore: false });
      return;
    }
    this.props
      .getCampaign(this.state.page + 1, this.state.type)
      .then(response => {
        this.setState({
          campaigns: [...this.state.campaigns, ...response.data],
          page: response.current_page
        });
      });
  };

  render() {
    const { t } = this.props;
    const {
      type,
      campaigns,
      isLoadingCampaign,
      matching,
      ready,
      onGoing,
      closed,
      total,
      textTitle
    } = this.state;

    let totalCam = total;
    switch (type) {
      case TYPE.MATCHING: {
        totalCam = matching;
        break;
      }
      case TYPE.READY: {
        totalCam = ready;
        break;
      }
      case TYPE.ONGOING: {
        totalCam = onGoing;
        break;
      }
      case TYPE.CLOSED: {
        totalCam = closed;
        break;
      }
      default: {
        totalCam = total;
        break;
      }
    }

    return (
      <Fragment>
        <div className="content mypage-partner-campaign-tracking">
          <div className="top">
            <div className="title">
              <h4>
                <a href="javascript:viod(0)">
                  {t("CAMPAI_TRACKING.CTG_campaign_tracking")}
                </a>
              </h4>
            </div>
          </div>
          <div className="information-tracking">
            <ul className="list-information list-myfistar-title">
              <li
                className={`item item-myfistar${
                  type === TYPE.ALL ? " active" : ""
                }`}
                onClick={() => this.clickChangeTab(TYPE.ALL)}
              >
                <a href="javascript:void(0)" className="text">
                  <p className="number">
                    {isLoadingCampaign && !total ? "0" : total}
                  </p>
                  <p>{t("CAMPAI_TRACKING.CTG_amount_all")}</p>
                </a>
              </li>
              <li
                className={`item item-myfistar${
                  type === TYPE.MATCHING ? " active" : ""
                }`}
                onClick={() => this.clickChangeTab(TYPE.MATCHING)}
              >
                <a href="javascript:void(0)" className="text">
                  <p className="number">
                    {isLoadingCampaign && !matching ? "0" : matching}
                  </p>
                  <p>{t("CAMPAI_TRACKING.CTG_amount_matching")}</p>
                </a>
              </li>
              <li
                className={`item item-myfistar${
                  type === TYPE.READY ? " active" : ""
                }`}
                onClick={() => this.clickChangeTab(TYPE.READY)}
              >
                <a href="javascript:void(0)" className="text">
                  <p className="number">
                    {isLoadingCampaign && !ready ? "0" : ready}
                  </p>
                  <p>{t("CAMPAI_TRACKING.CTG_amount_ready")}</p>
                </a>
              </li>
              <li
                className={`item item-myfistar${
                  type === TYPE.ONGOING ? " active" : ""
                }`}
                onClick={() => this.clickChangeTab(TYPE.ONGOING)}
              >
                <a href="javascript:void(0)" className="text">
                  <p className="number">
                    {isLoadingCampaign && !onGoing ? "0" : onGoing}
                  </p>
                  <p>{t("CAMPAI_TRACKING.CTG_amount_on_going")}</p>
                </a>
              </li>
              <li
                className={`item item-myfistar${
                  type === TYPE.CLOSED ? " active" : ""
                }`}
                onClick={() => this.clickChangeTab(TYPE.CLOSED)}
              >
                <a href="javascript:void(0)" className="text">
                  <p className="number">
                    {isLoadingCampaign && !closed ? "0" : closed}
                  </p>
                  <p>{t("CAMPAI_TRACKING.CTG_amount_close")}</p>
                </a>
              </li>
            </ul>
          </div>
          <div className="list-campaign-track">
            <div className="title">
              <h4>{`${textTitle} ${totalCam}`}</h4>
            </div>
            {isLoadingCampaign ? (
              <div id="loading" />
            ) : campaigns.length > 0 ? (
              <InfiniteScroll
                dataLength={campaigns.length}
                next={this.fetchMoreData}
                loader={<h4>{t("LOADING.LOADING")}</h4>}
                hasMore={this.state.hasMore}
                className="box-item box-scroll item-scroll-box"
              >
                {campaigns.map((campaign, key) => (
                  <Fragment key={key}>
                    <Campaign campaign={campaign} {...this.props} />
                  </Fragment>
                ))}
              </InfiniteScroll>
            ) : (
              `${t("CAMPAIGN.CAMPAIGN_no_campaign")}`
            )}
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};
const mapDispatchToProps = dispatch => {
  return {
    getCampaign: (page = 1, type = "") =>
      dispatch(getCampaignAction(page, type, 1, 1)),
    getCountCampaign: () => dispatch(getCountCampaignAction())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(CampaignTracking));
